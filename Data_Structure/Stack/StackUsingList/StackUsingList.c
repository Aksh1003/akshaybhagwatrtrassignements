// headers
#include<stdio.h>
#include<malloc.h>

// macros
#define TRUE 1
#define FALSE 0

// typedefs
typedef char BOOL;
typedef struct node NODE;
typedef struct node* PNODE;
typedef struct node** PPNODE;

// global variable

// function declarations
void Display(PNODE);
BOOL Push(PPNODE, int);
int Pop(PPNODE);
int Peep(PNODE);

// structure declaration
struct node {
	int data;
	struct node* next;
};

// main function (Entrypoint Function)
int main()
{
	PNODE head = NULL;

	Push(&head, 10);
	Push(&head, 20);
	Push(&head, 30);
	Push(&head, 40);

	Display(head);

	printf("Pop() = %d\n", Pop(&head));

	Display(head);

	printf("Peep() = %d\n", Peep(head));

	Push(&head, 50);

	Display(head);

	printf("Peep() = %d\n", Peep(head));

	return 0;
}

BOOL Push(PPNODE first, int no)
{
	PNODE newn = NULL;
	PNODE temp = *first;

	newn = (PNODE)malloc(sizeof(NODE));
	if (newn == NULL)
		return FALSE;

	newn->data = no;
	newn->next = NULL;

	if (*first == NULL)
		*first = newn;
	else 
	{
		while (temp->next != NULL)
		{
			temp = temp->next;
		}

		temp->next = newn;
	}

	return TRUE;
}

void Display(PNODE first)
{
	if (first != NULL)
	{
		while (first != NULL)
		{
			printf("[%d] <- ", first->data);
			first = first->next;
		}
		printf("\n");
	}
}

int Pop(PPNODE first)
{
	PNODE temp = *first;
	int val = 0;

	if (*first == NULL)
		return val;
	else if ((*first)->next == NULL)
	{
		free(*first);
		*first = NULL;
	}
	else
	{
		while (temp->next->next != NULL)
		{
			temp = temp->next;
		}
		val = temp->next->data;
		free(temp->next);
		temp->next = NULL;
	}

	return val;
}

int Peep(PNODE first)
{
	int val = 0;
	if (first != NULL)
	{
		while (first->next != NULL)
		{
			first = first->next;
		}
	}

	return first->data;
}

