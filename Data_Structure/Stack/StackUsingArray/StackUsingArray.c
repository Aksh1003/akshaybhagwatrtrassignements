// headers
#include<stdio.h>

typedef char BOOL;

// macros
#define SIZE 10
#define TRUE 1
#define FALSE 0

// global variable
int Arr[SIZE];
int top = -1;

// function declaration
BOOL isStackEmpty();
BOOL isStackFull();
void Push(int);
int Pop();
int Peep();
void Display();

// main function (Entry Point Funtion)
int main()
{
	Push(10);
	Push(20);
	Push(30);
	Push(40);

	Display();

	printf("Pop() = %d\n", Pop());

	Display();

	printf("Peep() = %d\n", Peep());

	Display();

	return 0;
}

BOOL isStackEmpty()
{
	if (top == -1)
		return TRUE;
	else
		return FALSE;
}

BOOL isStackFull()
{
	if (top == SIZE - 1)
		return TRUE;
	else
		return FALSE;
}

void Push(int no)
{
	if(!isStackFull())
		Arr[++top] = no;
}

int Pop()
{
	if (!isStackEmpty())
		return (Arr[top--]);
	else
		return 0;
}

int Peep()
{
	if (!isStackEmpty())
		return Arr[top];
	else
		return 0;
}

void Display()
{
	if (!isStackEmpty())
	{
		for (int i = 0; i <= top; i++)
		{
			printf("\t[%d] <-", Arr[i]);
		}
		printf("\b\b\n");
	}
}
