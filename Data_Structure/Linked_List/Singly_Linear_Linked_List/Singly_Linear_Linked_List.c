// headers
#include<stdio.h>
#include<malloc.h>

// macros
#define TRUE 1
#define FALSE 0

// global variables

// typedef
typedef char BOOL;
typedef struct Node NODE;
typedef struct Node* PNODE;
typedef struct Node** PPNODE;

//  Node structue
struct Node {
	int data;
	struct Node* next;
};

// funtion declarations
BOOL InsertFirst(PPNODE, int);
BOOL DeleteFirst(PPNODE);
BOOL InsertLast(PPNODE, int);
BOOL DeleteLast(PPNODE);
BOOL InsertAtPosition(PPNODE, int, int);
BOOL DeleteAtPosition(PPNODE, int);
void Display(PNODE);
int Count(PNODE);

// main funtion (Entrypoint Funtion)
int main()
{
	PNODE head = NULL;

	InsertFirst(&head, 10);
	InsertFirst(&head, 20);
	InsertFirst(&head, 30);
	InsertFirst(&head, 40);
	InsertLast(&head, 50);
	InsertAtPosition(&head, 60, 4);

	Display(head);

	DeleteAtPosition(&head, 3);

	Display(head);

	DeleteFirst(&head);
	DeleteLast(&head);

	Display(head);
	
	printf("Count = %d\n", Count(head));

	return 0;
}

BOOL InsertFirst(PPNODE first, int no)
{
	PNODE newn = NULL;

	newn = (PNODE)malloc(sizeof(NODE));
	if (newn == NULL)
		return FALSE;

	newn->data = no;
	newn->next = NULL;

	if (*first == NULL)
		*first = newn;
	else
	{
		newn->next = *first;
		*first = newn;
	}

	return TRUE;
}

void Display(PNODE first)
{
	while (first != NULL)
	{
		printf("%d -> ", first->data);
		first = first->next;
	}
	printf("\n");
}

int Count(PNODE first)
{
	int count = 0;

	while (first != NULL)
	{
		count++;
		first = first->next;
	}

	return count;
}

BOOL DeleteFirst(PPNODE first)
{
	PNODE temp = NULL;

	if (*first == NULL)
		return FALSE;

	temp = (*first)->next;
	free(*first);
	*first = temp;

	return TRUE;
}

BOOL InsertLast(PPNODE first, int no)
{
	PNODE newn = NULL;
	PNODE temp = *first;

	newn = (PNODE)malloc(sizeof(NODE));
	if (newn == NULL)
		return FALSE;

	newn->data = no;
	newn->next = NULL;

	if (*first == NULL)
		*first = newn;
	else
	{
		while (temp->next != NULL)
		{
			temp = temp->next;
		}

		temp->next = newn;
	}

	return TRUE;
}

BOOL DeleteLast(PPNODE first)
{
	PNODE temp = *first;

	if (*first == NULL)
		return FALSE;
	else if((*first) -> next == NULL)
	{
		free(*first);
		*first = NULL;
	}
	else
	{
		while (temp->next->next != NULL)
			temp = temp->next;

		free(temp->next);
		temp->next = NULL;
	}

	return TRUE;
}

BOOL InsertAtPosition(PPNODE first, int no, int pos)
{
	PNODE newn = NULL;
	PNODE temp = *first;

	if ((pos < 1) || (pos > Count(*first) + 1))
		return FALSE;

	if (pos == 1)
		InsertFirst(first, no);
	else if (pos == Count(*first) + 1)
		InsertLast(first, no);
	else
	{
		newn = (PNODE)malloc(sizeof(NODE));
		if (newn == NULL)
			return FALSE;

		newn->data = no;
		newn->next = NULL;

		while (pos != 2)
		{
			temp = temp->next;
			pos--;
		}

		newn->next = temp->next;
		temp->next = newn;
	}

	return TRUE;
}

BOOL DeleteAtPosition(PPNODE first, int pos)
{
	PNODE temp = *first;
	PNODE temp2 = NULL;

	if ((pos < 1) || (pos > Count(*first)))
		return FALSE;

	if (pos == 1)
		DeleteFirst(first);
	else if (pos == Count(*first))
		DeleteLast(first);
	else
	{
		while (pos != 2)
		{
			temp = temp->next;
			pos--;
		}

		temp2 = temp->next;
		temp->next = temp2->next;
		free(temp2);
	}

	return TRUE;
}
