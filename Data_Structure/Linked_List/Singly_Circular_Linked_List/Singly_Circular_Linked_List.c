// headers 
#include<stdio.h>
#include<stdlib.h>

// macros
#define TRUE 1
#define FALSE 0

// typedefs
typedef char BOOL;
typedef struct node NODE;
typedef struct node* PNODE;
typedef struct node** PPNODE;

// structure declaration
struct node 
{
	int data;
	struct node* next;
};

// global variable

// function declaration
void Display(PNODE, PNODE);
int Count(PNODE, PNODE);
BOOL InsertFirst(PPNODE, PPNODE, int);
BOOL InsertLast(PPNODE, PPNODE, int);
BOOL InsertAtPosition(PPNODE, PPNODE, int, int);
BOOL DeleteFirst(PPNODE, PPNODE);
BOOL DeleteLast(PPNODE, PPNODE);
BOOL DeleteAtPosition(PPNODE, PPNODE, int);

// main function (Entrypoint Function)
int main()
{
	PNODE head = NULL, tail = NULL;

	InsertFirst(&head, &tail, 10);
	InsertFirst(&head, &tail, 20);
	InsertFirst(&head, &tail, 30);

	Display(head, tail);

	InsertLast(&head, &tail, 40);

	Display(head, tail);

	InsertAtPosition(&head, &tail, 50, 3);

	Display(head, tail);

	DeleteFirst(&head, &tail);

	Display(head, tail);

	DeleteLast(&head, &tail);

	Display(head, tail);

	DeleteAtPosition(&head, &tail, 2);

	Display(head, tail);

	printf("Count = %d\n", Count(head, tail));

	return 0;
}

BOOL InsertFirst(PPNODE first, PPNODE last, int no)
{
	PNODE newn = NULL;

	newn = (PNODE)malloc(sizeof(NODE));
	if (newn == NULL)
		return FALSE;

	newn->data = no;
	newn->next = NULL;

	if ((*first == NULL) && (*last == NULL))
	{
		*first = *last = newn;
		(*last)->next = *first;
	}
	else
	{
		newn->next = *first;
		*first = newn;
		(*last)->next = *first;
	}

	return TRUE;
}

BOOL InsertLast(PPNODE first, PPNODE last, int no)
{
	PNODE newn = NULL;

	newn = (PNODE)malloc(sizeof(NODE));
	if (newn == NULL)
		return FALSE;

	newn->data = no;
	newn->next = NULL;

	if ((*first == NULL) && (*last == NULL))
	{
		*first = *last = newn;
		(*last)->next = *first;
	}
	else
	{
		(*last)->next = newn;
		*last = newn;
		(*last)->next = *first;
	}

	return TRUE;
}

BOOL InsertAtPosition(PPNODE first, PPNODE last, int no, int pos)
{
	PNODE newn = NULL;
	PNODE temp = *first;

	if((pos < 1) || (pos > Count(*first, *last) + 1))
		return FALSE;

	if (pos == 1)
		InsertFirst(first, last, no);
	else if (pos == Count(*first, *last) + 1)
		InsertLast(first, last, no);
	else
	{
		newn = (PNODE)malloc(sizeof(NODE));
		if (newn == NULL)
			return FALSE;

		newn->data = no;
		newn->next = NULL;

		while (pos != 2)
		{
			temp = temp->next;
			pos--;
		}

		newn->next = temp->next;
		temp->next = newn;
	}

	return TRUE;
}

void Display(PNODE first, PNODE last)
{
	if ((first != NULL) && (last != NULL))
	{
		do {
			printf("%d <- ", first->data);
			first = first->next;
		} while (first != last->next);

		printf("\n");
	}
}

int Count(PNODE first, PNODE last)
{
	int count = 0;

	if ((first != NULL) && (last != NULL))
	{
		do {
			count++;
			first = first->next;
		} while (first != last->next);
	}

	return count;
}

BOOL DeleteFirst(PPNODE first, PPNODE last)
{
	if ((*first == NULL) && (*last == NULL))
		return FALSE;
	else if (*first == *last)
	{
		free(*first);
		*first = *last = NULL;
	}
	else
	{
		*first = (*first)->next;
		free((*last)->next);
		(*last)->next = *first;
	}

	return TRUE;
}

BOOL DeleteLast(PPNODE first, PPNODE last)
{
	PNODE temp = *first;

	if ((*first == NULL) && (*last == NULL))
		return FALSE;
	else if (*first == *last)
	{
		free(*first);
		*first = *last = NULL;
	}
	else
	{
		while (temp->next != *last)
		{
			temp = temp->next;
		}

		free(*last);
		*last = temp;
		(*last)->next = *first;
	}

	return TRUE;
}

BOOL DeleteAtPosition(PPNODE first, PPNODE last, int pos)
{
	PNODE temp1 = NULL, temp2 = NULL;

	if ((pos < 1) || (pos > Count(*first, *last)))
		return FALSE;

	if (pos == 1)
		DeleteFirst(first, last);
	else if (pos == Count(*first, *last))
		DeleteLast(first, last);
	else
	{
		temp1 = *first;

		while (pos != 2)
		{
			temp1 = temp1->next;
			pos--;
		}

		temp2 = temp1->next;
		temp1->next = temp2->next;
		free(temp2);
	}

	return TRUE;
}

