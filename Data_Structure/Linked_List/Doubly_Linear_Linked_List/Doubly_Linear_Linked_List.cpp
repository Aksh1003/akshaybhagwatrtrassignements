// headers
#include<iostream>
using namespace std;

// macros
#define TRUE 1
#define FALSE 0

// typedefs
typedef char BOOL;
typedef struct node NODE;
typedef struct node* PNODE;
typedef struct node** PPNODE;

// global variables

// structure declaration
struct node {
	struct node* prev;
	int data;
	struct node* next;
};

// class Doubly Linked List
class DoublyLL {
	PNODE head;

	public:
		DoublyLL();
		~DoublyLL();
		void Display();
		int Count();
		BOOL InsertFirst(int);
		BOOL InsertLast(int);
		BOOL InsertAtPosition(int, int);
		BOOL DeleteFirst();
		BOOL DeleteLast();
		BOOL DeleteAtPosition(int);
};

DoublyLL::DoublyLL()
{
	head = NULL;
}

DoublyLL::~DoublyLL()
{
	PNODE temp = head;

	while (head != NULL)
	{
		temp = head->next;
		delete head;
		head = temp;
	}
}

BOOL DoublyLL::InsertFirst(int no)
{
	PNODE newn = NULL;

	newn = new NODE;
	newn->data = no;
	newn->next = NULL;
	newn->prev = NULL;

	if (NULL == head)
		head = newn;
	else
	{
		newn->next = head;
		head->prev = newn;
		head = newn;
	}

	return TRUE;
}

BOOL DoublyLL::InsertLast(int no)
{
	PNODE newn = NULL;
	PNODE temp = head;

	newn = new NODE;
	newn->prev = NULL;
	newn->data = no;
	newn->next = NULL;

	if (NULL == head)
		head = newn;
	else
	{
		while (temp->next != NULL)
			temp = temp->next;

		newn->prev = temp;
		temp->next = newn;
	}

	return TRUE;
}

BOOL DoublyLL::InsertAtPosition(int no, int pos)
{
	PNODE newn = NULL;
	PNODE temp = NULL;

	if ((pos < 1) || (pos > Count() + 1))
		return FALSE;
	if (pos == 1)
		InsertFirst(no);
	else if (pos == Count() + 1)
		InsertLast(no);
	else
	{
		newn = new NODE;
		newn->prev = NULL;
		newn->data = no;
		newn->next = NULL;

		temp = head;

		while (pos != 2)
		{
			temp = temp->next;
			pos--;
		}

		temp->next->prev = newn;
		newn->next = temp->next;
		temp->next = newn;
		newn->prev = temp;
	}

	return TRUE;
}

BOOL DoublyLL::DeleteFirst()
{
	if (NULL == head)
		return FALSE;
	else
	{
		head = head->next;
		delete(head->prev);
		head->prev = NULL;
	}

	return TRUE;
}

BOOL DoublyLL::DeleteLast()
{
	PNODE temp = head;

	if (NULL == head)
		return FALSE;
	else
	{
		while (temp->next != NULL)
			temp = temp->next;

		if (temp->prev == NULL)
		{
			delete temp;
			temp = NULL;
		}
		else
		{
			temp->prev->next = NULL;
			delete temp;
		}
	}

	return TRUE;
}

BOOL DoublyLL::DeleteAtPosition(int pos)
{
	PNODE temp = head;

	if ((pos < 1) || (pos > Count()))
		return FALSE;

	if (pos == 1)
		DeleteFirst();
	else if (pos == Count())
		DeleteLast();
	else
	{
		while (pos != 2)
		{
			temp = temp->next;
			pos--;
		}

		temp->next = temp->next->next;
		delete (temp->next->prev);
		temp->next->prev = temp;
	}

	return TRUE;
}

void DoublyLL::Display()
{
	PNODE temp = head;

	while (temp != NULL)
	{
		cout << temp->data << " <- ";
		temp = temp->next;
	}
	cout << endl;
}

int DoublyLL::Count(void)
{
	PNODE temp = head;
	int count = 0;

	while (temp != NULL)
	{
		count++;
		temp = temp->next;
	}

	return count;
}

int main()
{
	DoublyLL obj;

	obj.InsertFirst(10);
	obj.InsertFirst(20);
	obj.InsertFirst(30);

	obj.InsertLast(40);

	obj.Display();

	obj.InsertAtPosition(50, 3);

	obj.Display();

	obj.DeleteFirst();

	obj.Display();

	obj.DeleteLast();

	obj.Display();

	obj.DeleteAtPosition(3);

	obj.Display();

	cout << "count = " << obj.Count() << endl;

	return 0;
}