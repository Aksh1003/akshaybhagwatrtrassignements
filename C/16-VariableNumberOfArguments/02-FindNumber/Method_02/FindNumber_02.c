#include <stdio.h>
#include <stdarg.h>

#define AAB_MAIN main
#define AAB_NUM_TO_BE_FOUND 3
#define AAB_NUM_ELEMENTS 10

int AAB_MAIN(void)
{
	void AAB_FindNumber(int, int, ...);

	printf("\n\n");

	AAB_FindNumber(AAB_NUM_TO_BE_FOUND, AAB_NUM_ELEMENTS, 3, 5, 9, 2, 3, 6, 9, 3, 1, 3);

	return(0);
}

void AAB_FindNumber(int aab_num_to_be_found, int aab_num, ...) // VARIADIC FUNCTION
{
	int va_FindNumber(int, int, va_list);

	int aab_count = 0;
	int aab_n;
	va_list aab_numbers_list;

	va_start(aab_numbers_list, aab_num);

	aab_count = va_FindNumber(AAB_NUM_TO_BE_FOUND, aab_num, aab_numbers_list);

	if (aab_count == 0)
		printf("Number %d Could Not Be Found !!!\n\n", aab_num_to_be_found);
	else
		printf("Number %d Found %d Times !!!\n\n", aab_num_to_be_found, aab_count);

	va_end(aab_numbers_list);
}

int va_FindNumber(int aab_num_to_be_found, int aab_num, va_list aab_list)
{
	int aab_count_of_num = 0;
	int aab_n;

	while (aab_num)
	{
		aab_n = va_arg(aab_list, int);
		if (aab_n == aab_num_to_be_found)
			aab_count_of_num++;
		aab_num--;
	}
	return(aab_count_of_num);
}
