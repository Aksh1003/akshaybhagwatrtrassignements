#include <stdio.h>
#include <stdarg.h>

#define AAB_MAIN main

int AAB_MAIN(void)
{
	int AAB_CalculateSum(int, ...);
	
	int aab_answer;
	
	printf("\n\n");
	
	aab_answer = AAB_CalculateSum(5, 10, 20, 30, 40, 50);
	printf("aab_Answer = %d\n\n", aab_answer);
	
	aab_answer = AAB_CalculateSum(10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
	printf("aab_Answer = %d\n\n", aab_answer);

	aab_answer = AAB_CalculateSum(0);
	printf("aab_Answer = %d\n\n", aab_answer);

	return(0);
}

int AAB_CalculateSum(int aab_num, ...) 
{
	int aab_sum_total = 0;
	int aab_n;
	va_list aab_numbers_list;

	va_start(aab_numbers_list, aab_num);
	
	while (aab_num)
	{
		aab_n = va_arg(aab_numbers_list, int);
		aab_sum_total = aab_sum_total + aab_n;
		aab_num--;
	}

	va_end(aab_numbers_list);
	return(aab_sum_total);
}
