#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int aab_MyStrlen(char[]);

	//variable declaraions
	// *** A 'STRING' IS AN ARRAY OF CHARACTERS ... so char[] IS A char ARRAY AND HENCE, char[] IS A 'STRING' * **
		// *** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS !!! ***
		// *** HENCE, char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
		// *** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS * **
		//Here, the string array can allow a maximum number of 10 strings (10 rows)	andeach of these 10 strings can have only upto 15 characters maximum(15	columns)
		
	char aab_strArray[10][15] = { "Hello!", "Mr.","AKSHAY", "ANIL", "BHAGWAT", "WELCOME", "To", "C", "AND", "C++" }; //IN-LINE INITIALIZATION
	int aab_iStrLengths[10]; //1D Integer Array - Stores lengths of those strings at corressponding indices in 'aab_strArray[]' e.g: aab_iStrLengths[0] 
						//will be the length of string at aab_strArray[0], aab_iStrLengths[1] will be the length of string
						//at aab_strArray[1]...10 strings, 10 lengths...
	int aab_strArray_size;
	int aab_strArray_num_rows;
	int i, j;
	
	//code
	aab_strArray_size = sizeof(aab_strArray);
	aab_strArray_num_rows = aab_strArray_size / sizeof(aab_strArray[0]);

	//Storing in lengths of all the strings...
	for (i = 0; i < aab_strArray_num_rows; i++)
		aab_iStrLengths[i] = aab_MyStrlen(aab_strArray[i]);

	printf("\n\n");
	printf("The Entire String Array : \n\n");
	for (i = 0; i < aab_strArray_num_rows; i++)
		printf("%s ", aab_strArray[i]);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");
	//Since, char[][] is an array of strings, referencing only by the row number (first[]) will give the row or the string
	//The Column Number (second []) is the particular character in that string / row

	for (i = 0; i < aab_strArray_num_rows; i++)
	{
		printf("String Number %d => %s\n\n", (i + 1), aab_strArray[i]);
		for (j = 0; j < aab_iStrLengths[i]; j++)
		{
			printf("Character %d = %c\n", (j + 1), aab_strArray[i][j]);
		}
		printf("\n\n");
	}

	return(0);
}

int aab_MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// *** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURENCE OF NULL - TERMINATING CHARACTER(\0) * **

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	
	return(string_length);
}