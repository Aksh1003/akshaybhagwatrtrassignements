#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int aab_aab_MyStrlen(char[]);
	//variable declaraions
	// *** A 'STRING' IS AN ARRAY OF CHARACTERS ... so char[] IS A char ARRAY AND HENCE, char[] IS A 'STRING' * **
	// *** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS !!! ***
	// *** HENCE, char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
	// *** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS * **
	//Here, the string array can allow a maximum number of 10 strings (10 rows) and each of these 10 strings can have only upto 15 characters maximum(15 columns)

	char aab_aab_strArray[10][15] = { "Hello!", "Mr.","AKSHAY", "ANIL", "BHAGWAT", "WELCOME", "To", "C", "AND", "C++"}; //IN-LINE INITIALIZATION
	int aab_char_size;
	int aab_aab_strArray_size;
	int aab_aab_strArray_num_elements, aab_aab_strArray_num_rows, aab_aab_strArray_num_columns;
	int aab_strActual_num_chars = 0;
	int i;
	
	//code
	printf("\n\n");
	aab_char_size = sizeof(char);
	aab_aab_strArray_size = sizeof(aab_aab_strArray);
	
	printf("Size Of Two Dimensional ( 2D ) Character Array (String Array) Is = %d\n\n", aab_aab_strArray_size);

	aab_aab_strArray_num_rows = aab_aab_strArray_size / sizeof(aab_aab_strArray[0]);
	
	printf("Number of Rows (Strings) In Two Dimensional ( 2D ) Character Array (String Array) Is = % d\n\n", aab_aab_strArray_num_rows);
		
	aab_aab_strArray_num_columns = sizeof(aab_aab_strArray[0]) / aab_char_size;

	printf("Number of Columns In Two Dimensional ( 2D ) Character Array (String Array) Is = % d\n\n", aab_aab_strArray_num_columns);
		
	aab_aab_strArray_num_elements = aab_aab_strArray_num_rows * aab_aab_strArray_num_columns;

	printf("Maximum Number of Elements (Characters) In Two Dimensional ( 2D ) Character Array(String Array) Is = % d\n\n", aab_aab_strArray_num_elements);

	for (i = 0; i < aab_aab_strArray_num_rows; i++)
	{
		aab_strActual_num_chars = aab_strActual_num_chars + aab_aab_MyStrlen(aab_aab_strArray[i]);
	}

	printf("Actual Number of Elements (Characters) In Two Dimensional ( 2D ) Character Array(String Array) Is = % d\n\n", aab_strActual_num_chars);
		
	printf("\n\n");
	
	printf("Strings In The 2D Array : \n\n");

	//Since, char[][] is an array of strings, referencing only by the row number (first[]) will give the row or the string
	//The Column Number (second []) is the particular character in that string / row

	printf("%s ", aab_aab_strArray[0]);
	printf("%s ", aab_aab_strArray[1]);
	printf("%s ", aab_aab_strArray[2]);
	printf("%s ", aab_aab_strArray[3]);
	printf("%s ", aab_aab_strArray[4]);
	printf("%s ", aab_aab_strArray[5]);
	printf("%s ", aab_aab_strArray[6]);
	printf("%s ", aab_aab_strArray[7]);
	printf("%s ", aab_aab_strArray[8]);
	printf("%s\n\n", aab_aab_strArray[9]);
	
	return(0);
}

int aab_aab_MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// *** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURENCE OF NULL - TERMINATING CHARACTER(\0) * **

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	
	return(string_length);
}