#include <stdio.h>

int main(void)
{
	//variable declaraions
	int aab_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; //IN-LINE INITIALIZATION
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements, aab_iArray_num_rows, aab_iArray_num_columns;
	int i, j;

	//code
	printf("\n\n");
	aab_int_size = sizeof(int);
	aab_iArray_size = sizeof(aab_iArray);

	printf("Size Of Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_size);
	
	aab_iArray_num_rows = aab_iArray_size / sizeof(aab_iArray[0]);

	printf("Number of Rows In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_rows);
	
	aab_iArray_num_columns = sizeof(aab_iArray[0]) / aab_int_size;

	printf("Number of Columns In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_columns);

	aab_iArray_num_elements = aab_iArray_num_rows * aab_iArray_num_columns;

	printf("Number of Elements In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_elements);
		
	printf("\n\n");

	printf("Elements In The 2D Array : \n\n");
	// *** ARRAY INDICES BEGIN FROM 0, HENCE, 1ST ROW IS ACTUALLY 0TH ROW AND 1ST COLUMN IS ACTUALLY 0TH COLUMN * **
	for (i = 0; i < aab_iArray_num_rows; i++)
	{
		printf("****** ROW %d ******\n", (i + 1));
		for (j = 0; j < aab_iArray_num_columns; j++)
		{
			printf("aab_iArray[%d][%d] = %d\n", i, j, aab_iArray[i][j]);
		}
		printf("\n\n");
	}

	return(0);
}