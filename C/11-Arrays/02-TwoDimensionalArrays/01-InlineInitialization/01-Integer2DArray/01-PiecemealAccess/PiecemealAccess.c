#include <stdio.h>

int main(void)
{
	//variable declaraions
	int aab_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; //IN-LINE INITIALIZATION
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements, aab_iArray_num_rows, aab_iArray_num_columns;

	//code
	printf("\n\n");
	aab_int_size = sizeof(int);
	aab_iArray_size = sizeof(aab_iArray);

	printf("Size Of Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_size);
	
	aab_iArray_num_rows = aab_iArray_size / sizeof(aab_iArray[0]);

	printf("Number of Rows In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_rows);
	
	aab_iArray_num_columns = sizeof(aab_iArray[0]) / aab_int_size;

	printf("Number of Columns In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_columns);
		
	aab_iArray_num_elements = aab_iArray_num_rows * aab_iArray_num_columns;

	printf("Number of Elements In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_elements);
		
	printf("\n\n");

	printf("Elements In The 2D Array : \n\n");
	// *** ARRAY INDICES BEGIN FROM 0, HENCE, 1ST ROW IS ACTUALLY 0TH ROW AND 1ST COLUMN IS ACTUALLY 0TH COLUMN * **
		// *** ROW 1 ***
	printf("****** ROW 1 ******\n");
	printf("aab_iArray[0][0] = %d\n", aab_iArray[0][0]); // *** COLUMN 1 *** (0th Element)
	printf("aab_iArray[0][1] = %d\n", aab_iArray[0][1]); // *** COLUMN 2 *** (1st Element)
	printf("aab_iArray[0][2] = %d\n", aab_iArray[0][2]); // *** COLUMN 3 *** (2nd Element)
	printf("\n\n");

		// *** ROW 2 ***
	printf("****** ROW 2 ******\n");
	printf("aab_iArray[1][0] = %d\n", aab_iArray[1][0]); // *** COLUMN 1 *** (0th Element)
	printf("aab_iArray[1][1] = %d\n", aab_iArray[1][1]); // *** COLUMN 2 *** (1st Element)
	printf("aab_iArray[1][2] = %d\n", aab_iArray[1][2]); // *** COLUMN 3 *** (2nd Element)
	printf("\n\n");

		// *** ROW 3 ***
	printf("****** ROW 3 ******\n");
	printf("aab_iArray[2][0] = %d\n", aab_iArray[2][0]); // *** COLUMN 1 *** (0th Element)
	printf("aab_iArray[2][1] = %d\n", aab_iArray[2][1]); // *** COLUMN 2 *** (1st Element)
	printf("aab_iArray[2][2] = %d\n", aab_iArray[2][2]); // *** COLUMN 3 *** (2nd Element)
	printf("\n\n");
	
		// *** ROW 4 ***
	printf("****** ROW 4 ******\n");
	printf("aab_iArray[3][0] = %d\n", aab_iArray[3][0]); // *** COLUMN 1 *** (0th Element)
	printf("aab_iArray[3][1] = %d\n", aab_iArray[3][1]); // *** COLUMN 2 *** (1st Element)
	printf("aab_iArray[3][2] = %d\n", aab_iArray[3][2]); // *** COLUMN 3 *** (2nd Element)
	printf("\n\n");

		// *** ROW 5 ***
	printf("****** ROW 5 ******\n");
	printf("aab_iArray[4][0] = %d\n", aab_iArray[4][0]); // *** COLUMN 1 *** (0th Element)
	printf("aab_iArray[4][1] = %d\n", aab_iArray[4][1]); // *** COLUMN 2 *** (1st Element)
	printf("aab_iArray[4][2] = %d\n", aab_iArray[4][2]); // *** COLUMN 3 *** (2nd Element)
	printf("\n\n");
	
	return(0);
}