#include <stdio.h>

#define AAB_NUM_ROWS 5
#define AAB_NUM_COLUMNS 3

int main()
{
	int aab_iArray_2D[AAB_NUM_ROWS][AAB_NUM_COLUMNS];
	int aab_iArray_1D[AAB_NUM_ROWS * AAB_NUM_COLUMNS];
	int aab_i, aab_j;
	int aab_num;

	printf("Enter ELements Of Yoyr Choice To Fill Up The Integer 2D Array : \n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		printf("For Row Number %d : \n", (aab_i + 1));
		
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("Enter Elements Number %d : \n", (aab_j + 1));
			scanf("%d", &aab_num);
			aab_iArray_2D[aab_i][aab_j] = aab_num;
		}

		printf("\n\n");
	}

	printf("\n\nTwo Dimensional (2D) Array Of Integers : \n\n" );
	for (aab_i = 0 ; aab_i < AAB_NUM_ROWS ; aab_i++)
	{
		printf("***Row %d ***\n", (aab_i + 1));

		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("aab_iArray_2D[%d][%d] = %d\n", aab_i, aab_j, aab_iArray_2D[aab_i][aab_j]);
		}

		printf("\n\n");
	}

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			aab_iArray_1D[(aab_i * AAB_NUM_COLUMNS) + aab_j] = aab_iArray_2D[aab_i][aab_j];
		}
	}

	printf("\n\nOne Dimensional (1D0 Array Of Integers : \n\n");
	for (aab_i = 0; aab_i < (AAB_NUM_ROWS * AAB_NUM_COLUMNS); aab_i++)
	{
		printf("aab_iArray_1D[%d] = %d\n", aab_i, aab_iArray_1D[aab_i]);
	}

	printf("\n\n");

	return 0;
}