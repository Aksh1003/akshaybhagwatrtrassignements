#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	char aab_strArray[5][10];
	int aab_char_size;
	int aab_strArray_size;
	int aab_strArray_num_elements, aab_strArray_num_rows, aab_strArray_num_columns;
	int i;

	//code
	printf("\n\n");
	aab_char_size = sizeof(char);
	aab_strArray_size = sizeof(aab_strArray);

	printf("Size Of Two Dimensional ( 2D ) Character Array (String Array) Is = %d\n\n", aab_strArray_size);
		
	aab_strArray_num_rows = aab_strArray_size / sizeof(aab_strArray[0]);

	printf("Number of Rows (Strings) In Two Dimensional ( 2D ) Character Array(String Array) Is = % d\n\n", aab_strArray_num_rows);
		
	aab_strArray_num_columns = sizeof(aab_strArray[0]) / aab_char_size;

	printf("Number of Columns In Two Dimensional ( 2D ) Character Array (String Array) Is = % d\n\n", aab_strArray_num_columns);
		
	aab_strArray_num_elements = aab_strArray_num_rows * aab_strArray_num_columns;

	printf("Maximum Number of Elements (Characters) In Two Dimensional ( 2D ) Character Array(String Array) Is = % d\n\n", aab_strArray_num_elements);

	// *** PIECE-MEAL ASSIGNMENT ***
		// ****** ROW 1 / STRING 1 ******
	aab_strArray[0][0] = 'M';
	aab_strArray[0][1] = 'y';
	aab_strArray[0][2] = '\0';
	
		// ****** ROW 2 / STRING 2 ******
	aab_strArray[1][0] = 'N';
	aab_strArray[1][1] = 'a';
	aab_strArray[1][2] = 'm';
	aab_strArray[1][3] = 'e';
	aab_strArray[1][4] = '\0';
	
		// ****** ROW 3 / STRING 3 ******
	aab_strArray[2][0] = 'I';
	aab_strArray[2][1] = 's';
	aab_strArray[2][2] = '\0';

		// ****** ROW 4 / STRING 4 ******
	aab_strArray[3][0] = 'A';
	aab_strArray[3][1] = 'K';
	aab_strArray[3][2] = 'S';
	aab_strArray[3][3] = 'H';
	aab_strArray[3][4] = 'A';
	aab_strArray[3][5] = 'Y';
	aab_strArray[3][6] = '\0';
	
		// ****** ROW 5 / STRING 5 ******
	aab_strArray[4][0] = 'B';
	aab_strArray[4][1] = 'H';
	aab_strArray[4][2] = 'A';
	aab_strArray[4][3] = 'G';
	aab_strArray[4][4] = 'W';
	aab_strArray[4][5] = 'A';
	aab_strArray[4][6] = 'T';
	aab_strArray[4][7] = '\0';
	
	printf("\n\n");
	printf("The Strings In the 2D Character Array Are : \n\n");
	
	for (i = 0; i < aab_strArray_num_rows; i++)
		printf("%s ", aab_strArray[i]);
	
	printf("\n\n");
	
	return(0);
}

int aab_MyStrlen(char str[])
{
	//variable declarations
	int j;
	int aab_string_length = 0;

	//code

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}
