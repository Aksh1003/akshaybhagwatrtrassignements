#include <stdio.h>

int main(void)
{
	//variable declaraions
	int aab_iArray[3][5]; // 3 ROWS (0, 1, 2) AND 5 COLUMNS (0, 1, 2, 3, 4)
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements, aab_iArray_num_rows, aab_iArray_num_columns;
	int i, j;

	//code
	printf("\n\n");
	aab_int_size = sizeof(int);
	aab_iArray_size = sizeof(aab_iArray);
	
	printf("Size Of Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_size);
	
	aab_iArray_num_rows = aab_iArray_size / sizeof(aab_iArray[0]);

	printf("Number of Rows In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_rows);

	aab_iArray_num_columns = sizeof(aab_iArray[0]) / aab_int_size;

	printf("Number of Columns In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_columns);

	aab_iArray_num_elements = aab_iArray_num_rows * aab_iArray_num_columns;
	
	printf("Number of Elements In Two Dimensional ( 2D ) Integer Array Is = %d\n\n", aab_iArray_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");
	// ****** PIECE-MEAL ASSIGNMENT ******
	
		// ****** ROW 1 ******
	aab_iArray[0][0] = 21;
	aab_iArray[0][1] = 42;
	aab_iArray[0][2] = 63;
	aab_iArray[0][3] = 84;
	aab_iArray[0][4] = 105;
	
		// ****** ROW 2 ******
	aab_iArray[1][0] = 22;
	aab_iArray[1][1] = 44;
	aab_iArray[1][2] = 66;
	aab_iArray[1][3] = 88;
	aab_iArray[1][4] = 110;
	
		// ****** ROW 3 ******
	aab_iArray[2][0] = 23;
	aab_iArray[2][1] = 46;
	aab_iArray[2][2] = 69;
	aab_iArray[2][3] = 92;
	aab_iArray[2][4] = 115;
		
		// *** DISPLAY ***
	for (i = 0; i < aab_iArray_num_rows; i++)
	{
		printf("****** ROW %d ******\n", (i + 1));
		for (j = 0; j < aab_iArray_num_columns; j++)
		{
			printf("aab_iArray[%d][%d] = %d\n", i, j, aab_iArray[i][j]);
		}
		printf("\n\n");
	}

	return(0);
}