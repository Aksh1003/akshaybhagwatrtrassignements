#include <stdio.h>

#define AAB_Main main

int AAB_Main()
{
	int aab_iArray[5][3][2] = { { { 9, 18 }, { 27, 36 }, { 45, 54 } },
								{ { 8, 16 }, { 24, 32 }, { 40, 48 } },
								{ { 7, 14 }, { 21, 28 }, { 35, 42 } },
								{ { 6, 12 }, { 18, 24 }, { 30, 36 } },
								{ { 5, 10 }, { 15, 20 }, { 25, 30 } } };
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements, aab_iArray_width, aab_iArray_height, aab_iArray_depth;
	int aab_i, aab_j, aab_k;

	printf("\n\n");
	aab_int_size = sizeof(int);

	aab_iArray_size = sizeof(aab_iArray);

	printf("Size Of Three Dimensional ( 3D ) Integer Array Is = %d\n\n", aab_iArray_size);
	
	aab_iArray_width = aab_iArray_size / sizeof(aab_iArray[0]);

	printf("Number of Rows (Width) In Three Dimensional ( 3D ) Integer Array Is = % d\n\n", aab_iArray_width);
		
	aab_iArray_height = sizeof(aab_iArray[0]) / sizeof(aab_iArray[0][0]);

	printf("Number of Columns (Height) In Three Dimensional ( 3D ) Integer Array Is = % d\n\n", aab_iArray_height);

	aab_iArray_depth = sizeof(aab_iArray[0][0]) / aab_int_size;
	
	printf("Depth In Three Dimensional ( 3D ) Integer Array Is = %d\n\n", aab_iArray_depth);
	
	aab_iArray_num_elements = aab_iArray_width * aab_iArray_height * aab_iArray_depth;

	printf("Number of Elements In Three Dimensional ( 3D ) Integer Array Is = %d\n\n", aab_iArray_num_elements);

	printf("\n\n");

	printf("Elements In Integer 3D Array : \n\n");
	for (aab_i = 0; aab_i < aab_iArray_width; aab_i++)
	{
		printf("****** ROW %d ******\n", (aab_i + 1));
		for (aab_j = 0; aab_j < aab_iArray_height; aab_j++)
		{
			printf("****** COLUMN %d ******\n", (aab_j + 1));
			for (aab_k = 0; aab_k < aab_iArray_depth; aab_k++)
			{
				printf("aab_iArray[%d][%d][%d] = %d\n", aab_i, aab_j, aab_k, aab_iArray[aab_i][aab_j][aab_k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}

	return(0);
}
