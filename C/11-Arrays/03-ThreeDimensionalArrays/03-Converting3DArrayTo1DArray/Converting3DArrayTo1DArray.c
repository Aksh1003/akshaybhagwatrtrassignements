#include <stdio.h>

#define AAB_MAIN main
#define AAB_NUM_ROWS 5
#define AAB_NUM_COLUMNS 3
#define AAB_DEPTH 2

int AAB_MAIN()
{
	int aab_iArray[AAB_NUM_ROWS][AAB_NUM_COLUMNS][AAB_DEPTH] = { { { 9, 18 }, { 27, 36 }, { 45, 54 } },
																{ { 8, 16 }, { 24, 32 }, { 40, 48 } },
																{ { 7, 14 }, { 21, 28 }, { 35, 42 } },
																{ { 6, 12 }, { 18, 24 }, { 30, 36 } },
																{ { 5, 10 }, { 15, 20 }, { 25, 30 } } };
	int aab_i, aab_j, aab_k;
	int aab_iArray_1D[AAB_NUM_ROWS * AAB_NUM_COLUMNS * AAB_DEPTH];

	printf("\n\n");
	
	printf("Elements In The 3D Array : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		printf("****** ROW %d ******\n", (aab_i + 1));
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("****** COLUMN %d ******\n", (aab_j + 1));
			for (aab_k = 0; aab_k < AAB_DEPTH; aab_k++)
			{
				printf("aab_iArray[%d][%d][%d] = %d\n", aab_i, aab_j, aab_k, aab_iArray[aab_i][aab_j][aab_k]);
			}
			printf("\n");
		}
		printf("\n");
	}

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			for (aab_k = 0; aab_k < AAB_DEPTH; aab_k++)
			{
				aab_iArray_1D[(aab_i * AAB_NUM_COLUMNS * AAB_DEPTH) + (aab_j * AAB_DEPTH) + aab_k] = aab_iArray[aab_i][aab_j][aab_k];
			}
		}
	}

	printf("\n\n\n\n");
	printf("Elements In The 1D Array : \n\n");
	for (aab_i = 0; aab_i < (AAB_NUM_ROWS * AAB_NUM_COLUMNS * AAB_DEPTH); aab_i++)
	{
		printf("aab_iArray_1D[%d] = %d\n", aab_i, aab_iArray_1D[aab_i]);
	}

	return(0);
}
