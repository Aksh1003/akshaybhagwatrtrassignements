#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_iArray_One[5];
	int aab_iArray_Two[5][3];
	int aab_iArray_Three[100][100][5];

	int aab_num_rows_2D;
	int aab_num_columns_2D;

	int aab_num_rows_3D;
	int aab_num_columns_3D;
	int aab_depth_3D;

	printf("\n\nSize Of 1-D integer array aab_iArrat_One = %lu\n", sizeof(aab_iArray_One));
	printf("Number of elements in 1-D aab_iArray_One = %lu\n", (sizeof(aab_iArray_One) / sizeof(int)));

	printf("\n\nSize Of 2-D integer array aab_iArrat_Two = %lu\n", sizeof(aab_iArray_Two));
	printf("Number of rows in 2-D aab_iArray_Two = %lu\n", (sizeof(aab_iArray_Two) / sizeof(aab_iArray_Two[0])));
	aab_num_rows_2D = (sizeof(aab_iArray_Two) / sizeof(aab_iArray_Two[0]));
	printf("Number of columns in 2-D aab_iArray_Two = %lu\n", (sizeof(aab_iArray_Two[0]) / sizeof(aab_iArray_Two[0][0])));
	aab_num_columns_2D = (sizeof(aab_iArray_Two[0]) / sizeof(aab_iArray_Two[0][0]));
	printf("Number of elements in 2-D aab_iArray_Two = %lu\n", (aab_num_rows_2D * aab_num_columns_2D));

	printf("\n\nSize Of 3-D integer array aab_iArrat_Three = %lu\n", sizeof(aab_iArray_Three));
	printf("Number of rows in 3-D aab_iArray_Three = %lu\n", (sizeof(aab_iArray_Three) / sizeof(aab_iArray_Three[0])));
	aab_num_rows_3D = (sizeof(aab_iArray_Three) / sizeof(aab_iArray_Three[0]));
	printf("Number of columns in 3-D aab_iArray_Three = %lu\n", (sizeof(aab_iArray_Three[0]) / sizeof(aab_iArray_Three[0][0])));
	aab_num_columns_3D = (sizeof(aab_iArray_Three[0]) / sizeof(aab_iArray_Three[0][0]));
	printf("Number of depths in 3-D aab_iArray_Three = %lu\n", (sizeof(aab_iArray_Three[0][0]) / sizeof(aab_iArray_Three[0][0][0])));
	aab_depth_3D = (sizeof(aab_iArray_Three[0][0]) / sizeof(aab_iArray_Three[0][0][0]));
	printf("Number of elements in 3-D aab_iArray_Three = %lu\n", (aab_num_rows_3D * aab_num_columns_3D * aab_depth_3D));

	printf("\n\n");

	return 0;
}