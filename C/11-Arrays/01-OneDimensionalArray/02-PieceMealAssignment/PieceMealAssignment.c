#include <stdio.h>
int main(void)
{
	//variable declaraions
	int aab_iArrayOne[10];
	int aab_iArrayTwo[10];
	
	//code
	// ****** aab_iArrayOne[] ******
	aab_iArrayOne[0] = 3;
	aab_iArrayOne[1] = 6;
	aab_iArrayOne[2] = 9;
	aab_iArrayOne[3] = 12;
	aab_iArrayOne[4] = 15;
	aab_iArrayOne[5] = 18;
	aab_iArrayOne[6] = 21;
	aab_iArrayOne[7] = 24;
	aab_iArrayOne[8] = 27;
	aab_iArrayOne[9] = 30;
	
	printf("\n\n");
	printf("Piece-meal (Hard-coded) Assignment And Display Of Elements to Array 'aab_iArrayOne[]': \n\n");
	printf("1st Element Of Array 'aab_iArrayOne[]' Or Element At 0th Index Of Array 'aab_iArrayOne[]' = % d\n", aab_iArrayOne[0]);
	printf("2nd Element Of Array 'aab_iArrayOne[]' Or Element At 1st Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[1]);
	printf("3rd Element Of Array 'aab_iArrayOne[]' Or Element At 2nd Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[2]);
	printf("4th Element Of Array 'aab_iArrayOne[]' Or Element At 3rd Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[3]);
	printf("5th Element Of Array 'aab_iArrayOne[]' Or Element At 4th Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[4]);
	printf("6th Element Of Array 'aab_iArrayOne[]' Or Element At 5th Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[5]);
	printf("7th Element Of Array 'aab_iArrayOne[]' Or Element At 6th Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[6]);
	printf("8th Element Of Array 'aab_iArrayOne[]' Or Element At 7th Index Of Array	'aab_iArrayOne[]' = % d\n", aab_iArrayOne[7]);
	printf("9th Element Of Array 'aab_iArrayOne[]' Or Element At 8th Index Of Array 'aab_iArrayOne[]' = % d\n", aab_iArrayOne[8]);
	printf("10th Element Of Array 'aab_iArrayOne[]' Or Element At 9th Index Of Array 'aab_iArrayOne[]' = % d\n\n", aab_iArrayOne[9]);

	// ****** aab_iArrayTwo[] ******
	printf("\n\n");
	printf("Enter 1st Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[0]);
	printf("Enter 2nd Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[1]);
	printf("Enter 3rd Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[2]);
	printf("Enter 4th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[3]);
	printf("Enter 5th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[4]);
	printf("Enter 6th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[5]);
	printf("Enter 7th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[6]);
	printf("Enter 8th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[7]);
	printf("Enter 9th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[8]);
	printf("Enter 10th Element Of Array 'aab_iArrayTwo[]' : ");
	scanf("%d", &aab_iArrayTwo[9]);
	
	printf("\n\n");
	printf("Piece-meal (User Input) Assignment And Display Of Elements to Array 'aab_iArrayTwo[]' : \n\n");
	printf("1st Element Of Array 'aab_iArrayTwo[]' Or Element At 0th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[0]);
	printf("2nd Element Of Array 'aab_iArrayTwo[]' Or Element At 1st Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[1]);
	printf("3rd Element Of Array 'aab_iArrayTwo[]' Or Element At 2nd Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[2]);
	printf("4th Element Of Array 'aab_iArrayTwo[]' Or Element At 3rd Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[3]);
	printf("5th Element Of Array 'aab_iArrayTwo[]' Or Element At 4th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[4]);
	printf("6th Element Of Array 'aab_iArrayTwo[]' Or Element At 5th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[5]);
	printf("7th Element Of Array 'aab_iArrayTwo[]' Or Element At 6th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[6]);
	printf("8th Element Of Array 'aab_iArrayTwo[]' Or Element At 7th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[7]);
	printf("9th Element Of Array 'aab_iArrayTwo[]' Or Element At 8th Index Of Array 'aab_iArrayTwo[]' = % d\n", aab_iArrayTwo[8]);
	printf("10th Element Of Array 'aab_iArrayTwo[]' Or Element At 9th Index Of Array 'aab_iArrayTwo[]' = % d\n\n", aab_iArrayTwo[9]);

	return(0);
}