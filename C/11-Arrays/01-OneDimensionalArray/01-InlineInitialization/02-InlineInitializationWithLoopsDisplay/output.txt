

In-line Initialization And Loop (for) Display Of Elements of Array 'aab_iArray[]': 

aab_iArray[0] (Element 1) = 9
aab_iArray[1] (Element 2) = 30
aab_iArray[2] (Element 3) = 6
aab_iArray[3] (Element 4) = 12
aab_iArray[4] (Element 5) = 98
aab_iArray[5] (Element 6) = 95
aab_iArray[6] (Element 7) = 20
aab_iArray[7] (Element 8) = 23
aab_iArray[8] (Element 9) = 2
aab_iArray[9] (Element 10) = 45


Size Of Data type 'int' = 4 bytes
Number Of Elements In 'int' Array 'aab_iArray[]' = 10 Elements
Size Of Array 'aab_iArray[]' (10 Elements * 4 Bytes) = 40 Bytes



In-line Initialization And Loop (while) Display Of Elements of Array 'aab_fArray[]': 

aab_fArray[0] (Element 1) = 1.200000
aab_fArray[1] (Element 2) = 2.300000
aab_fArray[2] (Element 3) = 3.400000
aab_fArray[3] (Element 4) = 4.500000
aab_fArray[4] (Element 5) = 5.600000
aab_fArray[5] (Element 6) = 6.700000
aab_fArray[6] (Element 7) = 7.800000
aab_fArray[7] (Element 8) = 8.900000


Size Of Data type 'float' = 4 bytes
Number Of Elements In 'float' Array 'aab_fArray[]' = 8 Elements
Size Of Array 'aab_fArray[]' (8 Elements * 4 Bytes) = 32 Bytes



In-line Initialization And Loop (do while) Display Of Elements of Array 'aab_cArray[]': 

aab_cArray[0] (Element 1) = A
aab_cArray[1] (Element 2) = K
aab_cArray[2] (Element 3) = S
aab_cArray[3] (Element 4) = H
aab_cArray[4] (Element 5) = A
aab_cArray[5] (Element 6) = Y
aab_cArray[6] (Element 7) = B
aab_cArray[7] (Element 8) = H
aab_cArray[8] (Element 9) = A
aab_cArray[9] (Element 10) = G
aab_cArray[10] (Element 11) = W
aab_cArray[11] (Element 12) = A
aab_cArray[12] (Element 13) = T


Size Of Data type 'char' = 1 bytes
Number Of Elements In 'char' Array 'aab_cArray[]' = 13 Elements
Size Of Array 'aab_cArray[]' (13 Elements * 1 Bytes) = 13 Bytes

