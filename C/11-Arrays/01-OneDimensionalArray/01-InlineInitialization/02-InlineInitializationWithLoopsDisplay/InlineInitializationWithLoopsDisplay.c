#include <stdio.h>

int main(void)
{
	//variable declaraions
	int aab_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements;
	float aab_fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int aab_float_size;
	int aab_fArray_size;
	int aab_fArray_num_elements;
	char aab_cArray[] = { 'A', 'K', 'S', 'H', 'A', 'Y', 'B', 'H', 'A', 'G', 'W','A', 'T' };
	int aab_char_size;
	int aab_cArray_size;
	int aab_cArray_num_elements;
	int aab_i;
	
	//code
	// ****** iArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Loop (for) Display Of Elements of Array 'aab_iArray[]': \n\n");

	aab_int_size = sizeof(int);
	aab_iArray_size = sizeof(aab_iArray);

	aab_iArray_num_elements = aab_iArray_size / aab_int_size;

	for (aab_i = 0 ; aab_i < aab_iArray_num_elements ; aab_i++)
	{
		printf("aab_iArray[%d] (Element %d) = %d\n", aab_i, (aab_i + 1), aab_iArray[aab_i]);
	}

	printf("\n\n");
	printf("Size Of Data type 'int' = %d bytes\n", aab_int_size);
	printf("Number Of Elements In 'int' Array 'aab_iArray[]' = %d Elements\n", aab_iArray_num_elements);
	printf("Size Of Array 'aab_iArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_iArray_num_elements, aab_int_size, aab_iArray_size);

	// ****** fArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Loop (while) Display Of Elements of Array 'aab_fArray[]': \n\n");
		
	aab_float_size = sizeof(float);
	aab_fArray_size = sizeof(aab_fArray);

	aab_fArray_num_elements = aab_fArray_size / aab_float_size;

	aab_i = 0;
	
	while (aab_i < aab_fArray_num_elements)
	{
		printf("aab_fArray[%d] (Element %d) = %f\n", aab_i, (aab_i + 1), aab_fArray[aab_i]);
		aab_i++;
	}
	printf("\n\n");
	printf("Size Of Data type 'float' = %d bytes\n", aab_float_size);
	printf("Number Of Elements In 'float' Array 'aab_fArray[]' = %d Elements\n", aab_fArray_num_elements);
	printf("Size Of Array 'aab_fArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_fArray_num_elements, aab_float_size, aab_fArray_size);

	// ****** cArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Loop (do while) Display Of Elements of Array 'aab_cArray[]': \n\n");

	aab_char_size = sizeof(char);
	aab_cArray_size = sizeof(aab_cArray);
	
	aab_cArray_num_elements = aab_cArray_size / aab_char_size;

	aab_i = 0;
	
	do
	{
		printf("aab_cArray[%d] (Element %d) = %c\n", aab_i, (aab_i + 1), aab_cArray[aab_i]);
		aab_i++;
	} while (aab_i < aab_cArray_num_elements);

	printf("\n\n");
	printf("Size Of Data type 'char' = %d bytes\n", aab_char_size);
	printf("Number Of Elements In 'char' Array 'aab_cArray[]' = %d Elements\n", aab_cArray_num_elements);
	printf("Size Of Array 'aab_cArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_cArray_num_elements, aab_char_size, aab_cArray_size);

	return(0);
}