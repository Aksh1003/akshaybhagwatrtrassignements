#include <stdio.h>

int main(void)
{
	//variable declaraions
	int aab_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int aab_int_size;
	int aab_iArray_size;
	int aab_iArray_num_elements;
	float aab_fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int aab_float_size;
	int aab_fArray_size;
	int aab_fArray_num_elements;
	char aab_cArray[] = { 'A', 'K', 'S', 'H', 'A', 'Y', 'B', 'H', 'A', 'G', 'W','A', 'T'};
	int aab_char_size;
	int aab_cArray_size;
	int aab_cArray_num_elements;
	
	//code
	// ****** iArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'aab_iArray[]': \n\n");
	printf("aab_iArray[0] (1st Element) = %d\n", aab_iArray[0]);
	printf("aab_iArray[1] (2nd Element) = %d\n", aab_iArray[1]);
	printf("aab_iArray[2] (3rd Element) = %d\n", aab_iArray[2]);
	printf("aab_iArray[3] (4th Element) = %d\n", aab_iArray[3]);
	printf("aab_iArray[4] (5th Element) = %d\n", aab_iArray[4]);
	printf("aab_iArray[5] (6th Element) = %d\n", aab_iArray[5]);
	printf("aab_iArray[6] (7th Element) = %d\n", aab_iArray[6]);
	printf("aab_iArray[7] (8th Element) = %d\n", aab_iArray[7]);
	printf("aab_iArray[8] (9th Element) = %d\n", aab_iArray[8]);
	printf("aab_iArray[9] (10th Element) = %d\n\n", aab_iArray[9]);

	aab_int_size = sizeof(int);
	aab_iArray_size = sizeof(aab_iArray);
	aab_iArray_num_elements = aab_iArray_size / aab_int_size;

	printf("Size Of Data type 'int' = %d bytes\n", aab_int_size);
	printf("Number Of Elements In 'int' Array 'aab_iArray[]' = %d Elements\n", aab_iArray_num_elements);
	printf("Size Of Array 'aab_iArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_iArray_num_elements, aab_int_size, aab_iArray_size);
	
	// ****** fArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'aab_fArray[]': \n\n");
	printf("aab_fArray[0] (1st Element) = %f\n", aab_fArray[0]);
	printf("aab_fArray[1] (2nd Element) = %f\n", aab_fArray[1]);
	printf("aab_fArray[2] (3rd Element) = %f\n", aab_fArray[2]);
	printf("aab_fArray[3] (4th Element) = %f\n", aab_fArray[3]);
	printf("aab_fArray[4] (5th Element) = %f\n", aab_fArray[4]);
	printf("aab_fArray[5] (6th Element) = %f\n", aab_fArray[5]);
	printf("aab_fArray[6] (7th Element) = %f\n", aab_fArray[6]);
	printf("aab_fArray[7] (8th Element) = %f\n", aab_fArray[7]);
	printf("aab_fArray[8] (9th Element) = %f\n", aab_fArray[8]);
	printf("aab_fArray[9] (10th Element) = %f\n\n", aab_fArray[9]);

	aab_float_size = sizeof(float);
	aab_fArray_size = sizeof(aab_fArray);

	aab_fArray_num_elements = aab_fArray_size / aab_float_size;
	
	printf("Size Of Data type 'float' = %d bytes\n", aab_float_size);
	printf("Number Of Elements In 'float' Array 'aab_fArray[]' = %d Elements\n", aab_fArray_num_elements);
	printf("Size Of Array 'aab_fArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_fArray_num_elements, aab_float_size, aab_fArray_size);

	// ****** cArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'aab_cArray[]': \n\n");
	printf("aab_cArray[0] (1st Element) = %c\n", aab_cArray[0]);
	printf("aab_cArray[1] (2nd Element) = %c\n", aab_cArray[1]);
	printf("aab_cArray[2] (3rd Element) = %c\n", aab_cArray[2]);
	printf("aab_cArray[3] (4th Element) = %c\n", aab_cArray[3]);
	printf("aab_cArray[4] (5th Element) = %c\n", aab_cArray[4]);
	printf("aab_cArray[5] (6th Element) = %c\n", aab_cArray[5]);
	printf("aab_cArray[6] (7th Element) = %c\n", aab_cArray[6]);
	printf("aab_cArray[7] (8th Element) = %c\n", aab_cArray[7]);
	printf("aab_cArray[8] (9th Element) = %c\n", aab_cArray[8]);
	printf("aab_cArray[9] (10th Element) = %c\n", aab_cArray[9]);
	printf("aab_cArray[10] (11th Element) = %c\n", aab_cArray[10]);
	printf("aab_cArray[11] (12th Element) = %c\n", aab_cArray[11]);
	printf("aab_cArray[12] (13th Element) = %c\n\n", aab_cArray[12]);

	aab_char_size = sizeof(char);
	aab_cArray_size = sizeof(aab_cArray);
	
	aab_cArray_num_elements = aab_cArray_size / aab_char_size;

	printf("Size Of Data type 'char' = %d bytes\n", aab_char_size);
	printf("Number Of Elements In 'char' Array 'aab_cArray[]' = %d Elements\n", aab_cArray_num_elements);
	printf("Size Of Array 'cArray[]' (%d Elements * %d Bytes) = %d Bytes\n\n", aab_cArray_num_elements, aab_char_size, aab_cArray_size);

	return(0);
}