#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int aab_iArray[NUM_ELEMENTS];
	int aab_i, aab_num, aab_sum = 0;

	//code
	printf("\n\n");

	// *** ARRAY ELEMENTS INPUT ***
	printf("Enter Integer Elememts For Array : \n\n");
	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		scanf("%d", &aab_num);
		aab_iArray[aab_i] = aab_num;
	}

	// *** SEPARATING OUT EVEN NUMBERS FROM ARRAY ELEMENTS ***
	printf("\n\n");
	printf("Even Numbers Amongst The Array Elements Are : \n\n");
	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		if ((aab_iArray[aab_i] % 2) == 0)
			printf("%d\n", aab_iArray[aab_i]);
	}

	// *** SEPARATING OUT ODD NUMBERS FROM ARRAY ELEMENTS ***
	printf("\n\n");
	printf("Odd Numbers Amongst The Array Elements Are : \n\n");
	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		if ((aab_iArray[aab_i] % 2) != 0)
			printf("%d\n", aab_iArray[aab_i]);
	}

	return(0);
}