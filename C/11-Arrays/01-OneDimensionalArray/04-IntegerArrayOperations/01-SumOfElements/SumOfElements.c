#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int aab_iArray[NUM_ELEMENTS];
	int aab_i, aab_num, aab_sum = 0;

	//code
	printf("\n\n");
	printf("Enter Integer Elememts For Array : \n\n");
	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		scanf("%d", &aab_num);
		aab_iArray[aab_i] = aab_num;
	}

	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		aab_sum = aab_sum + aab_iArray[aab_i];
	}

	printf("\n\n");
	printf("Sum Of ALL Elements Of Array = %d\n\n", aab_sum);
	
	return(0);
}