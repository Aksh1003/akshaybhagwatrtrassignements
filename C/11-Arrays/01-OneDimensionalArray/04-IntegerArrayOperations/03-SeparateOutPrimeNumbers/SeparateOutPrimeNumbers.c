#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int aab_iArray[NUM_ELEMENTS];
	int aab_i, aab_num, aab_j, aab_count = 0;
	
	//code
	printf("\n\n");
	
	// *** ARRAY ELEMENTS INPUT ***
	printf("Enter Integer Elememts For Array : \n\n");
	for (aab_i = 0 ; aab_i < NUM_ELEMENTS ; aab_i++)
	{
		scanf("%d", &aab_num);
		
		if (aab_num < 0)
			aab_num = -1 * aab_num;
		aab_iArray[aab_i] = aab_num;
	}

	// *** PRINTING ENTIRE ARRAY ***
	printf("\n\n");
	printf("Array Elements Are : \n\n");
	for (aab_i = 0; aab_i < NUM_ELEMENTS ; aab_i++)
		printf("%d\n", aab_iArray[aab_i]);

	// *** SEPARATING OUT EVEN NUMBERS FROM ARRAY ELEMENTS ***
	printf("\n\n");
	printf("Prime Numbers Amongst The Array Elements Are : \n\n");
	for (aab_i = 0; aab_i < NUM_ELEMENTS; aab_i++)
	{
		for (aab_j = 1; aab_j <= aab_iArray[aab_i]; aab_j++)
		{
			if ((aab_iArray[aab_i] % aab_j) == 0)
				aab_count++;
		}

		if (aab_count == 2)
			printf("%d\n", aab_iArray[aab_i]);

		aab_count = 0;
	}

	return(0);
}