#include <stdio.h>

#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 15

int main(void)
{
	//variable declarations
	int aab_iArray[INT_ARRAY_NUM_ELEMENTS];
	float aab_fArray[FLOAT_ARRAY_NUM_ELEMENTS];
	char aab_cArray[CHAR_ARRAY_NUM_ELEMENTS];
	int aab_i, aab_num;

	//code
	// ********** ARRAY ELEMENTS INPUT **********

	printf("\n\n");
	printf("Enter Elements For 'Integer' Array : \n");
	for (aab_i = 0 ; aab_i < INT_ARRAY_NUM_ELEMENTS ; aab_i++)
		scanf("%d", &aab_iArray[aab_i]);

	printf("\n\n");
	printf("Enter Elements For 'Floating-Point' Array : \n");
	for (aab_i = 0 ; aab_i < FLOAT_ARRAY_NUM_ELEMENTS ; aab_i++)
		scanf("%f", &aab_fArray[aab_i]);

	printf("\n\n");
	printf("Enter Elements For 'Character' Array : \n");
	for (aab_i = 0; aab_i < CHAR_ARRAY_NUM_ELEMENTS ; aab_i++)
	{
		aab_cArray[aab_i] = getch();
		printf("%c\n", aab_cArray[aab_i]);
	}

	// ********** ARRAY ELEMENTS OUTPUT **********
	printf("\n\n");
	printf("Integer Array Entered By You : \n\n");
	for (aab_i = 0 ; aab_i < INT_ARRAY_NUM_ELEMENTS ; aab_i++)
		printf("%d\n", aab_iArray[aab_i]);
	
	printf("\n\n");
	printf("Floating-Point Array Entered By You : \n\n");
	for (aab_i = 0 ; aab_i < FLOAT_ARRAY_NUM_ELEMENTS ; aab_i++)
		printf("%f\n", aab_fArray[aab_i]);

	printf("\n\n");
	printf("Character Array Entered By You : \n\n");
	for (aab_i = 0 ; aab_i < CHAR_ARRAY_NUM_ELEMENTS ; aab_i++)
		printf("%c\n", aab_cArray[aab_i]);

	return(0);
}