#include <stdio.h>

int main(void)
{
	//variable declarations
	char aab_chArray_01[] = { 'A', 'K', 'S', 'H', 'A', 'Y', 'B', 'H', 'A', 'G', 'W', 'A', 'T', '\0' }; 
	char aab_chArray_02[9] = { 'W', 'E', 'L', 'C', 'O', 'M', 'E', 'S', '\0' }; 
	char aab_chArray_03[] = { 'Y', 'O', 'U', '\0' };
	char aab_chArray_04[] = "To"; 
	char aab_chArray_05[] = "REAL TIME RENDERING BATCH OF 2020-21";
	char aab_chArray_WithoutNullTerminator[] = { 'H', 'e', 'l', 'l', 'o' };
	
	//code
	printf("\n\n");
	printf("Size Of aab_chArray_01 : %lu\n\n", sizeof(aab_chArray_01));
	printf("Size Of aab_chArray_02 : %lu\n\n", sizeof(aab_chArray_02));
	printf("Size Of aab_chArray_03 : %lu\n\n", sizeof(aab_chArray_03));
	printf("Size Of aab_chArray_04 : %lu\n\n", sizeof(aab_chArray_04));
	printf("Size Of aab_chArray_05 : %lu\n\n", sizeof(aab_chArray_05));
	
	printf("\n\n");
	printf("The Strings Are : \n\n");
	printf("aab_chArray_01 : %s\n\n", aab_chArray_01);
	printf("aab_chArray_02 : %s\n\n", aab_chArray_02);
	printf("aab_chArray_03 : %s\n\n", aab_chArray_03);
	printf("aab_chArray_04 : %s\n\n", aab_chArray_04);
	printf("aab_chArray_05 : %s\n\n", aab_chArray_05);
	
	printf("\n\n");
	printf("Size Of aab_chArray_WithoutNullTerminator : %lu\n\n", sizeof(aab_chArray_WithoutNullTerminator));
	printf("aab_chArray_WithoutNullTerminator : %s\n\n", aab_chArray_WithoutNullTerminator);

	return(0);
}