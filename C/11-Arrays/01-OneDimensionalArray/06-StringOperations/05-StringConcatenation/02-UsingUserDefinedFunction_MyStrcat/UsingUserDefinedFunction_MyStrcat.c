#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void aab_MyStrcat(char[], char[]);
	
	//variable declarations
	char aab_chArray_One[MAX_STRING_LENGTH], aab_chArray_Two[MAX_STRING_LENGTH];
	
	//code
	// *** STRING INPUT ***
	
	printf("\n\n");
	printf("Enter First String : \n\n");
	gets_s(aab_chArray_One, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter Second String : \n\n");
	gets_s(aab_chArray_Two, MAX_STRING_LENGTH);
	
	// *** STRING CONCAT ***
	printf("\n\n");
	printf("****** BEFORE CONCATENATION ******");
	printf("\n\n");
	printf("The Original First String Entered By You (i.e : 'chArray_One[]') Is :\n\n");
	printf("%s\n", aab_chArray_One);

	printf("\n\n");
	printf("The Original Second String Entered By You (i.e : 'chArray_Two[]') Is :\n\n");
	printf("%s\n", aab_chArray_Two);

	aab_MyStrcat(aab_chArray_One, aab_chArray_Two);

	printf("\n\n");
	printf("****** AFTER CONCATENATION ******");
	printf("\n\n");
	printf("'chArray_One[]' Is : \n\n");
	printf("%s\n", aab_chArray_One);

	printf("\n\n");
	printf("'chArray_Two[]' Is : \n\n");
	printf("%s\n", aab_chArray_Two);

	return(0);
}

void aab_MyStrcat(char aab_str_destination[], char aab_str_source[])
{
	//function prototype
	int aab_MyStrlen(char[]);
	
	//variable declarations
	int aab_iStringLength_Source = 0, aab_iStringLength_Destination = 0;
	int aab_i, aab_j;

	//code
	aab_iStringLength_Source = aab_MyStrlen(aab_str_source);
	aab_iStringLength_Destination = aab_MyStrlen(aab_str_destination);
	// ARRAY INDICES BEGIN FROM 0, HENCE, LAST VALID INDEX OF ARRAY WILL ALWAYS BE (LENGTH - 1)
	// SO, CONCATENATION MUST BEGIN FROM INDEX NUMBER EQUAL TO LENGTH OF THE ARRAY 'str_destination'
	// WE NEED TO PUT THE CHARACTER WHICH IS AT FIRST INDEX OF 'str_source' TO THE (LAST INDEX + 1) OF 'str_destination'

	for (aab_i = aab_iStringLength_Destination, aab_j = 0; aab_j < aab_iStringLength_Source; aab_i++, aab_j++)
	{
		aab_str_destination[aab_i] = aab_str_source[aab_j];
	}

	aab_str_destination[aab_i] = '\0';
}

int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;

	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}