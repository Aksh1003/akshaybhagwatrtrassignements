#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void aab_MyStrrev(char[], char[]);
	
	//variable declarations
	char aab_chArray_Original[MAX_STRING_LENGTH], aab_chArray_Reversed[MAX_STRING_LENGTH]; // A Character Array Is A String

	//code
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray_Original, MAX_STRING_LENGTH);
	
	// *** STRING REVERSE ***
	aab_MyStrrev(aab_chArray_Reversed, aab_chArray_Original);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'chArray_Original[]') Is :\n\n");
	printf("%s\n", aab_chArray_Original);

	printf("\n\n");
	printf("The Reversed String (i.e : 'chArray_Reversed[]') Is : \n\n");
	printf("%s\n", aab_chArray_Reversed);

	return(0);
}
void aab_MyStrrev(char aab_str_destination[], char aab_str_source[])
{
	//function prototype
	int aab_MyStrlen(char[]);

	//variable declarations
	int aab_iStringLength = 0;
	int aab_i, aab_j, aab_len;

	//code
	aab_iStringLength = aab_MyStrlen(aab_str_source);

	// ARRAY INDICES BEGIN FROM 0, HENCE, LAST INDEX WILL ALWAYS BE (LENGTH - 1)
	aab_len = aab_iStringLength - 1;
	
	// WE NEED TO PUT THE CHARACTER WHICH IS AT LAST INDEX OF 'str_source' TO THE FIRST INDEX OF 'str_destination'
	// AND SECOND-LAST CHARACTER OF 'str_source' TO THE SECOND CHARACTER OF 'str_destination' and so on...

	for (aab_i = 0, aab_j = aab_len; aab_i < aab_iStringLength, aab_j >= 0; aab_i++, aab_j--)
	{
		aab_str_destination[aab_i] = aab_str_source[aab_j];
	}
	aab_str_destination[aab_i] = '\0';
}

int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;

	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}