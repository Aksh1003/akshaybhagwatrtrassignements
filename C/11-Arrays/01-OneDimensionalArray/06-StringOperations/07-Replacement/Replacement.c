// *** THSI PROGRAM REPLACES ALL VOWELS IN THE INPUT STRING WITH THE * (asterisk) SYMBOL***
// *** FOR EXAMPLE, ORIGINAL STRING 'AKSHAY ANIL BHAGWAT' WILL BECOME '*KSH*Y *N*L BH*GW*T'

#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int aab_MyStrlen(char[]);
	void aab_MyStrcpy(char[], char[]);
	
	//variable declarations
	char aab_chArray_Original[MAX_STRING_LENGTH], aab_chArray_VowelsReplaced[MAX_STRING_LENGTH]; // A Character Array Is A String
	int aab_iStringLength;
	int aab_i;

	//code
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray_Original, MAX_STRING_LENGTH);
	
	// *** STRING OUTPUT ***
	aab_MyStrcpy(aab_chArray_VowelsReplaced, aab_chArray_Original);
	aab_iStringLength = aab_MyStrlen(aab_chArray_VowelsReplaced);

	for (aab_i = 0; aab_i < aab_iStringLength; aab_i++)
	{
		switch (aab_chArray_VowelsReplaced[aab_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			aab_chArray_VowelsReplaced[aab_i] = '*';
			break;
		default:
			break;
		}
	}

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", aab_chArray_Original);

	printf("\n\n");
	printf("String After Replacement Of Vowels By * Is : \n\n");
	printf("%s\n", aab_chArray_VowelsReplaced);

	return(0);
}

int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;

	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}

void aab_MyStrcpy(char aab_str_destination[], char aab_str_source[])
{
	//function prototype
	int aab_MyStrlen(char[]);

	//variable declarations
	int aab_iStringLength = 0;
	int aab_j;
	
	//code
	aab_iStringLength = aab_MyStrlen(aab_str_source);
	for (aab_j = 0; aab_j < aab_iStringLength; aab_j++)
		aab_str_destination[aab_j] = aab_str_source[aab_j];
	aab_str_destination[aab_j] = '\0';
}
