#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void aab_MyStrcpy(char[], char[]);

	//variable declarations
	char aab_chArray_Original[MAX_STRING_LENGTH], aab_chArray_Copy[MAX_STRING_LENGTH]; 

	//code
	
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray_Original, MAX_STRING_LENGTH);
	
	// *** STRING COPY ***
	aab_MyStrcpy(aab_chArray_Copy, aab_chArray_Original);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'chArray_Original[]') Is :\n\n");
	printf("%s\n", aab_chArray_Original);

	printf("\n\n");
	printf("The Copied String (i.e : 'chArray_Copy[]') Is : \n\n");
	printf("%s\n", aab_chArray_Copy);

	return(0);
}
void aab_MyStrcpy(char aab_str_destination[], char aab_str_source[])
{
	//function prototype
	int aab_MyStrlen(char[]);

	//variable declarations
	int aab_iStringLength = 0;
	int aab_j;

	//code
	aab_iStringLength = aab_MyStrlen(aab_str_source);

	for (aab_j = 0; aab_j < aab_iStringLength; aab_j++)
		aab_str_destination[aab_j] = aab_str_source[aab_j];

	aab_str_destination[aab_j] = '\0';
}

int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;
	
	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}