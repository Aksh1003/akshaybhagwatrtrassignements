#include <stdio.h>

#define AAB_MAX_STRING_LENGTH 512

int main()
{
	int AAB_MyStrlen(char[]);

	char aab_chArray[AAB_MAX_STRING_LENGTH];
	int aab_iStringLength;
	int aab_i;
	int aab_word_count = 0, aab_space_count = 0;

	printf("\n\n");
	printf("Enter a string\n\n");
	gets(aab_chArray, AAB_MAX_STRING_LENGTH);

	aab_iStringLength = AAB_MyStrlen(aab_chArray);

	for (aab_i = 0; aab_i < aab_iStringLength; aab_i++)
	{
		switch (aab_chArray[aab_i])
		{
		case 32:
			aab_space_count++;
			break;

		default:
			break;
		}
	}

	aab_word_count = aab_space_count + 1;

	printf("\n\nString Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	printf("\nNumber Of Spaces In The Input String : %d\n\n", aab_space_count);
	printf("Number Of Words In The Input String : %d\n\n", aab_word_count);

	return 0;
}

int AAB_MyStrlen(char aab_str[])
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}

	return (aab_string_length);
}
