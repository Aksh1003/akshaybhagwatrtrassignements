#include <stdio.h>

#define AAB_MAX_STRING_LENGTH 512

int main()
{
	int AAB_MyStrlen(char[]);

	char aab_chArray[AAB_MAX_STRING_LENGTH], aab_chArray_SpacesRemoved[AAB_MAX_STRING_LENGTH];
	int aab_iStringLength;
	int aab_i, aab_j;

	printf("\n\nEnter A String : \n");
	gets(aab_chArray, AAB_MAX_STRING_LENGTH);

	aab_iStringLength = AAB_MyStrlen(aab_chArray);
	aab_j = 0;

	for (aab_i = 0; aab_i < aab_iStringLength; aab_i++)
	{
		if (aab_chArray[aab_i] == ' ')
			continue;
		else
		{
			aab_chArray_SpacesRemoved[aab_j] = aab_chArray[aab_i];
			aab_j++;
		}
	}

	aab_chArray_SpacesRemoved[aab_j] = '\0';

	printf("\n\nString Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	printf("\n\nString After Removed Spaces Is : \n\n");
	printf("%s\n", aab_chArray_SpacesRemoved);

	return 0;
}

int AAB_MyStrlen(char aab_str[])
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}

	return (aab_string_length);
}