#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int aab_MyStrlen(char[]);
	
	//variable declarations
	char aab_chArray[MAX_STRING_LENGTH]; // A Character Array Is A String
	int aab_iStringLength = 0;
	
	//code
	
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray, MAX_STRING_LENGTH);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	// *** STRING LENGTH ***
	printf("\n\n");
	aab_iStringLength = aab_MyStrlen(aab_chArray);

	printf("Length Of String Is = %d Characters !!!\n\n", aab_iStringLength);

	return(0);
}
int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;

	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}

	return(aab_string_length);
}