#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char aab_chArray[MAX_STRING_LENGTH]; // A Character Array Is A String
	int aab_iStringLength = 0;
	
	//code
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray, MAX_STRING_LENGTH);
	
	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	// *** STRING LENGTH ***
	printf("\n\n");
	aab_iStringLength = strlen(aab_chArray);

	printf("Length Of String Is = %d Characters !!!\n\n", aab_iStringLength);

	return(0);
}