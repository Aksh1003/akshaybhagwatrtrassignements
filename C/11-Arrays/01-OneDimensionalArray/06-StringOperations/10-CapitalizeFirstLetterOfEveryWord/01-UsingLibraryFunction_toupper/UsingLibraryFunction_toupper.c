#include <stdio.h>
#include <ctype.h>

#define AAB_MAX_STRING_LENGTH 512

int main()
{
	int AAB_MyStrlen(char[]);

	char aab_chArray[AAB_MAX_STRING_LENGTH], aab_chArray_CapitalizedFirstLetterOfEveryWord[AAB_MAX_STRING_LENGTH];
	int aab_iStringLength;
	int aab_i, aab_j;

	printf("\n\nEnter A String : \n");
	gets(aab_chArray, AAB_MAX_STRING_LENGTH);

	aab_iStringLength = AAB_MyStrlen(aab_chArray);
	aab_j = 0;

	for (aab_i = 0; aab_i < aab_iStringLength; aab_i++)
	{
		if (aab_i == 0)
			aab_chArray_CapitalizedFirstLetterOfEveryWord[aab_j] = toupper(aab_chArray[aab_i]);
		else if(aab_chArray[aab_i] == ' ')
		{
			aab_chArray_CapitalizedFirstLetterOfEveryWord[aab_j] = aab_chArray[aab_i];
			aab_chArray_CapitalizedFirstLetterOfEveryWord[aab_j + 1] = toupper(aab_chArray[aab_i + 1]);

			aab_i++;
			aab_j++;
		}
		else
			aab_chArray_CapitalizedFirstLetterOfEveryWord[aab_j] = aab_chArray[aab_i];

		aab_j++;

	}
	
	aab_chArray_CapitalizedFirstLetterOfEveryWord[aab_j] = '\0';

	printf("\n\nString Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	printf("\n\nString After Capitalizing Every First Letter Of Every Word Is : \n\n");
	printf("%s\n", aab_chArray_CapitalizedFirstLetterOfEveryWord);

	return 0;
}

int AAB_MyStrlen(char aab_str[])
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}

	return (aab_string_length);
}
