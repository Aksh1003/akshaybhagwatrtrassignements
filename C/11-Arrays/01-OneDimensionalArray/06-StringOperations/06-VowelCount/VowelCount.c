#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int aab_MyStrlen(char[]);
	
	//variable declarations
	char aab_chArray[MAX_STRING_LENGTH]; // A Character Array Is A String
	int aab_iStringLength;
	int aab_count_A = 0, aab_count_E = 0, aab_count_I = 0, aab_count_O = 0, aab_count_U = 0; // Initial Counts = 0
	int aab_i;
	
	//code
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(aab_chArray, MAX_STRING_LENGTH);
	
	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);
	
	aab_iStringLength = aab_MyStrlen(aab_chArray);
	for (aab_i = 0; aab_i < aab_iStringLength; aab_i++)
	{
		switch (aab_chArray[aab_i])
		{
		case 'A':
		case 'a':
			aab_count_A++;
			break;
		case 'E':
		case 'e':
			aab_count_E++;
			break;
		case 'I':
		case 'i':
			aab_count_I++;
			break;
		case 'O':
		case 'o':
			aab_count_O++;
			break;
		case 'U':
		case 'u':
			aab_count_U++;
			break;
		default:
			break;
		}
	}

	printf("\n\n");
	printf("In The String Entered By You, The Vowels And The Number Of Their occurences Are Are Follows : \n\n");
	printf("'A' Has Occured = %d Times !!!\n\n", aab_count_A);
	printf("'E' Has Occured = %d Times !!!\n\n", aab_count_E);
	printf("'I' Has Occured = %d Times !!!\n\n", aab_count_I);
	printf("'O' Has Occured = %d Times !!!\n\n", aab_count_O);
	printf("'U' Has Occured = %d Times !!!\n\n", aab_count_U);

	return(0);
}

int aab_MyStrlen(char aab_str[])
{
	//variable declarations
	int aab_j;
	int aab_string_length = 0;

	//code
	for (aab_j = 0; aab_j < MAX_STRING_LENGTH; aab_j++)
	{
		if (aab_str[aab_j] == '\0')
			break;
		else
			aab_string_length++;
	}
	
	return(aab_string_length);
}