#include <stdio.h>

int main()
{
	int aab_i, aab_j;
	char aab_ch_01, aab_ch_02;

	int aab_a, aab_result_int;
	float aab_f, aab_result_float;

	int aab_i_explicit;
	float aab_f_explicit;

	printf("\n\n");

	aab_i = 70;
	aab_ch_01 = aab_i;
	printf("aab_i = %d\n", aab_i);
	printf("character 1 (after aab_ch_01 = aab_i) = %c\n\n", aab_ch_01);

	aab_ch_02 = 'Q';
	aab_j = aab_ch_02;
	printf("charcter 2 = %c\n", aab_ch_02);
	printf("aab_j (after aab_j = aab_ch_02) = %d\n\n", aab_j);

	aab_a = 5;
	aab_f = 7.8f;
	aab_result_float = aab_a + aab_f;
	printf("integer aab_a = %d and floating point number %f added gives floating sum = %f\n\n", aab_a, aab_f, aab_result_float);

	aab_result_int = aab_a + aab_f;
	printf("integer aab_a = %d and floating point number %f added gives integer sum = %d\n\n", aab_a, aab_f, aab_result_int);

	aab_f_explicit = 30.121995f;
	aab_i_explicit = (int)aab_f_explicit;
	printf("floating point number which will be type casted explicitly = %f\n\n", aab_f_explicit);
	printf("resultant integer after explicit type casting of %f = %d\n\n", aab_f_explicit, aab_i_explicit);

	return 0;
}