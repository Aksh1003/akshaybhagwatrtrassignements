#include <stdio.h>
int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	// variable declarations
	unsigned int aab_num;

	// function prototype
	void aab_recursive(unsigned int);
	
	// code
	printf("\n\n");
	printf("Enter Any Number : \n\n");
	scanf("%u", &aab_num);

	printf("\n\n");
	printf("Output Of Recursive Function : \n\n");
	
	aab_recursive(aab_num);
	
	printf("\n\n");
	
	return(0);
}

void aab_recursive(unsigned int aab_n)
{
	// code
	printf("aab_n = %d\n", aab_n);
	
	if (aab_n > 0)
	{
		aab_recursive(aab_n - 1);
	}
}