#include <stdio.h>

#define MAX_NUMBER(aab_a, aab_b) ((aab_a > aab_b) ? aab_a : aab_b)

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	// variable declarations
	int aab_iNum_01;
	int aab_iNum_02;
	int aab_iResult;
	
	float aab_fNum_01;
	float aab_fNum_02;
	float aab_fResult;
	
	// code
	// ****** COMPARING INTEGER VALUES ******
	printf("\n\n");
	printf("Enter An Integer Number : \n\n");
	scanf("%d", &aab_iNum_01);
	
	printf("\n\n");
	printf("Enter Another Integer Number : \n\n");
	scanf("%d", &aab_iNum_02);
	
	aab_iResult = MAX_NUMBER(aab_iNum_01, aab_iNum_02);

	printf("\n\n");
	printf("Result Of Macro Function MAX_NUMBER() = %d\n", aab_iResult);
	printf("\n\n");

	// ****** COMPARING FLOATING-POINT VALUES ******
	printf("\n\n");
	printf("Enter A Floating Point Number : \n\n");
	scanf("%f", &aab_fNum_01);

	printf("\n\n");
	printf("Enter Another Floating Point Number : \n\n");
	scanf("%f", &aab_fNum_02);

	aab_fResult = MAX_NUMBER(aab_fNum_01, aab_fNum_02);

	printf("\n\n");
	printf("Result Of Macro Function MAX_NUMBER() = %f\n", aab_fResult);
	printf("\n\n");

	return(0);
}