#include <stdio.h>

int main(void)
{
	int aab_a = 5;
	
	void aab_change_count(void);
	
	printf("\n");
	printf("aab_a = %d\n\n", aab_a);
	
	aab_change_count();
	aab_change_count();
	aab_change_count();

	return(0);
}

void aab_change_count(void)
{
	static int aab_local_count = 0;
	
	aab_local_count = aab_local_count + 1;
	printf("Local Count = %d\n", aab_local_count);
}