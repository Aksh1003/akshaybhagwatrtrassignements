#include <stdio.h>
// *** GLOBAL SCOPE ***
//If not initialized by us, global variables are initialized to their zero values (with respect to their data types i.e: 0 for int, 0.0 for floatand double,
//	etc.) by default.
//But still, for good programming discipline, we shall explicitly initialize our global variable with 0.
	
int aab_global_count = 0;
int main(void)
{
	//function prototypes
	void aab_change_count_one(void);
	void aab_change_count_two(void);
	void aab_change_count_three(void);

	//code
	printf("\n");
	printf("main() : Value of global_count = %d\n", aab_global_count);
	aab_change_count_one();
	aab_change_count_two();
	aab_change_count_three();
	
	printf("\n");
	return(0);
}

// *** GLOBAL SCOPE ***
void aab_change_count_one(void)
{
	//code
	aab_global_count = 100;
	printf("change_count_one() : Value of global_count = %d\n", aab_global_count);
}

// *** GLOBAL SCOPE ***
void aab_change_count_two(void)
{
	//code
	aab_global_count = aab_global_count + 1;
	printf("change_count_two() : Value of global_count = %d\n", aab_global_count);
}

// *** GLOBAL SCOPE ***
void aab_change_count_three(void)
{
	//code
	aab_global_count = aab_global_count + 10;
	printf("change_count_three() : Value of global_count = %d\n", aab_global_count);
}

// *** GLOBAL SCOPE ***