#include <stdio.h>
// *** GLOBAL SCOPE ***

int main(void)
{
	//function prototypes
	void aab_change_count(void);
	
	//variable declarations
	extern int aab_global_count;
	
	//code
	printf("\n");
	printf("Value Of global_count before change_count() = %d\n", aab_global_count);
	aab_change_count();
	printf("Value Of global_count after change_count() = %d\n", aab_global_count);
	printf("\n");

	return(0);
}

int aab_global_count = 0;
void aab_change_count(void)
{
	//code
	aab_global_count = 5;
	printf("Value Of global_count in change_count() = %d\n", aab_global_count);
}
