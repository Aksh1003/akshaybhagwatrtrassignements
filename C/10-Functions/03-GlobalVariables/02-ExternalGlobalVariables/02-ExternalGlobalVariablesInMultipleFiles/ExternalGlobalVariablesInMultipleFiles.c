#include <stdio.h>

int aab_global_count = 0;
int main(void)
{
	//function prototypes
	void aab_change_count(void);
	void aab_change_count_one(void); //function defines in File_01.c
	void aab_change_count_two(void); //function defines in File_02.c
	
	//code
	printf("\n");
	aab_change_count();
	aab_change_count_one(); //function defines in File_01.c
	aab_change_count_two(); //function defines in File_02.c
	
	return(0);
}

void aab_change_count(void)
{
	//code
	aab_global_count = aab_global_count + 1;
	printf("Global Count = %d\n", aab_global_count);
}