#include <stdio.h>

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	//function prototype / declaration / signature
	int aab_MyAddition(void);

	//variable declarations : local variables to main()
	int aab_result;
	
	//code
	aab_result = aab_MyAddition(); //function call

	printf("\n\n");
	printf("Sum = %d\n\n", aab_result);

	return(0);
}

// ****** USER DEFINED FUNCTION : METHOD OF DEFINITION 2 ******
// ****** VALID (int) RETURN VALUE, NO PARAMETERS ******

int aab_MyAddition(void) //function definition
{
	//variable declarations : local variables to MyAddition()
	int aab_a, aab_b, aab_sum;

	//code
	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' : ");
	scanf("%d", &aab_b);

	aab_sum = aab_a + aab_b;
	
	return(aab_sum);
}
