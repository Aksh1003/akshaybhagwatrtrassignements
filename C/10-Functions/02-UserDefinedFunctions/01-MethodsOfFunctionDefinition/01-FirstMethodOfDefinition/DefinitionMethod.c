#include <stdio.h>

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	//function prototype / declaration / signature
	void aab_MyAddition(void);
	
	//code
	aab_MyAddition(); //function call

	return(0);
}

// ****** USER DEFINED FUNCTION : METHOD OF DEFINITION 1 ******
// ****** NO RETURN VALUE, NO PARAMETERS ******

void aab_MyAddition(void) //function definition
{
	//variable declarations : local variables to MyAddition()
	int aab_a, aab_b, aab_sum;
	
	//code
	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' : ");
	scanf("%d", &aab_b);
	
	aab_sum = aab_a + aab_b;
	
	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", aab_a, aab_b, aab_sum);
}