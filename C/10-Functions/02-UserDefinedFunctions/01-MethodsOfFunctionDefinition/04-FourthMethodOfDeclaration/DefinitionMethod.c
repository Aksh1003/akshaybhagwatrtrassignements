#include <stdio.h>

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	//function prototype / declaration / signature
	int aab_MyAddition(int, int);

	//variable declarations : local variables to main()
	int aab_a, aab_b, aab_result;

	//code
	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' : ");
	scanf("%d", &aab_b);

	aab_result = aab_MyAddition(aab_a, aab_b); //function call
	
	printf("\n\n");
	
	printf("Sum Of %d And %d = %d\n\n", aab_a, aab_b, aab_result);

	return(0);
}

// ****** USER DEFINED FUNCTION : METHOD OF DEFINITION 4 ******
// ****** VALID (int) RETURN VALUE, VALID PARAMETERS (int, int) ******

int aab_MyAddition(int aab_a, int aab_b) //function definition
{
	//variable declarations : local variables to MyAddition()
	int aab_sum;

	//code
	aab_sum = aab_a + aab_b;

	return(aab_sum);
}