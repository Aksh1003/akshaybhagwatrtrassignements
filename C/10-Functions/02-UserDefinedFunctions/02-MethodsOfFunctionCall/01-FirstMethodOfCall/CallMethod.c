#include <stdio.h>

int main(int argc, char* argv[], char* envp[])
{
	void aab_MyAddition(void);
	int aab_MySubtraction(void);
	void aab_MyMultiplication(int, int);
	int aab_MyDivision(int, int);

	int aab_result_subtraction;
	int aab_a_multiplication, aab_b_multiplication;
	int aab_a_division, aab_b_division, aab_result_division;

	aab_MyAddition();

	aab_result_subtraction = aab_MySubtraction();

	printf("\n\n");
	printf("Subtraction Yields Result = %d\n", aab_result_subtraction);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' For Multiplication : ");
	scanf("%d", &aab_a_multiplication);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' For Multiplication : ");
	scanf("%d", &aab_b_multiplication);

	aab_MyMultiplication(aab_a_multiplication, aab_b_multiplication);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' For Division : ");
	scanf("%d", &aab_a_division);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' For Division : ");
	scanf("%d", &aab_b_division);

	aab_result_division = aab_MyDivision(aab_a_division, aab_b_division);
	
	printf("\n\n");
	printf("Division Of %d and %d Gives = %d (Quotient)\n", aab_a_division, aab_b_division, aab_result_division);
	
	printf("\n\n");
	
	return(0);
}

void aab_MyAddition(void) 
{
	int aab_a, aab_b, aab_sum;

	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' For Addition : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' For Addition : ");
	scanf("%d", &aab_b);

	aab_sum = aab_a + aab_b;

	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", aab_a, aab_b, aab_sum);
}

int aab_MySubtraction(void)
{
	int aab_a, aab_b, aab_subtraction;

	printf("\n\n");
	printf("Enter Integer Value For 'aab_A' For Subtraction : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Integer Value For 'aab_B' For Subtraction : ");
	scanf("%d", &aab_b);

	aab_subtraction = aab_a - aab_b;

	return(aab_subtraction);
}

void aab_MyMultiplication(int aab_a, int aab_b)
{
	int aab_multiplication;

	aab_multiplication = aab_a * aab_b;

	printf("\n\n");
	printf("Multiplication Of %d And %d = %d\n\n", aab_a, aab_b, aab_multiplication);
}

int aab_MyDivision(int aab_a, int aab_b)
{
	int aab_division_quotient;

	if (aab_a > aab_b)
		aab_division_quotient = aab_a / aab_b;
	else
		aab_division_quotient = aab_b / aab_a;

	return(aab_division_quotient);
}
