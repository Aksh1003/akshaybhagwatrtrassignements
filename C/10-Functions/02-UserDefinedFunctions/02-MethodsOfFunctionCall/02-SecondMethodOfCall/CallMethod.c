#include <stdio.h>

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	void aab_display_information(void);
	void aab_Function_Country(void);

	aab_display_information();
	aab_Function_Country();
	return(0);
}

void aab_display_information(void) 
{
	void aab_Function_My(void);
	void aab_Function_Name(void);
	void aab_Function_Is(void);
	void aab_Function_FirstName(void);
	void aab_Function_MiddleName(void);
	void aab_Function_Surname(void);
	void aab_Function_OfAMC(void);
	
	aab_Function_My();
	aab_Function_Name();
	aab_Function_Is();
	aab_Function_FirstName();
	aab_Function_MiddleName();
	aab_Function_Surname();
	aab_Function_OfAMC();
}

void aab_Function_My(void)
{
	printf("\n\n");
	printf("My");
}
void aab_Function_Name(void)
{
	printf("\n\n");
	printf("Name");
}
void aab_Function_Is(void)
{
	printf("\n\n");
	printf("Is");
}
void aab_Function_FirstName(void)
{
	printf("\n\n");
	printf("Akshay");
}

void aab_Function_MiddleName(void)
{
	printf("\n\n");
	printf("Anil");
}

void aab_Function_Surname(void)
{
	printf("\n\n");
	printf("Bhagwat");
}

void aab_Function_OfAMC(void)
{
	printf("\n\n");
	printf("Of ASTROMEDICOMP");
}

void aab_Function_Country(void)
{
	printf("\n\n");
	printf("I live In India.");
	printf("\n\n");
}
