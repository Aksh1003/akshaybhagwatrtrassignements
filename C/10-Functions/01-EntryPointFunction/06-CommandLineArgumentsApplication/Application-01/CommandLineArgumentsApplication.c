#include <stdio.h> //'stdio.h' contains declaration of 'printf()'
#include <ctype.h> //'ctype.h' contains declaration of 'atoi()'
#include <stdlib.h> // 'stdlib.h' contains declaration of 'exit()'

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	//variable declarations
	int aab_i;
	int aab_num;
	int aab_sum = 0;

	//code
	if (aab_argc == 1)
	{
		printf("\n\n");
		printf("No Numbers Given For Addition !!! Exitting now ...\n\n");
		printf("Usage : CommandLineArgumentsApplication <first number> <second number > ...\n\n");

		exit(0);
	}

	// *** THIS PROGRAMS ADDS ALL COMMAND LINE ARGUMENTS GIVEN IN INTEGER FORM ONLY AND OUTPUTS THE SUM***
	// *** DUE TO USE OF atoi(), ALL COMMAND LINE ARGUMENTS OF TYPES OTHER THAN 'int' ARE IGNORED * **

	printf("\n\n");
	printf("Sum Of All Integer Command Line Arguments Is : \n\n");
	for (aab_i = 1 ; aab_i < aab_argc ; aab_i++) //loop starts from aab_i = 1 because, aab_i = 0 will result in 'aab_argv[aab_i]' = 'aab_argv[0]' which is the name of the program itself i.e : 'CommandLineArgumentsApplication.exe'
	{
		aab_num = atoi(aab_argv[aab_i]);
		aab_sum = aab_sum + aab_num;
	}

	printf("Sum = %d\n\n", aab_sum);

	return(0);
}