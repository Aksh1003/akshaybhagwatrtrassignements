#include <stdio.h> //'stdio.h' contains declaration of 'printf()'
#include <stdlib.h> // 'stdlib.h' contains declaration of 'exit()'

int main(int aab_argc, char* aab_argv[], char* aab_envp[])
{
	//variable declarations
	int aab_i;

	//code
	if (aab_argc != 4) // Program name + first name + middle name + surname = 4 command line arguments are required
	{
		printf("\n\n");
		printf("Invalid Usage !!! Exitting Now ... \n\n");
		printf("Usage : CommandLineArgumentsApplication <first name> <middle name> < surname > \n\n");

		exit(0);
	}

	// *** THIS PROGRAMS PRINTS YOUR FULL NAME AS ENTERED IN THE COMMAND LINE ARGUMENTS***
			
	printf("\n\n");
	printf("Your Full Name Is : ");
		
	for (aab_i = 1 ; aab_i < aab_argc ; aab_i++) //loop starts from aab_i = 1 because, aab_i = 0 will result in 'aab_argv[aab_i]' = 'aab_argv[0]' which is the name of the program itself i.e : 'CommandLineArgumentsApplication.exe'
	{
		printf("%s ", aab_argv[aab_i]);
	}

	printf("\n\n");
	
	return(0);
}
