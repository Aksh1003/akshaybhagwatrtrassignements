#include <stdio.h>

int main(void)
{
	int aab_a;
	int aab_b;
	int aab_c;
	int aab_result;

	printf("\n\n");
	printf("Enter first integer : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter second integer : ");
	scanf("%d", &aab_b);

	printf("\n\n");
	printf("Enter third integer : ");
	scanf("%d", &aab_c);

	printf("\n\n");
	printf("If answer = 0, It is 'FLASE'.\n");
	printf("If answer = 1, It is 'TRUE'.\n");

	aab_result = (aab_a <= aab_b) && (aab_b != aab_c);
	printf("Logical AND (&&) : Answer is TRUE (1) iff both conditions are TRUE. The Anser is FALSE (0), iff one or both conditions are false\n\n");
	printf("aab_a = %d is less than or eual to aab_b = %d AND aab_b = %d is not equal to aab_c = %d \t aab_answer = %d\n\n", aab_a, aab_b, aab_b, aab_c, aab_result);

	aab_result = (aab_b >= aab_a) || (aab_a == aab_c);
	printf("Logical OR (||) : Answer is FALSE (0) iff both conditions are FALSE. The Answer is TRUE (1), if any one or both conditions are TRUE\n");
	printf("aab_b = %d is greater than or equal to aab_b = %d OR aab_a = %d is equal to aab_c = %d \t aab_answer = %d\n\n", aab_b, aab_a, aab_a, aab_c, aab_result);

	aab_result = !aab_a;
	printf("aab_a = %d and using Logical NOT (!) operator on aab_a gives aab_result = %d\n\n", aab_a, aab_result);
	
	aab_result = !aab_b;
	printf("aab_b = %d and using Logical NOT (!) operator on aab_b gives aab_result = %d\n\n", aab_b, aab_result);

	aab_result = !aab_c;
	printf("aab_c = %d and using Logical NOT (!) operator on aab_a gives aab_result = %d\n\n", aab_c, aab_result);

	aab_result = (!(aab_a <= aab_b) && !(aab_b != aab_c));
	printf("Using Logical NOT (!) on (aab_a <= aab_b) and then AND-ing then afterwards gives aab_result = %d\n\n", aab_result);

	aab_result = !((aab_b >= aab_a) || (aab_a == aab_c));
	printf("Using Logical NOT (!) on enitr logical expression ((aab_b >= aab_a) || (aab_a == aab_c)) gives aab_result = %d\n\n", aab_result);


	return 0;
}