#include <stdio.h>

int main(void)
{
	int aab_a;
	int aab_b;
	int aab_x;

	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &aab_b);

	printf("\n\n");

	aab_x = aab_a;
	aab_a += aab_b; // aab_a = aab_a + aab_b
	printf("Addition Of A = %d And B = %d Gives %d.\n", aab_x, aab_b, aab_a);

	aab_x = aab_a;
	aab_a -= aab_b; // aab_a = aab_a - aab_b
	printf("Subtraction Of A = %d And B = %d Gives %d.\n", aab_x, aab_b, aab_a);

	aab_x = aab_a;
	aab_a *= aab_b; // aab_a = aab_a * aab_b
	printf("Multiplication Of A = %d And B = %d Gives %d.\n", aab_x, aab_b, aab_a);

	aab_x = aab_a;
	aab_a /= aab_b; // aab_a = aab_a / aab_b 
	printf("Division Of A = %d And B = %d Gives Quotient %d.\n", aab_x, aab_b, aab_a);

	aab_x = aab_a;
	aab_a %= aab_b; // aab_a = aab_a % aab_b
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", aab_x, aab_b, aab_a);

	printf("\n\n");

	return(0);
}
