#include <stdio.h>

int main(void)
{
	//variable declarations
	int aab_a = 5;
	int aab_b = 10;

	//code
	printf("\n\n");
	printf("aab_A = %d\n", aab_a);
	printf("aab_A = %d\n", aab_a++);
	printf("aab_A = %d\n", aab_a);
	printf("aab_A = %d\n\n", ++aab_a);

	printf("aab_B = %d\n", aab_b);
	printf("aab_B = %d\n", aab_b--);
	printf("aab_B = %d\n", aab_b);
	printf("aab_B = %d\n\n", --aab_b);

	return(0);
}
