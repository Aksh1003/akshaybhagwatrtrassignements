#include <stdio.h>

int main()
{
	int aab_a, aab_b;
	int aab_p, aab_q;
	char aab_ch_result_01, aab_ch_result_02;
	int aab_i_result_01, aab_i_result_02;

	printf("\n\n");

	aab_a = 7;
	aab_b = 5;

	aab_ch_result_01 = (aab_a > aab_b) ? 'A' : 'B';
	aab_i_result_01 = (aab_a > aab_b) ? aab_a : aab_b;
	printf("Ternary operator answer 1 ------- %c and %d\n\n", aab_ch_result_01, aab_i_result_01);

	aab_p = 30;
	aab_q = 30;
	aab_ch_result_02 = (aab_p != aab_q) ? 'P' : 'Q';
	aab_i_result_02 = (aab_p != aab_q) ? aab_p : aab_q;
	printf("Ternary operator answer 2 ------- %c and %d\n\n", aab_ch_result_02, aab_i_result_02);

	return 0;
}