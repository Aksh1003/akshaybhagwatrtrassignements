#include <stdio.h>

int main()
{
	int aab_a;
	int aab_b;
	int aab_result;

	printf("\n\n");
	printf("Enter One Integer : ");
	scanf("%d", &aab_a);

	printf("\n\n");
	printf("Enter One Integer : ");
	scanf("%d", &aab_b);

	printf("\n\n");
	printf("If answer = 0, it is 'FLASE'.\n");
	printf("If answer = 1, it is 'TRUE'.\n");

	aab_result = (aab_a < aab_b);
	printf("(aab_a < aab_b) aab_a = %d is less than aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	aab_result = (aab_a > aab_b);
	printf("(aab_a > aab_b) aab_a = %d is greater than aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	aab_result = (aab_a <= aab_b);
	printf("(aab_a <= aab_b) aab_a = %d is less than or equal to aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	aab_result = (aab_a >= aab_b);
	printf("(aab_a >= aab_b) aab_a = %d is greater than or equal to aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	aab_result = (aab_a == aab_b);
	printf("(aab_a == aab_b) aab_a = %d is equal to aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	aab_result = (aab_a != aab_b);
	printf("(aab_a != aab_b) aab_a = %d is not equal to aab_b = %d \t aab_answer = %d\n", aab_a, aab_b, aab_result);

	return 0;
}