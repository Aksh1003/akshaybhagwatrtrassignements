#include <stdio.h>

int main()
{
	void PrintBinaryNumber(unsigned int);

	unsigned int aab_a;
	unsigned int aab_num_bits;
	unsigned int aab_result;

	printf("\n\n");
	printf("Enter an integer : ");
	scanf("%u", &aab_a);

	printf("\n\n");
	printf("Enter an integer : ");
	scanf("%u", &aab_num_bits);

	printf("\n\n\n\n");
	aab_result = aab_a >> aab_num_bits;
	printf("Bitwise Right-Shift-ing of aab_a = %d by %d aab_num_bits \nGives aab_result = %d \n\n", aab_a, aab_num_bits, aab_result);

	PrintBinaryNumber(aab_a);
	PrintBinaryNumber(aab_result);

	return 0;
}

void PrintBinaryNumber(unsigned int aab_decimal)
{
	unsigned int aab_quotient, aab_remainder;
	unsigned int aab_num;
	unsigned int aab_binary_array[8];
	int aab_i;

	for (aab_i = 0; aab_i < 8; aab_i++)
		aab_binary_array[aab_i] = 0;

	printf("Binary form of decimal number %d is \t = \t", aab_decimal);
	aab_num = aab_decimal;
	aab_i = 7;

	while (0 != aab_num)
	{
		aab_quotient = aab_num / 2;
		aab_remainder = aab_num % 2;
		aab_binary_array[aab_i] = aab_remainder;
		aab_num = aab_quotient;
		aab_i--;
	}

	for (aab_i = 0; aab_i < 8; aab_i++)
		printf("%u", aab_binary_array[aab_i]);

	printf("\n\n");
}
