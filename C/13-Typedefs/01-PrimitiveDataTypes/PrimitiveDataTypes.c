#include <stdio.h>

// Global typedef
typedef int AAB_INT; // "type" int has been re"def"ined as MY_INT ... Now, "MY_INT" can be treated just like "int"

int main(void)
{
	// Function prototype
	AAB_INT AAB_Add(AAB_INT, AAB_INT);

	// Typedefs
	typedef int AAB_INT; // "type" int has been re"def"ined as AAB_INT ... Now, "AAB_INT" can be treated just like "int"
	typedef float AAB_FLOAT; // "type" float has been re"def"ined as AAB_FLOAT ...Now, "AAB_FLOAT" can be treated just like "float"
	typedef char AAB_CHARACTER; // "type" char has been re"def"ined as AAB_CHARACTER ... Now, "AAB_CHARACTER" can be treated just like "char"
	typedef double AAB_DOUBLE; // "type" double has been re"def"ined as	AAB_DOUBLE ... Now, "AAB_DOUBLE" can be treated just like "double"

	
	// ****** JUST LIKE IN Win32SDK !!! ******
	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINSTANCE;

	// variable declarations
	AAB_INT aab_a = 10, aab_i;
	AAB_INT aab_iArray[] = { 9, 18, 27, 36, 45, 54, 63, 72, 81, 90 };
	AAB_FLOAT aab_f = 30.9f;
	const AAB_FLOAT aab_f_pi = 3.14f;
	AAB_CHARACTER aab_ch = '*';
	AAB_CHARACTER aab_chArray_01[] = "Hello";
	AAB_CHARACTER aab_chArray_02[][10] = { "Akshay", "Anil", "Bhagwat" };
	AAB_DOUBLE aab_d = 8.041997;
	
	// ****** JUST RANDOM NUMBERS - THEY HAVE NOTHING TO DO WITH ANY WINDOW'S
	//HANDLE OR INSTANCE HANDLE !!!This is just for understanding******
	UINT uint = 3456;
	HANDLE handle = 987;
	HWND hwnd = 9876;
	HINSTANCE hInstance = 14466;
	
	// code
	printf("\n\n");
	printf("Type AAB_INT variable a = %d\n", aab_a);

	printf("\n\n");
	for (aab_i = 0; aab_i < (sizeof(aab_iArray) / sizeof(int)); aab_i++)
	{
		printf("Type AAB_INT array variable aab_iArray[%d] = %d\n", aab_i, aab_iArray[aab_i]);
	}

	printf("\n\n");
	printf("\n\n");
	printf("Type AAB_FLOAT variable aab_f = %f\n", aab_f);
	printf("Type AAB_FLOAT constanct aab_f_pi = %f\n", aab_f_pi);

	printf("\n\n");
	printf("Type AAB_DOUBLE variable aab_d = %lf\n", aab_d);

	printf("\n\n");
	printf("Type AAB_CHARACTER variable aab_ch = %c\n", aab_ch);

	printf("\n\n");
	printf("Type AAB_CHARACTER array variable aab_chArray_01 = %s\n", aab_chArray_01);

	printf("\n\n");
	for (aab_i = 0; aab_i < (sizeof(aab_chArray_02) / sizeof(aab_chArray_02[0])); aab_i++)
	{
		printf("%s\t", aab_chArray_02[aab_i]);
	}

	printf("\n\n");
	printf("\n\n");
	printf("Type UINT variable uint = %u\n\n", uint);
	printf("Type HANDLE variable handle = %u\n\n", handle);
	printf("Type HWND variable hwnd = %u\n\n", hwnd);
	printf("Type HINSTANCE variable hInstance = %u\n\n", hInstance);
	printf("\n\n");

	AAB_INT aab_x = 90;
	AAB_INT aab_y = 30;
	AAB_INT aab_ret;
	
	aab_ret = AAB_Add(aab_x, aab_y);

	printf("aab_ret = %d\n\n", aab_ret);

	return(0);
}

AAB_INT AAB_Add(AAB_INT aab_a, AAB_INT aab_b)
{
	// code
	AAB_INT aab_c;
	aab_c = aab_a + aab_b;

	return(aab_c);
}
