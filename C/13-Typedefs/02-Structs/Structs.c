#include <stdio.h>

#define AAB_MAX_NAME_LENGTH 100

struct AAB_Employee
{
	char aab_name[AAB_MAX_NAME_LENGTH];
	unsigned int aab_age;
	char aab_gender;
	double aab_salary;
};

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int main(void)
{
	// Typedefs
	typedef struct AAB_Employee AAB_EMPLOYEE_TYPE;
	typedef struct AAB_MyData AAB_DATA_TYPE;

	// variable declarations
	struct AAB_Employee aab_emp = { "Funny", 25, 'M', 10000.00 };
	AAB_EMPLOYEE_TYPE aab_emp_typedef = { "Bunny", 23, 'F', 20400.00 };
	struct AAB_MyData aab_md = { 30, 11.45f, 26.122017, 'X' };
	AAB_DATA_TYPE aab_md_typedef;

	// code
	aab_md_typedef.aab_i = 9;
	aab_md_typedef.aab_f = 1.5f;
	aab_md_typedef.aab_d = 8.041997;
	aab_md_typedef.aab_c = 'P';

	printf("\n\n");
	printf("struct AAB_Employee : \n\n");
	printf("aab_emp.aab_name = %s\n", aab_emp.aab_name);
	printf("aab_emp.aab_age = %d\n", aab_emp.aab_age);
	printf("aab_emp.aab_gender = %c\n", aab_emp.aab_gender);
	printf("aab_emp.aab_salary = %lf\n", aab_emp.aab_salary);

	printf("\n\n");
	printf("AAB_EMPLOYEE_TYPE : \n\n");
	printf("aab_emp_typedef.aab_name = %s\n", aab_emp_typedef.aab_name);
	printf("aab_emp_typedef.aab_age = %d\n", aab_emp_typedef.aab_age);
	printf("aab_emp_typedef.aab_gender = %c\n", aab_emp_typedef.aab_gender);
	printf("aab_emp_typedef.aab_salary = %lf\n", aab_emp_typedef.aab_salary);

	printf("\n\n");
	printf("struct AAB_MyData : \n\n");
	printf("aab_md.aab_i = %d\n", aab_md.aab_i);
	printf("aab_md.aab_f = %f\n", aab_md.aab_f);
	printf("aab_md.aab_d = %lf\n", aab_md.aab_d);
	printf("aab_md.aab_c = %c\n", aab_md.aab_c);

	printf("\n\n");
	printf("AAB_DATA_TYPE : \n\n");
	printf("aab_md_typedef.aab_i = %d\n", aab_md_typedef.aab_i);
	printf("aab_md_typedef.aab_f = %f\n", aab_md_typedef.aab_f);
	printf("aab_md_typedef.aab_d = %lf\n", aab_md_typedef.aab_d);
	printf("aab_md_typedef.aab_c = %c\n", aab_md_typedef.aab_c);

	printf("\n\n");

	return(0);
}
