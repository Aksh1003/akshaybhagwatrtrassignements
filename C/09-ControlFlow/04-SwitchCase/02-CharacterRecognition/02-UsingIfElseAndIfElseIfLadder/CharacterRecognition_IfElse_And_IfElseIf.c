#include <stdio.h>
#include <conio.h>

// ASCII values for 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDDING 90

// ASCII values for 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDDING 122

// ASCII values for '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDDING 57

int main()
{
	char aab_ch;
	int aab_ch_value;

	printf("\n\n");

	printf("Enter Character :");
	aab_ch = getch();

	printf("\n\n");

	if (('A' == aab_ch) || ('a' == aab_ch) || ('E' == aab_ch) || ('e' == aab_ch) || ('I' == aab_ch) || ('i' == aab_ch) || ('O' == aab_ch) || ('o' == aab_ch) || ('U' == aab_ch) || ('u' == aab_ch))
		printf("Character \'%c\' Entered By You, Is A VOWEL Character From The English Alphabet!\n\n", aab_ch);
	else
	{
		aab_ch_value = (int)aab_ch;

		if (((aab_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING) && (aab_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDDING) || (aab_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING) && (aab_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDDING)))
			printf("Character \'%c\' Entered By You, Is A CONSONANT Character From The English Alphabet!\n\n", aab_ch);

		else if ((aab_ch_value >= CHAR_DIGIT_BEGINNING) && (aab_ch_value <= CHAR_DIGIT_ENDDING))
			printf("Character \'%c\' Entered By You, Is A DIGIT Character From The English Alphabet!\n\n", aab_ch);

		else
			printf("Character \'%c\' Entered By You, Is A SPECIAL Character From The English Alphabet!\n\n", aab_ch);
	}

	return 0;
}

