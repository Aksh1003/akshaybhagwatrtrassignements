#include <stdio.h>
#include <conio.h>

// ASCII Values For 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

// ASCII Values For 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

// ASCII Values For '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main()
{
	char aab_ch;
	int aab_ch_value;

	printf("\n\n");

	printf("Enter Character : ");
	aab_ch = getch();

	printf("\n\n");

	switch(aab_ch)
	{
	case 'A' :
	case 'a' :
	
	case 'E':
	case 'e':

	case 'I':
	case 'i':

	case 'O':
	case 'o':

	case 'U':
	case 'u':
		printf("Character \'%c\' Entered By You, Is A VOWEL CHARACTER From The English Alphabet !!!\n\n", aab_ch);
		break;

	default :
		aab_ch_value = (int)aab_ch;

		if ((aab_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING && aab_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) || (aab_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING && aab_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDING))
		{
			printf("Character \'%c\' Entered By You, Is A CONSONANT CHARACTER From The English Alphabet !!!\n\n", aab_ch);
		}
		else if (aab_ch_value >= CHAR_DIGIT_BEGINNING && aab_ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("Character \'%c\' Entered By You, Is A DIGIT CHARACTER !!!\n\n", aab_ch);
		}
		else
		{
			printf("Character \'%c\' Entered By You, Is A SPECIAL CHARACTER !!!\n\n", aab_ch);
		}
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	return 0;
}