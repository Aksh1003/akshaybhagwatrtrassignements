#include <stdio.h>

int main()
{
	int aab_num_month;

	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) :");
	scanf("%d", &aab_num_month);

	printf("\n\n");

	switch (aab_num_month)
	{
	case 1:
		printf("Month Number %d Is January !!!\n", aab_num_month);
		break;

	case 2:
		printf("Month Number %d Is February !!!\n", aab_num_month);
		break;

	case 3:
		printf("Month Number %d Is March !!!\n", aab_num_month);
		break;

	case 4:
		printf("Month Number %d Is April !!!\n", aab_num_month);
		break;

	case 5:
		printf("Month Number %d Is May !!!\n", aab_num_month);
		break;
	
	case 6:
		printf("Month Number %d Is Jun !!!\n", aab_num_month);
		break;

	case 7:
		printf("Month Number %d Is July !!!\n", aab_num_month);
		break;

	case 8:
		printf("Month Number %d Is August !!!\n", aab_num_month);
		break;
	
	case 9:
		printf("Month Number %d Is September !!!\n", aab_num_month);
		break;

	case 10:
		printf("Month Number %d Is October !!!\n", aab_num_month);
		break;
	
	case 11:
		printf("Month Number %d Is November !!!\n", aab_num_month);
		break;
	
	case 12:
		printf("Month Number %d Is December !!!\n", aab_num_month);
		break;

	default:
		printf("Invalid Month Number %d Entered !!! Please Try Again....\n\n", aab_num_month);
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	return 0;
}