#include <stdio.h>

int main()
{
	int aab_num_month;

	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) :");
	scanf("%d", &aab_num_month);

	printf("\n\n");

	if(1 == aab_num_month)
		printf("Month Number %d Is January !!!\n", aab_num_month);
	
	else if (2 == aab_num_month)
		printf("Month Number %d Is February !!!\n", aab_num_month);

	else if (3 == aab_num_month)
		printf("Month Number %d Is March !!!\n", aab_num_month);
	
	else if (4 == aab_num_month)
		printf("Month Number %d Is April !!!\n", aab_num_month);

	else if (5 == aab_num_month)
		printf("Month Number %d Is May !!!\n", aab_num_month);
	
	else if (6 == aab_num_month)
		printf("Month Number %d Is Jun !!!\n", aab_num_month);

	else if (7 == aab_num_month)
		printf("Month Number %d Is July !!!\n", aab_num_month);
	
	else if (8 == aab_num_month)
		printf("Month Number %d Is August !!!\n", aab_num_month);
	
	else if (9 == aab_num_month)
		printf("Month Number %d Is September !!!\n", aab_num_month);
	
	else if (10 == aab_num_month)
		printf("Month Number %d Is October !!!\n", aab_num_month);
	
	else if (11 == aab_num_month)
		printf("Month Number %d Is November !!!\n", aab_num_month);
	
	else if (12 == aab_num_month)
		printf("Month Number %d Is December !!!\n", aab_num_month);
	
	else
		printf("Invalid Month Number %d Entered !!! Please Try Again....\n\n", aab_num_month);

	printf("If-Else-If Ladder Complete !!!\n");

	return 0;
}