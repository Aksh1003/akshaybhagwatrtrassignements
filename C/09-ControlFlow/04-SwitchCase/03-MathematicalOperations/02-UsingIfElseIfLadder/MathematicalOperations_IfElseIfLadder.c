#include <stdio.h>
#include <conio.h>

int main()
{
	int aab_a, aab_b;
	int aab_result;

	char aab_option, aab_option_division = '\0';

	printf("\n\n");

	printf("Enter Value For aab_a :");
	scanf("%d", &aab_a);

	printf("Enter Value For aab_b :");
	scanf("%d", &aab_b);

	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");

	printf("Enter Option : ");
	aab_option = getch();
	printf("\n\n");

	if (aab_option == 'A' || aab_option == 'a')
	{
		aab_result = aab_a + aab_b;
		printf("Addition of aab_a = %d and aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
	}
	else if (aab_option == 'S' || aab_option == 's')
	{
		if (aab_a >= aab_b)
		{
			aab_result = aab_a - aab_b;
			printf("Substraction of aab_b = %d and aab_a = %d gives result %d\n\n", aab_a, aab_b, aab_result);
		}
		else
		{
			aab_result = aab_b - aab_a;
			printf("Substraction of aab_a = %d and aab_b = %d gives result %d\n\n", aab_b, aab_a, aab_result);
		}
	}
	else if (aab_option == 'M' || aab_option == 'm')
	{
		aab_result = aab_a * aab_b;
		printf("Multiplication of aab_a = %d and aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
	}
	else if (aab_option == 'D' || aab_option == 'd')
	{
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter Option : ");
		aab_option_division = getch();

		printf("\n\n");

		if ((aab_option_division == 'Q') || (aab_option_division == 'q') || (aab_option_division == '/'))
		{
			if (aab_a >= aab_b)
			{
				aab_result = aab_a / aab_b;
				printf("Division of aab_a = %d by aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
			}
			else
			{
				aab_result = aab_b / aab_a;
				printf("Multiplication of aab_b = %d by aab_a = %d gives result %d\n\n", aab_b, aab_a, aab_result);
			}
		}
		else if (aab_option_division == 'R' || aab_option_division == 'r' || aab_option_division == '%')
		{
			if (aab_a >= aab_b)
			{
				aab_result = aab_a % aab_b;
				printf("Division of aab_a = %d by aab_b = %d gives remainder %d\n\n", aab_a, aab_b, aab_result);
			}
			else
			{
				aab_result = aab_b % aab_a;
				printf("Division of aab_b = %d by aab_a = %d gives remainder %d\n\n", aab_b, aab_a, aab_result);
			}
		}
		else
		{
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", aab_option_division);
		}
	}
	else
	{
		printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", aab_option_division);
	}

	return 0;
}