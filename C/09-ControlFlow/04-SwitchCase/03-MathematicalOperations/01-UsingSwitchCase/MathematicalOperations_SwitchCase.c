#include <stdio.h>
#include <conio.h>

int main()
{
	int aab_a, aab_b;
	int aab_result;

	char aab_option, aab_option_division = '\0';

	printf("\n\n");

	printf("Enter Value For aab_a :");
	scanf("%d", &aab_a);

	printf("Enter Value For aab_b :");
	scanf("%d", &aab_b);

	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");
	
	printf("Enter Option : ");
	aab_option = getch();
	printf("\n\n");

	switch (aab_option)
	{
	case 'A' :
	case 'a' :
		aab_result = aab_a + aab_b;
		printf("Addition of aab_a = %d and aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
		break;

	case 'S' :
	case 's' :
		if (aab_a >= aab_b)
		{
			aab_result = aab_b - aab_a;
			printf("Substraction of aab_b = %d and aab_a = %d gives result %d\n\n", aab_b, aab_a, aab_result);
		}
		else
		{
			aab_result = aab_a - aab_b;
			printf("Substraction of aab_a = %d and aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
		}
		break;

	case 'M' :
	case 'm' :
		aab_result = aab_a * aab_b;
		printf("Multiplication of aab_a = %d and aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
		break;

	case 'D' :
	case 'd' :
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");
	
		printf("Enter Option : ");
		aab_option_division = getch();
		
		printf("\n\n");

		switch (aab_option_division)
		{
		case 'Q' :
		case 'q' :
		case '/' :
			if (aab_a >= aab_b)
			{
				aab_result = aab_a / aab_b;
				printf("Division of aab_a = %d by aab_b = %d gives result %d\n\n", aab_a, aab_b, aab_result);
			}
			else
			{
				aab_result = aab_b / aab_a;
				printf("Multiplication of aab_b = %d by aab_a = %d gives result %d\n\n", aab_b, aab_a, aab_result);
			}
			break;

		case 'R' :
		case 'r' :
		case '%' :
			if (aab_a >= aab_b)
			{
				aab_result = aab_a % aab_b;
				printf("Division of aab_a = %d by aab_b = %d gives remainder %d\n\n", aab_a, aab_b, aab_result);
			}
			else
			{
				aab_result = aab_b % aab_a;
				printf("Division of aab_b = %d by aab_a = %d gives remainder %d\n\n", aab_b, aab_a, aab_result);
			}
			break;

		default :
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", aab_option_division);
			break;
		}
		break;

	default :
		printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", aab_option_division);
		break;
	}

	printf("Switch Case Block Complete\n\n");

	return 0;
}