#include <stdio.h>

int main()
{
	int aab_num;

	printf("\n\nEnter Value For aab_num : ");
	scanf("%d", &aab_num);

	if (aab_num < 0)
	{
		printf("aab_num = %d Is Less Than 0 (NEGATIVE).\n\n", aab_num);
	}

	if ((aab_num >= 0) && (aab_num <= 100))
	{
		printf("aab_num = %d Is Between 0 and 100.\n\n", aab_num);
	}

	if ((aab_num > 100) && (aab_num <= 200))
	{
		printf("aab_num = %d Is Between 100 and 200.\n\n", aab_num);
	}

	if ((aab_num > 200) && (aab_num <= 300))
	{
		printf("aab_num = %d Is Between 200 and 300.\n\n", aab_num);
	}

	if ((aab_num > 300) && (aab_num <= 400))
	{
		printf("aab_num = %d Is Between 300 and 400.\n\n", aab_num);
	}

	if ((aab_num > 400) && (aab_num <= 500))
	{
		printf("aab_num = %d Is Between 400 and 500.\n\n", aab_num);
	}

	if (aab_num > 500)
	{
		printf("aab_num = %d Is Greater than 500.\n\n", aab_num);
	}

	return 0;
}