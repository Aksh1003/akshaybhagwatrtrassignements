#include <stdio.h>

int main()
{
	int aab_a;

	printf("\n\n");

	aab_a = 5;
	if (aab_a)
	{
		printf("if-block 1 : 'aab_a' exists and has value : %d!\n\n", aab_a);
	}

	aab_a = -5;
	if (aab_a)
	{
		printf("if-block 2 : 'aab_a' exists and has value : %d!\n\n", aab_a);
	}

	aab_a = 0;
	if (aab_a)
	{
		printf("if-block 3 : 'aab_a' exists and has value : %d!\n\n", aab_a);
	}

	printf("All Three if-statement Are Done!\n\n");


















	return 0;
}