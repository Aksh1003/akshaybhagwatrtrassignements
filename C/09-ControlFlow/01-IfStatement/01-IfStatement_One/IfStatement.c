#include <stdio.h>

int main()
{
	int aab_a, aab_b, aab_p;

	aab_a = 9;
	aab_b = 30;
	aab_p = 30;

	printf("\n\n");

	if (aab_a < aab_b)
	{
		printf("aab_a is less than aab_b\n\n");
	}

	if (aab_b != aab_p)
	{
		printf("aab_b is not equal to aab_p\n\n");
	}

	printf("both comparisions have been done\n\n");

	return 0;
}