#include <stdio.h>
#include <conio.h>

int main(void)
{
	int aab_i;
	char aab_ch;

	printf("\n\n");
	printf("Printing Even Numbers From 1 to 100 For Every User Input. Exitting the Loop When User Enters Character 'Q' or 'q' : \n\n");
	printf("Enter Character 'Q' or 'q' To Exit Loop : \n\n");

	for (aab_i = 1 ; aab_i <= 100 ; aab_i++)
	{
		printf("\t%d\n", aab_i);
		
		aab_ch = getch();

		if (aab_ch == 'Q' || aab_ch == 'q')
		{
			break;
		}
	}

	printf("\n\n");
	printf("EXITTING LOOP...");
	printf("\n\n");

	return(0);
}