#include <stdio.h>
#include <conio.h>

int main(void)
{
	int aab_i, aab_j;

	printf("\n\n");

	for (aab_i = 1 ; aab_i <= 20 ; aab_i++)
	{
		for (aab_j = 1 ; aab_j <= 20 ; aab_j++)
		{
			if (aab_j > aab_i)
			{
				break;
			}
			else
			{
				printf("* ");
			}
		}
		printf("\n");
	}
	printf("\n\n");

	return(0);
}