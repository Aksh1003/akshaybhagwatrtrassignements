#include <stdio.h>

int main()
{
	int aab_i;

	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	aab_i = 10;

	while (aab_i >= 1)
	{
		printf("\t%d\n", aab_i);
		aab_i--;
	}

	printf("\n\n");

	return 0;
}