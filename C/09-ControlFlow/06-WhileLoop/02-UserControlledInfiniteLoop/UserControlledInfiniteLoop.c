#include <stdio.h>

int main()
{
	char aab_option, aab_ch = '\0';

	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	printf("Enter 'Y' oy 'y' To Initiate User Controlled Infinite Loop : ");
	printf("\n\n");

	aab_option = getch();
	
	if (aab_option == 'Y' || aab_option == 'y')
	{
		while (1) 
		{
			printf("In Loop...\n");
			aab_ch = getch();
			if (aab_ch == 'Q' || aab_ch == 'q')
				break; 
		}
		
		printf("\n\n");
		printf("EXITTING USER CONTROLLED INFINITE LOOP...");
		printf("\n\n");
	}
	else
		printf("You Must Press 'Y' or 'y' To Initiate The User Controlled Infinite Loop....Please Try Again...\n\n");
	
	return 0;
}