#include <stdio.h>

int main()
{
	int aab_i, aab_j, aab_k;

	printf("\n\n");

	aab_i = 1;
	
	while (aab_i <= 10)
	{
		printf("aab_i = %d\n", aab_i);
		printf("--------\n\n");
		aab_j = 1;
		
		while (aab_j <= 5)
		{
			printf("\taab_j = %d\n", aab_j);
			printf("\t--------\n\n");
			aab_k = 1;
			
			while (aab_k <= 3)
			{
				printf("\t\taab_k = %d\n", aab_k);
				aab_k++;
			}

			printf("\n\n");
			aab_j++;
		}

		printf("\n\n");
		aab_i++;
	}

	return 0;
}