#include <stdio.h>

int main(void)
{
	int aab_i, aab_j;

	printf("\n\n");
	printf("Outer Loop Prints Odd Numbers Between 1 and 10. \n\n");
	printf("Inner Loop Prints Even Numbers Between 1 and 10 For Every Odd Number Printed By Outer Loop. \n\n");

	for (aab_i = 1 ; aab_i <= 10 ; aab_i++)
	{
		if (aab_i % 2 != 0)
		{
			printf("aab_i = %d\n", aab_i);
			printf("---------\n");
			for (aab_j = 1 ; aab_j <= 10 ; aab_j++)
			{
				if (aab_j % 2 == 0)
				{
					printf("\taab_j = %d\n", aab_j);
				}
				else
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else 
		{
			continue;
		}
	}

	printf("\n\n");
	
	return(0);
}