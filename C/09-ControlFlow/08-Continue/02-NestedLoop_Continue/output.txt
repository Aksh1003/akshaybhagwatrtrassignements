

Outer Loop Prints Odd Numbers Between 1 and 10. 

Inner Loop Prints Even Numbers Between 1 and 10 For Every Odd Number Printed By Outer Loop. 

aab_i = 1
---------
	aab_j = 2
	aab_j = 4
	aab_j = 6
	aab_j = 8
	aab_j = 10


aab_i = 3
---------
	aab_j = 2
	aab_j = 4
	aab_j = 6
	aab_j = 8
	aab_j = 10


aab_i = 5
---------
	aab_j = 2
	aab_j = 4
	aab_j = 6
	aab_j = 8
	aab_j = 10


aab_i = 7
---------
	aab_j = 2
	aab_j = 4
	aab_j = 6
	aab_j = 8
	aab_j = 10


aab_i = 9
---------
	aab_j = 2
	aab_j = 4
	aab_j = 6
	aab_j = 8
	aab_j = 10




