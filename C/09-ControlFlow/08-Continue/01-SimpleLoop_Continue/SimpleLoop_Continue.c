#include <stdio.h>

int main(void)
{
	int aab_i;

	printf("\n\n");
	printf("Printing Even Numbers From 0 to 100: \n\n");
	
	for (aab_i = 0; aab_i <= 100; aab_i++)
	{
			if (aab_i % 2 != 0)
			{
				continue;
			}
			else
			{
				printf("\t%d\n", aab_i);
			}
	}

	printf("\n\n");

	return(0);
}