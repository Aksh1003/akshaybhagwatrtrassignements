#include <stdio.h>

int main(void)
{
	int aab_i, aab_j, aab_k;

	printf("\n\n");

	aab_i = 1;
	
	do
	{
		printf("aab_i = %d\n", aab_i);
		printf("--------\n\n");
		aab_j = 1;
		
		do
		{
			printf("\taab_j = %d\n", aab_j);
			printf("\t--------\n\n");
			aab_k = 1;
			
			do
			{
				printf("\t\taab_k = %d\n", aab_k);
				aab_k++;
			} while (aab_k <= 3);
			
			printf("\n\n");
			aab_j++;
		} while (aab_j <= 5);
		printf("\n\n");
		aab_i++;
	} while (aab_i <= 10);

	return(0);
}