#include <stdio.h>

int main(void)
{
	int aab_i, aab_j;

	printf("\n\n");

	aab_i = 1;
	
	do
	{
		printf("aab_i = %d\n", aab_i);
		printf("--------\n\n");

		aab_j = 1;
		
		do
		{
			printf("\taab_j = %d\n", aab_j);
			aab_j++;
		} while (aab_j <= 5);
		aab_i++;
		printf("\n\n");
	} while (aab_i <= 10);
	
	return(0);
}