#include <stdio.h>

int main(void)
{
	int aab_i, aab_j, aab_c;

	printf("\n\n");
	
	aab_i = 0;
	
	do
	{
		aab_j = 0;
		
		do
		{
			aab_c = ((aab_i & 0x8) == 0) ^ ((aab_j & 0x8) == 0);
			if (aab_c == 0)
				printf(" ");
			if (aab_c == 1)
				printf("* ");
			aab_j++;
		} while (aab_j < 64);
		printf("\n\n");
		aab_i++;
	} while (aab_i < 64);

	return(0);
}