#include <stdio.h>

int main(void)
{
	int aab_i_num, aab_num, aab_i;

	printf("\n\n");
	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &aab_i_num);
	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", aab_i_num);
	scanf("%d", &aab_num);
	printf("Printing Digits %d to %d : \n\n", aab_i_num, (aab_i_num + aab_num));
	aab_i = aab_i_num;
	do
	{
		printf("\t%d\n", aab_i);
		aab_i++;
	} while (aab_i <= (aab_i_num + aab_num));

	printf("\n\n");
	
	return(0);
}