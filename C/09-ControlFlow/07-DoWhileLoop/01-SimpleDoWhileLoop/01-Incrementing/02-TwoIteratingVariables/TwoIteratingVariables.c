#include <stdio.h>

int main(void)
{
	int aab_i, aab_j;

	printf("\n\n");
	printf("Printing Digits 1 to 10 and 10 to 100: \n\n");
	
	aab_i = 1;
	aab_j = 10;
	
	do
	{
		printf("\t %d \t %d\n", aab_i, aab_j);
		aab_i++;
		aab_j = aab_j + 10;
	} while (aab_i <= 10, aab_j <= 100);

	printf("\n\n");
	
	return(0);
}