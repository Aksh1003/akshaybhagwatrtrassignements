#include <stdio.h>

int main()
{
	int aab_i;

	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	aab_i = 1;
	
	do
	{
		printf("\t%d\n", aab_i);
		aab_i++;
	} while (aab_i <= 10);

	printf("\n\n");
	
	return 0;
}