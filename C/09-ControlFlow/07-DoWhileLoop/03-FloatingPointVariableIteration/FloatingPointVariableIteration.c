#include <stdio.h>

int main(void)
{
	float aab_f;
	float aab_f_num = 1.7f; 

	printf("\n\n");
	printf("Printing Numbers %f to %f : \n\n", aab_f_num, (aab_f_num * 10.0f));
	
	aab_f = aab_f_num;

	do
	{
		printf("\t%f\n", aab_f);
		aab_f = aab_f + aab_f_num;
	} while (aab_f <= (aab_f_num * 10.0f));

	printf("\n\n");

	return(0);
}