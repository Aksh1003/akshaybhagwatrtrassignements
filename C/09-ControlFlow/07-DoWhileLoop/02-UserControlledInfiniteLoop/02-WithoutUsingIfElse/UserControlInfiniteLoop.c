#include <stdio.h>

int main(void)
{
	char aab_option, aab_ch = '\0';

	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	
	do
	{
		do
		{
			printf("\n");
			printf("In Loop...\n");

			aab_ch = getch();
		} while (aab_ch != 'Q' && aab_ch != 'q');

		printf("\n\n");
		printf("EXITTING USER CONTROLLED INFINITE LOOP...");
		printf("\n\n");
		printf("DO YOU WANT TO BEGIN USER CONTROLLED INFINITE LOOP AGAIN?...(Y/y - Yes, Any Other Key - No) : ");
		aab_option = getch();
	} while (aab_option == 'Y' || aab_option == 'y');
	
	return(0);
}