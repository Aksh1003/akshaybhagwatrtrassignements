#include <stdio.h>

int main()
{
	char aab_option, aab_ch = '\0';

	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	printf("Enter 'Y' oy 'y' To Initiate User Controlled Infinite Loop : ");
	printf("\n\n");

	aab_option = getch();
	if (aab_option == 'y' || aab_option == 'Y')
	{
		for (;;)
		{
			printf("In Loop...\n");
			aab_ch = getch();
			
			if (aab_ch == 'Q' || aab_ch == 'q')
				break;
		}
	}

	printf("\n\n");
	printf("EXITTING USER CONTROLLED INFINITE LOOP...");
	printf("\n\n");

	return 0;
}