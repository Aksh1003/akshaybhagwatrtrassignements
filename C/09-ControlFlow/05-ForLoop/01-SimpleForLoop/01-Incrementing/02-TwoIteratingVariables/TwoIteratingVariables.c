#include <stdio.h>

int main()
{
	int aab_i, aab_j;

	printf("\n\n");
	printf("Printing Digits 1 to 10 and 10 to 100 : \n\n");
	for (aab_i = 1, aab_j = 10 ; aab_i <= 10, aab_j <= 100 ; aab_i++, aab_j = aab_j + 10)
	{
		printf("\t %d \t %d\n", aab_i, aab_j);
	}

	printf("\n\n");
	return(0);
}