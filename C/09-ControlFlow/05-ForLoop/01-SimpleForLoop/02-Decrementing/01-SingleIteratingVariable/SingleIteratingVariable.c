#include <stdio.h>

int main()
{
	int aab_i;

	printf("\n\n");
	printf("Printing Digits 10 to 1 : \n\n");
	for (aab_i = 10; aab_i >= 1; aab_i--)
	{
		printf("\t%d\n", aab_i);
	}

	printf("\n\n");

	return 0;
}