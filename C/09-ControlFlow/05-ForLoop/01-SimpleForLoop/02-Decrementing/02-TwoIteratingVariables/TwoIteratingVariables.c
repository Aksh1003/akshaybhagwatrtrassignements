#include <stdio.h>

int main()
{
	int aab_i, aab_j;

	printf("\n\n");
	printf("Printing Digits 10 to 1 and 100 to 10 : \n\n");
	for (aab_i = 10, aab_j = 100 ; aab_i >= 1, aab_j >= 10 ; aab_i--, aab_j -= 10)
	{
		printf("\t %d \t %d\n", aab_i, aab_j);
	}

	printf("\n\n");

	return 0;
}