#include <stdio.h>

int main()
{
	int aab_i, aab_j, aab_k;

	printf("\n\n");
	for (aab_i = 1 ; aab_i <= 10 ; aab_i++)
	{
		printf("aab_i = %d\n", aab_i);
		printf("--------\n\n");
		for (aab_j = 1 ; aab_j <= 5 ; aab_j++)
		{
			printf("\taab_j = %d\n", aab_j);
			printf("\t--------\n\n");
			for (aab_k = 1 ; aab_k <= 3 ; aab_k++)
			{
				printf("\t\taab_k = %d\n", aab_k);
			}
			printf("\n\n");
		}
		printf("\n\n");
	}
	return(0);
}