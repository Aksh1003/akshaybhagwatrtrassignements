#include <stdio.h>

int main()
{
	int aab_a, aab_b, aab_p;

	aab_a = 9;
	aab_b = 30;
	aab_p = 30;

	printf("\n\n");

	if (aab_a < aab_b)
	{
		printf("Entering First if-block...\n\n");
		printf("aab_a is less than aab_b\n\n");
	}
	else
	{
		printf("Entering First else-block...\n\n");
		printf("aab_a is not less than aab_b\n\n");
	}

	printf("First if-else Pair Done!\n\n");

	if (aab_b != aab_p)
	{
		printf("Entering Second if-block...\n\n");
		printf("aab_b is not equal to aab_p\n\n");
	}
	else
	{
		printf("Entering Second else-block...\n\n");
		printf("aab_a is equal to aab_b\n\n");
	}

	printf("Second if-else Pair Done!\n\n");

	return 0;
}