#include <stdio.h>

int main()
{
	int aab_age;

	printf("\n\n");
	printf("enter aab_age : ");
	scanf("%d", &aab_age);

	if (aab_age >= 18)
	{
		printf("Enterring if-block...\n\n");
		printf("You Are Eligible For Voting!\n\n\n");
	}
	else
	{
		printf("Enterring else-block...\n\n");
		printf("You Are NOT Eligible For Voting!\n\n\n");
	}

	printf("Bye!!!\n\n\n");
	return 0;
}