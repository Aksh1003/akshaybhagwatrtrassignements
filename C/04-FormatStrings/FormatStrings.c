#include <stdio.h>
int main(void)
{
	//code
    printf("\n\n");
    printf("**************************************************************************************");
    printf("\n\n");

    printf("Hello World !!!\n\n");

	int aab_a = 15;
	printf("Integer Decimal Value Of 'aab_a' = %d\n", aab_a);
	printf("Integer Octal Value Of 'aab_a' = %o\n", aab_a);
	printf("Integer Hexadecimal Value Of 'aab_a' (Hexadecimal Letters In Lower Case) = %x\n", aab_a);
	printf("Integer Hexadecimal Value Of 'aab_a' (Hexadecimal Letters In Uppar Case) = %X\n\n", aab_a);

	char aab_ch = 'A';
	printf("Character aab_ch = %c\n", aab_ch);
	char aab_str[] = "Akshay Bhagwat";
	printf("String aab_str = %s\n\n", aab_str);

	long aab_num = 30121995L;
	printf("Long Integer = %ld\n\n", aab_num);

	unsigned int aab_b = 8;
	printf("Unsigned Integer 'aab_b' = %u\n\n", aab_b);

	float aab_f_num = 3012.1995f;
	printf("Floating Point Number With Just %%f 'aab_f_num' = %f\n", aab_f_num);
	printf("Floating Point Number With %%4.2f 'aab_f_num' = %4.2f\n", aab_f_num);
	printf("Floating Point Number With %%6.5f 'aab_f_num' = %6.5f\n\n", aab_f_num);

	double aab_d_pi = 3.14159265358979323846;
	printf("Double Precision Floating Point Number Without Exponential = %g\n", aab_d_pi);
	printf("Double Precision Floating Point Number With Exponential (Lower Case) = %e\n", aab_d_pi);
	printf("Double Precision Floating Point Number With Exponential (Upper Case) = %E\n\n", aab_d_pi);
	printf("Double Hexadecimal Value Of 'aab_d_pi' (Hexadecimal Letters In Lower Case) = %a\n", aab_d_pi);
	printf("Double Hexadecimal Value Of 'aab_d_pi' (Hexadecimal Letters In Upper Case) = %A\n\n", aab_d_pi);
    
    printf("**************************************************************************************\n");
    printf("\n\n");
	return(0);
}
