#include <stdio.h>

int main(void)
{
    int aab_i = 4;
    float aab_f = 2.9f;
    double aab_d = 7.041997;
    char aab_c = 'A';
    
	printf("\n\n");
    
    printf("aab_i = %d\n", aab_i);
    printf("aab_f = %f\n", aab_f);
    printf("aab_d = %lf\n", aab_d);
    printf("aab_c = %c\n", aab_c);

    printf("\n\n");

    aab_i = 43;
    aab_f = 6.54f;
    aab_d = 26.1294;
    aab_c = 'P';
    
    printf("aab_i = %d\n", aab_i);
    printf("aab_f = %f\n", aab_f);
    printf("aab_d = %lf\n", aab_d);
    printf("aab_c = %c\n", aab_c);

    printf("\n\n");

    return(0);
}



