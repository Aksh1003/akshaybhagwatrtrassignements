#include<stdio.h>
#include<stdlib.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int* aab_ptr_iArray = NULL; 
	unsigned int aab_intArrayLength = 0;
	int aab_i;

	printf("\n\nEnter The Number Of Elements You Want In Your Integer Array : ");
	scanf("%d", &aab_intArrayLength);
	
	aab_ptr_iArray = (int*)malloc(sizeof(int) * aab_intArrayLength);

	if (aab_ptr_iArray == NULL)
	{
		printf("\n\nMEMORY ALLOCATION FOR INTEGER ARRAY HAS FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}
	else
	{
		printf("\n\nMEMORY ALLOCATION FOR INTEGER ARRAY HAS SUCCEEDED !!!\n\n");
		printf("MEMORY ADDRESSES FROM %p TO %p HAVE BEEN ALLOCATED TO INTEGER ARRAY !!!\n\n", aab_ptr_iArray, (aab_ptr_iArray + (aab_intArrayLength - 1)));
	}

	printf("\n\nEnter %d Elements For The Integer Array : \n\n", aab_intArrayLength);

	for (aab_i = 0; aab_i < aab_intArrayLength; aab_i++)
		scanf("%d", (aab_ptr_iArray + aab_i));

	printf("\n\nThe Integer Array Entered By You, Consisting Of %d Elements : \n\n", aab_intArrayLength);

	for (aab_i = 0; aab_i < aab_intArrayLength; aab_i++)
	{
		printf("aab_ptr_iArray[%d] = %d \t \t At Address &aab_ptr_iArray[%d] : %p\n", aab_i, aab_ptr_iArray[aab_i], aab_i, &aab_ptr_iArray[aab_i]);
	}

	printf("\n\n");

	for (aab_i = 0; aab_i < aab_intArrayLength; aab_i++)
	{
		printf("*(aab_ptr_iArray + %d) = %d \t \t At Address (aab_ptr_iArray + %d) : %p\n", aab_i, *(aab_ptr_iArray + aab_i), aab_i, (aab_ptr_iArray + aab_i));
	}

	if (aab_ptr_iArray)
	{
		free(aab_ptr_iArray);
		aab_ptr_iArray = NULL;
		
		printf("\n\nMEMORY ALLOCATED FOR INTEGER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return 0;
}
