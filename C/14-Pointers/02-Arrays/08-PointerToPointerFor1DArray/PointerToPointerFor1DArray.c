#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main

int AAB_MAIN(void)
{
	void AAB_MyAlloc(int** aab_ptr, unsigned int aab_numberOfElements);

	int* aab_piArray = NULL;
	unsigned int aab_num_elements;
	int aab_i;

	printf("\n\nHow Many Elements You Want In Integer Array ?\n\n");
	scanf("%u", &aab_num_elements);

	printf("\n\n");

	AAB_MyAlloc(&aab_piArray, aab_num_elements);
	
	printf("Enter %u Elements To Fill Up Your Integer Array : \n\n", aab_num_elements);

	for (aab_i = 0; aab_i < aab_num_elements; aab_i++)
		scanf("%d", &aab_piArray[aab_i]);

	printf("\n\nThe %u Elements Entered By You In The Integer Array : \n\n", aab_num_elements);

	for (aab_i = 0; aab_i < aab_num_elements; aab_i++)
		printf("%u\n", aab_piArray[aab_i]);

	printf("\n\n");

	if (aab_piArray)
	{
		free(aab_piArray);
		aab_piArray = NULL;
		printf("Memory Allocated Has Now Been Successfully Freed !!!\n\n");
	}

	return(0);
}

void AAB_MyAlloc(int** aab_ptr, unsigned int aab_numberOfElements)
{
	*aab_ptr = (int*)malloc(aab_numberOfElements * sizeof(int));

	if (*aab_ptr == NULL)
	{
		printf("Could Not Allocate Memory !!! Exitting Now ...\n\n");
		exit(0);
	}

	printf("AAB_MyAlloc() Has Successfully Allocated %lu Bytes For Integer Array !!!\n\n", (aab_numberOfElements * sizeof(int)));
}
