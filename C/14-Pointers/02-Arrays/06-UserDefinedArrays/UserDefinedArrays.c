#include<stdio.h>

#define AAB_MAIN main
#define AAB_INT_SIZE sizeof(int)
#define AAB_FLOAT_SIZE sizeof(float)
#define AAB_DOUBLE_SIZE sizeof(double)
#define AAB_CHAR_SIZE sizeof(char)

int AAB_MAIN()
{
	int* aab_ptr_iArray = NULL;
	unsigned int aab_intArrayLength = 0;
	float* aab_ptr_fArray = NULL;
	unsigned int aab_floatArrayLength = 0;
	double* aab_ptr_dArray = NULL;
	unsigned int aab_doubleArrayLength = 0;
	char* aab_ptr_cArray = NULL;
	unsigned int aab_charArrayLength = 0;
	int aab_i;

	printf("\n\nEnter The Number Of Elements You Want In The Integer Array : ");
	scanf("%u", &aab_intArrayLength);

	aab_ptr_iArray = (int*)malloc(AAB_INT_SIZE * aab_intArrayLength);

	if (aab_ptr_iArray == NULL)
	{
		printf("\n\nMEMORY ALLOCATION FOR INTEGER ARRAY FAILED !!! EXITTING NOW... \n\n");
		exit(0);
	}
	else
	{
		printf("\n\nMEMORY ALLOCATION FOR INTEGER ARRAY SUCCEEDED !!!\n\n");
	}

	printf("\n\nEnter The %d Integer Elements To Fill Up The Integer Array : \n\n", aab_intArrayLength);

	for (aab_i = 0; aab_i < aab_intArrayLength; aab_i++)
		scanf("%d", (aab_ptr_iArray + aab_i));
	
	printf("\n\nEnter The Number Of Elements You Want In The 'float' Array : ");
	scanf("%u", &aab_floatArrayLength);
	
	aab_ptr_fArray = (float*)malloc(AAB_FLOAT_SIZE * aab_floatArrayLength);

	if (aab_ptr_fArray == NULL)
	{
		printf("\n\nMEMORY ALLOCATION FOR FLOATING-POINT ARRAY FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}
	else
	{
		printf("\n\nMEMORY ALLOCATION FOR FLOATING-POINT ARRAY SUCCEEDED !!!\n\n");
	}

	printf("\n\nEnter The %d Floating-Point Elements To Fill Up The 'float' Array : \n\n", aab_floatArrayLength);

	for (aab_i = 0; aab_i < aab_floatArrayLength; aab_i++)
		scanf("%f", (aab_ptr_fArray + aab_i));

	printf("\n\nEnter The Number Of Elements You Want In The 'double' Array : ");
	scanf("%u", &aab_doubleArrayLength);
	
	aab_ptr_dArray = (double*)malloc(AAB_DOUBLE_SIZE * aab_doubleArrayLength);

	if (aab_ptr_dArray == NULL)
	{
		printf("\n\nMEMORY ALLOCATION FOR 'DOUBLE' ARRAY FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}
	else
	{
		printf("\n\nMEMORY ALLOCATION FOR 'DOUBLE' ARRAY SUCCEEDED !!!\n\n");
	}

	printf("\n\nEnter The %d Double Elements To Fill Up The 'double' Array : \n\n", aab_doubleArrayLength);

	for (aab_i = 0; aab_i < aab_doubleArrayLength; aab_i++)
		scanf("%lf", (aab_ptr_dArray + aab_i));

	printf("\n\nEnter The Number Of Elements You Want In The Character Array : ");
	scanf("%u", &aab_charArrayLength);
	
	aab_ptr_cArray = (char*)malloc(AAB_CHAR_SIZE * aab_charArrayLength);

	if (aab_ptr_cArray == NULL)
	{
		printf("\n\nMEMORY ALLOCATION FOR CHARACTER ARRAY FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}
	else
	{
		printf("\n\nMEMORY ALLOCATION FOR CHARACTER ARRAY SUCCEEDED !!!\n\n");
	}

	printf("\n\nEnter The %d Character Elements To Fill Up The Character Array : \n\n", aab_charArrayLength);

	for (aab_i = 0; aab_i < aab_charArrayLength; aab_i++)
	{
		*(aab_ptr_cArray + aab_i) = getch();
		printf("%c\n", *(aab_ptr_cArray + aab_i));
	}

	printf("\n\nThe Integer Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", aab_intArrayLength);

	for (aab_i = 0; aab_i < aab_intArrayLength; aab_i++)
		printf(" %d \t \t At Address : %p\n", *(aab_ptr_iArray + aab_i), (aab_ptr_iArray + aab_i));

	printf("\n\nThe Float Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", aab_floatArrayLength);

	for (aab_i = 0; aab_i < aab_floatArrayLength; aab_i++)
		printf(" %f \t \t At Address : %p\n", *(aab_ptr_fArray + aab_i), (aab_ptr_fArray + aab_i));

	printf("\n\nThe Double Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", aab_doubleArrayLength);

	for (aab_i = 0; aab_i < aab_doubleArrayLength; aab_i++)
		printf(" %lf \t \t At Address : %p\n", *(aab_ptr_dArray + aab_i), (aab_ptr_dArray + aab_i));

	printf("\n\nThe Character Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", aab_charArrayLength);

	for (aab_i = 0; aab_i < aab_charArrayLength; aab_i++)
			printf(" %c \t \t At Address : %p\n", *(aab_ptr_cArray + aab_i), (aab_ptr_cArray + aab_i));

	if (aab_ptr_cArray)
	{
		free(aab_ptr_cArray);
		aab_ptr_cArray = NULL;
		
		printf("\n\nMEMORY OCCUPIED BY CHARACTER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	if (aab_ptr_dArray)
	{
		free(aab_ptr_dArray);
		aab_ptr_dArray = NULL;
		
		printf("\n\nMEMORY OCCUPIED BY 'DOUBLE' ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	if (aab_ptr_fArray)
	{
		free(aab_ptr_fArray);
		aab_ptr_fArray = NULL;
		
		printf("\n\nMEMORY OCCUPIED BY FLOATING-POINT ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	if (aab_ptr_iArray)
	{
		free(aab_ptr_iArray);
		aab_ptr_iArray = NULL;
		
		printf("\n\nMEMORY OCCUPIED BY INTEGER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return 0;
}
