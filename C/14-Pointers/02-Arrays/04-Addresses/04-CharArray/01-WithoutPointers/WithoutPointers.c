#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	char aab_cArray[10];
	int aab_i;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_cArray[aab_i] = (char)(aab_i + 65);
	
	printf("\n\nElements Of The Character Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_cArray[%d] = %c\n", aab_i, aab_cArray[aab_i]);

	printf("\n\nElements Of The Character Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_cArray[%d] = %c \t \t Address = %p\n", aab_i, aab_cArray[aab_i], &aab_cArray[aab_i]);

	return 0;
}

