#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	float aab_fArray[10];
	float* aab_ptr_fArray = NULL;
	int aab_i;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_fArray[aab_i] = (float)(aab_i + 1) * 1.5f;
	
	aab_ptr_fArray = aab_fArray;
	
	printf("\n\nElements Of The 'aab_float' Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_fArray[%d] = %f\n", aab_i, *(aab_ptr_fArray + aab_i));

	printf("\n\nElements Of The 'aab_float' Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_fArray[%d] = %f \t \t Address = %p\n", aab_i, *(aab_ptr_fArray + aab_i), (aab_ptr_fArray + aab_i));

	return 0;
}

