#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	double aab_dArray[10];
	double* aab_ptr_dArray = NULL;
	int aab_i;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_dArray[aab_i] = (float)(aab_i + 1) * 1.333333f;
	
	aab_ptr_dArray = aab_dArray;
	
	printf("\n\nElements Of The 'aab_double' Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_dArray[%d] = %lf\n", aab_i, *(aab_ptr_dArray + aab_i));

	printf("\n\nElements Of The 'aab_double' Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_dArray[%d] = %lf \t \t Address = %p\n", aab_i, *(aab_ptr_dArray + aab_i), (aab_ptr_dArray + aab_i));

	return 0;
}
