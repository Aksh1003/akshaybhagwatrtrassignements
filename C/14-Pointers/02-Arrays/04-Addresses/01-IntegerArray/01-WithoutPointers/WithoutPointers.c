#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_iArray[10];
	int aab_i;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_iArray[aab_i] = (aab_i + 1) * 3;

	printf("\n\nElements Of The Integer Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_iArray[%d] = %d\n", aab_i, aab_iArray[aab_i]);

	printf("\n\nElements Of The Integer Array : \n\n");
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("aab_iArray[%d] = %d \t \t Address = %p\n\n", aab_i, aab_iArray[aab_i], &aab_iArray[aab_i]);

	return 0;
}
