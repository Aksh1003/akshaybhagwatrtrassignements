#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main

int AAB_MAIN(void)
{
	int** aab_ptr_iArray = NULL; 
	int aab_i, aab_j;
	int aab_num_rows, aab_num_columns;

	printf("\n\nEnter Number Of Rows : ");
	scanf("%d", &aab_num_rows);

	printf("\n\nEnter Number Of Columns : ");
	scanf("%d", &aab_num_columns);

	printf("\n\n**** MEMORY ALLOCATION TO 2D INTEGER ARRAY ****\n\n");

	aab_ptr_iArray = (int**)malloc(aab_num_rows * sizeof(int*));
	
	if (aab_ptr_iArray == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO %d ROWS OF 2D INTEGER ARRAY !!! EXITTING NOW...\n\n", aab_num_rows);
		exit(0);
	}
	else
		printf("MEMORY ALLOCATION TO %d ROWS OF 2D INTEGER ARRAY SUCCEEDED !!! \n\n", aab_num_rows);

	for (aab_i = 0; aab_i < aab_num_rows; aab_i++)
	{
		aab_ptr_iArray[aab_i] = (int*)malloc(aab_num_columns * sizeof(int)); 

		if (aab_ptr_iArray[aab_i] == NULL)
		{
			printf("FAILED TO ALLOCATE MEMORY TO COLUMNS OF ROW %d OF 2D INTEGER ARRAY !!!EXITTING NOW...\n\n", aab_i);
			exit(0);
		}
		else
				printf("MEMORY ALLOCATION TO COLUMNS OF ROW %d OF 2D INTEGER ARRAY SUCCEEDED !!!\n\n", aab_i);
	}

	for (aab_i = 0; aab_i < aab_num_rows; aab_i++)
	{
		for (aab_j = 0; aab_j < aab_num_columns; aab_j++)
		{
			aab_ptr_iArray[aab_i][aab_j] = (aab_i * 1) + (aab_j * 1);
		}
	}

	for (aab_i = 0; aab_i < aab_num_rows; aab_i++)
	{
		printf("Base Address Of Row %d : aab_ptr_iArray[%d] = %p \t At Address : %p	\n", aab_i, aab_i, aab_ptr_iArray[aab_i], &aab_ptr_iArray[aab_i]);
	}

	printf("\n\n");
	for (aab_i = 0; aab_i < aab_num_rows; aab_i++)
	{
		for (aab_j = 0; aab_j < aab_num_columns; aab_j++)
		{
			printf("aab_ptr_iArray[%d][%d] = %d \t At Address : %p\n", aab_i, aab_j, aab_ptr_iArray[aab_i][aab_j], &aab_ptr_iArray[aab_i][aab_j]); 
		}
		printf("\n");
	}

	for (aab_i = (aab_num_rows - 1); aab_i >= 0; aab_i--)
	{
		if (aab_ptr_iArray[aab_i])
		{
			free(aab_ptr_iArray[aab_i]);
			aab_ptr_iArray[aab_i] = NULL;
			printf("MEMORY ALLOCATED TO ROW %d HAS BEEN SUCCESSFULLY FREED !!!\n\n", aab_i);
		}
	}

	if (aab_ptr_iArray)
	{
		free(aab_ptr_iArray);
		aab_ptr_iArray = NULL;
		printf("MEMORY ALLOCATED TO ptr_iArray HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}
