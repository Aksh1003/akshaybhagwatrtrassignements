#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main
#define AAB_NUM_ROWS 5
#define AAB_NUM_COLUMNS 3

int AAB_MAIN(void)
{
	int aab_i, aab_j;
	int** aab_ptr_iArray = NULL;

	printf("\n\n");

	aab_ptr_iArray = (int**)malloc(AAB_NUM_ROWS * sizeof(int*)); 

	if (aab_ptr_iArray == NULL)
	{
		printf("MEMORY ALLOCATION TO THE 1D ARRAY OF BASE ADDRESSES OF %d ROWS FAILED !!!EXITTING NOW...\n\n", AAB_NUM_ROWS);
			exit(0);
	}
	else
			printf("MEMORY ALLOCATION TO THE 1D ARRAY OF BASE ADDRESSES OF %d ROWS HAS SUCCEEDED !!!\n\n", AAB_NUM_ROWS);

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		aab_ptr_iArray[aab_i] = (int*)malloc(AAB_NUM_COLUMNS * sizeof(int)); 

		if (aab_ptr_iArray == NULL)
		{
			printf("MEMORY ALLOCATION TO THE COLUMNS OF ROW %d FAILED !!! EXITTING NOW...\n\n", aab_i);
			exit(0);
		}
		else
			printf("MEMORY ALLOCATION TO THE COLUMNS OF ROW %d HAS SUCCEEDED !!!\n\n", aab_i);
	}

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			*(*(aab_ptr_iArray + aab_i) + aab_j) = (aab_i + 1) * (aab_j + 1); 
		}
	}

	printf("\n\n2D Integer Array Elements Along With Addresses : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("aab_ptr_iArray_Row[%d][%d] = %d \t \t At Address & aab_ptr_iArray_Row[% d][% d] : % p\n", aab_i, aab_j, aab_ptr_iArray[aab_i][aab_j], aab_i, aab_j, &aab_ptr_iArray[aab_i][aab_j]);
		}

		printf("\n\n");
	}

	for (aab_i = (AAB_NUM_ROWS - 1); aab_i >= 0; aab_i--)
	{
		if (*(aab_ptr_iArray + aab_i))
		{
			free(*(aab_ptr_iArray + aab_i)); 
			*(aab_ptr_iArray + aab_i) = NULL;
			printf("MEMORY ALLOCATED TO ROW %d HAS BEEN SUCCESSFULLY FREED !!! \n\n", aab_i);
		}
	}

	if (aab_ptr_iArray)
	{
		free(aab_ptr_iArray);
		aab_ptr_iArray = NULL;
		printf("MEMORY ALLOCATED TO aab_ptr_iArray HAS BEEN SUCCESSFULLY FREED !!! \n\n");
	}
	
	return(0);
}
