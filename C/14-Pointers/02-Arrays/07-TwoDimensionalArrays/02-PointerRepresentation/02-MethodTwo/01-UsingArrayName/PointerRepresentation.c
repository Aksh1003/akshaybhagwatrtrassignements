#include <stdio.h>

#define AAB_MAIN main
#define AAB_NUM_ROWS 5
#define AAB_NUM_COLUMNS 3

int AAB_MAIN(void)
{
	int aab_iArray[AAB_NUM_ROWS][AAB_NUM_COLUMNS];
	int aab_i, aab_j;

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
			*(*(aab_iArray + aab_i) + aab_j) = (aab_i + 1) * (aab_j + 1);
	}

	printf("\n\n2D Integer Array Elements Along With Addresses : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("*(*(aab_iArray + %d) + %d)= %d \t \t At Address (*(aab_iArray + %d)	+ % d) : % p\n", aab_i, aab_j, *(*(aab_iArray + aab_i) + aab_j), aab_i, aab_j, (*(aab_iArray + aab_i) + aab_j));
		}

		printf("\n\n");
	}
	return(0);
}
