#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main
#define AAB_NUM_ROWS 5
#define AAB_NUM_COLUMNS 3

int AAB_MAIN(void)
{
	int* aab_iArray[AAB_NUM_ROWS]; 
	int aab_i, aab_j;

	printf("\n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		aab_iArray[aab_i] = (int*)malloc(AAB_NUM_COLUMNS * sizeof(int));
		
		if (aab_iArray[aab_i] == NULL)
		{
			printf("FAILED TO ALLOCATE MEMORY TO ROW %d OF 2D INTEGER ARRAY !!! EXITTING NOW...\n\n", aab_i);
			exit(0);
		}
		else
				printf("MEMORY ALLOCATION TO ROW %d OF 2D INTEGER ARRAY	SUCCEEDED !!!\n\n", aab_i);
	}

	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			aab_iArray[aab_i][aab_j] = (aab_i + 1) * (aab_j + 1);
		}
	}
	
	printf("\n\nDISPLAYING 2D ARRAY : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ROWS; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_COLUMNS; aab_j++)
		{
			printf("aab_iArray[%d][%d] = %d\n", aab_i, aab_j, aab_iArray[aab_i][aab_j]);
		}
		printf("\n\n");
	}

	printf("\n\n");
	
	for (aab_i = (AAB_NUM_ROWS - 1); aab_i >= 0; aab_i--)
	{
		free(aab_iArray[aab_i]);
		aab_iArray[aab_i] = NULL;
		printf("MEMORY ALLOCATED TO ROW %d Of 2D INTEGER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n", aab_i);
	}

	return(0);
}
