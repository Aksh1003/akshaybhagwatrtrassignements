#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_iArray[] = {12, 24, 36, 48, 60, 72, 84, 96, 108, 120};
	float aab_fArray[] = {9.8f, 8.7f, 7.6f, 6.5f, 5.4f};
	double aab_dArray[] = {1.222222, 2.333333, 3.444444};
	char aab_cArray[] = {'A', 'K', 'S', 'H', 'A', 'Y', 'B', 'H', 'A', 'G', 'W', 'A', 'T', '\0'};

	printf("\n\nInteger Array Elements And The Addresses They Occupy Are As Follows\n\n");
	printf("aab_iArray[0] = %d \t At Address : %p\n", *(aab_iArray + 0), (aab_iArray + 0));
	printf("aab_iArray[1] = %d \t At Address : %p\n", *(aab_iArray + 1), (aab_iArray + 1));
	printf("aab_iArray[2] = %d \t At Address : %p\n", *(aab_iArray + 2), (aab_iArray + 2));
	printf("aab_iArray[3] = %d \t At Address : %p\n", *(aab_iArray + 3), (aab_iArray + 3));
	printf("aab_iArray[4] = %d \t At Address : %p\n", *(aab_iArray + 4), (aab_iArray + 4));
	printf("aab_iArray[5] = %d \t At Address : %p\n", *(aab_iArray + 5), (aab_iArray + 5));
	printf("aab_iArray[6] = %d \t At Address : %p\n", *(aab_iArray + 6), (aab_iArray + 6));
	printf("aab_iArray[7] = %d \t At Address : %p\n", *(aab_iArray + 7), (aab_iArray + 7));
	printf("aab_iArray[8] = %d \t At Address : %p\n", *(aab_iArray + 8), (aab_iArray + 8));
	printf("aab_iArray[9] = %d \t At Address : %p\n", *(aab_iArray + 9), (aab_iArray + 9));

	printf("\n\nFloat Array Elements And The Addresses They Occupy Are As Follows\n\n");
	printf("aab_fArray[0] = %f \t At Address : %p\n", *(aab_fArray + 0), (aab_fArray + 0));
	printf("aab_fArray[1] = %f \t At Address : %p\n", *(aab_fArray + 1), (aab_fArray + 1));
	printf("aab_fArray[2] = %f \t At Address : %p\n", *(aab_fArray + 2), (aab_fArray + 2));
	printf("aab_fArray[3] = %f \t At Address : %p\n", *(aab_fArray + 3), (aab_fArray + 3));
	printf("aab_fArray[4] = %f \t At Address : %p\n", *(aab_fArray + 4), (aab_fArray + 4));

	printf("\n\nDouble Array Elements And The Addresses They Occupy Are As Follows\n\n");
	printf("aab_dArray[0] = %lf \t At Address : %p\n", *(aab_dArray + 0), (aab_dArray + 0));
	printf("aab_dArray[1] = %lf \t At Address : %p\n", *(aab_dArray + 1), (aab_dArray + 1));
	printf("aab_dArray[2] = %lf \t At Address : %p\n", *(aab_dArray + 2), (aab_dArray + 2));

	printf("\n\nCharacter Array Elements And The Addresses They Occupy Are As Follows\n\n");
	printf("aab_cArray[0] = %c \t At Address : %p\n", *(aab_cArray + 0), (aab_cArray + 0));
	printf("aab_cArray[1] = %c \t At Address : %p\n", *(aab_cArray + 1), (aab_cArray + 1));
	printf("aab_cArray[2] = %c \t At Address : %p\n", *(aab_cArray + 2), (aab_cArray + 2));
	printf("aab_cArray[3] = %c \t At Address : %p\n", *(aab_cArray + 3), (aab_cArray + 3));
	printf("aab_cArray[4] = %c \t At Address : %p\n", *(aab_cArray + 4), (aab_cArray + 4));
	printf("aab_cArray[5] = %c \t At Address : %p\n", *(aab_cArray + 5), (aab_cArray + 5));
	printf("aab_cArray[6] = %c \t At Address : %p\n", *(aab_cArray + 6), (aab_cArray + 6));
	printf("aab_cArray[7] = %c \t At Address : %p\n", *(aab_cArray + 7), (aab_cArray + 7));
	printf("aab_cArray[8] = %c \t At Address : %p\n", *(aab_cArray + 8), (aab_cArray + 8));
	printf("aab_cArray[9] = %c \t At Address : %p\n", *(aab_cArray + 9), (aab_cArray + 9));
	printf("aab_cArray[10] = %c \t At Address : %p\n", *(aab_cArray + 10), (aab_cArray + 10));
	printf("aab_cArray[11] = %c \t At Address : %p\n", *(aab_cArray + 11), (aab_cArray + 11));
	printf("aab_cArray[12] = %c \t At Address : %p\n", *(aab_cArray + 12), (aab_cArray + 12));
	printf("aab_cArray[13] = %c \t At Address : %p\n", *(aab_cArray + 13), (aab_cArray + 13));

	return 0;
}
