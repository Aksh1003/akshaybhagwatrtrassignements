#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_iArray[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	int* aab_ptr_iArray = NULL;

	printf("\n\n**** Using Array As A Name i.e : Value Of xth Element Of aab_iArray : aab_iArray[x] And Address Of xth Element Of aab_iArray : &aab_iArray[x] ****\n\n");
	printf("Integer Array Elements And Their Addresses : \n\n");
	printf("aab_iArray[0] = %d\t At Address &aab_iArray[0] : %p\n", aab_iArray[0], &aab_iArray[0]);
	printf("aab_iArray[1] = %d\t At Address &aab_iArray[1] : %p\n", aab_iArray[1], &aab_iArray[1]);
	printf("aab_iArray[2] = %d\t At Address &aab_iArray[2] : %p\n", aab_iArray[2], &aab_iArray[2]);
	printf("aab_iArray[3] = %d\t At Address &aab_iArray[3] : %p\n", aab_iArray[3], &aab_iArray[3]);
	printf("aab_iArray[4] = %d\t At Address &aab_iArray[4] : %p\n", aab_iArray[4], &aab_iArray[4]);
	printf("aab_iArray[5] = %d\t At Address &aab_iArray[5] : %p\n", aab_iArray[5], &aab_iArray[5]);
	printf("aab_iArray[6] = %d\t At Address &aab_iArray[6] : %p\n", aab_iArray[6], &aab_iArray[6]);
	printf("aab_iArray[7] = %d\t At Address &aab_iArray[7] : %p\n", aab_iArray[7], &aab_iArray[7]);
	printf("aab_iArray[8] = %d\t At Address &aab_iArray[8] : %p\n", aab_iArray[8], &aab_iArray[8]);
	printf("aab_iArray[9] = %d\t At Address &aab_iArray[9] : %p\n", aab_iArray[9], &aab_iArray[9]);

	aab_ptr_iArray = aab_iArray;

	printf("\n\n**** Using Pointer As Pointer i.e : Value Of xth Element Of aab_iArray : *(aab_ptr_iArray + x) And Address Of xth Element Of aab_iArray : (aab_ptr_iArray + x) ****\n\n");
	printf("Integer Array Elements And Their Addresses : \n\n");
	printf("*(aab_ptr_iArray + 0) = %d \t At Address (aab_ptr_iArray + 0) : %p\n", *(aab_ptr_iArray + 0), (aab_ptr_iArray + 0));
	printf("*(aab_ptr_iArray + 1) = %d \t At Address (aab_ptr_iArray + 1) : %p\n", *(aab_ptr_iArray + 1), (aab_ptr_iArray + 1));
	printf("*(aab_ptr_iArray + 2) = %d \t At Address (aab_ptr_iArray + 2) : %p\n", *(aab_ptr_iArray + 2), (aab_ptr_iArray + 2));
	printf("*(aab_ptr_iArray + 3) = %d \t At Address (aab_ptr_iArray + 3) : %p\n", *(aab_ptr_iArray + 3), (aab_ptr_iArray + 3));
	printf("*(aab_ptr_iArray + 4) = %d \t At Address (aab_ptr_iArray + 4) : %p\n", *(aab_ptr_iArray + 4), (aab_ptr_iArray + 4));
	printf("*(aab_ptr_iArray + 5) = %d \t At Address (aab_ptr_iArray + 5) : %p\n", *(aab_ptr_iArray + 5), (aab_ptr_iArray + 5));
	printf("*(aab_ptr_iArray + 6) = %d \t At Address (aab_ptr_iArray + 6) : %p\n", *(aab_ptr_iArray + 6), (aab_ptr_iArray + 6));
	printf("*(aab_ptr_iArray + 7) = %d \t At Address (aab_ptr_iArray + 7) : %p\n", *(aab_ptr_iArray + 7), (aab_ptr_iArray + 7));
	printf("*(aab_ptr_iArray + 8) = %d \t At Address (aab_ptr_iArray + 8) : %p\n", *(aab_ptr_iArray + 8), (aab_ptr_iArray + 8));
	printf("*(aab_ptr_iArray + 9) = %d \t At Address (aab_ptr_iArray + 9) : %p\n", *(aab_ptr_iArray + 9), (aab_ptr_iArray + 9));

	return 0;
}
