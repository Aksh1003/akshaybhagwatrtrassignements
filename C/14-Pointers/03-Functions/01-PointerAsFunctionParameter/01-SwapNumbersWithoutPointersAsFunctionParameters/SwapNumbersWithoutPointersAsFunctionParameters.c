#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	void AAB_SwapNumbers(int, int);

	int aab_a;
	int aab_b;

	printf("\n\nEnter Value For 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\nEnter Value For 'aab_B' : ");
	scanf("%d", &aab_b);

	printf("\n\n**** BEFORE SWAPPING ****\n\n");
	printf("Value Of 'aab_A' = %d\n\n", aab_a);
	printf("Value Of 'aab_B' = %d\n\n", aab_b);

	AAB_SwapNumbers(aab_a, aab_b);
	
	printf("\n\n**** AFTER SWAPPING ****\n\n");
	printf("Value Of 'aab_A' = %d\n\n", aab_a);
	printf("Value Of 'aab_B' = %d\n\n", aab_b);

	return 0;
}

void AAB_SwapNumbers(int aab_x, int aab_y)
{
	int aab_temp;

	printf("\n\n**** BEFORE SWAPPING ****\n\n");
	printf("Value Of 'aab_X' = %d\n\n", aab_x);
	printf("Value Of 'aab_Y' = %d\n\n", aab_y);
	aab_temp = aab_x;
	aab_x = aab_y;
	aab_y = aab_temp;

	printf("\n\n**** AFTER SWAPPING ****\n\n");
	printf("Value Of 'aab_X' = %d\n\n", aab_x);
	printf("Value Of 'aab_Y' = %d\n\n", aab_y);
}
