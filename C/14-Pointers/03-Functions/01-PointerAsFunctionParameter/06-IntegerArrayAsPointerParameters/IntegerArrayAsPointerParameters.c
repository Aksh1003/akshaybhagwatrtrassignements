#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main

int AAB_MAIN(void)
{
	void AAB_MultiplyArrayElementsByNumber(int*, int, int);
	
	int* aab_iArray = NULL;
	int aab_num_elements;
	int aab_i, aab_num;

	printf("\n\nEnter How Many Elements You Want In The Integer Array : ");
	scanf("%d", &aab_num_elements);
	
	aab_iArray = (int*)malloc(aab_num_elements * sizeof(int));

	if (aab_iArray == NULL)
	{
		printf("MEMORY ALLOCATION TO 'aab_iArray' HAS FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("\n\nEnter %d Elements For The Integer Array : \n\n", aab_num_elements);

	for (aab_i = 0; aab_i < aab_num_elements; aab_i++)
		scanf("%d", &aab_iArray[aab_i]);

	printf("\n\nArray Before Passing To Function MultiplyArrayElementsByNumber() : \n\n");

	for (aab_i = 0; aab_i < aab_num_elements; aab_i++)
			printf("aab_iArray[%d] = %d\n", aab_i, aab_iArray[aab_i]);

	printf("\n\nEnter The Number By Which You Want To Multiply Each Array Element : ");
	scanf("%d", &aab_num);
	
	AAB_MultiplyArrayElementsByNumber(aab_iArray, aab_num_elements, aab_num);

	printf("\n\nArray Returned By Function AAB_MultiplyArrayElementsByNumber() : \n\n");

	for (aab_i = 0; aab_i < aab_num_elements; aab_i++)
			printf("aab_iArray[%d] = %d\n", aab_i, aab_iArray[aab_i]);

	if (aab_iArray)
	{
		free(aab_iArray);
		aab_iArray = NULL;
		printf("\n\nMEMORY ALLOCATED TO 'aab_iArray' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void AAB_MultiplyArrayElementsByNumber(int* aab_arr, int aab_iNumElements, int aab_n)
{
	int aab_i;

	for (aab_i = 0; aab_i < aab_iNumElements; aab_i++)
		aab_arr[aab_i] = aab_arr[aab_i] * aab_n;
}
