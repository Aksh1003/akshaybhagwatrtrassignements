#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	void AAB_MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	int aab_a, aab_b, aab_answer_sum, aab_answer_difference, aab_answer_product, aab_answer_quotient, aab_answer_remainder;

	printf("\n\nEnter Value of 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\nEnter Value of 'aab_B' : ");
	scanf("%d", &aab_b);

	AAB_MathematicalOperations(aab_a, aab_b, &aab_answer_sum, &aab_answer_difference, &aab_answer_product, &aab_answer_quotient, &aab_answer_remainder);

	printf("\n\n**** Result  ****: \n\n");
	printf("Sum = %d\n\n", aab_answer_sum);
	printf("Diifference = %d\n\n", aab_answer_difference);
	printf("Product = %d\n\n", aab_answer_product);
	printf("Quotient = %d\n\n", aab_answer_quotient);
	printf("Remainder = %d\n\n", aab_answer_remainder);

	return 0;
}

void AAB_MathematicalOperations(int aab_x, int aab_y, int* aab_sum, int* aab_difference, int* aab_product, int* aab_quotient, int* aab_remainder)
{
	*aab_sum = aab_x + aab_y; 
	*aab_difference = aab_x - aab_y;
	*aab_product = aab_x * aab_y; 
	*aab_quotient = aab_x / aab_y;
	*aab_remainder = aab_x % aab_y; 
}
