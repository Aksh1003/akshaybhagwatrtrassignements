#include<stdio.h>
#include<stdlib.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	void AAB_MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	int aab_a;
	int aab_b;
	int* aab_answer_sum = NULL;
	int* aab_answer_difference = NULL;
	int* aab_answer_product = NULL;
	int* aab_answer_quotient = NULL;
	int* aab_answer_remainder = NULL;

	printf("\n\nEnter Value Of 'A' : ");
	scanf("%d", &aab_a);

	printf("\n\nEnter Value Of 'B' : ");
	scanf("%d", &aab_b);

	aab_answer_sum = (int*)malloc(1 * sizeof(int));

	if (aab_answer_sum == NULL)
	{
		printf("Could Not Allocate Memory For 'aab_answer_sum'. Exitting Now...\n\n");
		exit(0);
	}
	
	aab_answer_difference = (int*)malloc(1 * sizeof(int));
	if (aab_answer_difference == NULL)
	{
		printf("Could Not Allocate Memory For 'aab_answer_difference'. Exitting Now...\n\n");
		exit(0);
	}

	aab_answer_product = (int*)malloc(1 * sizeof(int));
	
	if (aab_answer_product == NULL)
	{
		printf("Could Not Allocate Memory For 'aab_answer_product'. Exitting Now...\n\n");
		exit(0);
	}

	aab_answer_quotient = (int*)malloc(1 * sizeof(int));
	if (aab_answer_quotient == NULL)
	{
		printf("Could Not Allocate Memory For 'aab_answer_quotient'. Exitting Now...\n\n");
		exit(0);
	}

	aab_answer_remainder = (int*)malloc(1 * sizeof(int));

	if (aab_answer_remainder == NULL)
	{
		printf("Could Not Allocate Memory For 'aab_answer_remainder'. Exitting Now...\n\n");
		exit(0);
	}

	AAB_MathematicalOperations(aab_a, aab_b, aab_answer_sum, aab_answer_difference, aab_answer_product, aab_answer_quotient, aab_answer_remainder);

	printf("\n\n**** RESULTS **** \n\n");
	printf("Sum = %d\n\n", *aab_answer_sum);
	printf("Difference = %d\n\n", *aab_answer_difference);
	printf("Product = %d\n\n", *aab_answer_product);
	printf("Quotient = %d\n\n", *aab_answer_quotient);
	printf("Remainder = %d\n\n", *aab_answer_remainder);

	if (aab_answer_remainder)
	{
		free(aab_answer_remainder);
		aab_answer_remainder = NULL;
		printf("Memory Allocated For 'aab_answer_remainder' Successfully Freed !!!\n\n");
	}

	if (aab_answer_quotient)
	{
		free(aab_answer_quotient);
		aab_answer_quotient = NULL;
		printf("Memory Allocated For 'aab_answer_quotient' Successfully Freed !!!\n\n");
	}

	if (aab_answer_product)
	{
		free(aab_answer_product);
		aab_answer_product = NULL;
		printf("Memory Allocated For 'aab_answer_product' Successfully Freed !!!\n\n");
	}

	if (aab_answer_difference)
	{
		free(aab_answer_difference);
		aab_answer_difference = NULL;
		printf("Memory Allocated For 'aab_answer_difference' Successfully Freed !!!\n\n");
	}

	if (aab_answer_sum)
	{
		free(aab_answer_sum);
		aab_answer_sum = NULL;
		printf("Memory Allocated For 'aab_answer_sum' Successfully Freed !!!\n\n");
	}

	return(0);
}

void AAB_MathematicalOperations(int aab_x, int aab_y, int* aab_sum, int* aab_difference, int* aab_product, int* aab_quotient, int* aab_remainder)
{
	*aab_sum = aab_x + aab_y; 
	*aab_difference = aab_x - aab_y;
	*aab_product = aab_x * aab_y;
	*aab_quotient = aab_x / aab_y;
	*aab_remainder = aab_x % aab_y;
}
