#include<stdio.h>

#define AAB_MAIN main

enum
{
	AAB_NEGATIVE = -1,
	AAB_ZERO,
	AAB_POSITIVE
};

int AAB_MAIN()
{
	int AAB_Difference(int, int, int*);

	int aab_a, aab_b, aab_answer, aab_ret;

	printf("\n\nEnter Value of 'aab_A' : ");
	scanf("%d", &aab_a);

	printf("\n\nEnter Value of 'aab_B' : ");
	scanf("%d", &aab_b);

	aab_ret = AAB_Difference(aab_a, aab_b, &aab_answer);

	printf("\n\nDifference Of %d And %d = %d\n\n", aab_a, aab_b, aab_answer);

	if (aab_ret == AAB_POSITIVE)
		printf("The Difference Of %d And %d Is Positive !!!\n\n", aab_a, aab_b);
	else if (aab_ret == AAB_NEGATIVE)
		printf("The Difference Of %d And %d Is Negative !!!\n\n", aab_a, aab_b);
	else
		printf("The Difference Of %d And %d Is Zero !!!\n\n", aab_a, aab_b);

	return 0;
}

int AAB_Difference(int aab_x, int aab_y, int* aab_diff)
{
	*aab_diff = aab_x - aab_y;

	if (*aab_diff > 0)
		return (AAB_POSITIVE);
	else if (*aab_diff < 0)
		return (AAB_NEGATIVE);
	else
		return (AAB_ZERO);
}