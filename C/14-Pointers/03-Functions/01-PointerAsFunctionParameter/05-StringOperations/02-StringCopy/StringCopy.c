#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main
#define AAB_MAX_STRING_LENGTH 512

int AAB_MAIN(void)
{
	void AAB_MyStrcpy(char*, char*);
	int AAB_MyStrlen(char*);
	
	char* aab_chArray_Original = NULL, * aab_chArray_Copy = NULL; 
	int aab_original_string_length;

	printf("\n\n");
	aab_chArray_Original = (char*)malloc(AAB_MAX_STRING_LENGTH * sizeof(char));

	if (aab_chArray_Original == NULL)
	{
		printf("MEMORY ALLOCATION FOR ORIGINAL STRING FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("Enter A String : \n\n");
	gets_s(aab_chArray_Original, AAB_MAX_STRING_LENGTH);
	aab_original_string_length = AAB_MyStrlen(aab_chArray_Original);
	aab_chArray_Copy = (char*)malloc(aab_original_string_length * sizeof(char));

	if (aab_chArray_Copy == NULL)
	{
		printf("MEMORY ALLOCATION FOR COPIED STRING FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	AAB_MyStrcpy(aab_chArray_Copy, aab_chArray_Original);

	printf("\n\nThe Original String Entered By You (i.e : 'aab_chArray_Original') Is : \n\n");
	printf("%s\n", aab_chArray_Original);

	printf("\n\nThe Copied String (i.e : 'aab_chArray_Copy') Is : \n\n");
	printf("%s\n", aab_chArray_Copy);

	if (aab_chArray_Copy)
	{
		free(aab_chArray_Copy);
		aab_chArray_Copy = NULL;
		printf("\n\nMEMORY ALLOCATED FOR COPIED STRING HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	if (aab_chArray_Original)
	{
		free(aab_chArray_Original);
		aab_chArray_Original = NULL;
		printf("\n\nMEMORY ALLOCATED FOR ORIGINAL STRING HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void AAB_MyStrcpy(char* aab_str_destination, char* aab_str_source)
{
	int AAB_MyStrlen(char*);

	int aab_iStringLength = 0;
	int aab_j;

	aab_iStringLength = AAB_MyStrlen(aab_str_source);
	for (aab_j = 0; aab_j < aab_iStringLength; aab_j++)
		*(aab_str_destination + aab_j) = *(aab_str_source + aab_j);
	*(aab_str_destination + aab_j) = '\0';
}

int AAB_MyStrlen(char* aab_str)
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (*(aab_str + aab_j) == '\0')
			break;
		else
			aab_string_length++;
	}
	return(aab_string_length);
}
