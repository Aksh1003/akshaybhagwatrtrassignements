#include <stdio.h>

#define AAB_MAIN main
#define AAB_MAX_STRING_LENGTH 512

int AAB_MAIN(void)
{
	void AAB_MyStrcat(char*, char*);
	int AAB_MyStrlen(char*);
	
	char* aab_chArray_One = NULL, * aab_chArray_Two = NULL; 

	printf("\n\n");
	aab_chArray_One = (char*)malloc(AAB_MAX_STRING_LENGTH * sizeof(char));

	if (aab_chArray_One == NULL)
	{
		printf("MEMORY ALLOCATION TO FIRST STRING FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("Enter First String : \n\n");
	gets_s(aab_chArray_One, AAB_MAX_STRING_LENGTH);

	printf("\n\n");
	aab_chArray_Two = (char*)malloc(AAB_MAX_STRING_LENGTH * sizeof(char));

	if (aab_chArray_Two == NULL)
	{
		printf("MEMORY ALLOCATION TO SEOND STRING FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("Enter Second String : \n\n");
	gets_s(aab_chArray_Two, AAB_MAX_STRING_LENGTH);

	printf("\n\n**** BEFORE CONCATENATION ****");
	printf("\n\nThe Original First String Entered By You (i.e : 'aab_chArray_One[]') Is : \n\n");
	printf("%s\n", aab_chArray_One);
	
	printf("\n\nThe Original Second String Entered By You (i.e : 'aab_chArray_Two[]') Is : \n\n");
	printf("%s\n", aab_chArray_Two);
	
	AAB_MyStrcat(aab_chArray_One, aab_chArray_Two);

	printf("\n\n**** AFTER CONCATENATION ****");

	printf("\n\n'aab_chArray_One[]' Is : \n\n");
	printf("%s\n", aab_chArray_One);

	printf("\n\n'aab_chArray_Two[]' Is : \n\n");
	printf("%s\n", aab_chArray_Two);

	if (aab_chArray_Two)
	{
		free(aab_chArray_Two);
		aab_chArray_Two = NULL;
		printf("\n\nMEMORY ALLOCATED TO SECOND STRING HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	if (aab_chArray_One)
	{
		free(aab_chArray_One);
		aab_chArray_One = NULL;
		printf("\n\nMEMORY ALLOCATED TO FIRST STRING HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void AAB_MyStrcat(char* aab_str_destination, char* aab_str_source)
{
	int AAB_MyStrlen(char*);
	
	int aab_iStringLength_Source = 0, aab_iStringLength_Destination = 0;
	int aab_i, aab_j;

	aab_iStringLength_Source = AAB_MyStrlen(aab_str_source);
	aab_iStringLength_Destination = AAB_MyStrlen(aab_str_destination);
	
	for (aab_i = aab_iStringLength_Destination, aab_j = 0; aab_j < aab_iStringLength_Source; aab_i++, aab_j++)
	{
		*(aab_str_destination + aab_i) = *(aab_str_source + aab_j);
	}
	*(aab_str_destination + aab_i) = '\0';
}

int AAB_MyStrlen(char* aab_str)
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (*(aab_str + aab_j) == '\0')
			break;
		else
			aab_string_length++;
	}
	return(aab_string_length);
}
