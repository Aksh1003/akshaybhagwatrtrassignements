#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main
#define AAB_MAX_STRING_LENGTH 512

int AAB_MAIN(void)
{
	int AAB__MyStrlen(char*);
	
	char* aab_chArray = NULL;
	int aab_iStringLength = 0;

	aab_chArray = (char*)malloc(AAB_MAX_STRING_LENGTH * sizeof(char));

	if (aab_chArray == NULL)
	{
		printf("MEMORY ALOCATION TO CHARACTER ARRAY FAILED !!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("Enter A String : \n\n");
	gets_s(aab_chArray, AAB_MAX_STRING_LENGTH);

	printf("\n\nString Entered By You Is : \n\n");
	printf("%s\n", aab_chArray);

	aab_iStringLength = AAB_MyStrlen(aab_chArray);
	printf("Length Of String Is = %d Characters !!!\n\n", aab_iStringLength);

	if (aab_chArray)
	{
		free(aab_chArray);
		aab_chArray = NULL;
	}
	return(0);
}

int AAB_MyStrlen(char* aab_str)
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (*(aab_str + aab_j) == '\0')
			break;
		else
			aab_string_length++;
	}
	return(aab_string_length);
}
