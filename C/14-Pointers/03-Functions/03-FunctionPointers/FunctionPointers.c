#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN(void)
{
	int AAB_AddIntegers(int, int);
	int AAB_SubtractIntegers(int, int);
	float AAB_AddFloats(float, float);
	
	typedef int (*AAB_AddIntsFnPtr)(int, int);
	AAB_AddIntsFnPtr aab_ptrAddTwoIntegers = NULL;
	AAB_AddIntsFnPtr aab_ptrFunc = NULL;

	typedef float (*AAB_AddFloatsFnPtr)(float, float);
	AAB_AddFloatsFnPtr aab_ptrAddTwoFloats = NULL;
	
	int aab_iAnswer = 0;
	float aab_fAnswer = 0.0f;

	aab_ptrAddTwoIntegers = AAB_AddIntegers;
	aab_iAnswer = aab_ptrAddTwoIntegers(9, 30);
	printf("\n\nSum Of Integers = %d\n\n", aab_iAnswer);

	aab_ptrFunc = AAB_SubtractIntegers;
	aab_iAnswer = aab_ptrFunc(9, 30);
	printf("\n\nSubtraction Of Integers = %d\n\n", aab_iAnswer);

	aab_ptrAddTwoFloats = AAB_AddFloats;
	aab_fAnswer = aab_ptrAddTwoFloats(11.45f, 8.2f);
	printf("\n\nSum Of Floating-Point Numbers = %f\n\n", aab_fAnswer);

	return(0);
}

int AAB_AddIntegers(int aab_a, int aab_b)
{
	int aab_c;

	aab_c = aab_a + aab_b;

	return(aab_c);
}

int AAB_SubtractIntegers(int aab_a, int aab_b)
{
	int aab_c;

	if (aab_a > aab_b)
		aab_c = aab_a - aab_b;
	else
		aab_c = aab_b - aab_a;

	return(aab_c);
}

float AAB_AddFloats(float aab_f_num1, float aab_f_num2)
{
	float aab_ans;

	aab_ans = aab_f_num1 + aab_f_num2;

	return(aab_ans);
}
