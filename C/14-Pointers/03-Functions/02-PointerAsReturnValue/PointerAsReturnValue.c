#include <stdio.h>
#include <stdlib.h>

#define AAB_MAIN main
#define AAB_MAX_STRING_LENGTH 512

int AAB_MAIN(void)
{
	char* AAB_ReplaceVowelsWithHashSymbol(char*);
	
	char aab_string[AAB_MAX_STRING_LENGTH];
	char* aab_replaced_string = NULL;

	printf("\n\nEnter String : ");
	gets_s(aab_string, AAB_MAX_STRING_LENGTH);
	
	aab_replaced_string = AAB_ReplaceVowelsWithHashSymbol(aab_string);
	if (aab_replaced_string == NULL)
	{
		printf("AAB_ReplaceVowelsWithHashSymbol() Function Has Failed !!! Exitiing Now...\n\n");
		exit(0);
	}

	printf("\n\nReplaced String Is : \n\n");
	printf("%s\n\n", aab_replaced_string);

	if (aab_replaced_string)
	{
		free(aab_replaced_string);
		aab_replaced_string = NULL;
	}

	return(0);
}

char* AAB_ReplaceVowelsWithHashSymbol(char* aab_s)
{
	void AAB_MyStrcpy(char*, char*);
	int AAB_MyStrlen(char*);
	
	char* aab_new_string = NULL;
	int aab_i;

	aab_new_string = (char*)malloc(AAB_MyStrlen(aab_s) * sizeof(char));

	if (aab_new_string == NULL)
	{
		printf("COULD NOT ALLOCATE MEMORY FOR NEW STRING !!!\n\n");
		return(NULL);
	}

	AAB_MyStrcpy(aab_new_string, aab_s);

	for (aab_i = 0; aab_i < AAB_MyStrlen(aab_new_string); aab_i++)
	{
		switch (aab_new_string[aab_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			aab_new_string[aab_i] = '#';
			break;
		default:
			break;
		}
	}
	return(aab_new_string);
}

void AAB_MyStrcpy(char* aab_str_destination, char* aab_str_source)
{
	int AAB_MyStrlen(char*);
	
	int aab_iStringLength = 0;
	int aab_j;

	aab_iStringLength = AAB_MyStrlen(aab_str_source);

	for (aab_j = 0; aab_j < aab_iStringLength; aab_j++)
		*(aab_str_destination + aab_j) = *(aab_str_source + aab_j);
	*(aab_str_destination + aab_j) = '\0';
}

int AAB_MyStrlen(char* aab_str)
{
	int aab_j;
	int aab_string_length = 0;

	for (aab_j = 0; aab_j < AAB_MAX_STRING_LENGTH; aab_j++)
	{
		if (*(aab_str + aab_j) == '\0')
			break;
		else
			aab_string_length++;
	}
	return(aab_string_length);
}
