#include <stdio.h>

#define AAB_MAIN main

struct AAB_Employee
{
	char aab_name[100];
	int aab_age;
	float aab_salary;
	char aab_sex;
	char aab_material_status;
};

int AAB_MAIN()
{
	printf("\n\nSizes Of Data Types And Pointers To Those Respective Data Types Are : \n\n");
	printf("Size of (int)			: %d\t\tSize Of Pointer To int (int*)			: %d\n\n", sizeof(int), sizeof(int*));
	printf("Size of (float)			: %d\t\tSize Of Pointer To int (float*)			: %d\n\n", sizeof(float), sizeof(float*));
	printf("Size of (double)		: %d\t\tSize Of Pointer To int (double*)		: %d\n\n", sizeof(double), sizeof(double*));
	printf("Size of (char)			: %d\t\tSize Of Pointer To int (char*)			: %d\n\n", sizeof(char), sizeof(char*));
	printf("Size of (struct AAB_Employee)	: %d\t\tSize Of Pointer To int (struct AAB_Employee*)	: %d\n\n", sizeof(struct AAB_Employee), sizeof(struct AAB_Employee*));

	return 0;
}
