#include <stdio.h>

#define AAB_MAIN main

struct AAB_Employee
{
	char aab_name[100];
	int aab_age;
	float aab_salary;
	char aab_sex;
	char aab_marital_status;
};

int AAB_MAIN()
{
	printf("\n\nSIZES OF DATA TYPES AND POINTERS TO THOSE RESPECTIVE DATA TYPES ARE : \n\n");
	printf("Size of (int) : %d \t \t \t Size of pointer to int (int*) : % d \t \t \t Size of pointer to pointer	to int(int**) : % d\n\n", sizeof(int), sizeof(int*), sizeof(int**));
	printf("Size of (float) : %d \t \t \t Size of pointer to float (float*) : % d \t \t Size of pointer to pointer to float(float**) : % d\n\n",	sizeof(float), sizeof(float*), sizeof(float**));
	printf("Size of (double) : %d \t \t \t Size of pointer to double (double*) : % d \t \t Size of pointer to pointer to double(double**) : % d\n\n", sizeof(double), sizeof(double*), sizeof(double**));
	printf("Size of (char) : %d \t \t \t Size of pointer to char (char*) : % d \t \t \t Size of pointer to pointer to char(char**) : % d\n\n", sizeof(char), sizeof(char*), sizeof(char**));
	printf("Size of (struct AAB_Employee) : %d \nSize of pointer to struct AAB_Employee(struct AAB_Employee*) : % d\nSize of pointer to pointer to struct AAB_Employee(struct AAB_Employee**) : % d\n\n", sizeof(struct AAB_Employee), sizeof(struct AAB_Employee*), sizeof(struct AAB_AAB_Employee**));
	
	return 0;
}
