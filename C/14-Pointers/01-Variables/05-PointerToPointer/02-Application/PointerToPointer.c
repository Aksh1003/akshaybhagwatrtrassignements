#include<stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_num;
	int* aab_ptr = NULL;
	int** aab_pptr = NULL; 
	
	aab_num = 10;

	printf("\n\n**** BEFORE aab_ptr = &aab_num ****\n\n");
	printf("Value Of 'aab_num' = %d\n\n", aab_num);
	printf("Address Of 'aab_num' = %p\n\n", &aab_num);
	printf("Value At Address Of 'aab_num' = %d\n\n", *(&aab_num));

	aab_ptr = &aab_num;

	printf("\n\n**** AFTER aab_ptr = &aab_num ****\n\n");
	printf("Value Of 'aab_num' = %d\n\n", aab_num);
	printf("Address Of 'aab_num' = %p\n\n", aab_ptr);
	printf("Value At Address Of 'aab_num' = %d\n\n", *aab_ptr);

	aab_pptr = &aab_ptr;
	
	printf("\n\n**** AFTER aab_pptr = &aab_ptr ****\n\n");
	printf("Value Of 'aab_num' = %d\n\n", aab_num);
	printf("Address Of 'aab_num' (aab_ptr) = %p\n\n", aab_ptr);
	printf("Address Of 'aab_ptr' (aab_pptr) = %p\n\n", aab_pptr);
	printf("Value At Address Of 'aab_ptr' (*aab_pptr) = %p\n\n", *aab_pptr);
	printf("Value At Address Of 'aab_num'  (**aab_pptr) = %d\n\n", **aab_pptr);

	return 0;
}
