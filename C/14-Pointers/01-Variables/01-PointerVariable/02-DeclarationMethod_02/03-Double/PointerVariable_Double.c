#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	double aab_num;
	double *aab_ptr = NULL;

	aab_num = 1.123456f;

	printf("\n\n**** Before aab_ptr = &aab_num ****\n\n");
	printf("Value Of 'aab_num'		= %lf\n\n", aab_num);
	printf("Address Of 'aab_num'		= %p\n\n", &aab_num);
	printf("Value At Address Of 'aab_num'	= %lf\n\n", *(&aab_num));

	aab_ptr = &aab_num;

	printf("\n\n**** After aab_ptr = &aab_num ****\n\n");
	printf("Value Of 'aab_num'		= %lf\n\n", aab_num);
	printf("Address Of 'aab_num'		= %p\n\n", aab_ptr);
	printf("Value At Address Of 'aab_num'	= %lf\n\n", *aab_ptr);

	return 0;
}

