#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_num;
	int* aab_ptr = NULL;
	int* aab_copy_ptr = NULL;

	aab_num = 5;
	aab_ptr = &aab_num;

	printf("\n\n**** Before aab_copy_ptr = aab_ptr ****\n\n");
	printf("aab_num			= %d\n", aab_num);
	printf("&aab_num		= %p\n", &aab_num);
	printf("*(&aab_num)		= %d\n", *(&aab_num));
	printf("aab_ptr			= %p\n", aab_ptr);
	printf("*aab_ptr		= %d\n", *aab_ptr);

	aab_copy_ptr = aab_ptr;

	printf("\n\n**** After aab_copy_ptr = aab_ptr ****\n\n");
	printf("aab_num			= %d\n", aab_num);
	printf("&aab_num		= %p\n", &aab_num);
	printf("*(&aab_num)		= %d\n", *(&aab_num));
	printf("aab_ptr			= %p\n", aab_ptr);
	printf("*aab_ptr		= %d\n", *aab_ptr);
	printf("aab_copy_ptr		= %p\n", aab_copy_ptr);
	printf("*aab_copy_ptr		= %d\n", *aab_copy_ptr);

	return 0;
}
