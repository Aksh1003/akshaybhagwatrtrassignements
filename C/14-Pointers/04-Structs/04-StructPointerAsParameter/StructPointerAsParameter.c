#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
};

int AAB_MAIN(void)
{
	void AAB_ChangeValues(struct AAB_MyData*);

	struct AAB_MyData* aab_pData = NULL;

	printf("\n\n");

	aab_pData = (struct AAB_MyData*)malloc(sizeof(struct AAB_MyData));

	if (aab_pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'sturct AAB_MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
			printf("SUCCESSFULLY ALLOCATED MEMORY TO 'sturct AAB_MyData' !!!\n\n");

	aab_pData->aab_i = 30;
	aab_pData->aab_f = 11.45f;
	aab_pData->aab_d = 1.2995;

	printf("\n\nDATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_pData->aab_i);
	printf("aab_f = %f\n", aab_pData->aab_f);
	printf("aab_d = %lf\n", aab_pData->aab_d);

	AAB_ChangeValues(aab_pData);

	printf("\n\nDATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_pData->aab_i);
	printf("aab_f = %f\n", aab_pData->aab_f);
	printf("aab_d = %lf\n", aab_pData->aab_d);

	if (aab_pData)
	{
		free(aab_pData);
		aab_pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct AAB_MyData' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void AAB_ChangeValues(struct AAB_MyData* aab_pParam_Data)
{
	aab_pParam_Data->aab_i = 9;
	aab_pParam_Data->aab_f = 8.2f;
	aab_pParam_Data->aab_d = 6.1998;
}
