#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int* aab_ptr_i;
	int aab_i;
	float* aab_ptr_f;
	float aab_f;
	double* aab_ptr_d;
	double aab_d;
};

int AAB_MAIN(void)
{
	struct AAB_MyData* aab_pData = NULL;

	printf("\n\n");

	aab_pData = (struct AAB_MyData*)malloc(sizeof(struct AAB_MyData));

	if (aab_pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'struct AAB_MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
		printf("SUCCESSFULLY ALLOCATED MEMORY TO 'struct AAB_MyData' !!!\n\n");

	aab_pData->aab_i = 9;
	aab_pData->aab_ptr_i = &(aab_pData->aab_i);
	aab_pData->aab_f = 11.45f;
	aab_pData->aab_ptr_f = &(aab_pData->aab_f);
	aab_pData->aab_d = 30.121995;
	aab_pData->aab_ptr_d = &(aab_pData->aab_d);

	printf("\n\naab_i = %d\n", *(aab_pData->aab_ptr_i));
	printf("Adress Of 'aab_i' = %p\n", aab_pData->aab_ptr_i);

	printf("\n\naab_f = %f\n", *(aab_pData->aab_ptr_f));
	printf("Adress Of 'aab_f' = %p\n", aab_pData->aab_ptr_f);

	printf("\n\naab_d = %lf\n", *(aab_pData->aab_ptr_d));
	printf("Adress Of 'aab_d' = %p\n", aab_pData->aab_ptr_d);

	if (aab_pData)
	{
		free(aab_pData);
		aab_pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct AAB_MyData' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}
