#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int* aab_ptr_i;
	int aab_i;
	float* aab_ptr_f;
	float aab_f;
	double* aab_ptr_d;
	double aab_d;
};

int AAB_MAIN(void)
{
	struct AAB_MyData aab_data;

	aab_data.aab_i = 9;
	aab_data.aab_ptr_i = &aab_data.aab_i;
	aab_data.aab_f = 11.45f;
	aab_data.aab_ptr_f = &aab_data.aab_f;
	aab_data.aab_d = 30.121995;
	aab_data.aab_ptr_d = &aab_data.aab_d;

	printf("\n\naab_i = %d\n", *(aab_data.aab_ptr_i));
	printf("Adress Of 'aab_i' = %p\n", aab_data.aab_ptr_i);

	printf("\n\naab_f = %f\n", *(aab_data.aab_ptr_f));
	printf("Adress Of 'aab_f' = %p\n", aab_data.aab_ptr_f);

	printf("\n\naab_d = %lf\n", *(aab_data.aab_ptr_d));
	printf("Adress Of 'aab_d' = %p\n", aab_data.aab_ptr_d);

	return(0);
}
