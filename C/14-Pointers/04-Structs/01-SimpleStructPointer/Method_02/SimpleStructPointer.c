#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
};

int AAB_MAIN(void)
{
	int aab_i_size;
	int aab_f_size;
	int aab_d_size;
	int aab_struct_AAB_MyData_size;
	int aab_pointer_to_struct_AAB_MyData_size;
	struct AAB_MyData* aab_pData = NULL;

	printf("\n\n");

	aab_pData = (struct AAB_MyData*)malloc(sizeof(struct AAB_MyData));

	if (aab_pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'sturct AAB_MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
		printf("SUCCESSFULLY ALLOCATED MEMORY TO 'sturct AAB_MyData' !!!\n\n");

	aab_pData->aab_i = 30;
	aab_pData->aab_f = 11.45f;
	aab_pData->aab_d = 1.2995;

	printf("\n\nDATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_pData->aab_i);
	printf("aab_f = %f\n", aab_pData->aab_f);
	printf("aab_d = %lf\n", aab_pData->aab_d);

	aab_i_size = sizeof(aab_pData->aab_i);
	aab_f_size = sizeof(aab_pData->aab_f);
	aab_d_size = sizeof(aab_pData->aab_d);

	printf("\n\nSIZES (in bytes) OF DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("Size of 'aab_i' = %d bytes\n", aab_i_size);
	printf("Size of 'aab_f' = %d bytes\n", aab_f_size);
	printf("Size of 'aab_d' = %d bytes\n", aab_d_size);

	aab_struct_AAB_MyData_size = sizeof(struct AAB_MyData);
	aab_pointer_to_struct_AAB_MyData_size = sizeof(struct AAB_MyData*);

	printf("\n\nSize of 'struct AAB_MyData' : %d bytes\n\n", aab_struct_AAB_MyData_size);
	printf("Size of pointer to 'struct AAB_MyData' : %d bytes\n\n", aab_pointer_to_struct_AAB_MyData_size);

	if (aab_pData)
	{
		free(aab_pData);
		aab_pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct AAB_MyData' HAS BEEN SUCCESSFULLY  FREED !!!\n\n");
	}

	return(0);
}
