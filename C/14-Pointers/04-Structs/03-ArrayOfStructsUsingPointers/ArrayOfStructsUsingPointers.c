#include <stdio.h>
#include <ctype.h>

#define AAB_MAIN main
#define AAB_NAME_LENGTH 100
#define AAB_MARITAL_STATUS 10
struct AAB_Employee
{
	char aab_name[AAB_NAME_LENGTH];
	int aab_age;
	char aab_sex;
	float aab_salary;
	char aab_marital_status;
};

int AAB_MAIN(void)
{
	void AAB_MyGetString(char[], int);
	
	struct AAB_Employee* aab_pEmployeeRecord = NULL;
	int aab_num_employees, aab_i;

	printf("\n\nEnter Number Of Employees Whose Details You Want To Record : ");
	scanf("%d", &aab_num_employees);

	printf("\n\n");

	aab_pEmployeeRecord = (struct AAB_Employee*)malloc(sizeof(struct AAB_Employee) * aab_num_employees);

	if (aab_pEmployeeRecord == NULL)
	{
		printf("FAILED TO ALLOCATED MEMORY FOR %d EMPLOYEES !!! EXITTING NOW ...\n\n", aab_num_employees);
		exit(0);
	}
	else
			printf("SUCCESSFULLY ALLOCATED MEMORY FOR %d EMPLOYEES !!!\n\n", aab_num_employees);

	for (aab_i = 0; aab_i < aab_num_employees; aab_i++)
	{
		printf("\n\n\n\n");
		printf("**** DATA ENTRY FOR EMPLOYEE NUMBER %d ****\n", (aab_i + 1));

		printf("\n\nEnter Employee Name : ");
		AAB_MyGetString(aab_pEmployeeRecord[aab_i].aab_name, AAB_NAME_LENGTH);
		
		printf("\n\nEnter Employee's Age (in years) : ");
		scanf("%d", &aab_pEmployeeRecord[aab_i].aab_age);

		printf("\n\nEnter Employee's Sex (M/m For Male, F/f For Female) : ");
		aab_pEmployeeRecord[aab_i].aab_sex = getch();
		printf("%c", aab_pEmployeeRecord[aab_i].aab_sex);
		aab_pEmployeeRecord[aab_i].aab_sex = toupper(aab_pEmployeeRecord[aab_i].aab_sex);

		printf("\n\nEnter Employee's Salary (in Indian Rupees) : ");
		scanf("%f", &aab_pEmployeeRecord[aab_i].aab_salary);

		printf("\n\nIs The Employee Married? (Y/y For Yes, N/n For No) : ");
		aab_pEmployeeRecord[aab_i].aab_marital_status = getch();
		printf("%c", aab_pEmployeeRecord[aab_i].aab_marital_status);
		aab_pEmployeeRecord[aab_i].aab_marital_status = toupper(aab_pEmployeeRecord[aab_i].aab_marital_status);
	}

	printf("\n\n\n\n");
	printf("**** DISPLAYING EMPLOYEE RECORDS ****\n\n");
	for (aab_i = 0; aab_i < aab_num_employees; aab_i++)
	{
		printf("**** EMPLOYEE NUMBER %d ****\n\n", (aab_i + 1));
		printf("Name : %s\n", aab_pEmployeeRecord[aab_i].aab_name);
		printf("Age : %d years\n", aab_pEmployeeRecord[aab_i].aab_age);
		if (aab_pEmployeeRecord[aab_i].aab_sex == 'M')
			printf("Sex : Male\n");
		else if (aab_pEmployeeRecord[aab_i].aab_sex == 'F')
			printf("Sex : Female\n");
		else
			printf("Sex : Invalid Data Entered\n");
		printf("Salary : Rs. %f\n", aab_pEmployeeRecord[aab_i].aab_salary);
		if (aab_pEmployeeRecord[aab_i].aab_marital_status == 'Y')
			printf("Marital Status : Married\n");
		else if (aab_pEmployeeRecord[aab_i].aab_marital_status == 'N')
			printf("Marital Status : Unmarried\n");
		else
			printf("Marital Status : Invalid Data Entered\n");
		printf("\n\n");
	}

	if (aab_pEmployeeRecord)
	{
		free(aab_pEmployeeRecord);
		aab_pEmployeeRecord = NULL;
		printf("MEMORY ALLOCATED TO %d EMPLOYEES HAS BEEN SUCCESSFULLY FREED !!!\n\n", aab_num_employees);
	}

	return(0);
}

void AAB_MyGetString(char aab_str[], int aab_str_size)
{
	int aab_i;
	char aab_ch = '\0';

	aab_i = 0;
	do
	{
		aab_ch = getch();
		aab_str[aab_i] = aab_ch;
		printf("%c", aab_str[aab_i]);
		aab_i++;
	} while ((aab_ch != '\r') && (aab_i < aab_str_size));

	if (aab_i == aab_str_size)
		aab_str[aab_i - 1] = '\0';
	else
		aab_str[aab_i] = '\0';
}
