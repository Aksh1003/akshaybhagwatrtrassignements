#include <stdio.h>

#define AAB_MAIN main
#define AAB_NUM_EMPLOYEES 5
#define AAB_NAME_LENGTH 100
#define AAB_MATERIAL_STATUS 10

struct AAB_Employee
{
	char aab_name[AAB_NAME_LENGTH];
	int aab_age;
	float aab_salary;
	char aab_sex;
	char aab_material_status;
};

int AAB_MAIN()
{
	void AAB_MyGetString(char[], int);

	struct AAB_Employee aab_EmployeeRecord[AAB_NUM_EMPLOYEES];

	int aab_i;

	for (aab_i = 0; aab_i < AAB_NUM_EMPLOYEES; aab_i++)
	{
		printf("\n\n\n\n");
		printf("***DATA ENTRY FOR EMPLOYEE NUMBER %d ***\n", (aab_i + 1));
		printf("\n\nEnter Employee Name : ");
		AAB_MyGetString(aab_EmployeeRecord[aab_i].aab_name, AAB_NAME_LENGTH);

		printf("\n\nEnter Employee's Age (int Years) : ");
		scanf("%d", &aab_EmployeeRecord[aab_i].aab_age);

		printf("\n\nEnter Employee's Sex (M / m For Male, F / f For Female) : ");
		aab_EmployeeRecord[aab_i].aab_sex = getch();
		printf("%c", aab_EmployeeRecord[aab_i].aab_sex);
		aab_EmployeeRecord[aab_i].aab_sex = toupper(aab_EmployeeRecord[aab_i].aab_sex);

		printf("\n\nEnter Employee's Salary (In Indian Rupees) : ");
		scanf("%f", &aab_EmployeeRecord[aab_i].aab_salary);
	
		printf("\n\nIs The Employee Married ? (Y / y for Yes, N / n for No) : ");
		aab_EmployeeRecord[aab_i].aab_material_status = getch();
		printf("%c", aab_EmployeeRecord[aab_i].aab_material_status);
		aab_EmployeeRecord[aab_i].aab_material_status = toupper(aab_EmployeeRecord[aab_i].aab_material_status);
	}

	printf("\n\n\n*** DISPLAYING EMPLOYEE RECORDS ***\n\n");
	for (aab_i = 0; aab_i < AAB_NUM_EMPLOYEES; aab_i++)
	{
		printf("*** EMPLOYEE NUMBER %d ***\n\n", (aab_i + 1));
		printf("Name		: %s\n", aab_EmployeeRecord[aab_i].aab_name);
		printf("Age		: %d\n", aab_EmployeeRecord[aab_i].aab_age);

		if(aab_EmployeeRecord[aab_i].aab_sex == 'M')
			printf("Sex		: Male\n");
		else if (aab_EmployeeRecord[aab_i].aab_sex == 'F')
			printf("Sex		: Female\n");
		else
			printf("Sex		: Invalid Data Entered\n");

		printf("Salary		: %f\n", aab_EmployeeRecord[aab_i].aab_salary);

		if(aab_EmployeeRecord[aab_i].aab_material_status == 'Y')
			printf("Material Status		: Married\n");
		else if (aab_EmployeeRecord[aab_i].aab_material_status == 'N')
			printf("Material Status		: Unmarried\n");
		else
			printf("Material Status		: Invalid Data Entered\n");

		printf("\n\n");
	}

	return 0;
}

void AAB_MyGetString(char aab_str[], int aab_size)
{
	int aab_i;
	char aab_ch = '\0';

	aab_i = 0;

	do
	{
		aab_ch = getch();
		aab_str[aab_i] = aab_ch;
		printf("%c",aab_str[aab_i]);
		aab_i++;
	} while ((aab_ch != '\r') && (aab_i < aab_size));

	if (aab_i == aab_size)
		aab_str[aab_i - 1] = '\0';
	else
		aab_str[aab_i] = '\0';
}
