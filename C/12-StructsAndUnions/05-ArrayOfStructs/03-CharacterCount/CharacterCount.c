#include<stdio.h>
#include<string.h>

#define AAB_MAIN main
#define AAB_MAX_STRING_LENGTH 1024

struct AAB_CharacterCount
{
	char aab_ch;
	int aab_ch_count;
}aab_character_and_count[] = { {'A', 0}, {'B', 0}, {'C', 0}, 
								{'D', 0}, {'E', 0}, {'F', 0}, 
								{'G', 0}, {'H', 0}, {'I', 0}, 
								{'J', 0}, {'K', 0}, {'L', 0},
								{'M', 0}, {'N', 0}, {'O', 0}, 
								{'P', 0}, {'Q', 0}, {'R', 0}, 
								{'S', 0}, {'T', 0}, {'U', 0}, 
								{'V', 0}, {'W', 0}, {'X', 0}, 
								{'Y', 0}, {'Z', 0}};

#define AAB_SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS sizeof(aab_character_and_count)
#define AAB_SIZE_OF_ONE_STRUCT_FROM_THE_ARRAY_OF_STRUCTS sizeof(aab_character_and_count[0])
#define AAB_NUM_ELEMENTS_IN_ARRAY (AAB_SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS / AAB_SIZE_OF_ONE_STRUCT_FROM_THE_ARRAY_OF_STRUCTS)

int AAB_MAIN()
{
	char aab_str[AAB_MAX_STRING_LENGTH];
	int aab_i, aab_j, aab_actual_string_length = 0;

	printf("\n\nEnter A String : \n\n");
	gets(aab_str, AAB_MAX_STRING_LENGTH);

	aab_actual_string_length = strlen(aab_str);

	printf("\n\nThe String You Have Entered Is : \n\n");
	printf("%s\n\n", aab_str);

	for (aab_i = 0; aab_i < aab_actual_string_length; aab_i++)
	{
		for (aab_j = 0; aab_j < AAB_NUM_ELEMENTS_IN_ARRAY; aab_j++)
		{
			aab_str[aab_i] = toupper(aab_str[aab_i]);

			if (aab_str[aab_i] == aab_character_and_count[aab_j].aab_ch)
				aab_character_and_count[aab_j].aab_ch_count++;
		}
	}

	printf("\n\nThe Number Of Occurances Of All Characters From The Alphabet Are As Follows : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_ELEMENTS_IN_ARRAY; aab_i++)
	{
		printf("Character %c = %d\n", aab_character_and_count[aab_i].aab_ch, aab_character_and_count[aab_i].aab_ch_count);
	}

	printf("\n\n");

	return 0;
}