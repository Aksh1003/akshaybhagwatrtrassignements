#include <stdio.h>

#define AAB_MAIN main
#define AAB_NAME_LENGTH 100
#define AAB_MATERIAL_STATUS 10

struct AAB_Employee
{
	char aab_name[AAB_NAME_LENGTH];
	int aab_age;
	float aab_salary;
	char aab_sex;
	char aab_material_status[AAB_MATERIAL_STATUS];
};

int AAB_MAIN()
{
	struct AAB_Employee aab_EmployeeRecord[5];

	char aab_employee_Akansha[] = "Akansha";
	char aab_employee_Akshay[] = "Akshay";
	char aab_employee_Anil[] = "Anil";
	char aab_employee_Nitin[] = "Nitin";
	char aab_employee_Sunita[] = "Sunita";

	int aab_i;

	strcpy(aab_EmployeeRecord[0].aab_name, aab_employee_Akansha);
	aab_EmployeeRecord[0].aab_age = 22;
	aab_EmployeeRecord[0].aab_sex = 'F';
	aab_EmployeeRecord[0].aab_salary = 50000.00f;
	strcpy(aab_EmployeeRecord[0].aab_material_status, "Unmarried");

	strcpy(aab_EmployeeRecord[1].aab_name, aab_employee_Akshay);
	aab_EmployeeRecord[1].aab_age = 27;
	aab_EmployeeRecord[1].aab_sex = 'M';
	aab_EmployeeRecord[1].aab_salary = 62000.00f;
	strcpy(aab_EmployeeRecord[1].aab_material_status, "Unmarried");

	strcpy(aab_EmployeeRecord[2].aab_name, aab_employee_Anil);
	aab_EmployeeRecord[2].aab_age = 39;
	aab_EmployeeRecord[2].aab_sex = 'M';
	aab_EmployeeRecord[2].aab_salary = 80000.00f;
	strcpy(aab_EmployeeRecord[2].aab_material_status, "Married");

	strcpy(aab_EmployeeRecord[3].aab_name, aab_employee_Nitin);
	aab_EmployeeRecord[3].aab_age = 32;
	aab_EmployeeRecord[3].aab_sex = 'M';
	aab_EmployeeRecord[3].aab_salary = 80000.00f;
	strcpy(aab_EmployeeRecord[3].aab_material_status, "Married");

	strcpy(aab_EmployeeRecord[4].aab_name, aab_employee_Sunita);
	aab_EmployeeRecord[4].aab_age = 30;
	aab_EmployeeRecord[4].aab_sex = 'F';
	aab_EmployeeRecord[4].aab_salary = 45000.00f;
	strcpy(aab_EmployeeRecord[4].aab_material_status, "Married");

	printf("\n\n*** DISPLAYING EMPLOYEES RECORDS ***\n\n");
	for (aab_i = 0; aab_i < 5; aab_i++)
	{
		printf("*** EMPLOYEE NUMBER %d ***\n\n", (aab_i + 1));
		printf("Name		: %s\n", aab_EmployeeRecord[aab_i].aab_name);
		printf("Age		: %d years\n", aab_EmployeeRecord[aab_i].aab_age);
		
		if(aab_EmployeeRecord[aab_i].aab_sex == 'M' || aab_EmployeeRecord[aab_i].aab_sex == 'm')
			printf("Sex		: Male\n");
		else
			printf("Sex		: Female\n");

		printf("Salary		: Rs. %f\n", aab_EmployeeRecord[aab_i].aab_salary);
		printf("Material Status	: %s\n", aab_EmployeeRecord[aab_i].aab_material_status);

		printf("\n\n");
	}

	return 0;
}
