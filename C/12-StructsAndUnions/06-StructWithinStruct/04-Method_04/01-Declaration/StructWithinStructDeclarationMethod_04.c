#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	int aab_length, aab_breadth, aab_area;

	struct AAB_MyPoint
	{
		int aab_x;
		int aab_y;
	};

	struct AAB_Rectangle
	{
		struct AAB_MyPoint aab_point_01, aab_point_02;
	};

	struct AAB_Rectangle aab_rect;

	printf("\n\nEnter Leftmost X-Coordinate Of Rectangle : ");
	scanf("%d", &aab_rect.aab_point_01.aab_x);

	printf("\n\nEnter Leftmost Y-Coordinate Of Rectangle : ");
	scanf("%d", &aab_rect.aab_point_01.aab_y);

	printf("\n\nEnter Leftmost X-Coordinate Of Rectangle : ");
	scanf("%d", &aab_rect.aab_point_02.aab_x);

	printf("\n\nEnter Leftmost Y-Coordinate Of Rectangle : ");
	scanf("%d", &aab_rect.aab_point_02.aab_y);

	aab_length = aab_rect.aab_point_02.aab_y - aab_rect.aab_point_01.aab_y;

	if (aab_length < 0)
		aab_length = aab_length * -1;

	aab_breadth = aab_rect.aab_point_02.aab_x - aab_rect.aab_point_01.aab_x;

	if (aab_breadth < 0)
		aab_breadth = aab_breadth * -1;

	aab_area = aab_length * aab_breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d\n\n", aab_length);
	printf("Breadth Of Rectangle = %d\n\n", aab_breadth);
	printf("Area Of Rectangle = %d\n\n", aab_area);

	return 0;
}
