#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyData AAB_AddStructMembers(struct AAB_MyData, struct AAB_MyData, struct AAB_MyData);

	struct AAB_MyData aab_data1, aab_data2, aab_data3, aab_answer_data;

	printf("\n\n**** DATA 1 ****\n\n");
	printf("Enter Integer Value For 'aab_i' Of 'struct AAB_MyData aab_data1' : ");
	scanf("%d", &aab_data1.aab_i);

	printf("Enter Float Value For 'aab_f' Of 'struct AAB_MyData aab_data1' : ");
	scanf("%f", &aab_data1.aab_f);

	printf("Enter Double Value For 'aab_d' Of 'struct AAB_MyData aab_data1' : ");
	scanf("%lf", &aab_data1.aab_d);

	printf("Enter Character Value For 'aab_c' Of 'struct AAB_MyData aab_data1' : ");
	aab_data1.aab_c = getch();
	printf("%c", aab_data1.aab_c);

	printf("\n\n**** DATA 2 ****\n\n");
	printf("Enter Integer Value For 'aab_i' Of 'struct AAB_MyData aab_data2' : ");
	scanf("%d", &aab_data2.aab_i);

	printf("Enter Float Value For 'aab_f' Of 'struct AAB_MyData aab_data2' : ");
	scanf("%f", &aab_data2.aab_f);

	printf("Enter Double Value For 'aab_d' Of 'struct AAB_MyData aab_data2' : ");
	scanf("%lf", &aab_data2.aab_d);

	printf("Enter Character Value For 'aab_c' Of 'struct AAB_MyData aab_data2' : ");
	aab_data2.aab_c = getch();
	printf("%c", aab_data2.aab_c);

	printf("\n\n**** DATA 3 ****\n\n");
	printf("Enter Integer Value For 'aab_i' Of 'struct AAB_MyData aab_data3' : ");
	scanf("%d", &aab_data3.aab_i);

	printf("Enter Float Value For 'aab_f' Of 'struct AAB_MyData aab_data3' : ");
	scanf("%f", &aab_data3.aab_f);

	printf("Enter Double Value For 'aab_d' Of 'struct AAB_MyData aab_data3' : ");
	scanf("%lf", &aab_data3.aab_d);

	printf("Enter Character Value For 'aab_c' Of 'struct AAB_MyData aab_data3' : ");
	aab_data3.aab_c = getch();
	printf("%c", aab_data3.aab_c);

	aab_answer_data = AAB_AddStructMembers(aab_data1, aab_data2, aab_data3);

	printf("\n\n **** ANSWER ****\n\n");
	printf("aab_answer_data.aab_i = %d\n", aab_answer_data.aab_i);
	printf("aab_answer_data.aab_f = %f\n", aab_answer_data.aab_f);
	printf("aab_answer_data.aab_d = %lf\n", aab_answer_data.aab_d);

	aab_answer_data.aab_c = aab_data1.aab_c;
	printf("aab_answer_data.aab_c (from aab_data1) = %c\n\n", aab_answer_data.aab_c);

	aab_answer_data.aab_c = aab_data2.aab_c;
	printf("aab_answer_data.aab_c (from aab_data2) = %c\n\n", aab_answer_data.aab_c);

	aab_answer_data.aab_c = aab_data3.aab_c;
	printf("aab_answer_data.aab_c (from aab_data3) = %c\n\n", aab_answer_data.aab_c);

	return 0;
}

struct AAB_MyData AAB_AddStructMembers(struct AAB_MyData aab_md_one, struct AAB_MyData aab_md_two, struct AAB_MyData aab_md_three)
{
	struct AAB_MyData aab_answer;

	aab_answer.aab_i = aab_md_one.aab_i + aab_md_two.aab_i + aab_md_three.aab_i;
	aab_answer.aab_f = aab_md_one.aab_f + aab_md_two.aab_f + aab_md_three.aab_f;
	aab_answer.aab_d = aab_md_one.aab_d + aab_md_two.aab_d + aab_md_three.aab_d;

	return (aab_answer);
}
