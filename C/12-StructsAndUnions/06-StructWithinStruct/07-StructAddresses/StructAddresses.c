#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyData aab_data;

	aab_data.aab_i = 30;
	aab_data.aab_f = 11.45f;
	aab_data.aab_d = 1.2995;
	aab_data.aab_c = 'A';

	printf("\n\nData Members Of 'struct AAB_MyData' Are : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);
	printf("aab_c = %c\n", aab_data.aab_c);

	printf("\n\nAddress Of Data Members Of 'struct AAB_MyData' Are : \n\n");
	printf("'aab_i' Occupies Address From %p\n", &aab_data.aab_i);
	printf("'aab_f' Occupies Address From %p\n", &aab_data.aab_f);
	printf("'aab_d' Occupies Address From %p\n", &aab_data.aab_d);
	printf("'aab_c' Occupies Address From %p\n", &aab_data.aab_c);

	printf("Starting Address Of 'struct AAB_MyData' variable 'aab_data' = %p\n\n", &aab_data);

	return 0;
}

