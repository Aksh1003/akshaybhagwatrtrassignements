#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyStruct
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

union AAB_MyUnion
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyStruct aab_s;
	union AAB_MyUnion aab_u;

	printf("\n\nSize Of AAB_MyStruct = %lu\n", sizeof(aab_s));
	printf("\n\nSize Of AAB_MyUnion = %lu\n", sizeof(aab_u));

	return 0;
}

