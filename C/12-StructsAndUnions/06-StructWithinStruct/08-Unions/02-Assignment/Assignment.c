#include <stdio.h>

#define AAB_MAIN main

union AAB_MyUnion
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	union AAB_MyUnion aab_u1, aab_u2;

	printf("\n\nMembers Of Union aab_u1 Are : \n\n");
	aab_u1.aab_i = 6;
	aab_u1.aab_f = 1.2f;
	aab_u1.aab_d = 8.33333;
	aab_u1.aab_c = 'A';

	printf("aab_u1.aab_i = %d\n\n", aab_u1.aab_i);
	printf("aab_u1.aab_f = %f\n\n", aab_u1.aab_f);
	printf("aab_u1.aab_d = %lf\n\n", aab_u1.aab_d);
	printf("aab_u1.aab_c = %c\n\n", aab_u1.aab_c);

	printf("\nAddress Of Members Of Union aab_u1 Are : \n\n");
	printf("aab_u1.aab_i = %p\n\n", &aab_u1.aab_i);
	printf("aab_u1.aab_f = %p\n\n", &aab_u1.aab_f);
	printf("aab_u1.aab_d = %p\n\n", &aab_u1.aab_d);
	printf("aab_u1.aab_c = %p\n\n", &aab_u1.aab_c);

	printf("AAB_MyUnion aab_u1 = %p\n\n", &aab_u1);

	printf("\n\nMembers Of Union aab_u2 Are : \n\n");
	aab_u2.aab_i = 3;
	printf("aab_u2.aab_i = %d\n\n", aab_u2.aab_i);

	aab_u2.aab_f = 4.5f;
	printf("aab_u2.aab_f = %f\n\n", aab_u2.aab_f);

	aab_u2.aab_d = 5.123456;
	printf("aab_u2.aab_d = %lf\n\n", aab_u2.aab_d);

	aab_u2.aab_c = 'B';
	printf("aab_u2.aab_c = %c\n\n", aab_u2.aab_c);

	printf("\nAddress Of Members Of Union aab_u2 Are : \n\n");
	printf("aab_u2.aab_i = %p\n\n", &aab_u2.aab_i);
	printf("aab_u2.aab_f = %p\n\n", &aab_u2.aab_f);
	printf("aab_u2.aab_d = %p\n\n", &aab_u2.aab_d);
	printf("aab_u2.aab_c = %p\n\n", &aab_u2.aab_c);

	printf("AAB_MyUnion aab_u2 = %p\n\n", &aab_u2);

	return 0;
}

