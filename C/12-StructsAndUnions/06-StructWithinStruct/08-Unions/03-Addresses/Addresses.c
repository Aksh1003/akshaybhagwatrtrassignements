#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyStruct
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

union AAB_MyUnion
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyStruct aab_s;
	union AAB_MyUnion aab_u;

	printf("\n\nMembers Of Struct Are : \n\n");

	aab_s.aab_i = 9;
	aab_s.aab_f = 8.7f;
	aab_s.aab_d = 1.234567;
	aab_s.aab_c = 'A';

	printf("aab_s.aab_i = %d\n\n", aab_s.aab_i);
	printf("aab_s.aab_f = %f\n\n", aab_s.aab_f);
	printf("aab_s.aab_d = %lf\n\n", aab_s.aab_d);
	printf("aab_s.aab_c = %c\n\n", aab_s.aab_c);

	printf("Addresses Of Members Of Struct Are : \n\n");
	printf("aab_s.aab_i = %p\n\n", &aab_s.aab_i);
	printf("aab_s.aab_f = %p\n\n", &aab_s.aab_f);
	printf("aab_s.aab_d = %p\n\n", &aab_s.aab_d);
	printf("aab_s.aab_c = %p\n\n", &aab_s.aab_c);

	printf("AAB_MyStruct aab_s = %p\n\n", &aab_s);

	printf("\n\nMembers Of Union Are : \n\n");

	aab_u.aab_i = 3;
	printf("aab_u.aab_i = %d\n\n", aab_u.aab_i);

	aab_u.aab_f = 3.7f;
	printf("aab_u.aab_f = %f\n\n", aab_u.aab_f);

	aab_u.aab_d = 2.234567;
	printf("aab_u.aab_d = %lf\n\n", aab_u.aab_d);

	aab_u.aab_c = 'B';
	printf("aab_u.aab_c = %c\n\n", aab_u.aab_c);

	printf("Addresses Of Members Of Union Are : \n\n");
	printf("aab_u.aab_i = %p\n\n", &aab_u.aab_i);
	printf("aab_u.aab_f = %p\n\n", &aab_u.aab_f);
	printf("aab_u.aab_d = %p\n\n", &aab_u.aab_d);
	printf("aab_u.aab_c = %p\n\n", &aab_u.aab_c);

	printf("AAB_MyUnion aab_u = %p\n\n", &aab_u);

	return 0;
}
