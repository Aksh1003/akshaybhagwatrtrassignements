#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyNumber
{
	int aab_num;
	int aab_num_table[10];
};

struct AAB_NumTables
{
	struct AAB_MyNumber aab_n;
};

int AAB_MAIN()
{
	struct AAB_NumTables aab_tables[10];
	int aab_i, aab_j;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_tables[aab_i].aab_n.aab_num = (aab_i + 1);

	for (aab_i = 0; aab_i < 10; aab_i++)
	{
		printf("\n\nTable Of %d : \n\n", aab_tables[aab_i].aab_n.aab_num);
		
		for (aab_j = 0; aab_j < 10; aab_j++)
		{
			aab_tables[aab_i].aab_n.aab_num_table[aab_j] = aab_tables[aab_i].aab_n.aab_num * (aab_j + 1);
			printf("%d * %d = %d\n", aab_tables[aab_i].aab_n.aab_num, (aab_j + 1), aab_tables[aab_i].aab_n.aab_num_table[aab_j]);
		}
	}

	return 0;
}


