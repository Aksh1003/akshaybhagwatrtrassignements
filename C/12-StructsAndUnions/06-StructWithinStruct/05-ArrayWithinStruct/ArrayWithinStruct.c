#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyNumber
{
	int aab_num;
	int aab_num_table[10];
};

struct AAB_NumTables
{
	struct AAB_MyNumber a;
	struct AAB_MyNumber b;
	struct AAB_MyNumber c;
};

int AAB_MAIN()
{
	struct AAB_NumTables aab_tables;
	int aab_i;

	aab_tables.a.aab_num = 2;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_tables.a.aab_num_table[aab_i] = aab_tables.a.aab_num * (aab_i + 1);

	printf("\n\nTable Of %d : \n\n", aab_tables.a.aab_num);
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("%d * %d = %d\n", aab_tables.a.aab_num, (aab_i + 1), aab_tables.a.aab_num_table[aab_i]);

	aab_tables.b.aab_num = 3;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_tables.b.aab_num_table[aab_i] = aab_tables.b.aab_num * (aab_i + 1);

	printf("\n\nTable Of %d : \n\n", aab_tables.b.aab_num);
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("%d * %d = %d\n", aab_tables.b.aab_num, (aab_i + 1), aab_tables.b.aab_num_table[aab_i]);

	aab_tables.c.aab_num = 4;

	for (aab_i = 0; aab_i < 10; aab_i++)
		aab_tables.c.aab_num_table[aab_i] = aab_tables.c.aab_num * (aab_i + 1);

	printf("\n\nTable Of %d : \n\n", aab_tables.c.aab_num);
	for (aab_i = 0; aab_i < 10; aab_i++)
		printf("%d * %d = %d\n", aab_tables.c.aab_num, (aab_i + 1), aab_tables.c.aab_num_table[aab_i]);

	return 0;
}

