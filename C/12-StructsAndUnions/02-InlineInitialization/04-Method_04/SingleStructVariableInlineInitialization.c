#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyData aab_data_one = {35, 3.9f, 1.23765, 'A'};
	struct AAB_MyData aab_data_two = {'A', 6.9f, 11.23765, 65 };
	struct AAB_MyData aab_data_three = { 35, 'A' };
	struct AAB_MyData aab_data_four = { 35};

	printf("DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_data_one.aab_i);
	printf("aab_f = %f\n", aab_data_one.aab_f);
	printf("aab_d = %lf\n", aab_data_one.aab_d);
	printf("aab_c = %c\n\n", aab_data_one.aab_c);

	printf("aab_i = %d\n", aab_data_two.aab_i);
	printf("aab_f = %f\n", aab_data_two.aab_f);
	printf("aab_d = %lf\n", aab_data_two.aab_d);
	printf("aab_c = %c\n\n", aab_data_two.aab_c);

	printf("aab_i = %d\n", aab_data_three.aab_i);
	printf("aab_f = %f\n", aab_data_three.aab_f);
	printf("aab_d = %lf\n", aab_data_three.aab_d);
	printf("aab_c = %c\n\n", aab_data_three.aab_c);

	printf("aab_i = %d\n", aab_data_four.aab_i);
	printf("aab_f = %f\n", aab_data_four.aab_f);
	printf("aab_d = %lf\n", aab_data_four.aab_d);
	printf("aab_c = %c\n\n", aab_data_four.aab_c);

	return 0;
}