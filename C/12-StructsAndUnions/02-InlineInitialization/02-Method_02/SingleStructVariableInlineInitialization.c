#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

struct AAB_MyData aab_data = { 30, 4.5f, 11.451995, 'A' };

int AAB_MAIN()
{
	printf("DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);
	printf("aab_c = %c\n\n", aab_data.aab_c);

	return 0;
}