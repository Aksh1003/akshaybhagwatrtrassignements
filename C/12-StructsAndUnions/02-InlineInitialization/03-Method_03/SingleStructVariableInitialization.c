#include <stdio.h>

#define AAB_MAIN main

int AAB_MAIN()
{
	struct AAB_MyData
	{
		int aab_i;
		float aab_f;
		double aab_d;
		char aab_c;
	}aab_data = { 30, 4.5f, 11.451995, 'A' };

	printf("DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);
	printf("aab_c = %c\n\n", aab_data.aab_c);

	return 0;
}