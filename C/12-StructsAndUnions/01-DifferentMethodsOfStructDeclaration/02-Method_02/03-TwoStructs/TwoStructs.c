#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyPoint
{
	int aab_x;
	int aab_y;
};

struct AAB_MyPointProperties
{
	int aab_quadrant;
	char aab_axis_location[10];
};

struct AAB_MyPoint aab_point;

struct AAB_MyPointProperties aab_point_properties;

int AAB_MAIN()
{
	printf("\n\n");
	printf("Enter X-Coordinate For A Point : ");
	scanf("%d", &aab_point.aab_x);
	printf("Enter Y-Coordinate For A Point : ");
	scanf("%d", &aab_point.aab_y);

	printf("\n\n");
	printf("Point Co-ordinates (aab_x, aab_y) Are : (%d, %d) !!!\n\n", aab_point.aab_x, aab_point.aab_y);

	if (aab_point.aab_x == 0 && aab_point.aab_y == 0)
		printf("The Point Is The Origin (%d, %d) !!!\n", aab_point.aab_x, aab_point.aab_y);
	else
	{
		if (aab_point.aab_x == 0)
		{
			if (aab_point.aab_y < 0)
				strcpy(aab_point_properties.aab_axis_location, "Negative Y");

			if (aab_point.aab_y > 0)
				strcpy(aab_point_properties.aab_axis_location, "Positive Y");

			aab_point_properties.aab_quadrant = 0;

			printf("The Point Lies On The %s Axis !!!\n\n", aab_point_properties.aab_axis_location);
		}
		else if (aab_point.aab_y == 0)
		{
			if (aab_point.aab_x < 0) 
				strcpy(aab_point_properties.aab_axis_location, "Negative X");

			if (aab_point.aab_x > 0) 
				strcpy(aab_point_properties.aab_axis_location, "Positive X");

			aab_point_properties.aab_quadrant = 0;

			printf("The Point Lies On The %s Axis !!!\n\n", aab_point_properties.aab_axis_location);
		}
		else
		{
			aab_point_properties.aab_axis_location[0] = '\0'; 

			if (aab_point.aab_x > 0 && aab_point.aab_y > 0) 
				aab_point_properties.aab_quadrant = 1;
			else if (aab_point.aab_x < 0 && aab_point.aab_y > 0) 
				aab_point_properties.aab_quadrant = 2;
			else if (aab_point.aab_x < 0 && aab_point.aab_y < 0) 
				aab_point_properties.aab_quadrant = 3;
			else 
				aab_point_properties.aab_quadrant = 4;

			printf("The Point Lies In Quadrant Number %d !!!\n\n", aab_point_properties.aab_quadrant);
		}
	}

	return 0;
}