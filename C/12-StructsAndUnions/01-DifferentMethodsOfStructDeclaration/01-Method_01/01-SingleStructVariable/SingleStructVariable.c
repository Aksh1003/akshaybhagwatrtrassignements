#include <stdio.h>

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
} aab_data; //Declaring a single struct variable of type 'struct AAB_MyData' globally ...

int main(void)
{
	//variable declarations
	int aab_i_size;
	int aab_f_size;
	int aab_d_size;
	int aab_struct_AAB_MyData_size;
	
	//code
	//Assigning Data Values To The Data Members Of 'struct AAB_MyData'
	aab_data.aab_i = 30;
	aab_data.aab_f = 11.45f;
	aab_data.aab_d = 1.2995;
	
	//Displaying Values Of The Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);

	//Calculating Sizes (In Bytes) Of The Data Members Of 'struct MyData'
	aab_i_size = sizeof(aab_data.aab_i);
	aab_f_size = sizeof(aab_data.aab_f);
	aab_d_size = sizeof(aab_data.aab_d);

	//Displaying Sizes (In Bytes) Of The Data Members Of 'struct AAB_MyData'
	printf("\n\n");
	printf("SIZES (in bytes) OF DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("Size of 'aab_i' = %d bytes\n", aab_i_size);
	printf("Size of 'aab_f' = %d bytes\n", aab_f_size);
	printf("Size of 'aab_d' = %d bytes\n", aab_d_size);

	//Calculating Size (In Bytes) Of the entire 'struct AAB_Mydata'
	aab_struct_AAB_MyData_size = sizeof(struct AAB_MyData); //can also give struct name -> sizeof(AAB_MyData)

	//Displaying Sizes (In Bytes) Of the entire 'struct AAB_Mydata'
	printf("\n\n");
	printf("Size of 'struct AAB_MyData' : %d bytes\n\n", aab_struct_AAB_MyData_size);

	return(0);
}
