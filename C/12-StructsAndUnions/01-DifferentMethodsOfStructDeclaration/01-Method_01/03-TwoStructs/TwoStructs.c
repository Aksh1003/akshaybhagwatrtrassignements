#include <stdio.h>

struct AAB_MyPoint
{
	int aab_x;
	int aab_y;
} aab_point;

struct AAB_MyPointProperties
{
	int aab_quadrant;
	char aab_axis_location[10];
} aab_point_properties; 

int main(void)
{
	//code
	printf("\n\n");
	printf("Enter X-Coordinate For A Point : ");
	scanf("%d", &aab_point.aab_x);
	printf("Enter Y-Coordinate For A Point : ");
	scanf("%d", &aab_point.aab_y);

	printf("\n\n");
	printf("Point Co-ordinates (aab_x, aab_y) Are : (%d, %d) !!!\n\n", aab_point.aab_x, aab_point.aab_y);

	if (aab_point.aab_x == 0 && aab_point.aab_y == 0)
		printf("The Point Is The Origin (%d, %d) !!!\n", aab_point.aab_x, aab_point.aab_y);
	else // Atleast One of the two values (either 'X' or 'Y' or BOTH) is a nonzero value...
	{
		if (aab_point.aab_x == 0) // If 'X' IS ZERO...OBVIOUSLY 'Y' IS THE NON-ZERO VALUE
		{
			if (aab_point.aab_y < 0) // If 'Y' IS -ve
				strcpy(aab_point_properties.aab_axis_location, "Negative Y");
			
			if (aab_point.aab_y > 0) // If 'Y' IS +ve
				strcpy(aab_point_properties.aab_axis_location, "Positive Y");
			
			aab_point_properties.aab_quadrant = 0; // A Point Lying On Any Of The Coordinate Axes Is NOT A Part Of ANY Quadrant...

			printf("The Point Lies On The %s Axis !!!\n\n", aab_point_properties.aab_axis_location);
		}
		else if (aab_point.aab_y == 0) // If 'Y' IS ZERO...OBVIOUSLY 'X' IS THE NON-ZERO VALUE
		{
			if (aab_point.aab_x < 0) // If 'X' IS -ve
				strcpy(aab_point_properties.aab_axis_location, "Negative X");

			if (aab_point.aab_x > 0) // If 'X' IS +ve
				strcpy(aab_point_properties.aab_axis_location, "Positive X");

			aab_point_properties.aab_quadrant = 0; // A Point Lying On Any Of The Coordinate Axes Is NOT A Part Of ANY Quadrant...

			printf("The Point Lies On The %s Axis !!!\n\n", aab_point_properties.aab_axis_location);
		}
		else // BOTH 'X' AND 'Y' ARE NON-ZERO
		{
			aab_point_properties.aab_axis_location[0] = '\0'; // A Point Lying In ANY Of The 4 Quadrants Cannot Be Lying On Any Of The Co - ordinate Axes...

			if (aab_point.aab_x > 0 && aab_point.aab_y > 0) // 'X' IS +ve AND 'Y' IS +ve
				aab_point_properties.aab_quadrant = 1;
			else if (aab_point.aab_x < 0 && aab_point.aab_y > 0) // 'X' IS -ve AND 'Y' IS +ve
				aab_point_properties.aab_quadrant = 2;
			else if (aab_point.aab_x < 0 && aab_point.aab_y < 0) // 'X' IS -ve AND 'Y' IS -ve
				aab_point_properties.aab_quadrant = 3;
			else // 'X' IS +ve AND 'Y' IS -ve
				aab_point_properties.aab_quadrant = 4;
			
			printf("The Point Lies In Quadrant Number %d !!!\n\n", aab_point_properties.aab_quadrant);
		}
	}

	return(0);
}
