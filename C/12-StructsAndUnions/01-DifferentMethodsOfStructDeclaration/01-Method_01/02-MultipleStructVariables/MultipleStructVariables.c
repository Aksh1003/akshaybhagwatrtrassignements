#include <stdio.h>

struct AAB_MyPoint
{
	int aab_x;
	int aab_y;
} aab_point_A, aab_point_B, aab_point_C, aab_point_D, aab_point_E; //Declaring 5 struct variables of type 'struct AAB_MyPoint' globally...

int main(void)
{
	//code
	//Assigning Data Values To The Data Members Of 'struct MyPoint' variable 'point_A'
	aab_point_A.aab_x = 3;
	aab_point_A.aab_y = 0;
	
	//Assigning Data Values To The Data Members Of 'struct MyPoint' variable 'point_B'
	aab_point_B.aab_x = 1;
	aab_point_B.aab_y = 2;
	
	//Assigning Data Values To The Data Members Of 'struct MyPoint' variable 'point_C'
	aab_point_C.aab_x = 9;
	aab_point_C.aab_y = 6;
	
	//Assigning Data Values To The Data Members Of 'struct MyPoint' variable 'point_D'
	aab_point_D.aab_x = 8;
	aab_point_D.aab_y = 2;
	
	//Assigning Data Values To The Data Members Of 'struct MyPoint' variable 'point_E'
	aab_point_E.aab_x = 11;
	aab_point_E.aab_y = 8;
	
	//Displaying Values Of The Data Members Of 'struct MyPoint' (all variables)
	printf("\n\n");
	printf("Co-ordinates (aab_x, aab_y) Of Point 'A' Are : (%d, %d)\n\n", aab_point_A.aab_x, aab_point_A.aab_y);
	printf("Co-ordinates (aab_x, aab_y) Of Point 'B' Are : (%d, %d)\n\n", aab_point_B.aab_x, aab_point_B.aab_y);
	printf("Co-ordinates (aab_x, aab_y) Of Point 'C' Are : (%d, %d)\n\n", aab_point_C.aab_x, aab_point_C.aab_y);
	printf("Co-ordinates (aab_x, aab_y) Of Point 'D' Are : (%d, %d)\n\n", aab_point_D.aab_x, aab_point_D.aab_y);
	printf("Co-ordinates (aab_x, aab_y) Of Point 'E' Are : (%d, %d)\n\n", aab_point_E.aab_x, aab_point_E.aab_y);

	return(0);
}
