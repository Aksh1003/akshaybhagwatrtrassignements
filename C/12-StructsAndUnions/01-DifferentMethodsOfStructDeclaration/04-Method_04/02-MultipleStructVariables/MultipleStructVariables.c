#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyPoint
{
	int aab_x;
	int aab_y;
};

int AAB_MAIN()
{
	struct AAB_MyPoint aab_point_A, aab_point_B, aab_point_C, aab_point_D, aab_point_E;

	aab_point_A.aab_x = 3;
	aab_point_A.aab_y = 0;

	aab_point_B.aab_x = 1;
	aab_point_B.aab_y = 2;

	aab_point_C.aab_x = 9;
	aab_point_C.aab_y = 6;

	aab_point_D.aab_x = 8;
	aab_point_D.aab_y = 2;

	aab_point_E.aab_x = 11;
	aab_point_E.aab_y = 8;

	printf("Co-ordinates (x, y) Of Point 'A' Are : (%d, %d)\n\n", aab_point_A.aab_x, aab_point_A.aab_y);
	printf("Co-ordinates (x, y) Of Point 'B' Are : (%d, %d)\n\n", aab_point_B.aab_x, aab_point_B.aab_y);
	printf("Co-ordinates (x, y) Of Point 'C' Are : (%d, %d)\n\n", aab_point_C.aab_x, aab_point_C.aab_y);
	printf("Co-ordinates (x, y) Of Point 'D' Are : (%d, %d)\n\n", aab_point_D.aab_x, aab_point_D.aab_y);
	printf("Co-ordinates (x, y) Of Point 'E' Are : (%d, %d)\n\n", aab_point_E.aab_x, aab_point_E.aab_y);

	return 0;
}