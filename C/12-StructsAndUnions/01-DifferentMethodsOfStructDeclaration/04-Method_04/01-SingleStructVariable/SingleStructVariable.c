#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
};

int AAB_MAIN()
{
	struct AAB_MyData aab_data;

	int aab_i_size;
	int aab_f_size;
	int aab_d_size;
	int aab_struct_AAB_MyData_size;

	aab_data.aab_i = 30;
	aab_data.aab_f = 11.45f;
	aab_data.aab_d = 1.2995;

	printf("\n\nData Members Of 'struct AAB_MyData' Are : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);

	aab_i_size = sizeof(aab_data.aab_i);
	aab_f_size = sizeof(aab_data.aab_f);
	aab_d_size = sizeof(aab_data.aab_d);

	printf("\n\nSizes Of Data Members Of 'struct AAB_MyData' Are : \n");
	printf("Size Of 'aab_i' = %d bytes\n", aab_i_size);
	printf("Size Of 'aab_f' = %d bytes\n", aab_f_size);
	printf("Size Of 'aab_d' = %d bytes\n", aab_d_size);

	aab_struct_AAB_MyData_size = sizeof(struct AAB_MyData);

	printf("\n\nSize Of 'struct AAB_MyData' = %d bytes\n\n", aab_struct_AAB_MyData_size);

	return 0;
}