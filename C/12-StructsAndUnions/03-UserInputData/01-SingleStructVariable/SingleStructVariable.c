#include <stdio.h>

#define AAB_MAIN main

struct AAB_MyData
{
	int aab_i;
	float aab_f;
	double aab_d;
	char aab_c;
};

int AAB_MAIN()
{
	struct AAB_MyData aab_data;

	printf("Enter Integer Value For Data Member 'aab_i' Of 'struct AAB_MyData' : \n");
	scanf("%d", &aab_data.aab_i);
	printf("Enter Floating-Point Value For Data Member 'aab_f' Of 'struct AAB_MyData' : \n");
	scanf("%f", &aab_data.aab_f);
	printf("Enter 'Double' Value For Data Member 'aab_d' Of 'struct AAB_MyData' : \n");
	scanf("%lf", &aab_data.aab_d);
	printf("Enter Character Value For Data Member 'aab_c' Of 'struct MyData' : \n");
	aab_data.aab_c = getch();

	printf("DATA MEMBERS OF 'struct AAB_MyData' ARE : \n\n");
	printf("aab_i = %d\n", aab_data.aab_i);
	printf("aab_f = %f\n", aab_data.aab_f);
	printf("aab_d = %lf\n", aab_data.aab_d);
	printf("aab_c = %c\n\n", aab_data.aab_c);

	return 0;
}
