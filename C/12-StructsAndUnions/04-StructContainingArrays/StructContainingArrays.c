#include <stdio.h>

#define AAB_MAIN main
#define AAB_INT_ARRAY_SIZE 10
#define AAB_FLOAT_ARRAY_SIZE 5
#define AAB_CHAR_ARRAY_SIZE 26
#define AAB_NUM_STRINGS 10
#define AAB_MAX_CHARACTERS_PER_STRING 20
#define AAB_ALPHABET_BEGINING 65

struct AAB_MyDataOne
{
	int aab_iArray[AAB_INT_ARRAY_SIZE];
	float aab_fArray[AAB_FLOAT_ARRAY_SIZE];
};

struct AAB_MyDataTwo
{
	char aab_cArray[AAB_CHAR_ARRAY_SIZE];
	char aab_strArray[AAB_NUM_STRINGS][AAB_MAX_CHARACTERS_PER_STRING];
};

int AAB_MAIN()
{
	struct AAB_MyDataOne aab_data_one;
	struct AAB_MyDataTwo aab_data_two;
	int aab_i;

	aab_data_one.aab_fArray[0] = 0.1f;
	aab_data_one.aab_fArray[1] = 1.1f;
	aab_data_one.aab_fArray[2] = 2.1f;
	aab_data_one.aab_fArray[3] = 3.1f;
	aab_data_one.aab_fArray[4] = 4.1f;

	printf("\n\nEnter %d Integers : \n\n", AAB_INT_ARRAY_SIZE);
	for (aab_i = 0; aab_i < AAB_INT_ARRAY_SIZE; aab_i++)
		aab_data_one.aab_iArray[aab_i] = rand();

	for (aab_i = 0; aab_i < AAB_CHAR_ARRAY_SIZE; aab_i++)
		aab_data_two.aab_cArray[aab_i] = (char)(aab_i + AAB_ALPHABET_BEGINING);

	strcpy(aab_data_two.aab_strArray[0], "Welcome ");
	strcpy(aab_data_two.aab_strArray[1], "My ");
	strcpy(aab_data_two.aab_strArray[2], "Name ");
	strcpy(aab_data_two.aab_strArray[3], "Is ");
	strcpy(aab_data_two.aab_strArray[4], "Akshay ");
	strcpy(aab_data_two.aab_strArray[5], "Anil ");
	strcpy(aab_data_two.aab_strArray[6], "Bhagwat ");
	strcpy(aab_data_two.aab_strArray[7], "Thank ");
	strcpy(aab_data_two.aab_strArray[8], "You ");
	strcpy(aab_data_two.aab_strArray[9], "Bye ! ");

	printf("\n\nMembers Of 'struct AAB_MyDataOne' Alongwith Their Assigned Values Are : \n\n");

	printf("Integers Array (aab_data_one.aab_iArray[]) : \n\n");
	for (aab_i = 0; aab_i < AAB_INT_ARRAY_SIZE; aab_i++)
		printf("aab_data_one.aab_iArray[%d] = %d\n", aab_i, aab_data_one.aab_iArray[aab_i]);

	printf("Floating-Point Array (aab_data_one.aab_fArray[]) : \n\n");
	for (aab_i = 0; aab_i < AAB_FLOAT_ARRAY_SIZE; aab_i++)
		printf("aab_data_one.aab_fArray[%d] = %f\n", aab_i, aab_data_one.aab_fArray[aab_i]);

	printf("\n\nMembers Of 'struct AAB_MyDataTwo' Alongwith Their Assigned Values Are : \n\n");

	printf("Character Array (aab_data_two.aab_cArray[]) : \n\n");
	for (aab_i = 0; aab_i < AAB_CHAR_ARRAY_SIZE; aab_i++)
		printf("aab_data_two.aab_cArray[%d] = %c\n", aab_i, aab_data_two.aab_cArray[aab_i]);

	printf("String Array (aab_data_two.aab_strArray[]) : \n\n");
	for (aab_i = 0; aab_i < AAB_NUM_STRINGS; aab_i++)
		printf("aab_data_two.aab_strArray[%d] = %s\n", aab_i, aab_data_two.aab_strArray[aab_i]);

	printf("\n\n");

	return 0;
}