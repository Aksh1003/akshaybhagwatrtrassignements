#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<GL/gl.h> // for OPenGL
#include<GL/glx.h>	// for glx API
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// namespaces
 using namespace std;

#define pi 3.14

// global variable
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

// entry-point function
int main()
{
	// function declaration
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void Initialize(void);
	void Resize(int, int);
	void Draw(void);
	void UnInitialize();

	// variable declaration
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	// code
	CreateWindow();

	Initialize();

	// message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
				break;

				case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);

				switch(keysym)
				{
					case XK_Escape:
					bDone = true;
					break;

					case XK_F:
					case XK_f:
					if(bFullscreen == false)
					{
						ToggleFullscreen();
						bFullscreen = true;
					}
					else
					{
						ToggleFullscreen();
						bFullscreen = false;
					}
					break;

					default:
					break;
				}
				break;

				case MotionNotify:
				break;

				case ConfigureNotify:
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;

				case Expose:
				break;

				case DestroyNotify:
				break;

				case 33:
				bDone = true;
				break;
			}
		}
		Draw();
	}

	UnInitialize();

	return(0);
}

void CreateWindow(void)
{
	// function declaration
	void UnInitialize(void);

	// variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = {GLX_DOUBLEBUFFER,
											True,
											GLX_RGBA, 
											GLX_RED_SIZE, 8,
											GLX_GREEN_SIZE, 8,
											GLX_BLUE_SIZE, 8,
											GLX_ALPHA_SIZE, 8,
											0/*or None*/}; // coventional but not compulsary static keyword

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display.\nExitting Now...\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
							RootWindow(gpDisplay, gpXVisualInfo->screen),
							gpXVisualInfo->visual,
							AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window.\nExitting Now...\n");
        UnInitialize();
        exit(1);
	}

	XStoreName(gpDisplay, gWindow, "Akshay Bhagwat");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
	// variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{
	void Resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	Resize(giWindowWidth, giWindowHeight);
}

void Resize(int width, int height)
{
	if(height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Draw(void)
{
	// function declaration

	// local variable
	GLfloat axis;
	int i = 0;

	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.1f, 0.0f, -3.0f);

	glBegin(GL_QUADS);

		// I
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.3f, 0.5f, 0.0f);
		glVertex3f(-1.5f, 0.5f, 0.0f);

		glVertex3f(-1.5f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-1.5f, -0.5f, 0.0f);

		glVertex3f(-1.5f, -0.5f, 0.0f);
		glVertex3f(-1.3f, -0.5f, 0.0f);

		glVertex3f(-1.3f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.3f, 0.5f, 0.0f);

		// white quad color for I

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1.3f, 0.1f, 0.0f);
		glVertex3f(-1.5f, 0.1f, 0.0f);

		glVertex3f(-1.5f, 0.1f, 0.0f);
		glVertex3f(-1.5f, -0.1f, 0.0f);

		glVertex3f(-1.5f, -0.1f, 0.0f);
		glVertex3f(-1.3f, -0.1f, 0.0f);

		glVertex3f(-1.3f, -0.1f, 0.0f);
		glVertex3f(-1.3f, 0.1f, 0.0f);

		// N
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		glVertex3f(-1.2f, 0.5f, 0.0f);

		glVertex3f(-1.2f, 0.5f, 0.0f);
 glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-1.2f, -0.5f, 0.0f);

		glVertex3f(-1.2f, -0.5f, 0.0f);
		glVertex3f(-1.0f, -0.5f, 0.0f);

		glVertex3f(-1.0f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		
		// white quad color for N 1

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1.0f, 0.1f, 0.0f);
		glVertex3f(-1.2f, 0.1f, 0.0f);

		glVertex3f(-1.2f, 0.1f, 0.0f);
		glVertex3f(-1.2f, -0.1f, 0.0f);

		glVertex3f(-1.2f, -0.1f, 0.0f);
		glVertex3f(-1.0f, -0.1f, 0.0f);

		glVertex3f(-1.0f, -0.1f, 0.0f);
		glVertex3f(-1.0f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.2f, 0.0f);

		glVertex3f(-1.0f, 0.2f, 0.0f);
 glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.6f, -0.5f, 0.0f);

		glVertex3f(-0.6f, -0.5f, 0.0f);
		glVertex3f(-0.6f, -0.2f, 0.0f);

		glVertex3f(-0.6f, -0.2f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		
		// white quad color for N 2

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.77f, 0.1f, 0.0f);
		glVertex3f(-0.945f, 0.1f, 0.0f);

		glVertex3f(-0.945f, 0.1f, 0.0f);
		glVertex3f(-0.83f, -0.1f, 0.0f);

		glVertex3f(-0.83f, -0.1f, 0.0f);
		glVertex3f(-0.655f, -0.1f, 0.0f);

		glVertex3f(-0.655f, -0.1f, 0.0f);
		glVertex3f(-0.77f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.4f, 0.5f, 0.0f);
		glVertex3f(-0.6f, 0.5f, 0.0f);

		glVertex3f(-0.6f, 0.5f, 0.0f);
 glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.6f, -0.5f, 0.0f);

		glVertex3f(-0.6f, -0.5f, 0.0f);
		glVertex3f(-0.4f, -0.5f, 0.0f);

		glVertex3f(-0.4f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.4f, 0.5f, 0.0f);

		// white quad color for N 3

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.4f, 0.1f, 0.0f);
		glVertex3f(-0.6f, 0.1f, 0.0f);

		glVertex3f(-0.6f, 0.1f, 0.0f);
		glVertex3f(-0.6f, -0.1f, 0.0f);

		glVertex3f(-0.6f, -0.1f, 0.0f);
		glVertex3f(-0.4f, -0.1f, 0.0f);

		glVertex3f(-0.4f, -0.1f, 0.0f);
		glVertex3f(-0.4f, 0.1f, 0.0f);

		// D
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);

		glVertex3f(-0.3f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

		glVertex3f(-0.3f, -0.5f, 0.0f);
		glVertex3f(-0.1f, -0.5f, 0.0f);

		glVertex3f(-0.1f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);

		// white quad for D 1

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.1f, 0.1f, 0.0f);
		glVertex3f(-0.3f, 0.1f, 0.0f);

		glVertex3f(-0.3f, 0.1f, 0.0f);
		glVertex3f(-0.3f, -0.1f, 0.0f);

		glVertex3f(-0.3f, -0.1f, 0.0f);
		glVertex3f(-0.1f, -0.1f, 0.0f);

		glVertex3f(-0.1f, -0.1f, 0.0f);
		glVertex3f(-0.1f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.5f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);

		glVertex3f(0.1f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);

		glVertex3f(0.1f, -0.5f, 0.0f);
		glVertex3f(0.3f, -0.5f, 0.0f);

		glVertex3f(0.3f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.5f, 0.0f);

		// white quad for D 2

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(0.3f, 0.1f, 0.0f);
		glVertex3f(0.1f, 0.1f, 0.0f);

		glVertex3f(0.1f, 0.1f, 0.0f);
		glVertex3f(0.1f, -0.1f, 0.0f);

		glVertex3f(0.1f, -0.1f, 0.0f);
		glVertex3f(0.3f, -0.1f, 0.0f);

		glVertex3f(0.3f, -0.1f, 0.0f);
		glVertex3f(0.3f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);

		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.35f, 0.0f);

		glVertex3f(-0.1f, 0.35f, 0.0f);
		glVertex3f(0.1f, 0.35f, 0.0f);

		glVertex3f(0.1f, 0.35f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);

		//

	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);
		glVertex3f(0.1f, -0.35f, 0.0f);

		glVertex3f(0.1f, -0.35f, 0.0f);
		glVertex3f(-0.1f, -0.35f, 0.0f);

		glVertex3f(-0.1f, -0.35f, 0.0f);
		glVertex3f(-0.1f, -0.5f, 0.0f);

		glVertex3f(-0.1f, -0.5f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);

		// I
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);
		glVertex3f(0.4f, 0.5f, 0.0f);

		glVertex3f(0.4f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.4f, -0.5f, 0.0f);

		glVertex3f(0.4f, -0.5f, 0.0f);
		glVertex3f(0.6f, -0.5f, 0.0f);

		glVertex3f(0.6f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);

		// white quad for I

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(0.6f, 0.1f, 0.0f);
		glVertex3f(0.4f, 0.1f, 0.0f);

		glVertex3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.4f, -0.1f, 0.0f);

		glVertex3f(0.4f, -0.1f, 0.0f);
		glVertex3f(0.6f, -0.1f, 0.0f);

		glVertex3f(0.6f, -0.1f, 0.0f);
		glVertex3f(0.6f, 0.1f, 0.0f);

		// A
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.1f, 0.5f, 0.0f);
		glVertex3f(0.9f, 0.5f, 0.0f);

		glVertex3f(0.9f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.7f, -0.5f, 0.0f);

		glVertex3f(0.7f, -0.5f, 0.0f);
		glVertex3f(0.9f, -0.5f, 0.0f);

		glVertex3f(0.9f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.1f, 0.5f, 0.0f);
		
		// white quad for A 1

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(1.02f, 0.1f, 0.0f);
		glVertex3f(0.82f, 0.1f, 0.0f);

		glVertex3f(0.82f, 0.1f, 0.0f);
		glVertex3f(0.78f, -0.1f, 0.0f);

		glVertex3f(0.78f, -0.1f, 0.0f);
		glVertex3f(0.98, -0.1f, 0.0f);

		glVertex3f(0.98f, -0.1f, 0.0f);
		glVertex3f(1.02f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.2f, 0.5f, 0.0f);
		glVertex3f(1.0f, 0.5f, 0.0f);

		glVertex3f(1.0f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(1.2f, -0.5f, 0.0f);

		glVertex3f(1.2f, -0.5f, 0.0f);
		glVertex3f(1.4f, -0.5f, 0.0f);

		glVertex3f(1.4f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.2f, 0.5f, 0.0f);

		// white quad for A 2

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(1.277f, 0.1f, 0.0f);
		glVertex3f(1.08f, 0.1f, 0.0f);

		glVertex3f(1.08f, 0.1f, 0.0f);
		glVertex3f(1.12f, -0.1f, 0.0f);

		glVertex3f(1.12f, -0.1f, 0.0f);
		glVertex3f(1.32f, -0.1f, 0.0f);

		glVertex3f(1.32f, -0.1f, 0.0f);
		glVertex3f(1.277f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.09f, 0.05f, 0.0f);
		glVertex3f(1.01f, 0.05f, 0.0f);

		glVertex3f(1.01f, 0.05f, 0.0f);
	 glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(1.0f - 0.01f, -0.05f, 0.0f);

		glVertex3f(1.0f - 0.01f, -0.05f, 0.0f);
		glVertex3f(1.11f, -0.05f, 0.0f);

		glVertex3f(1.11f, -0.05f, 0.0f);
	 glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.09f, 0.05f, 0.0f);

		// white quad for A 3

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(1.1f, 0.01f, 0.0f);
		glVertex3f(1.01f, 0.01f, 0.0f);

		glVertex3f(1.01f, 0.01f, 0.0f);
		glVertex3f(0.81f, -0.05f, 0.0f);

		glVertex3f(0.81f, -0.01f, 0.0f);
		glVertex3f(1.11f, -0.01f, 0.0f);

		glVertex3f(1.11f, -0.01f, 0.0f);
		glVertex3f(1.1f, 0.01f, 0.0f);

	glEnd();

	// Ashoka Chakra
	glLoadIdentity();

	glTranslatef(0.1f, 0.0f, -3.0f);

	glColor3f(0.0f, 0.0f, 1.0f);

	glBegin(GL_POINTS);

		for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
		{
			glVertex3f((0.1f * cos(axis)), (0.1f * sin(axis)), 0.0f);
		}

	glEnd();

	glBegin(GL_LINES);

		for (axis = 0.0f; axis <= 2 * pi && (i < 13); axis += 0.24f, i++)
		{
			glVertex3f(0.1f * cos(axis), -0.1f * sin(axis), 0.0f);
			glVertex3f(-0.1f * cos(axis), 0.1f * sin(axis), 0.0f);
		}
		
	glEnd();

	glXSwapBuffers(gpDisplay, gWindow);
}

void UnInitialize(void)
{
	GLXContext currentGLXContext;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
}
