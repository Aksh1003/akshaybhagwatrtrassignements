// headers
#include<windows.h>
#include<stdio.h>
#include"Icon.h"
#include<gl/GL.h>
#include<gl/GLU.h>
#include<math.h>

// macros
#pragma comment(lib, "OPENGL32.LIB")
#pragma comment(lib, "GLU32.LIB")
#pragma comment(lib, "Winmm")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variables
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLfloat pi = 3.14f;
GLfloat axis;
bool flag = true;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize();
	void Display();

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.TXT", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
							szAppName,
							TEXT("AKSHAY BHAGWAT"),
							WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
							((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
							WIN_WIDTH,
							WIN_HEIGHT,
							NULL,
							NULL,
							hInstance,
							NULL);
	
	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen();
	void Resize(int, int);
	void UnInitialize();

	// local variable

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

}

void Initialize()
{
	// function declaration
	void Resize(int, int);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	// function declaration

	// local variable

	// code
	if (height == 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void Display()
{
	// function declaration
	void GetPlane();
	void GetPlaneFire();
	void GetSmoke();
	void GetIAF();

	// local variable
	GLfloat val = 0.001500f, val1 = 0.0013;
	int i = 0;
	static GLfloat x = 3.5f, y = 2.0f, z = -6.0f, z1 = -150.0f, z2 = -150.0f, x1 = 6.0f, y1 = 4.5f;
	static GLfloat star = 2.0f, mo = 0.9, mo1 = 1.75f, mo2 = 0.95f;

	// code
	if (flag == true)
	{
		PlaySound(TEXT("MeraRangDeBasantiChola.wav"), NULL, SND_ASYNC);
		flag = false;
	}

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	// moon

	if (mo1 <= 2.0)
		glTranslatef(mo1, mo2, -3.0f);

	mo -= 0.00010f;
	mo1 += 0.000348f;
	mo2 += 0.000348f;

	glColor3f(mo + 0.5f, mo, mo);

	glBegin(GL_LINES);

	for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
	{
		glVertex3f(0.15 * cos(-axis), 0.15 * sin(axis), 0.0f);
		glVertex3f(-0.15 * cos(-axis), 0.15 * sin(axis), 0.0f);
	}


	glEnd();

	glLoadIdentity();
	glTranslatef(2.0f, 1.2f, -3.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glBegin(GL_LINES);

	for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
	{
		glVertex3f(0.15 * cos(-axis), 0.15 * sin(axis), 0.0f);
		glVertex3f(-0.15 * cos(-axis), 0.15 * sin(axis), 0.0f);
	}

	glEnd();

	// stars

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor3f(0.5f, 0.5f, 0.5f);

	glPointSize(3.0f);

	if (star <= -star)
		star = 2.0f;
	else
		glTranslatef(-star, -star, -3.0f);

	glBegin(GL_POINTS);

		glVertex3f(1.5f, 0.5f, 0.0f);
		glVertex3f(-1.5f, 0.5f, 0.0f);
		glVertex3f(-1.5f, -0.5f, 0.0f);
		glVertex3f(1.5f, -0.5f, 0.0f);

		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glVertex3f(1.3f, -1.0f, 0.0f);
		glVertex3f(-1.3f, -1.0f, 0.0f);
		glVertex3f(-1.3f, 1.0f, 0.0f);
		glVertex3f(1.3f, 1.0f, 0.0f);

		glVertex3f(0.5f, -0.5f, 0.0f);
		glVertex3f(-0.5f, -0.5f, 0.0f);
		glVertex3f(-0.5f, 0.5f, 0.0f);
		glVertex3f(0.5f, 0.5f, 0.0f);

		glVertex3f(0.8f, 0.4f, 0.0f);
		glVertex3f(-0.8f, 1.4f, 0.0f);
		glVertex3f(0.8f, -0.4f, 0.0f);
		glVertex3f(-0.8f, -1.4f, 0.0f);

		glVertex3f(1.8f, 1.2f, 0.0f);
		glVertex3f(-1.8f, 1.2f, 0.0f);
		glVertex3f(1.8f, -1.2f, 0.0f);
		glVertex3f(-1.8f, -1.2f, 0.0f);

		glVertex3f(0.2f, 0.0129f, 0.0f);
		glVertex3f(-0.5f, 0.049f, 0.0f);
		glVertex3f(-0.6f, 0.209f, 0.0f);
		glVertex3f(0.3f, 0.9f, 0.0f);

		glVertex3f(0.0f, 0.9f, 0.0f);
		glVertex3f(0.3f, 0.0f, 0.0f);
		glVertex3f(-0.3f, -0.1111f, 0.0f);
		glVertex3f(2.0f, -0.2f, 0.0f);
		glVertex3f(-1.3f, 1.4f, 0.0f);
		glVertex3f(0.0f, -1.5f, 0.0f);

		glVertex3f(-1.8f, 0.0f, 0.0f);
		glVertex3f(0.3f, -1.0f, 0.0f);
		glVertex3f(-0.3f, -1.0f, 0.0f);

		glVertex3f(-3.3f, -3.3f, 0.0f);
		glVertex3f(3.3f, -3.3f, 0.0f);
		glVertex3f(3.3f, 3.3f, 0.0f);
		glVertex3f(-3.3f, 3.3f, 0.0f);

		glVertex3f(-2.5f, -2.5f, 0.0f);
		glVertex3f(2.5f, -2.5f, 0.0f);
		glVertex3f(2.5f, 2.5f, 0.0f);
		glVertex3f(-2.5f, 2.5f, 0.0f);

		glVertex3f(-3.0f, -3.0f, 0.0f);
		glVertex3f(3.0f, -3.0f, 0.0f);
		glVertex3f(3.0f, 3.0f, 0.0f);
		glVertex3f(-3.0f, 3.0f, 0.0f);

		glVertex3f(-3.2f, -2.0f, 0.0f);
		glVertex3f(3.2f, -2.0f, 0.0f);
		glVertex3f(3.2f, 2.0f, 0.0f);
		glVertex3f(-3.2f, 2.0f, 0.0f);

		glVertex3f(-2.7f, -2.7f, 0.0f);
		glVertex3f(2.7f, -2.7f, 0.0f);
		glVertex3f(2.7f, 2.7f, 0.0f);
		glVertex3f(-2.7f, 2.7f, 0.0f);

		glVertex3f(1.5f, 2.1f, 0.0f);
		glVertex3f(-1.5f, 2.1f, 0.0f);
		glVertex3f(-1.5f, -2.1f, 0.0f);
		glVertex3f(1.5f, -2.1f, 0.0f);

		glVertex3f(1.0f, -1.9f, 0.0f);
		glVertex3f(-1.0f, -1.9f, 0.0f);
		glVertex3f(-1.0f, 1.9f, 0.0f);
		glVertex3f(1.0f, 1.9f, 0.0f);

		glVertex3f(1.3f, -2.6f, 0.0f);
		glVertex3f(-1.3f, -2.6f, 0.0f);
		glVertex3f(-1.3f, 2.6f, 0.0f);
		glVertex3f(1.3f, 2.6f, 0.0f);

		glVertex3f(0.5f, -2.6f, 0.0f);
		glVertex3f(-0.5f, -2.6f, 0.0f);
		glVertex3f(-0.5f, 2.6f, 0.0f);
		glVertex3f(0.5f, 2.6f, 0.0f);

		glVertex3f(2.6f, 0.4f, 0.0f);
		glVertex3f(-2.6f, 1.4f, 0.0f);
		glVertex3f(2.6f, -0.4f, 0.0f);
		glVertex3f(-2.6f, -1.4f, 0.0f);

		glVertex3f(2.2f, 1.2f, 0.0f);
		glVertex3f(-2.2f, 1.2f, 0.0f);
		glVertex3f(2.2f, -1.2f, 0.0f);
		glVertex3f(-2.2f, -1.2f, 0.0f);

		glVertex3f(0.0f, 2.0f, 0.0f);
		glVertex3f(-2.0f, 0.049f, 0.0f);
		glVertex3f(-0.6f, 2.0f, 0.0f);
		glVertex3f(2.0f, 0.9f, 0.0f);

	glEnd();

	// INDIA

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (x <= 0.0f)
		glTranslatef(0.0f, 0.0f, z);
	else
		glTranslatef(-x, 0.0f, z);
		
	glBegin(GL_QUADS);

		// I
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.3f, 0.5f, 0.0f);
		glVertex3f(-1.5f, 0.5f, 0.0f);

		glVertex3f(-1.5f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-1.5f, -0.5f, 0.0f);

		glVertex3f(-1.5f, -0.5f, 0.0f);
		glVertex3f(-1.3f, -0.5f, 0.0f);

		glVertex3f(-1.3f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.3f, 0.5f, 0.0f);

		// white quad color for I

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1.3f, 0.1f, 0.0f);
		glVertex3f(-1.5f, 0.1f, 0.0f);

		glVertex3f(-1.5f, 0.1f, 0.0f);
		glVertex3f(-1.5f, -0.1f, 0.0f);

		glVertex3f(-1.5f, -0.1f, 0.0f);
		glVertex3f(-1.3f, -0.1f, 0.0f);

		glVertex3f(-1.3f, -0.1f, 0.0f);
		glVertex3f(-1.3f, 0.1f, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (x <= 0.0f)
		glTranslatef(0.0f, 0.0f, z);
	else
		glTranslatef(0.0f, x, z);
		
	glBegin(GL_QUADS);

		// N
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		glVertex3f(-1.2f, 0.5f, 0.0f);

		glVertex3f(-1.2f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-1.2f, -0.5f, 0.0f);

		glVertex3f(-1.2f, -0.5f, 0.0f);
		glVertex3f(-1.0f, -0.5f, 0.0f);

		glVertex3f(-1.0f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		
		// white quad color for N 1

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1.0f, 0.1f, 0.0f);
		glVertex3f(-1.2f, 0.1f, 0.0f);

		glVertex3f(-1.2f, 0.1f, 0.0f);
		glVertex3f(-1.2f, -0.1f, 0.0f);

		glVertex3f(-1.2f, -0.1f, 0.0f);
		glVertex3f(-1.0f, -0.1f, 0.0f);

		glVertex3f(-1.0f, -0.1f, 0.0f);
		glVertex3f(-1.0f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.2f, 0.0f);

		glVertex3f(-1.0f, 0.2f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.6f, -0.5f, 0.0f);

		glVertex3f(-0.6f, -0.5f, 0.0f);
		glVertex3f(-0.6f, -0.2f, 0.0f);

		glVertex3f(-0.6f, -0.2f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.5f, 0.0f);
		
		// white quad color for N 2

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.77f, 0.1f, 0.0f);
		glVertex3f(-0.945f, 0.1f, 0.0f);

		glVertex3f(-0.945f, 0.1f, 0.0f);
		glVertex3f(-0.83f, -0.1f, 0.0f);

		glVertex3f(-0.83f, -0.1f, 0.0f);
		glVertex3f(-0.655f, -0.1f, 0.0f);

		glVertex3f(-0.655f, -0.1f, 0.0f);
		glVertex3f(-0.77f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.4f, 0.5f, 0.0f);
		glVertex3f(-0.6f, 0.5f, 0.0f);

		glVertex3f(-0.6f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.6f, -0.5f, 0.0f);

		glVertex3f(-0.6f, -0.5f, 0.0f);
		glVertex3f(-0.4f, -0.5f, 0.0f);

		glVertex3f(-0.4f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.4f, 0.5f, 0.0f);

		// white quad color for N 3

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.4f, 0.1f, 0.0f);
		glVertex3f(-0.6f, 0.1f, 0.0f);

		glVertex3f(-0.6f, 0.1f, 0.0f);
		glVertex3f(-0.6f, -0.1f, 0.0f);

		glVertex3f(-0.6f, -0.1f, 0.0f);
		glVertex3f(-0.4f, -0.1f, 0.0f);

		glVertex3f(-0.4f, -0.1f, 0.0f);
		glVertex3f(-0.4f, 0.1f, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if(z1 <= z)
		glTranslatef(0.0f, 0.0f, z1);
	else
		glTranslatef(0.0f, 0.0f, z);

	glBegin(GL_QUADS);

		// D
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);

		glVertex3f(-0.3f, 0.5f, 0.0f);
		glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

		glVertex3f(-0.3f, -0.5f, 0.0f);
		glVertex3f(-0.1f, -0.5f, 0.0f);

		glVertex3f(-0.1f, -0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);

		// white quad for D 1

		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-0.1f, 0.1f, 0.0f);
		glVertex3f(-0.3f, 0.1f, 0.0f);

		glVertex3f(-0.3f, 0.1f, 0.0f);
		glVertex3f(-0.3f, -0.1f, 0.0f);

		glVertex3f(-0.3f, -0.1f, 0.0f);
		glVertex3f(-0.1f, -0.1f, 0.0f);

		glVertex3f(-0.1f, -0.1f, 0.0f);
		glVertex3f(-0.1f, 0.1f, 0.0f);

		//

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.35f, 0.0f);
		glVertex3f(0.1f, 0.35f, 0.0f);

		glVertex3f(0.1f, 0.35f, 0.0f);
		glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.1f, -0.35f, 0.0f);

		glVertex3f(0.1f, -0.35f, 0.0f);
		glVertex3f(0.3f, -0.35f, 0.0f);

		glVertex3f(0.3f, -0.35f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.35f, 0.0f);

		// white quad for D 2

		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(0.3f, 0.1f, 0.0f);
		glVertex3f(0.1f, 0.1f, 0.0f);

		glVertex3f(0.1f, 0.1f, 0.0f);
		glVertex3f(0.1f, -0.1f, 0.0f);

		glVertex3f(0.1f, -0.1f, 0.0f);
		glVertex3f(0.3f, -0.1f, 0.0f);

		glVertex3f(0.3f, -0.1f, 0.0f);
		glVertex3f(0.3f, 0.1f, 0.0f);


		// D uppar middle square

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);

		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.35f, 0.0f);

		glVertex3f(-0.1f, 0.35f, 0.0f);
		glVertex3f(0.1f, 0.35f, 0.0f);

		glVertex3f(0.1f, 0.35f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);

		// D lower middle square

		glColor3f(0.0f, 0.45f, 0.0f);

		glVertex3f(0.1f, -0.5f, 0.0f);
		glVertex3f(0.1f, -0.35f, 0.0f);

		glVertex3f(0.1f, -0.35f, 0.0f);
		glVertex3f(-0.1f, -0.35f, 0.0f);

		glVertex3f(-0.1f, -0.35f, 0.0f);
		glVertex3f(-0.1f, -0.5f, 0.0f);

		glVertex3f(-0.1f, -0.5f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);

		// D uppar triangle

		glColor3f(1.0f, 0.5f, 0.0f);

		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(0.1f, 0.35f, 0.0f);

		glVertex3f(0.1f, 0.35f, 0.0f);
		glVertex3f(0.3f, 0.35f, 0.0f);

		// D lower triangle

		glColor3f(0.0f, 0.45f, 0.0f);

		glVertex3f(0.1f, -0.35f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);

		glVertex3f(0.1f, -0.5f, 0.0f);
		glVertex3f(0.3f, -0.35f, 0.0f);

		glVertex3f(0.3f, -0.35f, 0.0f);
		glVertex3f(0.1f, -0.35f, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (x <= 0.0f)
		glTranslatef(0.0f, 0.0f, z);
	else
		glTranslatef(0.0f, -x, z);
		
	glBegin(GL_QUADS);

		// I
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);
		glVertex3f(0.4f, 0.5f, 0.0f);

		glVertex3f(0.4f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.4f, -0.5f, 0.0f);

		glVertex3f(0.4f, -0.5f, 0.0f);
		glVertex3f(0.6f, -0.5f, 0.0f);

		glVertex3f(0.6f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);

		// white quad for I

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(0.6f, 0.1f, 0.0f);
		glVertex3f(0.4f, 0.1f, 0.0f);

		glVertex3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.4f, -0.1f, 0.0f);

		glVertex3f(0.4f, -0.1f, 0.0f);
		glVertex3f(0.6f, -0.1f, 0.0f);

		glVertex3f(0.6f, -0.1f, 0.0f);
		glVertex3f(0.6f, 0.1f, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (x <= 0.0f)
		glTranslatef(0.0f, 0.0f, z);
	else
		glTranslatef(x, 0.0f, z);
		
	glBegin(GL_QUADS);

		// A
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.1f, 0.5f, 0.0f);
		glVertex3f(0.9f, 0.5f, 0.0f);

		glVertex3f(0.9f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(0.7f, -0.5f, 0.0f);

		glVertex3f(0.7f, -0.5f, 0.0f);
		glVertex3f(0.9f, -0.5f, 0.0f);

		glVertex3f(0.9f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.1f, 0.5f, 0.0f);
		
		// white quad for A 1

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(1.02f, 0.1f, 0.0f);
		glVertex3f(0.82f, 0.1f, 0.0f);

		glVertex3f(0.82f, 0.1f, 0.0f);
		glVertex3f(0.78f, -0.1f, 0.0f);

		glVertex3f(0.78f, -0.1f, 0.0f);
		glVertex3f(0.98, -0.1f, 0.0f);

		glVertex3f(0.98f, -0.1f, 0.0f);
		glVertex3f(1.02f, 0.1f, 0.0f);

		//

	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.2f, 0.5f, 0.0f);
		glVertex3f(1.0f, 0.5f, 0.0f);

		glVertex3f(1.0f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.45f, 0.0f);
		glVertex3f(1.2f, -0.5f, 0.0f);

		glVertex3f(1.2f, -0.5f, 0.0f);
		glVertex3f(1.4f, -0.5f, 0.0f);

		glVertex3f(1.4f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(1.2f, 0.5f, 0.0f);

		// white quad for A 2

	glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(1.277f, 0.1f, 0.0f);
		glVertex3f(1.08f, 0.1f, 0.0f);

		glVertex3f(1.08f, 0.1f, 0.0f);
		glVertex3f(1.12f, -0.1f, 0.0f);

		glVertex3f(1.12f, -0.1f, 0.0f);
		glVertex3f(1.32f, -0.1f, 0.0f);

		glVertex3f(1.32f, -0.1f, 0.0f);
		glVertex3f(1.277f, 0.1f, 0.0f);

	glEnd();


	// middle flag

	if (x1 <= -1.20f)
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, z);

		glBegin(GL_QUADS);

			// A middle flag
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(1.09f, 0.05f, 0.0f);
			glVertex3f(1.01f, 0.05f, 0.0f);

			glVertex3f(1.01f, 0.05f, 0.0f);
			glColor3f(0.0f, 0.45f, 0.0f);
			glVertex3f(1.0f - 0.01f, -0.05f, 0.0f);

			glVertex3f(1.0f - 0.01f, -0.05f, 0.0f);
			glVertex3f(1.11f, -0.05f, 0.0f);

			glVertex3f(1.11f, -0.05f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(1.09f, 0.05f, 0.0f);

			// white quad for A 3

			glColor3f(0.6f, 0.6f, 0.6f);
			glVertex3f(1.1f, 0.01f, 0.0f);
			glVertex3f(1.01f, 0.01f, 0.0f);

			glVertex3f(1.01f, 0.01f, 0.0f);
			glVertex3f(0.81f, -0.05f, 0.0f);

			glVertex3f(0.81f, -0.01f, 0.0f);
			glVertex3f(1.11f, -0.01f, 0.0f);

			glVertex3f(1.11f, -0.01f, 0.0f);
			glVertex3f(1.1f, 0.01f, 0.0f);

		glEnd();
	}
		
		
	
	// Ashoka Chakra
	glLoadIdentity();

	glLineWidth(1.5f);
	glPointSize(1.0f);

	if (z2 <= z)
		glTranslatef(0.0f, 0.0f, z2);
	else
		glTranslatef(0.0f, 0.0f, z);

	glColor3f(0.0f, 0.0f, 1.0f);

	glBegin(GL_POINTS);

	for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
	{
		glVertex3f((0.1f * cos(axis)), (0.1f * sin(axis)), 0.0f);
	}

	glEnd();

	glBegin(GL_LINES);

	for (axis = 0.0f; axis <= 2 * pi && (i < 13); axis += 0.24f, i++)
	{
		glVertex3f(0.1f * cos(axis), -0.1f * sin(axis), 0.0f);
		glVertex3f(-0.1f * cos(axis), 0.1f * sin(axis), 0.0f);
	}
		
	glEnd();

	// uppar plane with orange smoke
	// plane center circle
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.0f, y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.0f, y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.0f, 0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	glColor3f(0.0f, 0.498039f, 1.0f);

	GetPlane();

	// plane uppar fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.18f, y + 0.08f, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.18f, y + 0.08f, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.18f, 0.38f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// plane below fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.18f, y - 0.08f, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.18f, y - 0.08f, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.18f, 0.22f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// orange smoke

	glLoadIdentity();
	glColor3f(0.8f, 0.4f, 0.1f);

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.65f, y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.65f, y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.65f, 0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetSmoke();

	// IAF
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.0f, y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.0f, y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.0f, 0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetIAF();

	// middle plane with white smoke
	// plane center circle
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		glTranslatef(-x1, 0.0f, z);

		x1 -= val1;
		y1 -= val1;
	}
	//else
	//	glTranslatef(x1, y1, z);

	glColor3f(0.0f, 0.498039f, 1.0f);

	GetPlane();

	// plane uppar fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		glTranslatef(-x1 - 0.18f, 0.08f, z);
		x1 -= val1;
		y1 -= val1;
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// plane below fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		glTranslatef(-x1 - 0.18f, -0.08f, z);
		x1 -= val1;
		y1 -= val1;
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// white smoke

	glLoadIdentity();
	glColor3f(0.6f, 0.6f, 0.6f);

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		glTranslatef(-x1 - 0.65f, 0.0f, z);
		x1 -= val1;
		y1 -= val1;
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetSmoke();

	// IAF
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		glTranslatef(-x1, 0.0f, z);

		x1 -= val1;
		y1 -= val1;
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetIAF();


	// below plane with green smoke
	// plane center circle
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.0f, -y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.0f, -y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.0f, -0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	glColor3f(0.0f, 0.498039f, 1.0f);

	GetPlane();

	// plane uppar fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.18f, -y + 0.08f, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.18f, -y + 0.08f, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.18f, -0.38f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// plane below fire

	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.18f, -y - 0.08f, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.18f, -y - 0.08f, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.18f, -0.22f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetPlaneFire();

	// green smoke

	glLoadIdentity();
	glColor3f(0.1f, 0.4f, 0.1f);

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.65f, -y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.65f, -y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.65f, -0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetSmoke();

	// IAF
	glLoadIdentity();

	if ((x1 >= -6.0f) && (x <= 0.0f))
	{
		if ((y >= 0.3f) && (x1 >= -3.3f))
		{
			glTranslatef(-x1 - 1.0f, -y, z);
			y -= val;
		}
		else if ((x1 <= -3.3f) && (y <= 3.5f))
		{
			glTranslatef(-x1 - 1.0f, -y, z);
			y += val;
		}
		else
		{
			glTranslatef(-x1 - 1.0f, -0.3f, z);
		}
	}
	//else
	//	glTranslatef(x1, y1, z);

	GetIAF();

	x -= 0.01795;
	x1 -= 0.0125;
	z1 += 0.7029;
	z2 += 0.7029;
	star -= 0.0019;

	SwapBuffers(ghdc);
}

void GetPlane()
{
	// function declaration

	// local variable

	// code
	glBegin(GL_LINES);

	for (axis = 0.0f; axis <= pi; axis += 0.0001f)
	{
		glVertex3f((0.16f * sin(axis)), -(0.02f * cos(axis)), 0.0f);
		glVertex3f(0.0f, (0.02f * cos(axis)), 0.0f);
	}

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(0.0f, 0.035f, 0.0f);
	glVertex3f(-0.4f, 0.035f, 0.0f);

	glVertex3f(-0.4f, 0.035f, 0.0f);
	glVertex3f(-0.4f, -0.035f, 0.0f);

	glVertex3f(-0.4f, -0.035f, 0.0f);
	glVertex3f(0.0f, -0.035f, 0.0f);

	glVertex3f(0.0f, -0.035f, 0.0f);
	glVertex3f(0.0f, 0.035f, 0.0f);

	// below blade

	glVertex3f(-0.01f, -0.01f, 0.0f);
	glVertex3f(-0.3f, -0.15f, 0.0f);

	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(-0.33f, -0.14f, 0.0f);

	glVertex3f(-0.33f, -0.14f, 0.0f);
	glVertex3f(-0.25f, -0.01f, 0.0f);

	glVertex3f(-0.25f, -0.01f, 0.0f);
	glVertex3f(-0.01f, -0.01f, 0.0f);

	// uppar blade

	glVertex3f(-0.01f, 0.01f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);

	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(-0.33f, 0.14f, 0.0f);

	glVertex3f(-0.33f, 0.14f, 0.0f);
	glVertex3f(-0.25f, 0.01f, 0.0f);

	glVertex3f(-0.25f, 0.01f, 0.0f);
	glVertex3f(-0.01f, 0.01f, 0.0f);

	// back below blade

	glVertex3f(-0.30f, -0.035f, 0.0f);
	glVertex3f(-0.35f, -0.1f, 0.0f);

	glVertex3f(-0.35f, -0.1f, 0.0f);
	glVertex3f(-0.38f, -0.1f, 0.0f);

	glVertex3f(-0.38f, -0.1f, 0.0f);
	glVertex3f(-0.37f, -0.035f, 0.0f);

	glVertex3f(-0.37f, -0.035f, 0.0f);
	glVertex3f(-0.30f, -0.035f, 0.0f);

	// back uppar blade

	glVertex3f(-0.30f, 0.035f, 0.0f);
	glVertex3f(-0.35f, 0.1f, 0.0f);

	glVertex3f(-0.35f, 0.1f, 0.0f);
	glVertex3f(-0.38f, 0.1f, 0.0f);

	glVertex3f(-0.38f, 0.1f, 0.0f);
	glVertex3f(-0.37f, 0.035f, 0.0f);

	glVertex3f(-0.37f, 0.035f, 0.0f);
	glVertex3f(-0.30f, 0.035f, 0.0f);

	// last back triangle

	glVertex3f(-0.35f, -0.0150f, 0.0f);
	glVertex3f(-0.45f, 0.0f, 0.0f);

	glVertex3f(-0.45f, 0.0f, 0.0f);
	glVertex3f(-0.35f, 0.0150f, 0.0f);

	glVertex3f(-0.35f, 0.0150f, 0.0f);
	glVertex3f(-0.35f, -0.0150f, 0.0f);

	glEnd();
}

void GetPlaneFire()
{
	// function declaration

	// local variable

	// code
	glBegin(GL_LINES);

	for (axis = 0.0f; axis <= pi; axis += 0.0001f)
	{
		glVertex3f((0.1f * sin(axis)), -(0.01f * cos(axis)), 0.0f);
		glVertex3f(0.0f, (0.01f * cos(axis)), 0.0f);
	}

	glEnd();
}

void GetIAF()
{
	// function declaration

	// local variable

	// code
	glLineWidth(2.5f);
	glColor3f(1.0f, 1.0f, 1.0f);

	glBegin(GL_LINES);

	// I
	glVertex3f(-0.25f, -0.06f, 0.0f);
	glVertex3f(-0.25f, 0.06f, 0.0f);

	// A
	glVertex3f(-0.22f, -0.06f, 0.0f);
	glVertex3f(-0.20f, 0.06f, 0.0f);

	glVertex3f(-0.20f, 0.06f, 0.0f);
	glVertex3f(-0.18f, -0.06f, 0.0f);

	glVertex3f(-0.21f, 0.0f, 0.0f);
	glVertex3f(-0.19f, 0.0f, 0.0f);

	// F
	glVertex3f(-0.16f, -0.06f, 0.0f);
	glVertex3f(-0.16f, 0.06f, 0.0f);

	glVertex3f(-0.16f, 0.06f, 0.0f);
	glVertex3f(-0.12f, 0.06f, 0.0f);

	glVertex3f(-0.16f, 0.02f, 0.0f);
	glVertex3f(-0.12f, 0.02f, 0.0f);

	glEnd();
}

void GetSmoke()
{
	// function declaration

	// local variable

	// code
	glBegin(GL_QUADS);

	glVertex3f(0.2f, 0.01f, 0.0f);
	glVertex3f(-0.2f, 0.01f, 0.0f);

	glVertex3f(-0.2f, 0.05f, 0.0f);
	glVertex3f(-0.2f, -0.05f, 0.0f);

	glVertex3f(-0.2f, -0.05f, 0.0f);
	glVertex3f(0.2f, -0.01f, 0.0f);

	glVertex3f(0.2f, -0.01f, 0.0f);
	glVertex3f(0.2f, 0.01f, 0.0f);

	glEnd();
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}
