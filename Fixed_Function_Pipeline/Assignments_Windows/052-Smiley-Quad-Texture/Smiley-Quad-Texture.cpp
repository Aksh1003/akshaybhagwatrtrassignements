// headers
#include<windows.h>
#include<stdio.h>
#include"Icon.h"
#include<gl/GL.h>
#include<gl/GLU.h>

// macros
#pragma comment(lib, "OPENGL32.LIB")
#pragma comment(lib, "GLU32.LIB")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variables
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLuint smiley_texture;
int pressedDigit;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize();
	void Display();

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.TXT", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
							szAppName,
							TEXT("AKSHAY BHAGWAT"),
							WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
							((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
							WIN_WIDTH,
							WIN_HEIGHT,
							NULL,
							NULL,
							hInstance,
							NULL);
	
	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen();
	void Resize(int, int);
	void UnInitialize();

	// local variable

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		case 49:
			glEnable(GL_TEXTURE_2D);
			pressedDigit = 1;
			break;

		case 50:
			glEnable(GL_TEXTURE_2D);
			pressedDigit = 2;
			break;

		case 51:
			glEnable(GL_TEXTURE_2D);
			pressedDigit = 3;
			break;

		case 52:
			glEnable(GL_TEXTURE_2D);
			pressedDigit = 4;
			break;

		default:
			glDisable(GL_TEXTURE_2D);
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	LoadGLTexture(&smiley_texture, MAKEINTRESOURCE(SMILEY_BITMAP));

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	// function declaration

	// local variable
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	// code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bResult = true;

		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// push data into graphic memory with help of graphics driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits); //(gluBuild2DMipmaps() = glText2D() + glGenerateBitMap())
		DeleteObject(hBitmap);
	}

	return bResult;
}

void Resize(int width, int height)
{
	// function declaration

	// local variable

	// code
	if (height == 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void Display()
{
	// function declaration
	static GLfloat angle = 0.0f;

	// local variable

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	glScalef(0.75f, 0.75f, 0.75f);

	glRotatef(angle, 1.0f, 0.0f, 0.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, smiley_texture);

	// front
	if (pressedDigit == 1)
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			// back
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// right side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// left side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			// top side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			// down side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

		glEnd();
	}
	else if (pressedDigit == 2)
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			// back
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// right side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// left side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			// top side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			// down side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(0.5f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

		glEnd();
	}
	else if (pressedDigit == 3)
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			// back
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// right side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// left side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			// top side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, -0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(0.5f, 0.5f, 0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			// down side
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glTexCoord2f(2.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glTexCoord2f(0.0f, 2.0f);
			glVertex3f(0.5f, -0.5f, -0.5f);

		glEnd();
	}
	else if (pressedDigit == 4)
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(0.5f, 0.5f);

		glBegin(GL_QUADS);
		glVertex3f(0.5f, 0.5f, 0.5f);
		glVertex3f(-0.5f, 0.5f, 0.5f);
		glVertex3f(-0.5f, -0.5f, 0.5f);
		glVertex3f(0.5f, -0.5f, 0.5f);

		// back
		glVertex3f(0.5f, 0.5f, -0.5f);
		glVertex3f(-0.5f, 0.5f, -0.5f);
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(0.5f, -0.5f, -0.5f);

		// right side
		glVertex3f(0.5f, 0.5f, -0.5f);
		glVertex3f(0.5f, 0.5f, 0.5f);
		glVertex3f(0.5f, -0.5f, 0.5f);
		glVertex3f(0.5f, -0.5f, -0.5f);

		// left side
		glVertex3f(-0.5f, 0.5f, -0.5f);
		glVertex3f(-0.5f, 0.5f, 0.5f);
		glVertex3f(-0.5f, -0.5f, 0.5f);
		glVertex3f(-0.5f, -0.5f, -0.5f);

		// top side
		glVertex3f(-0.5f, 0.5f, -0.5f);
		glVertex3f(-0.5f, 0.5f, 0.5f);
		glVertex3f(0.5f, 0.5f, 0.5f);
		glVertex3f(0.5f, 0.5f, -0.5f);

		// down side
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f, -0.5f, 0.5f);
		glVertex3f(0.5f, -0.5f, 0.5f);
		glVertex3f(0.5f, -0.5f, -0.5f);

		glEnd();
	}
	else
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glBegin(GL_QUADS);
			glVertex3f(0.5f, 0.5f, 0.5f);
			glVertex3f(-0.5f, 0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			// back
			glVertex3f(0.5f, 0.5f, -0.5f);
			glVertex3f(-0.5f, 0.5f, -0.5f);
			glVertex3f(-0.5f, -0.5f, -0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// right side
			glVertex3f(0.5f, 0.5f, -0.5f);
			glVertex3f(0.5f, 0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

			// left side
			glVertex3f(-0.5f, 0.5f, -0.5f);
			glVertex3f(-0.5f, 0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, 0.5f);
			glVertex3f(-0.5f, -0.5f, -0.5f);

			// top side
			glVertex3f(-0.5f, 0.5f, -0.5f);
			glVertex3f(-0.5f, 0.5f, 0.5f);
			glVertex3f(0.5f, 0.5f, 0.5f);
			glVertex3f(0.5f, 0.5f, -0.5f);

			// down side
			glVertex3f(-0.5f, -0.5f, -0.5f);
			glVertex3f(-0.5f, -0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, 0.5f);
			glVertex3f(0.5f, -0.5f, -0.5f);

		glEnd();
	}

	angle = angle + 0.2f;

	if (angle >= 360.0f)
		angle = 0.0f;

	SwapBuffers(ghdc);
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glDeleteTextures(1, &smiley_texture);

	if (gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}
