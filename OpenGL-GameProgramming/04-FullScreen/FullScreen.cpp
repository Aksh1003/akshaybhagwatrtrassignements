// macros
#define WIN_LEAN_AND_MEAN	// trim the excess fat from windows
#pragma comment(lib, "OPENGL32.LIB")
#pragma comment(lib, "GLU32.LIB")

// headers
#include<windows.h>	// standard windows app include
#include<gl/GL.h>	// standard OpenGL include
#include<gl/GLU.h>	// OpenGL utilities
//#include<gl/glaux.h>	// OpenGL auxillary functions

// global variables
float angle = 0.0f;	// current angle of the rotating triangle
HDC g_HDC;	// global device context
bool bFullScreen = TRUE;	// start off in full-screen mode

// function to set the pixel format for the device context
void SetupPixelFormat(HDC hdc)
{
	// local variable
	int nPixelFormat;	// your pixel format index

	static PIXELFORMATDESCRIPTOR pfd = {
	sizeof(PIXELFORMATDESCRIPTOR),	// size of the structure
	1,	// version, always set to 1
	PFD_DRAW_TO_WINDOW |	// support window
	PFD_SUPPORT_OPENGL |	// support OpenGL
	PFD_DOUBLEBUFFER,	// support double buffering
	PFD_TYPE_RGBA,	// RGBA color mode
	32,	// go for 32 bit color mode
	0, 0, 0, 0, 0, 0,	// ignore color bits, not used
	0,	// no alfa buffer
	0,	// ignore shift bit
	0,	// no accumulation buffer
	0, 0, 0, 0,	// ignore a	cc	u	mulation bits
	16,	// 16-bit z-buffer size
	0,	// no stencil buffer
	0,	// no auxillary buffer
	PFD_MAIN_PLANE,	// main drawing plane
	0,	// reserved
	0, 0, 0};	// layer masks ignored\
	
	// code
	// choose best matching pixel format, return index
	nPixelFormat = ChoosePixelFormat(hdc, &pfd);

	// set pixel format to device context
	SetPixelFormat(hdc, nPixelFormat, &pfd);
}

// full-screen function
void ToggleFullScreen()
{
	// local variable
	DEVMODE devModeScreen;
	BOOL extendedWindowStyle;
	BOOL windowStyle;

	// code
	memset(&devModeScreen, 0, sizeof(devModeScreen));	// clear the DEVMODE structure
	devModeScreen.dmSize = sizeof(DEVMODE);	// size of the structure
	devModeScreen.dmPelsWidth = 1920;	// screen size
	devModeScreen.dmPelsHeight = 1080;	// screen height
//	devModeScreen.dmBitsPerPel = screenBpp;	// bits per pixel
	devModeScreen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if (ChangeDisplaySettings(&devModeScreen, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)	// change has failed, you�ll run in windowed mode
		bFullScreen = false;

	if (bFullScreen)
	{
		extendedWindowStyle = WS_EX_APPWINDOW;	// hide top level windows
		windowStyle = WS_POPUP;	// no border on your window
		ShowCursor(FALSE);	// hide the cursor
	}
	else
	{
		extendedWindowStyle = NULL;	// same as earlier example
		windowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	}

	RECT windowRect; // client area coordinates of the window
	windowRect.top = 0; // top left
	windowRect.left = 0;
	windowRect.bottom = 1080; // bottom right
	windowRect.right = 1920;
	// readjust your window
	AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extendedWindowStyle);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable
	static HGLRC hrc;	// rendering context
	static HDC hdc;	// device context
	char string[] = "Hello World";	// text to be displayed
	int width, height;	// window width and height

	// code
	switch (iMsg)
	{
	case WM_KEYDOWN:
		ToggleFullScreen();
		switch (wParam)
		{
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CREATE:	// window is being created
		hdc = GetDC(hwnd);	// get current window's device context
		g_HDC = hdc;
		SetupPixelFormat(hdc);	// call your pixel setup function

		// create rendering context and make it current
		hrc = wglCreateContext(hdc);
		wglMakeCurrent(hdc, hrc);

		return 0;
		break;

	case WM_CLOSE:	// windows is closing
		// deselect rendering context and delete it
		wglMakeCurrent(hdc, NULL);
		wglDeleteContext(hrc);

		// send WM_QUIT to message queue
		PostQuitMessage(0);

		return 0;
		break;

	case WM_SIZE:
		height = HIWORD(lParam);	// retrieve width and height
		width = LOWORD(lParam);

		if (height == 0)	// don't wnat a divide by a zero
			height = 1;

		// reset the viewport to new dimensions
		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);	// set projection matrix
		glLoadIdentity();	// reset projection matrix

		// calculate aspect ratio of window
		gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 1000.0f);

		glMatrixMode(GL_MODELVIEW);	// set modelview matrix
		glLoadIdentity();	// reset modelview matrix

		return 0;
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// main function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// local variable
	WNDCLASSEX wndclass;	// window class
	HWND hwnd;	// window handle;
	MSG msg;	// message
	bool done;	// flag saying when your app is complete

	// code
	// fill out the window class structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = NULL;
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = "MyClass";
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register the window class
	if (!RegisterClassEx(&wndclass))
		return 0;

	// class registered, so now create your window
	hwnd = CreateWindowEx(NULL,	// extended style
		"MyClass",	// class name
		"AKSHAY BHAGWAT",	// app name
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100, 100,	// x, y co-ordinate
		400, 400,	// width, height
		NULL,	// handle to parent
		NULL,	// handle to menu
		hInstance,	// application instance
		NULL);	// no extra params

	// check if window creation failed (hwnd would equal to NULL
	if (!hwnd)
		return 0;

	ShowWindow(hwnd, SW_SHOW);	// display the window
	UpdateWindow(hwnd);	// update the window

	done = false;

	while (!done)
	{
		PeekMessage(&msg, hwnd, NULL, NULL, PM_REMOVE);

		if (msg.message == WM_QUIT)	// do you WM_QUIT message?
		{
			done = true;	// if so, time to quit the application
		}
		else
		{
			// do rendering here
			// clear screen and depth buffer
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();	// reset modelview matrix

			angle = angle + 0.1f;	// increase your rotation angle counter
			if (angle >= 360.0f)	// reset angle counter
				angle = 0.0f;

			glTranslatef(0.0f, 0.0f, -5.0f);	// move back 5 units
			glRotatef(angle, 0.0f, 0.0f, 1.0f);	// roate along z-axis

			glColor3f(1.0f, 0.0f, 0.0f);	// set color to red

			glBegin(GL_TRIANGLES);

				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(1.0f, 0.0f, 0.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

			glEnd();

			SwapBuffers(g_HDC);	// bring buffer to foreground

			TranslateMessage(&msg);	// translate / dispatch to event queue
			DispatchMessage(&msg);
		}
	}
	return msg.wParam;
}
