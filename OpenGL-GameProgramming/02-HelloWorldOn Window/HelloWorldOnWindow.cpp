#define WIN32_LEAN_AND_MEAN	// trim the excess fat from windows

#include<windows.h>	// std windows app include

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT paintstruct;
	HDC hdc;	// device context
	char string[] = "Hello World";	// text to be displayed

	switch (message)
	{
	case WM_CREATE:	// window is being created
		return 0;
		break;

	case WM_CLOSE:	// window is closing
		PostQuitMessage(0);
		break;

	case WM_PAINT:	// window needs updating
		hdc = BeginPaint(hwnd, &paintstruct);

		// set text color to blue
		SetTextColor(hdc, COLORREF(0x00FF0000));

		// display text in middle of window
		TextOut(hdc, 150, 150, string, sizeof(string) - 1);

		EndPaint(hwnd, &paintstruct);
		return 0;
		break;

	default:
		break;
	}
	
	return (DefWindowProc(hwnd, message, wParam, lParam));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShoe)
{
	WNDCLASSEX wndclass;	// window class
	HWND hwnd;	// window handle
	MSG msg;	// message
	bool done;	// flag saying when your app is complete

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = "MyClass";
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register the window class
	if (!RegisterClassEx(&wndclass))
		return 0;

	// class registered, so you can create your window
	hwnd = CreateWindowEx(NULL,	// extended style
		"MyClass",	// class name
		"AKSHAY BHAGWAT",	// app name
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	// window style
		100, 100,	// x, y co-ordinate
		400, 400,	// width, height
		NULL,	// handle to parent
		NULL,	// handle to menu
		hInstance,	// application instance
		NULL);	// no extra params

	// check if window creation failed (hwnd would equal NULL)
	if (!hwnd)
		return 0;

	done = false;

	// main message loop (GAME LOOP)
	while (!done)
	{
		PeekMessage(&msg, hwnd, NULL, NULL, PM_REMOVE);

		if (msg.message == WM_QUIT)
		{
			done = true;
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}
