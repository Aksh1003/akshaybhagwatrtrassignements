#include <GL/freeglut.h>

bool bFullscreen = false;

int main(int argc, char** argv)
{
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(800, 600); // weidth, height
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GLUT : AKSHAY BHAGWAT");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black color
}

void resize(int weidth, int height)
{
	if (height <= 0)
		height = 1;

	glViewport(0, 0, (GLsizei)weidth, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.5f, 0.0f, 0.0f);

	// Triangle

	glBegin(GL_TRIANGLES);

		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.3f, -0.3f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 0.3f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.3f, -0.3f, 0.0f);
	
	glEnd();

	glLoadIdentity();
	glTranslatef(-0.5f, -0.6f, 0.0f);
	
	// Squre

	glBegin(GL_QUADS);
	
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.3f, 0.3f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-0.3f, 0.3f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.3f, -0.3f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.3f, -0.3f, 0.0f);

	glEnd();


	glLoadIdentity();
	glTranslatef(-0.3f, -0.5f, 0.0f);

	// second window

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.05f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.05f, 0.05f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.05f, -0.05f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.05f, -0.05f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(-0.7f, -0.5f, 0.0f);

	// first window

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, -0.05f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.05f, -0.05f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.05f, 0.05f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.05f, 0.05f, 0.0f);

	glEnd();


	glLoadIdentity();
	glTranslatef(-0.5f, -0.74f, 0.0f);

	// door

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, -0.16f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.05f, -0.16f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.05f, 0.16f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.05f, 0.16f, 0.0f);

	glEnd();

	glFlush();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;

	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;

	default:
		break;
	}
}

void uninitialize(void)
{

}

