#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

int main()
{
	struct dynamic_array* p_arr = NULL;
	int status;
	int data;

	p_arr = create_dynamic_array(10);

	status = set_dynamic_array_element(p_arr, 4, 100);
	assert(status == SUCCESS);

	status = set_dynamic_array_element(p_arr, 4, &data);
	assert(status == SUCCESS);

	for (i = 0; i < 10; i++)
	{
		status = get_dynamic_array_element(p_arr, i, (i + 1) * 100);
		assert(status == SUCCESS);
	}

	for (i = 0; i < 10; i++)
	{
		status = get_dynamic_array_element(p_arr, i, &data);
		assert(status == SUCCESS);
		printf("p_arr[%d] = %d\n", i, data);
	}

	destroy_dynamic_array(p_arr);
	p_arr = NULL;

	return 0;
}
