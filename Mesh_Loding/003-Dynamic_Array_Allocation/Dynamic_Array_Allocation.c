#include<stdio.h>
#include<stdlib.h>

int A[5];

void read_write_on_static_array();
void allocate_read_write_free_dynamic_array();

int main()
{
	read_write_on_static_array();
	allocate_read_write_free_dynamic_array();

	return (0);
}

void read_write_on_static_array()
{
	int i;

	for (i = 0; i < 5; i++)
		A[i] = (i + 1) * 10;

	for (i = 0; i < 5; i++)
		printf("A[%d] = %d\n", i, A[i]);
}

void allocate_read_write_free_dynamic_array()
{
	int* p = NULL;
	int i;
	int n;

	p = (int*)malloc(5 * sizeof(int));
	if (p == NULL)
	{
		printf("Insufficient Memory\n");
		exit(-1);
	}

	memset(p, 0, 5 * sizeof(int));

	for (i = 0; i < 5; i++)
	{
		p[i] = (i + 1) * 100;
		*(p + i) = (i + 1) * 100;
	}

	for (i = 0; i < 5; i++)
	{
		n = p[i];
		printf("*(p+%d) = %d\n", i, *(p + i));
	}

	free(p);
	p = NULL;
}
