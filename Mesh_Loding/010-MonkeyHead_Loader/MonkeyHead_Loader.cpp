#include<windows.h>
#include"icon.h"

#include<gl/GL.h>
#include<gl/GLU.h>

#include<stdio.h>
#include<stdlib.h>

#define TRUE	1
#define FALSE	0

#define BUFFER_SIZE	256
#define S_EQUAL		0

#define WIN_INIT_X		100
#define WIN_INIT_Y		100
#define WIN_WIDTH		800
#define WIN_HEIGHT		600

#define VK_F	0x46
#define VK_f	0x60

#define NR_COLOR_BITS		32
#define NR_RED_BITS			8
#define NR_GREEN_BITS		8
#define NR_BLUE_BITS		8
#define NR_ALPHA_BITS		8
#define NR_DEPTH_BITS		24

#define NR_POINTS_COORDS		3
#define NR_TEXTURE_COORDS		2
#define NR_NORMAL_COORDS		3
#define NR_FACE_TOKENS			3
#define NR_TRIANGLE_VERTICES	3

#define FOY_ANGLE	45
#define ZNEAR		0.1
#define ZFAR		200.0

#define VIWPORT_BOTTOMLEFT_X	0
#define VIWPORT_BOTTOMLEFT_Y	0

#define MONKEYHEAD_X_TRANSLATE	0.0f
#define MONKEYHEAD_Y_TRANSLATE	-0.0f
#define MONKEYHEAD_Z_TRANSLATE	-5.0f

#define MONKEYHEAD_X_SCALE_FACTOR	1.5f
#define MONKEYHEAD_Y_SCALE_FACTOR	1.5f
#define MONKEYHEAD_Z_SCALE_FACTOR	1.5f

#define START_ANGLE_POS				0.0f
#define END_ANGLE_POS				360.0f
#define MONKEYHEAD_ANGLE_INCREMENT	1.0f

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLuint kundali_texture;

#define ERRORBOX1(lpszErrorMessage, lpszCaption){	\
	MessageBox((HWND), NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);	\
	ExitProcess(EXIT_FAILURE);	\
}

#define ERRORBOX2(hwnd, lpszErrorMessage, lpszCaption){	\
	MessageBox((HWND), NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);	\
	DestroyWindow(hwnd);	\
}

typedef struct vec_2d_int
{
	GLint** pp_arr;
	size_t size;
}vec_2d_int_t;

typedef struct vec_2d_float
{
	GLfloat** pp_arr;
	size_t size;
}vec_2d_float_t;

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HWND g_hwnd = NULL;
HDC g_hdc = NULL;
HGLRC g_hrc = NULL;

DWORD g_dwStyle = NULL;
WINDOWPLACEMENT g_wpPrev;

bool g_bActiveWindow = false;
bool g_bEscapeKeyIsPressed = false;
bool g_bFullScreen = false;

GLfloat g_rotate;

vec_2d_float_t* gp_vertices;
vec_2d_float_t* gp_texture;
vec_2d_float_t* gp_normals;
vec_2d_int_t* gp_face_tri, *gp_face_texture, *gp_face_normals;

FILE* g_fp_meshfile = NULL;
FILE* g_fp_logfile = NULL;

char g_line[BUFFER_SIZE];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Update(void);
	void Display(void);
	void UnInitialize(void);

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("AKSHAY BHAGWAT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
		((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	// local variable

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	void UnInitialize(void);
	void LoadMeshData(void);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	LoadMeshData();

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	// function declaration

	// local variable

	// code
	if (height == 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 30.0f);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void LoadMeshData(void)
{
	void UnInitialize(void);
	vec_2d_int_t* create_vec_2d_int(void);
	vec_2d_float_t* create_vec_2d_float(void);
	void push_back_vec_2d_int(vec_2d_int_t * p_vec, int* p_arr);
	void push_back_vec_2d_float(vec_2d_float_t * p_vec, float* p_arr);
	void* xcalloc(int, size_t);

	g_fp_meshfile = fopen("MonkeyHead.obj", "r");
	if (!g_fp_meshfile)
		UnInitialize();

	gp_vertices = create_vec_2d_float();
	gp_texture = create_vec_2d_float();
	gp_normals = create_vec_2d_float();


	gp_face_tri = create_vec_2d_int();
	gp_face_texture = create_vec_2d_int();
	gp_face_normals = create_vec_2d_int();

	char* sep_space = " ";
	char* sep_fslash = "/";
	char* first_token = NULL;
	char* token = NULL;
	char* face_tokens[NR_FACE_TOKENS];
	int nr_tokens;
	char* token_vertex_index = NULL;
	char* token_texture_index = NULL;
	char* token_normal_index = NULL;

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(g_line, sep_space);
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			GLfloat* pvec_point_coord = (GLfloat*)xcalloc(NR_POINTS_COORDS, sizeof(GLfloat));

			for (int i = 0; i != NR_POINTS_COORDS; i++)
				pvec_point_coord[i] = atof(strtok(NULL, sep_space));

			push_back_vec_2d_float(gp_vertices, pvec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			GLfloat* pvec_texture_coord = (GLfloat*)xcalloc(NR_TEXTURE_COORDS, sizeof(GLfloat));

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				pvec_texture_coord[i] = atof(strtok(NULL, sep_space));

			push_back_vec_2d_float(gp_texture, pvec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			GLfloat* pvec_normal_coord = (GLfloat*)xcalloc(NR_NORMAL_COORDS, sizeof(GLfloat));

			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				pvec_normal_coord[i] = atof(strtok(NULL, sep_space));

			push_back_vec_2d_float(gp_normals, pvec_normal_coord);
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			GLint* pvec_vertex_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_texture_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_normal_indices = (GLint*)xcalloc(3, sizeof(GLint));

			memset((void*)face_tokens, 0, NR_FACE_TOKENS);
			nr_tokens = 0;
			while (token == strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			for (int i = 0; i != NR_FACE_TOKENS; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);

				pvec_vertex_indices[i] = atoi(token_vertex_index);
				pvec_texture_indices[i] = atoi(token_texture_index);
				pvec_normal_indices[i] = atoi(token_normal_index);
			}

			push_back_vec_2d_int(gp_face_tri, pvec_vertex_indices);
			push_back_vec_2d_int(gp_face_texture, pvec_texture_indices);
			push_back_vec_2d_int(gp_face_normals, pvec_normal_indices);
		}

		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(g_fp_logfile, "gp_vertices->size:%zu gp_texture->size:%zu g_normals:%zu g_face_tri->size:%zu\n", gp_vertices->size, gp_texture->size, gp_normals->size, gp_face_tri->size);
}

void Update(void)
{
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;

	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;
}

void Display(void)
{
	void UnInitialize(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (int i = 0; i != gp_face_tri->size; i++)
	{
		glBegin(GL_TRIANGLES);
			for (int j = 0; j != NR_TRIANGLE_VERTICES; j++)
			{
				int vi = gp_face_tri->pp_arr[i][j] - 1;
				glVertex3f(gp_vertices->pp_arr[vi][0], gp_vertices->pp_arr[vi][1], gp_vertices->pp_arr[vi][2]);
			}
		glEnd();
	}

	SwapBuffers(g_hdc);
}

void UnInitialize()
{
	// function declaration
	void clean_vec_2d_int(vec_2d_int_t * *pp_vec);
	void clean_vec_2d_float(vec_2d_float_t * *pp_vec);

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	clean_vec_2d_float(&gp_vertices);
	clean_vec_2d_float(&gp_normals);
	clean_vec_2d_float(&gp_texture);

	clean_vec_2d_int(&gp_face_tri);
	clean_vec_2d_int(&gp_face_texture);
	clean_vec_2d_int(&gp_face_normals);

	DestroyWindow(g_hwnd);

	if (gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

vec_2d_int_t* create_vec_2d_int(void)
{
	void* xcalloc(int nr_elements, size_t size_per_element);

	return (vec_2d_int_t*)xcalloc(1, sizeof(vec_2d_int_t));
}

vec_2d_float_t* create_vec_2d_float(void)
{
	void* xcalloc(int nr_elements, size_t size_per_element);

	return (vec_2d_float_t*)xcalloc(1, sizeof(vec_2d_float_t));
}

void push_back_vec_2d_int(vec_2d_int_t* p_vec, GLint* p_arr)
{
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLint**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(int**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void push_back_vec_2d_float(vec_2d_float_t* p_vec, GLfloat* p_arr)
{
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLfloat**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(float**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void clean_vec_2d_int(vec_2d_int_t** pp_vec)
{
	vec_2d_int_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
		free(p_vec->pp_arr[i]);

	free(p_vec);
	*pp_vec = NULL;
}

void clean_vec_2d_float(vec_2d_float_t** pp_vec)
{
	vec_2d_float_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
		free(p_vec->pp_arr[i]);

	free(p_vec);
	*pp_vec = NULL;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{
	void UnInitialize(void);
	void* p = calloc(nr_elements, size_per_element);

	if (!p)
	{
		fprintf(g_fp_logfile, "Calloc : Fatal : Out Of Memory\n");
		UnInitialize();
	}

	return p;
}

void* xrealloc(void* p, size_t new_size)
{
	void UnInitialize(void);
	void* ptr = realloc(p, new_size);

	if (!ptr)
	{
		fprintf(g_fp_logfile, "Realloc : Fatal : Out Of Memory\n");
		UnInitialize();
	}

	return ptr;
}
