var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros =
{
	AAB_ATTRIBUTE_VERTEX: 0,
	AAB_ATTRIBUTE_COLOR: 1,
	AAB_ATTRIBUTE_NORMAL: 2,
	AAB_ATTRIBUTE_TEXTURE: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_vertices_cube;
var vbo_texture_cube;
var vbo_colors_cube;
var cube_angle = 0.0;

var mvpUniform;
var texture_sampler_uniform;

var CHECKERBOARD_TEXTURE;
var checkImageWidth = 64;
var checkImageHeight = 64;
var byteSize = 4;

var checkImage = new Uint8Array(checkImageHeight * checkImageWidth * 4);

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;

var cancelRequestAnimationFrame = window.cancelAnimationFrame ||
								window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame
								window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame 
								window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame
								window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();	// warmup repaint call

	draw();	// warmup repaint call
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
    }
}

function init()
{
	// get drawing gl
	gl = canvas.getContext("webgl2");
	if (!gl)
		console.log("Obtaining webgl2 failed\n");
	else
		console.log("Obtaining webgl2 succeded\n");

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexture;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec2 out_texture;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_texture = vTexture;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

	// provide source code to shader
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
        }
	}

	/* Fragment Shader */
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 out_texture;" +
		"uniform sampler2D u_texture_sampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = texture(u_texture_sampler, out_texture);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	// provide source code to shader
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(fragmentShaderObject);
	
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_TEXTURE, "vTexture");

	gl.linkProgram(shaderProgramObject);
	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_texture_sampler");

	var texCoord = new Float32Array
	([
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
	]);

	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	vbo_vertices_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_vertices_cube);
	gl.bufferData(gl.ARRAY_BUFFER, 4 * 3 * 4, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_texture_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	gl.bufferData(gl.ARRAY_BUFFER, texCoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_TEXTURE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);

	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	gl.depthFunc(gl.LEQUAL);

	gl.clearDepth(1.0);

	loadProceduralTexture();

	gl.clearColor(0.0, 0.0, 0.0, 1.0);	

	perspectiveProjectionMatrix = mat4.create();
}

function loadProceduralTexture()
{
	makeCheckImage();

	CHECKERBOARD_TEXTURE = gl.createTexture();
	CHECKERBOARD_TEXTURE.image = new Image();
	gl.bindTexture(gl.TEXTURE_2D, CHECKERBOARD_TEXTURE);
	gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, checkImageWidth, checkImageHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, checkImage);
}

function makeCheckImage()
{
	var i, j, c;

	for (i = 0; i < checkImageWidth; i++) {
		for (j = 0; j < checkImageHeight; j++) {

			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkImage[4 * (i * checkImageHeight + j) + 0] = c;
			checkImage[4 * (i * checkImageHeight + j) + 1] = c;
			checkImage[4 * (i * checkImageHeight + j) + 2] = c;
			checkImage[4 * (i * checkImageHeight + j) + 3] = 255;

		}
	}
}

function resize()
{
	if (bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	var verticesCubeDynamic = new Float32Array(12);

	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	mat4.scale(modelViewMatrix, modelViewMatrix, [0.85, 0.85, 0.85]);
	mat4.rotate(modelViewMatrix, modelViewMatrix, deg2Rads(cube_angle), [1.0, 0.0, 0.0]);
	mat4.rotate(modelViewMatrix, modelViewMatrix, deg2Rads(cube_angle), [0.0, 1.0, 0.0]);

	mat4.rotateZ(modelViewMatrix, modelViewMatrix, deg2Rads(cube_angle));

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, CHECKERBOARD_TEXTURE);
	gl.uniform1i(textureSamplerUniform, 0);

	verticesCubeDynamic[0] = 0.0; verticesCubeDynamic[1] = 1.0; verticesCubeDynamic[2] = 0.0;
	verticesCubeDynamic[3] = -2.0; verticesCubeDynamic[4] = 1.0; verticesCubeDynamic[5] = 0.0;
	verticesCubeDynamic[6] = -2.0; verticesCubeDynamic[7] = -1.0; verticesCubeDynamic[8] = 0.0;
	verticesCubeDynamic[9] = 0.0; verticesCubeDynamic[10] = -1.0; verticesCubeDynamic[11] = 0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_vertices_cube);
	gl.bufferData(gl.ARRAY_BUFFER, verticesCubeDynamic, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);

	verticesCubeDynamic[0] = 2.41421; verticesCubeDynamic[1] = 1.0; verticesCubeDynamic[2] = -1.41421;
	verticesCubeDynamic[3] = 1.0; verticesCubeDynamic[4] = 1.0; verticesCubeDynamic[5] = 0.0;
	verticesCubeDynamic[6] = 1.0; verticesCubeDynamic[7] = -1.0; verticesCubeDynamic[8] = 0.0;
	verticesCubeDynamic[9] = 2.41421; verticesCubeDynamic[10] = -1.0; verticesCubeDynamic[11] = -1.41421;

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_vertices_cube);
	gl.bufferData(gl.ARRAY_BUFFER, verticesCubeDynamic, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	requestAnimationFrame(draw, canvas);
}

function deg2Rads(degree)
{
	return rad = (degree * Math.PI) / 180.0;
}

function uninitialize()
{
	if (vao_square)
	{
		gl.deleteVertexArray(vao_square);
		vao_square = null;
	}

	if (vbo_square)
	{
		gl.deleteBuffer(vao_square);
		vao_square = null;
	}

	if (vbo_square_texture)
	{
		gl.deleteBuffer(vbo_square_texture);
		vbo_square_texture = null;
	}

	if (shaderProgramObject)
	{
		if (vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if (fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;

		case 70:
			toggleFullscreen();
			break;

		case 27:
			uninitialize();
			window.close();
			break;

		default:
			break;
	}
}

function mouseDown()
{

}


