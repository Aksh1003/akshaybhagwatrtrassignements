var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros =
{
	AAB_ATTRIBUTE_VERTEX: 0,
	AAB_ATTRIBUTE_COLOR: 1,
	AAB_ATTRIBUTE_NORMAL: 2,
	AAB_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vbo_triangle;
var vbo_triangle_color;
var triangle_angle = 0.0;

var vao_square;
var vbo_square;
var vbo_suqare_color;
var square_angle = 0.0;

var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;

var cancelRequestAnimationFrame = window.cancelAnimationFrame ||
								window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame
								window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame 
								window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame
								window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();	// warmup repaint call

	draw();	// warmup repaint call
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
    }
}

function init()
{
	// get drawing gl
	gl = canvas.getContext("webgl2");
	if (!gl)
		console.log("Obtaining webgl2 failed\n");
	else
		console.log("Obtaining webgl2 succeded\n");

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

	// provide source code to shader
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
        }
	}

	/* Fragment Shader */
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	// provide source code to shader
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(fragmentShaderObject);
	
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_COLOR, "vColor");

	gl.linkProgram(shaderProgramObject);
	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	var triangleVertices = new Float32Array
		([
			0.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
		]);

	var triangleColor = new Float32Array
		([
			1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 1.0
		]);

	var squareVertices = new Float32Array
	([
			1.0, 1.0, 0.0,
			-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
	]);

	var squareColor = new Float32Array
		([
			1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 1.0,
			1.0, 0.0, 1.0
		]);

	vao_triangle = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);

	vbo_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle);
	gl.bufferData(gl.ARRAY_BUFFER,
		triangleVertices,
		gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_triangle_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER,
		triangleColor,
		gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_square = gl.createVertexArray();
	gl.bindVertexArray(vao_square);

	vbo_square = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square);
	gl.bufferData(gl.ARRAY_BUFFER,
					squareVertices,
					gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX,
							3,
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_square_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square_color);
	gl.bufferData(gl.ARRAY_BUFFER,
					squareColor,
					gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR,
							3,
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearColor(0.0, 0.0, 1.0, 1.0);	// blue

	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if (bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var rotateMatrix = mat4.create();

	// triangle
	mat4.translate(translateMatrix, translateMatrix, [-2.0, 0.0, -6.0]);
	mat4.rotateY(rotateMatrix, rotateMatrix, triangle_angle);

	mat4.multiply(modelViewMatrix, translateMatrix, rotateMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vao_triangle);

	gl.drawArrays(gl.TRIANGLES, 0, 3);

	gl.bindVertexArray(null);

	// square
	modelViewMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.0, 0.0, -6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, square_angle);

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vao_square);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

	gl.bindVertexArray(null);

	triangle_angle += 0.1;
	if (triangle_angle >= 360.0)
		triangle_angle = 0.0;

	square_angle += 0.1;
	if (square_angle >= 360.0)
		square_angle = 0.0;

	gl.useProgram(null);

	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if (vao_triangle) {
		gl.deleteVertexArray(vao_triangle);
		vao_triangle = null;
	}

	if (vbo_triangle) {
		gl.deleteBuffer(vao_triangle);
		vao_triangle = null;
	}

	if (vbo_triangle_color) {
		gl.deleteBuffer(vbo_triangle_color);
		vbo_triangle_color = null;
	}

	if (vao_square)
	{
		gl.deleteVertexArray(vao_square);
		vao_square = null;
	}

	if (vbo_square)
	{
		gl.deleteBuffer(vao_square);
		vao_square = null;
	}

	if (vbo_square_color)
	{
		gl.deleteBuffer(vbo_square_color);
		vbo_square_color = null;
	}

	if (shaderProgramObject)
	{
		if (vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if (fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;

		case 70:
			toggleFullscreen();
			break;
	}
}

function mouseDown()
{

}


