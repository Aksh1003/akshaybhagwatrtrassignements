function main()
{
	// get Canvas from Doc
	var canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	// get height and width of canvas
	console.log("Canvas Height = " + canvas.height + " Canvas Width = " + canvas.width + "\n");

	// get drawing context
	var context = canvas.getContext("2d");
	if (!context)
		console.log("Obtaining context failed\n");
	else
		console.log("Obtaining context succeded\n");

	// paint background by black color
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);

	// center the text
	context.textAlign = "center";	// horizontal
	context.textBaseline = "middle"; // vertical
	context.font = "48PX sans-serif";

	// string to be declare
	var str = "Hello World!!!";

	// color the text
	context.fillStyle = "white";

	// display the text
	context.fillText(str, canvas.width / 2, canvas.height / 2);

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	alert("key is pressed");
}

function mouseDown()
{
	alert("mouse button is clicked");
}
