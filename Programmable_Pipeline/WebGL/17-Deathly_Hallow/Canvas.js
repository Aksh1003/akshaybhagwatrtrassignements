var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros =
{
	AAB_ATTRIBUTE_VERTEX: 0,
	AAB_ATTRIBUTE_COLOR: 1,
	AAB_ATTRIBUTE_NORMAL: 2,
	AAB_ATTRIBUTE_TEXTURE: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// vao's and vbo's
var vao_circle1;
var vbo_circle1_vertex;
var vbo_circle1_color;

var vao_triangle;
var vbo_triangle_vertex;
var vbo_triangle_color;

var vao_line;
var vbo_line_vertex;
var vbo_line_color;

var InCenterX;
var InCenterY;

var mvpUniform;

var perspectiveProjectionMatrix;

var startCloak = false;
var startResurctionStone = false;
var startElderWand = false;

var TranslateXTriangle = -2.0;
var TranslateYTriangle = -2.0;

var AngleOfRotationTriangle = 0.0;

var TranslateXCircle = 2.0;
var TranslateYCircle = -2.0;

var AngleOfRotationCircle = 0.0;

var TranslateXWand = 0.0;
var TranslateYWand = 2.0;

var WandBottomBool = true;
var bounceBoolUpOnce = false;
var bounceBoolDown = false;

var requestAnimationFrame = window.requestAnimationFrame ||
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;

var cancelRequestAnimationFrame = window.cancelAnimationFrame ||
								window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame
								window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame 
								window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame
								window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();	// warmup repaint call

	draw();	// warmup repaint call
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
    }
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 70:
            toggleFullscreen();
            break;

		case 27:
			uninitialize();
			window.close();
			break;

		default:
			break;
	}
}

function init()
{
	// get drawing gl
	gl = canvas.getContext("webgl2");
	if (!gl)
		console.log("Obtaining webgl2 failed\n");
	else
		console.log("Obtaining webgl2 succeded\n");

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;"+ 
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;"+
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"gl_PointSize = 1.0;"+
		"out_color = vColor;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

	// provide source code to shader
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
        }
	}

	/* Fragment Shader */
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{" +
		"FragColor = out_color;" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	// provide source code to shader
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(fragmentShaderObject);
	
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_COLOR, "vColor");

	gl.linkProgram(shaderProgramObject);
	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	// Triangle Hollow
	var x1 = 0.0;
	var y1 = 0.5;
	var x2 = -0.5;
	var y2 = -0.5;
	var x3 = 0.5;
	var y3 = -0.5;

	const traingleVertices = new Float32Array([
		x1, y1, 0.0,
		x2, y2, 0.0,

		x2, y2, 0.0,
		x3, y3, 0.0,

		x3, y3, 0.0,
		x1, y1, 0.0,
	]);

	const triangleColors = new Float32Array([
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0
	]);
	vao_triangle = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);

	vbo_triangle_vertex = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_vertex);
	gl.bufferData(gl.ARRAY_BUFFER, traingleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_triangle_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER, triangleColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);
	// End Of Triangle Hollow


	// Incircle 
	// Side a = BC 
	var a = Math.sqrt(((x2 - x3) * (x2 - x3)) + ((y2 - y3) * (y2 - y3)));
	// Side b = AC
	var b = Math.sqrt(((x1 - x3) * (x1 - x3)) + ((y1 - y3) * (y1 - y3)));
	// Side c = AB
	var c = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
	// Incenter
	// X = ax1 + bx2 + cx3
	InCenterX = ((a * x1) + (b * x2) + (c * x3)) / (a + b + c);
	// Y = ay1 + by2 + cy3
	InCenterY = ((a * y1) + (b * y2) + (c * y3)) / (a + b + c);
	var Perimeter = (a + b + c) / 2;
	var Area = Math.sqrt(Perimeter * (Perimeter - a) * (Perimeter - b) * (Perimeter - c));
	var radiusInCircle = Area / Perimeter;

	const circle1Vertices = new Float32Array(360 * 9);
	const circle1Colors = new Float32Array(360 * 9);

	var angle = 0.0;
	for (var i = 0; i < 360 * 9; i = i + 3) {
		circle1Vertices[i] = radiusInCircle * Math.cos(angle);
		circle1Vertices[i + 1] = radiusInCircle * Math.sin(angle);
		circle1Vertices[i + 2] = 0.0;

		angle = angle + 0.006;

		circle1Colors[i] = 1.5;
		circle1Colors[i + 1] = 0.05;
		circle1Colors[i + 2] = 0.0;
	}
	vao_circle1 = gl.createVertexArray();
	gl.bindVertexArray(vao_circle1);

	vbo_circle1_vertex = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle1_vertex);
	gl.bufferData(gl.ARRAY_BUFFER, circle1Vertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_circle1_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle1_color);
	gl.bufferData(gl.ARRAY_BUFFER, circle1Colors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	// Wand
	const elderWandVertices = new Float32Array([
		0.0, 0.5, 0.0,
		0.0, -0.5, 0.0
	]);
	const elderWandColors = new Float32Array([
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0
	]);
	vao_line = gl.createVertexArray();
	gl.bindVertexArray(vao_line);

	vbo_line_vertex = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_line_vertex);
	gl.bufferData(gl.ARRAY_BUFFER, elderWandVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_line_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_line_color);
	gl.bufferData(gl.ARRAY_BUFFER, elderWandColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);



	gl.bindVertexArray(null);
	// End of Incircle


	gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

	perspectiveProjectionMatrix = mat4.create();

	startCloak = true;
}

function resize()
{
	if (bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// Variable
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var rotationMatrix = mat4.create();
	var translateMatrix = mat4.create();

	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	if (startCloak === true) {

		// create and make identity from same call
		modelViewMatrix = mat4.create();
		modelViewProjectionMatrix = mat4.create();
		rotationMatrix = mat4.create();
		translateMatrix = mat4.create();

		translateMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [TranslateXTriangle, TranslateYTriangle, -3.0]);
		rotationMatrix = mat4.rotate(modelViewMatrix, modelViewMatrix, AngleOfRotationTriangle, [0.0, 1.0, 0.0]);
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

		// Pushing value into shader
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

		// Triangle
		gl.bindVertexArray(vao_triangle);
		for (var i = 0; i < 6; i++) {
			gl.drawArrays(gl.LINES, i * 2, 2);
		}
		gl.bindVertexArray(null);

		if (TranslateXTriangle < 0.0) {
			TranslateXTriangle = TranslateXTriangle + 0.0025;
			AngleOfRotationTriangle = AngleOfRotationTriangle + 0.02;
		}
		else {
			AngleOfRotationTriangle = 0.0;
			// Starting Circle
			startResurctionStone = true;
		}

		if (TranslateYTriangle < 0.0) {
			TranslateYTriangle = TranslateYTriangle + 0.0025;
		}
	}

	if (startResurctionStone === true) {
		// create and make identity from same call
		modelViewMatrix = mat4.create();
		modelViewProjectionMatrix = mat4.create();
		rotationMatrix = mat4.create();
		translateMatrix = mat4.create();

		translateMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [TranslateXCircle, TranslateYCircle, -3.0]);
		rotationMatrix = mat4.rotate(modelViewMatrix, modelViewMatrix, AngleOfRotationCircle, [0.0, 1.0, 0.0]);
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

		// Pushing value into shader
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

		// Circle 1
		gl.bindVertexArray(vao_circle1);
		for (var i = 0; i < 360 * 9; i++) {
			gl.drawArrays(gl.POINTS, i, 1);
		}
		gl.bindVertexArray(null);

		if (TranslateXCircle > InCenterX) {
			TranslateXCircle = TranslateXCircle - 0.0025;
			AngleOfRotationCircle = AngleOfRotationCircle + 0.02;
		}
		else {
			AngleOfRotationCircle = 0.0;
			// Starting Circle
			startElderWand = true;

		}
		if (TranslateYCircle < InCenterY) {
			TranslateYCircle = TranslateYCircle + 0.0025;
		}
	}

	if (startElderWand === true) {
		// create and make identity from same call
		modelViewMatrix = mat4.create();
		modelViewProjectionMatrix = mat4.create();
		translateMatrix = mat4.create();

		translateMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [TranslateXWand, TranslateYWand, -3.0]);
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

		// Pushing value into shader
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

		gl.bindVertexArray(vao_line);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		if (WandBottomBool) {
			if (TranslateYWand >= 0.0) {
				TranslateYWand = TranslateYWand - 0.01;
			}
			else {
				bounceBoolUpOnce = true;
				WandBottomBool = false;
			}
		}


		if (bounceBoolUpOnce) {
			if (TranslateYWand <= 0.1) {
				TranslateYWand = TranslateYWand + 0.01;
			}
			else {
				bounceBoolDown = true;
				bounceBoolUpOnce = false;
			}
		}

		if (bounceBoolDown) {
			if (TranslateYWand >= 0.0) {
				TranslateYWand = TranslateYWand - 0.001;
			}
		}
	}


	gl.useProgram(null);

	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if (vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}

	if (vbo_vertices)
	{
		gl.deleteBuffer(vbo_vertices);
		vbo_vertices = null;
	}

	if (shaderProgramObject)
	{
		if (vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if (fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function mouseDown()
{

}


