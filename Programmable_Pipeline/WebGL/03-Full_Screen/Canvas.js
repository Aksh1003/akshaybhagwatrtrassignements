var canvas = null;
var context = null;

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	// get height and width of canvas
	console.log("Canvas Height = " + canvas.height + " Canvas Width = " + canvas.width + "\n");

	// get drawing context
	context = canvas.getContext("2d");
	if (!context)
		console.log("Obtaining context failed\n");
	else
		console.log("Obtaining context succeded\n");

	// paint background by black color
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);

	drawText("hello world!!!");

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function drawText(text) // there is no repaint event
{
	// center the text
	context.textAlign = "center";	// horizontal
	context.textBaseline = "middle"; // vertical
	context.font = "48PX sans-serif";

	// color the text
	context.fillStyle = "white";

	// display the text
	context.fillText(text, canvas.width / 2, canvas.height / 2);
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		fullScreen_Element = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		fullScreen_Element = false;
    }
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 70:
			toggleFullscreen();
			drawText("hello world!!!");
			break;
	}
}

function mouseDown()
{

}
