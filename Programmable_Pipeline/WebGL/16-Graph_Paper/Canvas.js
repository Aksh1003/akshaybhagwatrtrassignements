var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros =
{
	AAB_ATTRIBUTE_VERTEX: 0,
	AAB_ATTRIBUTE_COLOR: 1,
	AAB_ATTRIBUTE_NORMAL: 2,
	AAB_ATTRIBUTE_TEXTURE: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_graph;
var vbo_graph_vertex;
var vbo_graph_color;

var vao_axis;
var vbo_axis_vertex;
var vbo_axis_color;

var vao_rect;
var vbo_rect_vertex;
var vbo_rect_color;

var vao_circle1;
var vbo_circle1_vertex;
var vbo_circle1_color;

var vao_triangle;
var vbo_triangle_vertex;
var vbo_triangle_color;

var vao_circle2;
var vbo_circle2_vertex;
var vbo_circle2_color;

var InCenterX;
var InCenterY;

var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;

var cancelRequestAnimationFrame = window.cancelAnimationFrame ||
								window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame
								window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame 
								window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame
								window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();	// warmup repaint call

	draw();	// warmup repaint call
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
    }
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 70:
            toggleFullscreen();
            break;

		case 27:
			uninitialize();
			window.close();
			break;

		default:
			break;
	}
}

function init()
{
	// get drawing gl
	gl = canvas.getContext("webgl2");
	if (!gl)
		console.log("Obtaining webgl2 failed\n");
	else
		console.log("Obtaining webgl2 succeded\n");

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;"+ 
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;"+
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"gl_PointSize = 1.0;"+
		"out_color = vColor;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

	// provide source code to shader
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
        }
	}

	/* Fragment Shader */
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{" +
		"FragColor = out_color;" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	// provide source code to shader
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(fragmentShaderObject);
	
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_COLOR, "vColor");

	gl.linkProgram(shaderProgramObject);
	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // Graph
        const graphVertices = new Float32Array(42*2*3*2);
        // Y Lines
        var x = -1.0;
        for(var i=0; i<41*2*3; i=i+6){
            graphVertices[i] = x;
            graphVertices[i+1] 	= 1.0;
		    graphVertices[i+2]	= 0.0;

		    graphVertices[i+3] 	= x;
		    graphVertices[i+4] 	= -1.0;
		    graphVertices[i+5]	= 0.0;

		    x = x + 0.05;
        }

        // X Lines
        var y = 1.0;
        for(var i=41*2*3; i<41*2*3*2; i=i+6){
            graphVertices[i] 	= -1.0;
		    graphVertices[i+1] 	= y;
		    graphVertices[i+2]	= 0.0;

		    graphVertices[i+3] 	= 1.0;
		    graphVertices[i+4] 	= y;
		    graphVertices[i+5]	= 0.0;

		    y = y - 0.05;
        }

        // Graph Colors
        const graphColors = new Float32Array(42*2*3*2);
        for(var i=0; i<42*2*3*2; i=i+3){
            graphColors[i] 		= 0.0;
            graphColors[i+1] 	= 0.0;
            graphColors[i+2] 	= 1.0;
        }
        vao_graph = gl.createVertexArray();
        gl.bindVertexArray(vao_graph);

        vbo_graph_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graph_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, graphVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_graph_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graph_color);
        gl.bufferData(gl.ARRAY_BUFFER, graphColors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End Of Graph


        // Axis
        const AxisVertices = new Float32Array([
            -1.0, 0.0, 0.0,
		    1.0, 0.0, 0.0,
		    0.0, 1.0, 0.0,
		    0.0, -1.0, 0.0
        ]);

        const AxisColors = new Float32Array([
            1.0, 0.0, 0.0,
		    1.0, 0.0, 0.0,
		    0.0, 1.0, 0.0,
		    0.0, 1.0, 0.0
        ]);

        vao_axis = gl.createVertexArray();
        gl.bindVertexArray(vao_axis);

        vbo_axis_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_axis_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, AxisVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_axis_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_axis_color);
        gl.bufferData(gl.ARRAY_BUFFER, AxisColors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End of Axis

        // Start of Rect
        const rectVertices = new Float32Array([
            -1.0, 0.75, 0.0,
		    -1.0, -0.75, 0.0,

		    -1.0, -0.75, 0.0,
		    1.0, -0.75, 0.0,

		    1.0, -0.75, 0.0,
		    1.0, 0.75, 0.0,

		    1.0, 0.75, 0.0,
		    -1.0, 0.75, 0.0
        ]);

        const rectColors = new Float32Array([
            1.0, 1.0, 0.0,
		    1.0, 1.0, 0.0,

		    1.0, 1.0, 0.0,
		    1.0, 1.0, 0.0,

		    1.0, 1.0, 0.0,
		    1.0, 1.0, 0.0,

		    1.0, 1.0, 0.0,
		    1.0, 1.0, 0.0
        ]);

        vao_rect = gl.createVertexArray();
        gl.bindVertexArray(vao_rect);

        vbo_rect_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_rect_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, rectVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_rect_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_rect_color);
        gl.bufferData(gl.ARRAY_BUFFER, rectColors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End of Rect

        // Circle 1
        var radius = 0.75;
        const circle1Vertices = new Float32Array(360*9);
        const circle1Colors = new Float32Array(360*9);
        var angle = 0.0;
        for(var i=0; i<360*9; i=i+3){
            circle1Vertices[i] = radius * Math.cos(angle);
            circle1Vertices[i+1] = radius * Math.sin(angle);
            circle1Vertices[i+2] = 0.0;

            angle = angle + 0.006;

            circle1Colors[i] = 1.0;
		    circle1Colors[i+1] = 0.0;
		    circle1Colors[i+2] = 0.0;
        }

        console.log(circle1Vertices);
        vao_circle1 = gl.createVertexArray();
        gl.bindVertexArray(vao_circle1);

        vbo_circle1_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle1_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, circle1Vertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_circle1_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle1_color);
        gl.bufferData(gl.ARRAY_BUFFER, circle1Colors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End of Circle 1

        // Triangle Hollow
        var x1 = 0.0;
        var y1 = 0.75;
        var x2 = Math.cos(120.0) * 0.75;
        var y2 = -(Math.sin(120.0) * 0.75);
        var x3 = -(Math.cos(120.0) * 0.75);
        var y3 = -(Math.sin(120.0) * 0.75);
        const traingleVertices = new Float32Array([
            x1, y1, 0.0,
		    x2, y2, 0.0,

		    x2, y2, 0.0,
		    x3, y3, 0.0,

		    x3, y3, 0.0,
		    x1, y1, 0.0,
        ]);

        const triangleColors = new Float32Array([
            0.0, 1.0, 1.0,
            0.0, 1.0, 1.0,
            0.0, 1.0, 1.0,
            0.0, 1.0, 1.0,
            0.0, 1.0, 1.0,
            0.0, 1.0, 1.0
        ]);
        vao_triangle = gl.createVertexArray();
        gl.bindVertexArray(vao_triangle);

        vbo_triangle_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, traingleVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_triangle_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_color);
        gl.bufferData(gl.ARRAY_BUFFER, triangleColors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End Of Triangle Hollow


        // Incircle 
        // Side a = BC 
	    var a = Math.sqrt(((x2 - x3) * (x2 - x3)) + ((y2 - y3) * (y2 - y3)));
	    // Side b = AC
	    var b = Math.sqrt(((x1 - x3) * (x1 - x3)) + ((y1 - y3) * (y1 - y3)));
	    // Side c = AB
	    var c = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
	    // Incenter
	    // X = ax1 + bx2 + cx3
	    InCenterX = ((a * x1) + (b * x2) + (c * x3))/(a+b+c);
	    // Y = ay1 + by2 + cy3
	    InCenterY = ((a * y1) + (b * y2) + (c * y3))/(a+b+c);
	    var Perimeter = (a + b + c) / 2;
	    var Area = Math.sqrt(Perimeter * (Perimeter - a) * (Perimeter - b) * (Perimeter - c));
	    var radiusInCircle = Area / Perimeter;

	    const circle2Vertices = new Float32Array(360*9);
	    const circle2Colors = new Float32Array(360*9);

	    angle = 0.0;
	    for(var i=0; i<360*9; i=i+3){
		    circle2Vertices[i] = radiusInCircle * Math.cos(angle);
		    circle2Vertices[i+1] = radiusInCircle * Math.sin(angle);
		    circle2Vertices[i+2] = 0.0;

		    angle = angle + 0.006;

		    circle2Colors[i] = 1.5;
		    circle2Colors[i+1] = 0.05;
		    circle2Colors[i+2] = 0.0;
	    }
        vao_circle2 = gl.createVertexArray();
        gl.bindVertexArray(vao_circle2);

        vbo_circle2_vertex = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle2_vertex);
        gl.bufferData(gl.ARRAY_BUFFER, circle2Vertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        vbo_circle2_color = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle2_color);
        gl.bufferData(gl.ARRAY_BUFFER, circle2Colors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);
        // End of Incircle


        gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

        perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if (bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    // create and make identity from same call
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var translateMatrix = mat4.create();

    translateMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

    // Pushing value into shader
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vao_graph);
    for(var i=0; i<41*2; i++){
        gl.drawArrays(gl.LINES, i*2, 2);
    }
    gl.bindVertexArray(null);


    // Axis
    gl.bindVertexArray(vao_axis);
    gl.drawArrays(gl.LINES, 0, 2);
    gl.drawArrays(gl.LINES, 2, 4);
    gl.bindVertexArray(null);

    // Rect
    gl.bindVertexArray(vao_rect);
    for(var i = 0; i<8; i++){
        gl.drawArrays(gl.LINES, i*2, 2);
    }
    gl.bindVertexArray(null);

    // Circle 1
    gl.bindVertexArray(vao_circle1);
    for(var i = 0; i<360*9; i++){
        gl.drawArrays(gl.POINTS, i, 1);
    }
    gl.bindVertexArray(null);

    // Triangle
    gl.bindVertexArray(vao_triangle);
    for(var i = 0; i<6; i++){
        gl.drawArrays(gl.LINES, i*2, 2);
    }
    gl.bindVertexArray(null);


    // Incircle
    modelViewMatrix = mat4.create();
    translateMatrix = mat4.create();
    modelViewProjectionMatrix = mat4.create();
    translateMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [InCenterX, InCenterY, -3.0]);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vao_circle2);
    for(var i = 0; i<360*9; i++){
        gl.drawArrays(gl.POINTS, i, 1);
    }
    gl.bindVertexArray(null);

    gl.useProgram(null);

    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if (vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}

	if (vbo_vertices)
	{
		gl.deleteBuffer(vbo_vertices);
		vbo_vertices = null;
	}

	if (shaderProgramObject)
	{
		if (vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if (fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function mouseDown()
{

}


