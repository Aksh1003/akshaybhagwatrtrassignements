"use strict";
var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros =
{
	AAB_ATTRIBUTE_VERTEX: 0,
	AAB_ATTRIBUTE_COLOR: 1,
	AAB_ATTRIBUTE_NORMAL: 2,
	AAB_ATTRIBUTE_TEXTURE: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere_vertices;
var sphere_normals;

var vao_sphere;
var vbo_vertices_sphere;
var vbo_normals_sphere;

//GLuint mvpMatrixUniform;
var modelMatrixUniform;
var viewMatrixUniform;
var perspectiveProjectionUniform;
var lKeyPressedUniform;

var LightPositionUniform1;
var LightSpecularUniform1;
var LightDiffuseUniform1;
var LightAmbientUniform1;

var KaUniform; // Material Ambient
var KdUniform; // Diffuse
var KsUniform; // Specular
var MaterialShineUniform;

var bAnimate = false;
var bLight = false;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;

var cancelRequestAnimationFrame = window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function getSphereVertices(radius, lats, longs) {
	// Variable declaration
	var vertices = Array();

	const rad = Math.PI / 180.0;
	const latitudes = 180.0 / lats;
	const longitudes = 360.0 / longs;

	for (var i = 0.0; i < 360.0; i += longitudes) {
		var iNext = i + longitudes;
		for (var j = 0.0; j < 180.0; j += latitudes) {
			var jNext = j + latitudes;

			var points = Array(4);
			var x, y, z;

			x = radius * Math.cos(i * rad) * Math.sin(j * rad);
			y = radius * Math.cos(j * rad);
			z = radius * Math.sin(i * rad) * Math.sin(j * rad);
			points[0] = Array(x, y, z);

			x = radius * Math.cos(iNext * rad) * Math.sin(j * rad);
			y = radius * Math.cos(j * rad);
			z = radius * Math.sin(iNext * rad) * Math.sin(j * rad);
			points[1] = Array(x, y, z);

			x = radius * Math.cos(iNext * rad) * Math.sin(jNext * rad);
			y = radius * Math.cos(jNext * rad);
			z = radius * Math.sin(iNext * rad) * Math.sin(jNext * rad);
			points[2] = Array(x, y, z);

			x = radius * Math.cos(i * rad) * Math.sin(jNext * rad);
			y = radius * Math.cos(jNext * rad);
			z = radius * Math.sin(i * rad) * Math.sin(jNext * rad);
			points[3] = Array(x, y, z);

			var tri1 = points[0].concat(points[1], points[2]);
			var tri2 = points[2].concat(points[3], points[0]);

			vertices = vertices.concat(tri1, tri2);
		}
	}
	return Float32Array.from(vertices);
}

function getSphereNormals(lats, longs) {
	// Variable declaration
	var normals = Array();

	const rad = Math.PI / 180.0;
	const latitudes = 180.0 / lats;
	const longitudes = 360.0 / longs;

	for (var i = 0.0; i < 360.0; i += longitudes) {
		var iNext = i + longitudes;
		for (var j = 0.0; j < 180.0; j += latitudes) {
			var jNext = j + latitudes;

			var points = Array(4);
			var x, y, z;

			x = Math.cos(i * rad) * Math.sin(j * rad);
			y = Math.cos(j * rad);
			z = Math.sin(i * rad) * Math.sin(j * rad);
			points[0] = Array(x, y, z);

			x = Math.cos(iNext * rad) * Math.sin(j * rad);
			y = Math.cos(j * rad);
			z = Math.sin(iNext * rad) * Math.sin(j * rad);
			points[1] = Array(x, y, z);

			x = Math.cos(iNext * rad) * Math.sin(jNext * rad);
			y = Math.cos(jNext * rad);
			z = Math.sin(iNext * rad) * Math.sin(jNext * rad);
			points[2] = Array(x, y, z);

			x = Math.cos(i * rad) * Math.sin(jNext * rad);
			y = Math.cos(jNext * rad);
			z = Math.sin(i * rad) * Math.sin(jNext * rad);
			points[3] = Array(x, y, z);

			var tri1 = points[0].concat(points[1], points[2]);
			var tri2 = points[2].concat(points[3], points[0]);

			var tri1_norm = Array(3);
			var tri2_norm = Array(3);
			for (var n = 0; n < 3; n++) {
				tri1_norm[n] = (tri1[0 + n] + tri1[3 + n] + tri1[6 + n]) / 3;
				tri2_norm[n] = (tri2[0 + n] + tri2[3 + n] + tri2[6 + n]) / 3;
			}

			normals = normals.concat(tri1, tri2);
		}
	}
	return Float32Array.from(normals);
}

function main()
{
	// get Canvas from Doc
	canvas = document.getElementById("AAB");
	if (!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas succeded\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();	// warmup repaint call

	draw();	// warmup repaint call
}

function toggleFullscreen()
{
	var fullScreen_Element = document.fullscreenElement ||
							document.webkitFullscreenElement ||
							document.mozFullScreenElement ||
							document.msFullscreenElement ||
							null;

	// if not fullscreen
	if (fullScreen_Element == null)
	{
		if (canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if (mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if (document.exitFullscreen)
			document.exitFullscreen();
		else if (document.mozCancelFullScreen)
			document.mozCancelFullscreen();
		else if (document.msRequestFullscreen)
			document.msRequestFullscreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
    }
}

function keyDown(event)
{
	switch (event.keyCode)
	{
		case 76:
			if (bLight == true) {
				bLight = false;
			}
			else {
				console.log("Light on");
				bLight = true;
			}
			break;

		case 65:
			if (bAnimate == true) {
				bAnimate = false;
			}
			else {
				bAnimate = true;
			}
			break;

		case 70:
            toggleFullscreen();
            break;

		case 27:
			uninitialize();
			window.close();
			break;

		default:
			break;
	}
}

function init()
{
	// get drawing gl
	gl = canvas.getContext("webgl2");
	if (!gl)
		console.log("Obtaining webgl2 failed\n");
	else
		console.log("Obtaining webgl2 succeded\n");

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_modelMatrix;" +
		"uniform mat4 u_viewMatrix;" +
		"uniform mat4 u_projectionMatrix;" +
		"uniform int u_lKeyPressed;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ls;" +
		"uniform vec4 light_position;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_ks;" +
		"uniform float materialShineUniform;" +
		"out vec3 phong_ads_light;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyPressed == 1){" +
		"vec4 eyeCoordinate = u_viewMatrix * u_modelMatrix * vPosition;" +
		"vec3 transformed_Normal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" +
		"vec3 light_direction = normalize(vec3(light_position - eyeCoordinate));" +
		"vec3 reflection_vector = reflect(-light_direction, transformed_Normal);" +
		"vec3 view_vector = normalize(-eyeCoordinate.xyz);" +
		"vec3 ambient = u_la * u_ka;" +
		"vec3 diffuse = u_ld * u_kd * max(dot(light_direction, transformed_Normal) , 0.0f);" +
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0f) ,  materialShineUniform);" +
		"phong_ads_light = ambient + diffuse + specular;" +
		"}" +
		"else{" +
		"phong_ads_light = vec3(1.0f,1.0f,1.0f);" +
		"}" +
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

	// provide source code to shader
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
        }
	}

	/* Fragment Shader */
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 phong_ads_light;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	// provide source code to shader
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	// compile shader & check for errors
	gl.compileShader(fragmentShaderObject);
	
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AAB_ATTRIBUTE_NORMAL, "vNormal");

	gl.linkProgram(shaderProgramObject);
	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
	perspectiveProjectionUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyPressed");

	LightAmbientUniform1 = gl.getUniformLocation(shaderProgramObject, "u_la");
	LightDiffuseUniform1 = gl.getUniformLocation(shaderProgramObject, "u_ld");
	LightSpecularUniform1 = gl.getUniformLocation(shaderProgramObject, "u_ls");
	LightPositionUniform1 = gl.getUniformLocation(shaderProgramObject, "light_position");

	KdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
	KaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
	KsUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
	MaterialShineUniform = gl.getUniformLocation(shaderProgramObject, "materialShineUniform");

	sphere_vertices = getSphereVertices(0.5, 40, 40);
	sphere_normals = getSphereNormals(40, 40);

	vao_sphere = gl.createVertexArray();
	gl.bindVertexArray(vao_sphere);

	vbo_vertices_sphere = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_vertices_sphere);
	gl.bufferData(gl.ARRAY_BUFFER, sphere_vertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	vbo_normals_sphere = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normals_sphere);
	gl.bufferData(gl.ARRAY_BUFFER, sphere_normals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AAB_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AAB_ATTRIBUTE_NORMAL);

	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	gl.bindVertexArray(null);


	// Imp for 3D rendering
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.clearDepth(1.0);
	//

	gl.clearColor(0.1, 0.1, 0.1, 1.0); // Blue

	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if (bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	var materialAmbient = new Float32Array([
		0.0, 0.0, 0.0
	]);

	var materialDiffuse = new Float32Array([
		0.5, 0.2, 0.7
	]);

	var materialSpecular = new Float32Array([
		0.7, 0.7, 0.7
	]);

	var materialShine = 128.0;

	//
	var LightAmbient1 = new Float32Array([0.1, 0.1, 0.1]);
	var LightDiffuse1 = new Float32Array([1.0, 1.0, 1.0]);
	var LightSpecular1 = new Float32Array([1.0, 1.0, 1.0]);
	var LightPosition1 = new Float32Array([100.0, 100.0, 100.0, 1.0]);


	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	gl.uniform3fv(LightAmbientUniform1, LightAmbient1);
	gl.uniform3fv(LightDiffuseUniform1, LightDiffuse1);
	gl.uniform3fv(LightSpecularUniform1, LightSpecular1);
	gl.uniform4fv(LightPositionUniform1, LightPosition1); // Here 4fv needed all else can be if needed 3fv

	// Here 4fv needed all else can be if needed 3fv

	if (bLight == true) {
		gl.uniform1i(lKeyPressedUniform, 1);

		gl.uniform3fv(KdUniform, materialDiffuse);
		gl.uniform3fv(KaUniform, materialAmbient);
		gl.uniform3fv(KsUniform, materialSpecular);
		gl.uniform1f(MaterialShineUniform, materialShine);
	}
	else {
		gl.uniform1i(lKeyPressedUniform, 0);
	}

	// create and make identity from same call
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
	// Pushing value into shader
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(perspectiveProjectionUniform, false, perspectiveProjectionMatrix);

	gl.bindVertexArray(vao_sphere);
	gl.drawArrays(gl.TRIANGLES, 0, sphere_vertices.length / 3);
	gl.bindVertexArray(null);
	// -------------------------------


	gl.useProgram(null);

	if (bAnimate == true) {
		animate();
	}

	requestAnimationFrame(draw, canvas);
}

function deg2Rads(degree) {
	//console.log((degree * Math.PI) / 180.0);
	return rad = (degree * Math.PI) / 180.0;
}


function animate() {
	
}

function uninitialize()
{
	if (shaderProgramObject)
	{
		if (vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if (fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function mouseDown()
{

}


