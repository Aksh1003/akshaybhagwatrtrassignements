#include<windows.h>
#include<stdio.h>
#include"icon.h"

#include<gl/glew.h>
#include<gl/GL.h>

#include"vmath.h"
#include "Sphere.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAB_ATTRIBUTE_VERTEX = 0,
	AAB_ATTRIBUTE_COLOR,
	AAB_ATTRIBUTE_NORMAL,
	AAB_ATTRIBUTE_TEXTURE0,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

// per fragment
GLuint gVertexShaderObject_pf;
GLuint gFragmentShaderObject_pf;
GLuint gShaderProgramObject_pf;

GLuint gprojectionUniform_pf;
GLuint gModelMatrixUniform_pf;
//GLuint gviewMatrixUniform_pf;

// light
GLuint ldUniform_pf;	// defuse light
GLuint laUniform_pf;	// light component of ambient(specular)
GLuint lsUniform_pf;	// specular light
GLuint lpUniform_pf;

// material
GLuint kdUniform_pf;	// defuse material
GLuint kaUniform_pf;	// material of ambient
GLuint ksUniform_pf;	// specular material

GLuint materialShinynessUniform_pf;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLint gNumVertices, gNumElements;

mat4 gPerspectiveProjectionMatrix;

GLuint lKeypressedUniform;

bool bLight;
GLuint shaderChoose;

GLfloat angleRotationX = 0.0f;
GLfloat angleRotationY = 0.0f;
GLfloat angleRotationZ = 0.0f;

GLint keyPressed = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("AKSHAY BHAGWAT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
		((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	Initialize();

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	UnInitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	// local variable

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if(HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if(gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
				break;

		case 0x46:
		case 0x66:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 49:
			shaderChoose = 1;
			break;

		case 50:
			shaderChoose = 2;
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (bLight == true)
			{
				bLight = false;
			}
			else
			{
				bLight = true;
			}
			break;

		case 'x':
		case 'X':
			keyPressed = 1;
			angleRotationX = 0.0f;
			break;

		case 'y':
		case 'Y':
			keyPressed = 2;
			angleRotationY = 0.0f;
			break;

		case 'z':
		case 'Z':
			keyPressed = 3;
			angleRotationZ = 0.0f;
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		UnInitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}
}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	void UnInitialize(void);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar* vertexShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modeview_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 transform_normal;" \
		"out vec3 view_vector;" \
		"out vec4 eye_coordinates;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"eye_coordinates = u_modeview_matrix * vPosition;" \
		"transform_normal = mat3(u_modeview_matrix) * vNormal;" \
		"view_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_modeview_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_pf);
	GLint iInfoLogLength_pf = 0;
	GLint iShaderCompiledStatus_pf = 0;
	char* szInfoLog_pf = NULL;

	glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompiledStatus_pf);
	if (iShaderCompiledStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gVertexShaderObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// fragment shader
	// create shader
	gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

	// provide sourcecode to shader
	const GLchar* fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd[26];" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ka[26];" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ks[26];" \
		"uniform vec4 u_light_position;" \
		"uniform float u_material_shineyness;" \
		"uniform int u_lKeyPressed;" \
		"in vec3 transform_normal;" \
		"in vec3 view_vector;" \
		"in vec4 eye_coordinates;" \
		"void main(void)" \
		"{" \
		"vec3 color;" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"vec3 normalized_transform_normal = normalize(transform_normal);" \
		"vec3 noramlized_view_vector = normalize(view_vector);" \
		"for(int i= 0 ; i < 26 ; i++)" \
		"{" \
		"vec3 light_direction = vec3(u_light_position - eye_coordinates);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transform_normal);" \
		"vec3 ambiet = u_la * u_ka[i];" \
		"vec3 defuse = u_ld * u_kd[i] * max(dot(normalized_light_direction, normalized_transform_normal), 0.0f);" \
		"vec3 specular = u_ls * u_ks[i] * pow(max(dot(reflection_vector, noramlized_view_vector), 0.0f), u_material_shineyness);" \
		"color += ambiet + defuse + specular;" \
		"}" \
		"}" \
		"else" \
		"{" \
		"color = vec3(1.0f, 1.0f, 1.0f);" \
		"}" \
		"FragColor = vec4(color, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject_pf);

	glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompiledStatus_pf);
	if (iShaderCompiledStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gFragmentShaderObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// shader program
	// create
	gShaderProgramObject_pf = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject_pf, AAB_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pf, AAB_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject_pf);
	GLint iShaderProgramLinkStatus_pf = 0;
	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &iShaderProgramLinkStatus_pf);
	if (iShaderProgramLinkStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gShaderProgramObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	gModelMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_modeview_matrix");
	if (gModelMatrixUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	//gviewMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_viewMatrix");
	//if (gviewMatrixUniform_pf == -1) {
	//	fprintf(gpFile, "%d", __LINE__);
	//}

	gprojectionUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
	if (gprojectionUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lKeypressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");
	if (lKeypressedUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ldUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ld");
	if (ldUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kdUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kd");
	if (kdUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lpUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_light_position");
	if (lpUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	laUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_la");
	if (laUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kaUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ka");
	if (kaUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lsUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ls");
	if (lsUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ksUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ks");
	if (ksUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	materialShinynessUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_material_shineyness");
	if (materialShinynessUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	//gMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_matrix");
	//if (gMatrixUniform_pf == -1) {
	//	fprintf(gpFile, "%d", __LINE__);
	//}

	// vertices, colors, shader attribs, vbo, vao, initializations
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AAB_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAB_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AAB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAB_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	void Draw();

	//variables
	mat4 TranslateMatrix;
	mat4 RotationMatrix;

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 matrix;

	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShine;

	GLfloat lightAmbiant[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat lightPosition[] = { 0.0f, 3.0f, 3.0f, 0.0f };
	GLfloat lightSpecular[] = { 1.0f,0.0f,0.0f,1.0f }; 

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (bLight == true) 
	{
		if (keyPressed == 1)
		{
			lightPosition[0] = 0.0f;
			lightPosition[1] = 8.0f * sin(angleRotationX);
			lightPosition[2] = 8.0f * cos(angleRotationX);
			lightPosition[3] = 0.0f;

			RotationMatrix = vmath::rotate(angleRotationX, 1.0f, 0.0f, 0.0f);

			angleRotationX += 0.01f;
		}
		//else if (keyPressed == 2)
		//{
		//	glRotatef(angleRotationY, 0.0f, 1.0f, 0.0f);
		//	lightPosition[2] = angleRotationY;

		//	angleRotationY += 1.0f;
		//}
		//else if (keyPressed == 3)
		//{
		//	glRotatef(angleRotationZ, 0.0f, 0.0f, 1.0f);
		//	lightPosition[0] = angleRotationZ;

		//	angleRotationZ += 1.0f;
		//}
	}

	TranslateMatrix = vmath::translate(1.5f, 7.0f, -20.0f);
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	matrix = mat4::identity();

		if (bLight == true)
		{
			glUniform1i(lKeypressedUniform, 1);

			glUniform3fv(ldUniform_pf, 1, lightDefuse);
			glUniform3fv(laUniform_pf, 1, lightAmbiant);
			glUniform4fv(lpUniform_pf, 1, lightPosition); // Here 4fv needed all else can be if needed 3fv
			glUniform3fv(lsUniform_pf, 1, lightSpecular);
		}
		else
		{
			glUniform1i(lKeypressedUniform, 0);
		}

		// start using opengl program object
		glUseProgram(gShaderProgramObject_pf);

		// opengl drawing
		// set modelview & modelviewprojection matrix to identity
		modelViewMatrix = TranslateMatrix * RotationMatrix;

//		matrix = TranslateMatrix * RotationMatrix;

		//------------------------------------------------
		// Sphere 1 - Emrald | Row 0 - Coloumn 0 
		// Ambient
		materialAmbient[0] = 0.0215f;
		materialAmbient[1] = 0.1745f;
		materialAmbient[2] = 0.0215f;
		materialAmbient[3] = 1.0f;

		// Diffuse
		materialDiffuse[0] = 0.07568f;
		materialDiffuse[1] = 0.61424f;
		materialDiffuse[2] = 0.07568f;
		materialDiffuse[3] = 1.0f;

		// Specular
		materialSpecular[0] = 0.633f;
		materialSpecular[1] = 0.727811f;
		materialSpecular[2] = 0.633f;
		materialSpecular[3] = 1.0f;

		glUniform3fv(kdUniform_pf, 1, materialDiffuse);
		glUniform3fv(kaUniform_pf, 1, materialAmbient);
		glUniform3fv(ksUniform_pf, 1, materialSpecular);
		glUniform1f(materialShinynessUniform_pf, 0.6f * 128.0f);


		// pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		glUniformMatrix4fv(gModelMatrixUniform_pf, 1, GL_FALSE, modelViewMatrix);
		glUniformMatrix4fv(gprojectionUniform_pf, 1, GL_FALSE, gPerspectiveProjectionMatrix);
//		glUniformMatrix4fv(gviewMatrixUniform_pf, 1, GL_FALSE, matrix);

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Draw()
{
	// variable declaration
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShine;

	GLfloat lightAmbiant[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat lightPosition[] = { 0.0f, 3.0f, 3.0f, 0.0f };

	glUniform1i(lKeypressedUniform, 1);

	glUniform3fv(ldUniform_pf, 1, lightDefuse);
	glUniform3fv(laUniform_pf, 1, lightAmbiant);
	glUniform4fv(lpUniform_pf, 1, lightPosition); // Here 4fv needed all else can be if needed 3fv

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//------------------------------------------------
	// Sphere 1 - Emrald | Row 0 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.0215f;
	materialAmbient[1] = 0.1745f;
	materialAmbient[2] = 0.0215f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 7.0f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);

	glUniform3fv(kdUniform_pf, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf, 1, materialAmbient);
	glUniform3fv(ksUniform_pf, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 2 - Jade | Row 0 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.135f;
	materialAmbient[1] = 0.2225f;
	materialAmbient[2] = 0.1575f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, 7.0f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);
	glUniform3fv(kdUniform_pf + 1, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 1, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 1, 1, materialSpecular);


	//------------------------------------------------
	// Sphere 3 - Obsidian | Row 0 - Coloumn 3 
	// Ambient
	materialAmbient[0] = 0.05375f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.06625f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.3f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 7.0f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 2, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 2, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 2, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 4 - Pearl | Row 0 - Coloumn 4 
	// Ambient
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.20725f;
	materialAmbient[2] = 0.20725f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.088f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, 7.0f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 3, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 3, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 3, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 5 - Ruby | Row  1 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.1745f;
	materialAmbient[1] = 0.01175f;
	materialAmbient[2] = 0.01175f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, 4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 4, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 4, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 4, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 6 - Turquoise | Row 1 - Coloumn 1 
	// Ambient
	materialAmbient[0] = 0.1f;
	materialAmbient[1] = 0.18725f;
	materialAmbient[2] = 0.1745f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.297254f;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.306678f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 5, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 5, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 5, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 7 - Brass | Row 1 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.992157f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.21794872f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 6, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 6, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 6, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 8 - Bronze | Row 1 - Coloumn 3 
	// Ambient
	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.2f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, 4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 7, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 7, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 7, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 9 - Chrome | Row 2 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, 1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 8, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 8, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 8, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 10 - Copper | Row 2 - Coloumn 1 
	// Ambient
	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.137622f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 9, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 9, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 9, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 11 - Gold | Row 2 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 10, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 10, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 10, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 12 - Silver | Row 2 - Coloumn 4 
	// Ambient
	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, 1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 11, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 11, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 11, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 13 - Black | Row 3 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.50f;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, -1.5f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);
	glUniform3fv(kdUniform_pf + 12, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 12, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 12, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 14 - Cyan | Row 3 - Coloumn 1 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, -1.5f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);
	glUniform3fv(kdUniform_pf + 13, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 13, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 13, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 15 - Green | Row 3 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, -1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 14, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 14, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 14, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 16 - Red | Row 3 - Coloumn 3 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, -1.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 15, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 15, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 15, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 17 - White | Row 4 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, -4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 16, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 16, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 16, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 18 - Yellow Plastic | Row 4 - Coloumn 1 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, -4.5f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);
	glUniform3fv(kdUniform_pf + 17, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 17, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 17, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 19 - Black2 | Row 4 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, -4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 18, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 18, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 18, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 20 - Cyan2 | Row 4 - Coloumn 3 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, -4.5f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 19, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 19, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 19, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 21 - Green2 | Row 5 - Coloumn 0 
	// Ambient
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, -7.0f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 20, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 20, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 20, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 22 - Red2 | Row 5 - Coloumn 1 
	// Ambient
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, -7.0f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 21, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 21, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 21, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 23 - White2 | Row 5 - Coloumn 2 
	// Ambient
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, -7.0f, -20.0f);
	//quadric = gluNewQuadric();
	//gluSphere(quadric, 1.0f, 30, 30);
	glUniform3fv(kdUniform_pf + 22, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 22, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 22, 1, materialSpecular);

	//------------------------------------------------
	// Sphere 24 - Yellow Rubber | Row 5 - Coloumn 3 
	// Ambient
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	// Diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// Specular
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	// Shine
	materialShine = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShine);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-4.0f, -7.0f, -20.0f);
	/*quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);*/
	glUniform3fv(kdUniform_pf + 23, 1, materialDiffuse);
	glUniform3fv(kaUniform_pf + 23, 1, materialAmbient);
	glUniform3fv(ksUniform_pf + 23, 1, materialSpecular);

	glUniform1f(materialShinynessUniform_pf, materialShine);

}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glOrtho(left, right, bottom, top, near, far);
	//if (width <= height)
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	//else
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);

	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_pf);
	gVertexShaderObject_pf = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_pf);
	gFragmentShaderObject_pf = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_pf);
	gShaderProgramObject_pf = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
