#include<windows.h>
#include<stdio.h>

#include<gl/glew.h>
#include<gl/GL.h>

#include"vmath.h"
#include"Sphere.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAB_ATTRIBUTE_VERTEX = 0,
	//AAB_ATTRIBUTE_COLOR,
	AAB_ATTRIBUTE_NORMAL,
	AAB_ATTRIBUTE_TEXTURE0,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLint gNumVertices, gNumElements;

// per vertex
GLuint gVertexShaderObject_pv;
GLuint gFragmentShaderObject_pv;
GLuint gShaderProgramObject_pv;

GLuint gMVPUniform_pv;
GLuint gModelViewMatrixUniform_pv;
GLuint gMatrixUniform_pv;

// light
GLuint ldUniform_pv;	// defuse light
GLuint laUniform_pv;	// light component of ambient(specular)
GLuint lsUniform_pv;	// specular light

// material
GLuint kdUniform_pv;	// defuse material
GLuint kaUniform_pv;	// material of ambient
GLuint ksUniform_pv;	// specular material

GLuint lightPositionUniform_pv;
GLfloat materialShineynessUniform_pv;

// per fragment
GLuint gVertexShaderObject_pf;
GLuint gFragmentShaderObject_pf;
GLuint gShaderProgramObject_pf;

GLuint gMVPUniform_pf;
GLuint gModelViewMatrixUniform_pf;
GLuint gMatrixUniform_pf;

// light
GLuint ldUniform_pf;	// defuse light
GLuint laUniform_pf;	// light component of ambient(specular)
GLuint lsUniform_pf;	// specular light

// material
GLuint kdUniform_pf;	// defuse material
GLuint kaUniform_pf;	// material of ambient
GLuint ksUniform_pf;	// specular material

GLuint lightPositionUniform_pf;
GLfloat materialShineynessUniform_pf;

GLuint lKeypressedUniform;
GLuint shaderChoose;

mat4 gPerspectiveProjectionMatrix;

bool bLight;

GLfloat cubeAngle = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("AKSHAY BHAGWAT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
		((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	Initialize();

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
				Display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	UnInitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	// local variable

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if(HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if(gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
				break;

		case 0x46:
		case 0x66:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 49:
			shaderChoose = 1;
			break;

		case 50:
			shaderChoose = 2;
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (bLight == true)
			{
				bLight = false;
			}
			else
			{
				bLight = true;
			}
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		UnInitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}
}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	void UnInitialize(void);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// per vertex
	// vertex shader
	// create shader
	gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar* vertexShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modeview_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform mat4 u_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ks;" \
		"uniform vec4 u_light_position;" \
		"uniform float u_material_shineyness;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates = u_matrix * u_modeview_matrix * vPosition;" \
		"vec3 transform_normal = normalize(mat3(u_matrix * u_modeview_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));" \
		"vec3 reflection_vector = reflect(-light_direction, transform_normal);" \
		"vec3 view_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 ambiet = u_la * u_ka;" \
		"vec3 defuse = u_ld * u_kd * max(dot(light_direction, transform_normal), 0.0f);" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0f), u_material_shineyness);" \
		"fong_ads_light = ambiet + defuse + specular;" \
		"}" \
		"else {" \
		"fong_ads_light = vec3(1.0f, 1.0f, 1.0f);}" \
		"gl_Position = u_projection_matrix * u_matrix * u_modeview_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&vertexShaderSourceCode_pv, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_pv);
	GLint iInfoLogLength_pv = 0;
	GLint iShaderCompiledStatus_pv = 0;
	char* szInfoLog_pv = NULL;

	glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompiledStatus_pv);
	if (iShaderCompiledStatus_pv == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength_pv);
		if (iInfoLogLength_pv > 0)
		{
			szInfoLog_pv = (char*)malloc(iInfoLogLength_pv);
			if (szInfoLog_pv != NULL)
			{
				GLsizei written_pv;
				glGetShaderInfoLog(gVertexShaderObject_pv, iInfoLogLength_pv, &written_pv, szInfoLog_pv);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog_pv);
				free(szInfoLog_pv);
				UnInitialize();
				exit(0);
			}
		}
	}

	// fragment shader
	// create shader
	gFragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

	// provide sourcecode to shader
	const GLchar* fragmentShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(fong_ads_light, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_pv, 1, (const GLchar**)&fragmentShaderSourceCode_pv, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject_pv);

	glGetShaderiv(gFragmentShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompiledStatus_pv);
	if (iShaderCompiledStatus_pv == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength_pv);
		if (iInfoLogLength_pv > 0)
		{
			szInfoLog_pv = (char*)malloc(iInfoLogLength_pv);
			if (szInfoLog_pv != NULL)
			{
				GLsizei written_pv;
				glGetShaderInfoLog(gFragmentShaderObject_pv, iInfoLogLength_pv, &written_pv, szInfoLog_pv);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog_pv);
				free(szInfoLog_pv);
				UnInitialize();
				exit(0);
			}
		}
	}

	// shader program
	// create
	gShaderProgramObject_pv = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject_pv, AAB_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pv, AAB_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject_pv);
	GLint iShaderProgramLinkStatus_pv = 0;
	glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &iShaderProgramLinkStatus_pv);
	if (iShaderProgramLinkStatus_pv == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength_pv);
		if (iInfoLogLength_pv > 0)
		{
			szInfoLog_pv = (char*)malloc(iInfoLogLength_pv);
			if (szInfoLog_pv != NULL)
			{
				GLsizei written_pv;
				glGetShaderInfoLog(gShaderProgramObject_pv, iInfoLogLength_pv, &written_pv, szInfoLog_pv);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog_pv);
				free(szInfoLog_pv);
				UnInitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	gModelViewMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_modeview_matrix");
	if (gModelViewMatrixUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	gMVPUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_projection_matrix");
	if (gMVPUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lKeypressedUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_lKeyPressed");
	if (lKeypressedUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ldUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ld");
	if (ldUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kdUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kd");
	if (kdUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lightPositionUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position");
	if (lightPositionUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	laUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_la");
	if (laUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kaUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ka");
	if (kaUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lsUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ls");
	if (lsUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ksUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ks");
	if (ksUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	materialShineynessUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_material_shineyness");
	if (materialShineynessUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	gMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_matrix");
	if (gMatrixUniform_pv == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	// per fragment
	// vertex shader
	// create shader
	gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar* vertexShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modeview_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform mat4 u_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 transform_normal;" \
		"out vec3 light_direction;" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates = u_matrix * u_modeview_matrix * vPosition;" \
		"transform_normal = mat3(u_matrix * u_modeview_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position - eye_coordinates);" \
		"view_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_matrix * u_modeview_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_pf);
	GLint iInfoLogLength_pf = 0;
	GLint iShaderCompiledStatus_pf = 0;
	char* szInfoLog_pf = NULL;

	glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompiledStatus_pf);
	if (iShaderCompiledStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gVertexShaderObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// fragment shader
	// create shader
	gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

	// provide sourcecode to shader
	const GLchar* fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shineyness;" \
		"uniform int u_lKeyPressed;" \
		"in vec3 transform_normal;" \
		"in vec3 light_direction;" \
		"in vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"vec3 color;" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"vec3 normalized_transform_normal = normalize(transform_normal);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 noramlized_view_vector = normalize(view_vector);" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transform_normal);" \
		"vec3 ambiet = u_la * u_ka;" \
		"vec3 defuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transform_normal), 0.0f);" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, noramlized_view_vector), 0.0f), u_material_shineyness);" \
		"color = ambiet + defuse + specular;" \
		"}" \
		"else" \
		"{" \
		"color = vec3(1.0f, 1.0f, 1.0f);" \
		"}" \
		"FragColor = vec4(color, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject_pf);

	glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompiledStatus_pf);
	if (iShaderCompiledStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gFragmentShaderObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// shader program
	// create
	gShaderProgramObject_pf = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject_pf, AAB_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pf, AAB_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject_pf);
	GLint iShaderProgramLinkStatus_pf = 0;
	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &iShaderProgramLinkStatus_pf);
	if (iShaderProgramLinkStatus_pf == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength_pf);
		if (iInfoLogLength_pf > 0)
		{
			szInfoLog_pf = (char*)malloc(iInfoLogLength_pf);
			if (szInfoLog_pf != NULL)
			{
				GLsizei written_pf;
				glGetShaderInfoLog(gShaderProgramObject_pf, iInfoLogLength_pf, &written_pf, szInfoLog_pf);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog_pf);
				free(szInfoLog_pf);
				UnInitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	gModelViewMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_modeview_matrix");
	if (gModelViewMatrixUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	gMVPUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
	if (gMVPUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lKeypressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");
	if (lKeypressedUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ldUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ld");
	if (ldUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kdUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kd");
	if (kdUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lightPositionUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_light_position");
	if (lightPositionUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	laUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_la");
	if (laUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kaUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ka");
	if (kaUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lsUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ls");
	if (lsUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ksUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ks");
	if (ksUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	materialShineynessUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_material_shineyness");
	if (materialShineynessUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	gMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_matrix");
	if (gMatrixUniform_pf == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	// vertices, colors, shader attribs, vbo, vao, initializations

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AAB_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAB_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AAB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAB_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	bLight = false;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	//variables
	static GLfloat light_ambient[] = { 0.1f, 0.1f, 0.1f };
	static GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f };
	static GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f };
	static GLfloat light_position[] = { 100.0f, 100.0f, 100.0f, 1.0f };

	static GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	static GLfloat material_diffuse[] = { 0.5f, 0.2f, 0.7f, 1.0f };
	static GLfloat material_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	static GLfloat material_shininess = 40.0f;

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// opengl drawing
	// set modelview & modelviewprojection matrix to identity
	mat4 TranslateMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 matrix;

	TranslateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	matrix = mat4::identity();

	// per vertex
	if (shaderChoose == 1)
	{
		// start using opengl program object
		glUseProgram(gShaderProgramObject_pv);

		if (bLight == true)
		{
			glUniform1i(lKeypressedUniform, 1);

			glUniform3fv(laUniform_pv, 1, light_ambient);
			glUniform3fv(ldUniform_pv, 1, light_diffuse);
			glUniform3fv(lsUniform_pv, 1, light_specular);
			glUniform4fv(lightPositionUniform_pv, 1, light_position);

			glUniform3fv(kaUniform_pv, 1, material_ambient);
			glUniform3fv(kdUniform_pv, 1, material_diffuse);
			glUniform3fv(ksUniform_pv, 1, material_specular);
			glUniform1f(materialShineynessUniform_pv, material_shininess);
		}
		else
		{
			glUniform1i(lKeypressedUniform, 0);
		}

		// opengl drawing
		// set modelview & modelviewprojection matrix to identity
		modelViewMatrix = TranslateMatrix;

		// pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		glUniformMatrix4fv(gModelViewMatrixUniform_pv, 1, GL_FALSE, modelViewMatrix);
		glUniformMatrix4fv(gMatrixUniform_pv, 1, GL_FALSE, matrix);
		glUniformMatrix4fv(gMVPUniform_pv, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	}
	// per fragment
	else if (shaderChoose == 2)
	{
		// start using opengl program object
		glUseProgram(gShaderProgramObject_pf);

		if (bLight == true)
		{
			glUniform1i(lKeypressedUniform, 1);

			glUniform3fv(laUniform_pf, 1, light_ambient);
			glUniform3fv(ldUniform_pf, 1, light_diffuse);
			glUniform3fv(lsUniform_pf, 1, light_specular);
			glUniform4fv(lightPositionUniform_pf, 1, light_position);

			glUniform3fv(kaUniform_pf, 1, material_ambient);
			glUniform3fv(kdUniform_pf, 1, material_diffuse);
			glUniform3fv(ksUniform_pf, 1, material_specular);
			glUniform1f(materialShineynessUniform_pf, material_shininess);
		}
		else
		{
			glUniform1i(lKeypressedUniform, 0);
		}

		// opengl drawing
		// set modelview & modelviewprojection matrix to identity
		modelViewMatrix = TranslateMatrix;

		// pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		glUniformMatrix4fv(gModelViewMatrixUniform_pf, 1, GL_FALSE, modelViewMatrix);
		glUniformMatrix4fv(gMatrixUniform_pf, 1, GL_FALSE, matrix);
		glUniformMatrix4fv(gMVPUniform_pf, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);
	
	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glOrtho(left, right, bottom, top, near, far);
	//if (width <= height)
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	//else
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);

	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_pv);
	gVertexShaderObject_pv = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_pv);
	gFragmentShaderObject_pv = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_pv);
	gShaderProgramObject_pv = 0;

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);

	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_pf);
	gVertexShaderObject_pf = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_pf);
	gFragmentShaderObject_pf = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_pf);
	gShaderProgramObject_pf = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
