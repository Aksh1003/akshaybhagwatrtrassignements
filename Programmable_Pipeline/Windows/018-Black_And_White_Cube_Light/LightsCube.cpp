/*
Assignment Number	:

Description			:	Diffuse Lights - Cube - PP - Windows

Assignment Date		:	3rd April 2021

Date Of Assignment
Completion Date		:	4th April 2021
*/


#include<windows.h>
#include<stdio.h>

#include<GL\glew.h>
#include<gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")


#define WIN_WIDTH 800 
#define WIN_HEIGHT 600

using namespace vmath;

// Vertex and it's properties
enum {
	VVM_ATTRIBUTE_POSITION = 0,
	//VVM_ATTRIBUTE_COLOR,
	VVM_ATTRIBUTE_NORMAL,
	VVM_ATTRIBUTE_TEXCOORD,
};

// Prototype of WndProc() declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declarations
FILE* gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

//major
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//
GLuint vao;  // vertex array objects
GLuint vbo;  // vertex buffer objects
GLuint vbo_cube_normal;

//GLuint mvpMatrixUniform;
GLint modelViewMatrixUniform;
GLint perspectiveProjectionUniform;
GLint lKeyPressedUniform;
GLint LdUniform;
GLint KdUniform;
GLint LightPositionUniform;

bool bAnimate;
bool bLight = false;

mat4 perspectiveProjectionMatrix;

GLfloat rotationAngleXCube = 0.0f;
GLfloat rotationAngleYCube = 0.0f;
GLfloat rotationAngleZCube = 0.0f;

// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);


	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("D Lights - CUBE - PP  : Window");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log File Can Not Be Create\nExitting..."),
			TEXT("ERROR"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else {
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("D Lights - CUBE - PP  : Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			display();

			if (gbActiveWindow == true) {
				if (gbEscapeKeyIsPressed == true) {
					bDone = true;
				}
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);

}


// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) {
			gbActiveWindow = true;
		}
		else {
			gbActiveWindow = false;
		}
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false) {
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else {
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (bLight == true) {
				bLight = false;
			}
			else {
				bLight = true;
			}
			break;
		case 'A':
		case 'a':
			if (bAnimate == true) {
				bAnimate = false;
			}
			else {
				bAnimate = true;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


// Function Toggle FullScreen
void ToggleFullscreen(void) {
	// Local Variables
	MONITORINFO mi;

	// Code

	if (gbFullscreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else {
		dwStyle = SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}




// Function Definition
void initialize(void) {

	// Function Declaration
	void uninitialize(void);
	void resize(int, int);

	// Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == false) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//new
	//OpenGL related Log
	fprintf(gpFile, "OpenGL Vendor		:%s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer	:%s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version		:%s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version		:%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i++) {
		fprintf(gpFile, "%d :%s\n", i, glGetStringi(GL_EXTENSIONS, i));
	}

	// new
	// Vertex - Create Shader - Step 1
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Provide Source Code - Step 2
	// 450 -> version 4.5 | version number * 100
	const GLchar* vertexShaderSourceCode =
		// core -> core profile means - programmable pipeline
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelViewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 light_position;" \
		"out vec3 diffuse_light;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyPressed == 1){" \
		"vec4 eyeCoordinate = u_modelViewMatrix * vPosition;" \
		"mat3 normal_matrix = mat3(transpose(inverse(u_modelViewMatrix)));" \
		"vec3 tNormal = normalize(normal_matrix * vNormal);" \
		"vec3 s = normalize(vec3(light_position - eyeCoordinate));" \
		"diffuse_light = u_ld * u_kd * max(dot(s, tNormal) , 0.0);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;" \
		"}";
	// feed
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile - step 4
	glCompileShader(gVertexShaderObject);
	// here there should be shader compilation error checking
	GLint infoLogLength = 0;
	GLint shaderCompileStatus = 0;
	char* szBuffer = NULL;

	// Step 1 - compile cheking error
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);
	if (shaderCompileStatus == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {
			szBuffer = (char*)malloc(sizeof(char) * infoLogLength);
			if (szBuffer != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, infoLogLength, &written, szBuffer);

				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Fragment - Create Shader - Step 1
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Provide Source Code - Step 2
	// 450 -> version 4.5 | version number * 100
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 diffuse_light;" \
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec4 color;" \
		"if(u_lKeyPressed == 1){" \
		"color = vec4(diffuse_light , 1.0);" \
		"}" \
		"else{" \
		"color = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = color;" \
		"}";
	// feed
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile - step 4
	glCompileShader(gFragmentShaderObject);
	// here there should be shader compilation error checking
	// reset log
	infoLogLength = 0;
	shaderCompileStatus = 0;
	szBuffer = NULL;
	// Step 1 - compile cheking error
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);
	if (shaderCompileStatus == GL_FALSE) {
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {
			szBuffer = (char*)malloc(sizeof(char) * infoLogLength);
			if (szBuffer != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, infoLogLength, &written, szBuffer);

				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}


	// Shader Linking Code
	// *** Shader Program ***
	// Create - Step 1 - Link all 
	gShaderProgramObject = glCreateProgram();

	// Step 2 - attach vertex shader to shader program 
					// Shader Program Created, Which Shader
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Step 2 - attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	glBindAttribLocation(gShaderProgramObject, VVM_ATTRIBUTE_POSITION, "vPosition");
	// Light
	glBindAttribLocation(gShaderProgramObject, VVM_ATTRIBUTE_NORMAL, "vNormal");

	// Link Shader - Step 3
	glLinkProgram(gShaderProgramObject);
	// Here Linking Error CHecking Should Be Done
	infoLogLength = 0;
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {
			szBuffer = (char*)malloc(sizeof(char) * infoLogLength);
			if (szBuffer != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, infoLogLength, &written, szBuffer);

				fprintf(gpFile, "Shader Program Log : %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	modelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelViewMatrix");
	if (modelViewMatrixUniform == -1) {
		fprintf(gpFile, "Model View Matrix Error\n");
	}

	perspectiveProjectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	if (perspectiveProjectionUniform == -1) {
		fprintf(gpFile, "perspectiveProjectionUniform Error\n");
	}

	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	if (lKeyPressedUniform == -1) {
		fprintf(gpFile, "lKeyPressedUniform Error\n");
	}

	LdUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	if (LdUniform == -1) {
		fprintf(gpFile, "LdUniform Error\n");
	}

	KdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	if (KdUniform == -1) {
		fprintf(gpFile, "KdUniform Error\n");
	}

	LightPositionUniform = glGetUniformLocation(gShaderProgramObject, "light_position");
	if (LightPositionUniform == -1) {
		fprintf(gpFile, "LightPositionUniform Error\n");
	}

		const GLfloat cubeVertices[] = {
		//Front
		 1.0f,  1.0f, 1.0f,
		-1.0f,  1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		 1.0f, -1.0f, 1.0f,

		 //Right
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,

		 //Back
		 -1.0f,  1.0f, -1.0f,
		  1.0f,  1.0f, -1.0f,
		  1.0f, -1.0f, -1.0f,
		 -1.0f, -1.0f, -1.0f,

		 //Left
		 -1.0f,  1.0f,  1.0f,
		 -1.0f,  1.0f, -1.0f,
		 -1.0f, -1.0f, -1.0f,
		 -1.0f, -1.0f,  1.0f,

		 //Top
		  1.0f, 1.0f, -1.0f,
		 -1.0f, 1.0f, -1.0f,
		 -1.0f, 1.0f,  1.0f,
		  1.0f, 1.0f,  1.0f,

		  //Bottom
		   1.0f, -1.0f, -1.0f,
		   1.0f, -1.0f,  1.0f,
		  -1.0f, -1.0f,  1.0f,
		  -1.0f, -1.0f, -1.0f
	};

		const GLfloat cubeNormals[] = {
		//Front
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		//Right
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		//Back
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		//Left
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,

		//Top
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		//Bottom
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VVM_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VVM_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//glBindVertexArray(0);

	// Light
	glGenBuffers(1, &vbo_cube_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(VVM_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VVM_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	// end of light

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	bAnimate = false;
}

void display(void) {
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Start Using OpenGL program object
	glUseProgram(gShaderProgramObject);

	// lights
	GLfloat lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f }; // positional vector light because of 1.0f

	if (bLight == true) {
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3f(LdUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(KdUniform, 0.5f, 0.5f, 0.5f);
		glUniform4fv(LightPositionUniform, 1, lightPosition);
	}
	else {
		// light disabled
		glUniform1i(lKeyPressedUniform, 0);
	}

	// Transformation Matrix
	mat4 translateMatrix = mat4::identity();
	mat4 rotationMatrixX = mat4::identity();
	mat4 rotationMatrixY = mat4::identity();
	mat4 rotationMatrixZ = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	// ModelView Matrix
	mat4 modelViewMatrix = mat4::identity();

	translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrixX = vmath::rotate(rotationAngleXCube, 1.0f, 0.0f, 0.0f);
	rotationMatrixY = vmath::rotate(rotationAngleYCube, 0.0f, 1.0f, 0.0f);
	rotationMatrixZ = vmath::rotate(rotationAngleZCube, 0.0f, 0.0f, 1.0f);

	modelViewMatrix = translateMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;
	// order is important hence -> perspectiveProjectMatrix comes first

	glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(perspectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);



	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);


	// stop using OpenGL program object
	glUseProgram(0);

	// Animation
	if (bAnimate == true) {
		rotationAngleXCube += 0.1f;
		rotationAngleYCube += 0.1f;
		rotationAngleZCube += 0.1f;
	}

	// OpenGL Drawing
	SwapBuffers(ghdc);
}


void resize(int width, int height) {

	// Code
	if (height == 0) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	// or
	// without vmath
	// perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}


void uninitialize(void) {

	// code
	if (gbFullscreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (vao) {
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo) {
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if (vbo_cube_normal) {
		glDeleteBuffers(1, &vbo_cube_normal);
		vbo_cube_normal = 0;
	}


	// Detach Vertex Shader from Shader Program Object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// Detach Fragment Shader from Shader Program Object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// Delete Vertex Shader Object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// Delete Fragment Shader Object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete Shader Program Object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink Shader Program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile) {
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}

}





