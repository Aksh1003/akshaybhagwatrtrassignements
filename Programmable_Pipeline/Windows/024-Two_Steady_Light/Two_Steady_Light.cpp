#include<windows.h>
#include<stdio.h>

#include<gl/glew.h>
#include<gl/GL.h>

#include"vmath.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAB_ATTRIBUTE_VERTEX = 0,
	AAB_ATTRIBUTE_COLOR,
	AAB_ATTRIBUTE_NORMAL,
	AAB_ATTRIBUTE_TEXTURE0,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

// pyramid
GLuint gVao_Pyramid;
GLuint gVbo_Position_Pyramid;
GLuint gVbo_Pyramid_Normal;

GLuint gprojectionUniform;
GLuint gModelViewMatrixUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat pyramidAngle = 0.0f;

struct Light {
	vec3 lightAmbiant;
	vec3 lightDefuse;
	vec3 lightSpecular;
	vec4 lightPosition;
};

struct Light gLight[2];

// light
GLuint ldUniform;	// defuse light
GLuint laUniform;	// light component of ambient(specular)
GLuint lsUniform;	// specular light
GLuint lpUniform;

// material
GLuint kdUniform;	// defuse material
GLuint kaUniform;	// material of ambient
GLuint ksUniform;	// specular material

//GLfloat materialAmbiant[] = { 0.0f, 0.0f, 0.0f, 1.0f };	
//GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };	
//GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//
//GLfloat materialShinyness = 128.0f;

GLuint materialShinynessUniform;
GLuint lKeypressedUniform;

bool bLight;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("AKSHAY BHAGWAT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
		((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	Initialize();

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	UnInitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	// local variable

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if(HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if(gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
				break;

		case 0x46:
		case 0x66:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (bLight == true)
			{
				bLight = false;
			}
			else
			{
				bLight = true;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		UnInitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}
}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	void UnInitialize(void);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// vertex shader
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modeview_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_ld[2];" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_la[2];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ls[2];" \
		"uniform vec3 u_ks;" \
		"uniform vec4 u_light_position[2];" \
		"uniform float u_material_shineyness;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"fong_ads_light = vec3(0.0f, 0.0f, 0.0f);" \
		"if(u_lKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates = u_modeview_matrix * vPosition;" \
		"vec3 transform_normal = normalize(mat3(u_modeview_matrix) * vNormal);" \
		"vec3 view_vector = normalize(-eye_coordinates.xyz);" \
		"for(int i = 0 ; i < 2 ; i++)" \
		"{" \
		"vec3 light_direction = normalize(vec3(u_light_position[i] - eye_coordinates));" \
		"vec3 reflection_vector = reflect(-light_direction, transform_normal);" \
		"vec3 ambiet = u_la[i] * u_ka;" \
		"vec3 defuse = u_ld[i] * u_kd * max(dot(light_direction, transform_normal), 0.0f);" \
		"vec3 specular = u_ls[i] * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0f), u_material_shineyness);" \
		"fong_ads_light += ambiet + defuse + specular;" \
		"}" \
		"}" \
		"else {" \
		"fong_ads_light = vec3(1.0f, 1.0f, 1.0f);}" \
		"gl_Position = u_projection_matrix * u_modeview_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				exit(0);
			}
		}
	}

	// fragment shader
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide sourcecode to shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(fong_ads_light, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				exit(0);
			}
		}
	}

	// shader program
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, AAB_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AAB_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modeview_matrix");
	if (gModelViewMatrixUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	gprojectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	if (gprojectionUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	if (lKeypressedUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}


	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	if (ldUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	if (kdUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lpUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	if (lpUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	if (laUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	if (kaUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	if (lsUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	if (ksUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shineyness");
	if (materialShinynessUniform == -1) {
		fprintf(gpFile, "%d", __LINE__);
	}

	//gMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_matrix");
	//if (gMatrixUniform == -1) {
	//	fprintf(gpFile, "%d", __LINE__);
	//}

	// vertices, colors, shader attribs, vbo, vao, initializations
	const GLfloat pyramidVertices[] = {	0.0f, 0.5f, 0.0f,
										-0.5f, -0.5f, 0.5f,
										0.5f, -0.5f, 0.5f,

										0.0f, 0.5f, 0.0f,
										-0.5f, -0.5f, -0.5f,
										-0.5f, -0.5f, 0.5f,

										0.0f, 0.5f, 0.0f,
										0.5f, -0.5f, -0.5f,
										-0.5f, -0.5f, -0.5f,

										0.0f, 0.5f, 0.0f,
										0.5f, -0.5f, 0.5f,
										0.5f, -0.5f, -0.5f };

	const GLfloat pyramidNormals[] = {	0.0f, 0.447214f, 0.894427f,
										0.0f, 0.447214f, 0.894427f,
										0.0f, 0.447214f, 0.894427f,
										
										-0.894427f, 0.447214f, 0.0f,
										-0.894427f, 0.447214f, 0.0f,
										-0.894427f, 0.447214f, 0.0f,

										0.0f, 0.447214f, -0.894427f,
										0.0f, 0.447214f, -0.894427f,
										0.0f, 0.447214f, -0.894427f,

										0.894427f, 0.447214f, 0.0f,
										0.894427f, 0.447214f, 0.0f,
										0.894427f, 0.447214f, 0.0f };

	/*gLight[0].lightAmbiant = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	gLight[0].lightDefuse = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	gLight[0].lightSpecular = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	gLight[0].lightPosition = { -2.0f, 0.0f, 0.0f, 1.0f };

	gLight[1].lightAmbiant = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	gLight[1].lightDefuse = vec4(0.0f, 0.0f, 1.0f, 1.0f);
	gLight[1].lightSpecular = vec4(0.0f, 0.0f, 1.0f, 1.0f);
	gLight[1].lightPosition = { 2.0f, 0.0f, 0.0f, 1.0f };*/

	glGenVertexArrays(1, &gVao_Pyramid);
	glBindVertexArray(gVao_Pyramid);

	glGenBuffers(1, &gVbo_Position_Pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position_Pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAB_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAB_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_Pyramid_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);

	glVertexAttribPointer(AAB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAB_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	GLfloat lightAmbiant[] = { 0.0f, 0.0f, 0.0f, 1.0f };	
	GLfloat lightDefuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };	
	GLfloat lightSpecular[] = { 1.0f, 0.0f, 0.0f, 0.0f };	
	GLfloat lightPosition[] = { 2.0f, 0.0f, 0.0f, 1.0f };	

	GLfloat materialAmbiant[] = { 0.0f, 0.0f, 0.0f, 1.0f };	
	GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };	
	GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };	
	GLfloat materialShinyness = 128.0f;

	GLfloat lightAmbiant1[] = { 0.0f, 0.0f, 0.0f, 1.0f };	
	GLfloat lightDefuse1[] = { 0.0f, 0.0f, 1.0f, 1.0f };	
	GLfloat lightSpecular1[] = { 0.0f, 0.0f, 1.0f, 1.0f };	
	GLfloat lightPosition1[] = { -2.0f, 0.0f, 0.0f, 1.0f };	

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);

	if (bLight == true)
	{
		glUniform1i(lKeypressedUniform, 1);

		glUniform3fv(laUniform, 1, lightAmbiant);
		glUniform3fv(ldUniform, 1, lightDefuse);
		glUniform3fv(lsUniform, 1, lightSpecular);
		glUniform4fv(lpUniform, 1, lightPosition);

		glUniform3fv(laUniform + 1, 1, lightAmbiant1);
		glUniform3fv(ldUniform + 1, 1, lightDefuse1);
		glUniform3fv(lsUniform + 1, 1, lightSpecular1);
		glUniform4fv(lpUniform + 1, 1, lightPosition1);

		glUniform3fv(kaUniform, 1, materialAmbiant);
		glUniform3fv(kdUniform, 1, materialDefuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShinynessUniform, materialShinyness);
	}
	else
	{
		glUniform1i(lKeypressedUniform, 0);
	}

	// opengl drawing
	// set modelview & modelviewprojection matrix to identity
	mat4 TranslateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 RotationMatrix = vmath::rotate(pyramidAngle, 0.0f, 1.0f, 0.0f);
	mat4 modelViewMatrix = TranslateMatrix * RotationMatrix;
	//mat4 modelViewProjectionMatrix = mat4::identity();

	// multiply modelview and orthographic matrix to get modelviewprojection matrix
	//modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// order is important

	// pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
	// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
	glUniformMatrix4fv(gprojectionUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);

	// bind vao
	glBindVertexArray(gVao_Pyramid);

	// draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLES, 0, 12);	// 12 total vertex

	// unbind vao
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	pyramidAngle += 0.2f;
	if (pyramidAngle >= 360.0f)
		pyramidAngle = 0.0f;

	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glOrtho(left, right, bottom, top, near, far);
	//if (width <= height)
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	//else
	//	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1, &gVao_Pyramid);
		gVao_Pyramid = 0;
	}

	// destroy vbo
	if (gVbo_Position_Pyramid)
	{
		glDeleteVertexArrays(1, &gVbo_Position_Pyramid);
		gVbo_Position_Pyramid = 0;
	}

	if (gVbo_Pyramid_Normal)
	{
		glDeleteVertexArrays(1, &gVbo_Pyramid_Normal);
		gVbo_Pyramid_Normal = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
