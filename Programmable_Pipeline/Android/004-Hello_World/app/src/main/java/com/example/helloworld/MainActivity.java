package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;

import android.graphics.Color;

import android.view.Gravity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));

		AppCompatTextView myTextView = new AppCompatTextView(this);
		myTextView.setText("Hello World !!!");
		myTextView.setTextSize(32);
		myTextView.setTextColor(Color.rgb(0, 255, 0));
		myTextView.setGravity(Gravity.CENTER);
		myTextView.setBackgroundColor(Color.rgb(0, 0, 0));
		
		setContentView(myTextView);
    }

	// @Override
    // protected void onResume(Bundle savedInstanceState) {
		// super.onResume(savedInstanceState);
    // }

	// @Override
    // protected void onPause(Bundle savedInstanceState) {
		// super.onPause(savedInstanceState);
    // }
	
}