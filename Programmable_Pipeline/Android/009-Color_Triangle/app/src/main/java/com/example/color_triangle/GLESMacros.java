package com.example.color_triangle;

public class GLESMacros
{
	// attribute index
	public static final int AAB_ATTRIBUTE_VERTEX = 0;
	public static final int AAB_ATTRIBUTE_COLOR = 1;
	public static final int AAB_ATTRIBUTE_NORMAL = 2;
	public static final int AAB_ATTRIBUTE_TEXTURE = 3;
}
