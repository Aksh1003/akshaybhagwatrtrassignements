package com.example.two_3d_shape_animation;

import android.content.Context;

import android.opengl.GLES32;

import android.opengl.GLSurfaceView;

import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;

import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	
	private GestureDetector gestureDetector;

	private int vertexShaderObject;

	private int fragmentShaderObject;

	private int shaderProgramObject;

	private int[] vao_triangle = new int[1];
	private int[] vbo_triangle = new int[1];
	private int[] vbo_triangle_color = new int[1];

	private int[] vao_square = new int[1];
	private int[] vbo_square = new int[1];
	
	float triangleAngle = 0.0f;
	float squareAngle = 0.0f;

	private int mvpUniform;

	private float perspectiveProjectionMatrix[] = new float[16];

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("AAB: OpenGL-ES Version = " + version);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("AAB: GLSL Version = " + glslVersion);

		Initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();

		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);

		return true;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAB: " + "Double Tap");
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAB: " + "Single Tap");
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAB: " + "Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		System.out.println("AAB: " + "Scroll");
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	private void Initialize(GL10 gl)
	{
		/* vertex shader */
		// create shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode= String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"out_color = vColor;"+
			"}"
		);

		// provide source code to shader
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		// compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAB: Vertex Shader Compilation Log = " + szInfoLog);
				UnInitialize();
				System.exit(0);
			}
		}

		/* Fragment Shader */
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragmentShaderSourceCode= String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = out_color;"+
			"}"
		);

		// provide source code to shader
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		// compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAB: Fragment Shader Compilation Log = " + szInfoLog);
				UnInitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AAB_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AAB_ATTRIBUTE_COLOR, "vColor");

		GLES32.glLinkProgram(shaderProgramObject);

		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompiledStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("AAB: Shader Program Link Log = " + szInfoLog);
				UnInitialize();
				System.exit(0);
			}
		}

		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

		final float triangleVertices[] = new float[]
									{
										0.0f, 0.5f, 0.0f,
										-0.5f, -0.5f, 0.5f,
										0.5f, -0.5f, 0.5f,

										0.0f, 0.5f, 0.0f,
										-0.5f, -0.5f, -0.5f,
										-0.5f, -0.5f, 0.5f,

										0.0f, 0.5f, 0.0f,
										0.5f, -0.5f, -0.5f,
										-0.5f, -0.5f, -0.5f,

										0.0f, 0.5f, 0.0f,
										0.5f, -0.5f, 0.5f,
										0.5f, -0.5f, -0.5f 
									};

		final float colorTriangle[] = new float[]
								{	0.0f, 0.0f, 1.0f,
									0.0f, 0.0f, 1.0f,
									0.0f, 0.0f, 1.0f,

									1.0f, 0.0f, 1.0f,
									1.0f, 0.0f, 1.0f,
									1.0f, 0.0f, 1.0f,

									0.0f, 1.0f, 1.0f,
									0.0f, 1.0f, 1.0f,
									0.0f, 1.0f, 1.0f,

									0.0f, 0.5f, 1.0f,
									0.0f, 0.5f, 1.0f,
									0.0f, 0.5f, 1.0f
								};

		final float squareVertices[] = new float[]
								{	0.5f, 0.5f, 0.5f,
									-0.5f, 0.5f, 0.5f,
									-0.5f, -0.5f, 0.5f,
									0.5f, -0.5f, 0.5f,

									-0.5f, 0.5f, -0.5f,
									0.5f, 0.5f, -0.5f,
									0.5f, -0.5f, -0.5f,
									-0.5f, -0.5f, -0.5f,

									0.5f, 0.5f, -0.5f,
									0.5f, 0.5f, 0.5f,
									0.5f, -0.5f, 0.5f,
									0.5f, -0.5f, -0.5f,

									-0.5f, 0.5f, 0.5f,
									-0.5f, 0.5f, -0.5f,
									-0.5f, -0.5f, -0.5f,
									-0.5f, -0.5f, 0.5f,

									0.5f, 0.5f, -0.5f,
									-0.5f, 0.5f, -0.5f,
									-0.5f, 0.5f, 0.5f,
									0.5f, 0.5f, 0.5f,

									-0.5f, -0.5f, -0.5f,
									0.5f, -0.5f, -0.5f,
									0.5f, -0.5f, 0.5f,
									-0.5f, -0.5f, 0.5f,
								};

		GLES32.glGenVertexArrays(1, vao_triangle, 0);
		GLES32.glBindVertexArray(vao_triangle[0]);

		GLES32.glGenBuffers(1, vbo_triangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_triangle[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(triangleVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							triangleVertices.length * 4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAB_ATTRIBUTE_VERTEX,
										3,
										GLES32.GL_FLOAT,
										false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAB_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_triangle_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_triangle_color[0]);

		byteBuffer = ByteBuffer.allocateDirect(colorTriangle.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(colorTriangle);
		colorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							colorTriangle.length * 4,
							colorBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAB_ATTRIBUTE_COLOR,
										3,
										GLES32.GL_FLOAT,
										false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAB_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		GLES32.glGenVertexArrays(1, vao_square, 0);
		GLES32.glBindVertexArray(vao_square[0]);

		GLES32.glGenBuffers(1, vbo_square, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square[0]);

		byteBuffer = ByteBuffer.allocateDirect(squareVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(squareVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							squareVertices.length * 4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAB_ATTRIBUTE_VERTEX,
										3,
										GLES32.GL_FLOAT,
										false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAB_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glVertexAttrib3f(GLESMacros.AAB_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);

		GLES32.glBindVertexArray(0);

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);

		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		GLES32.glEnable(GLES32.GL_CULL_FACE);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	
		GLES32.glUseProgram(shaderProgramObject);

		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float translateMatrix[] = new float[16];
		float rotateMatrix[] = new float[16];
		float scaleMatrix[] = new float[16];
		float tempMatrix[] = new float[16];
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.setIdentityM(tempMatrix, 0);

		Matrix.setRotateM(rotateMatrix, 0, triangleAngle, 0.0f, 1.0f, 0.0f);
		Matrix.translateM(translateMatrix, 0, -1.0f, 0.0f, -3.0f);
		
		Matrix.multiplyMM(modelViewMatrix, 0, translateMatrix, 0, rotateMatrix, 0);

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_triangle[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);

		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.setIdentityM(tempMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		Matrix.translateM(translateMatrix, 0, 1.0f, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);
		Matrix.setRotateM(rotateMatrix, 0, squareAngle, 0.0f, 1.0f, 0.0f);

		Matrix.multiplyMM(tempMatrix, 0, translateMatrix, 0, rotateMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, tempMatrix, 0, scaleMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_square[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		triangleAngle += 0.3f;
		if(triangleAngle >= 360.0f)
			triangleAngle = 0.0f;

		squareAngle += 0.3f;
		if(squareAngle >= 360.0f)
			squareAngle = 0.0f;

		requestRender();
	}

	void UnInitialize()
	{
		if(vao_triangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_triangle, 0);
			vao_triangle[0] = 0;
		}

		if(vbo_triangle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_triangle, 0);
			vbo_triangle[0] = 0;
		}

		if(vbo_triangle_color[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_triangle_color, 0);
			vbo_triangle_color[0] = 0;
		}

		if(vao_square[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_square, 0);
			vao_square[0] = 0;
		}

		if(vbo_square[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_square, 0);
			vbo_square[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
