#pragma once

#define _USE_MATH_DEFINES

#include <Windows.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>
#include <gl\glu.h>

//macro
#define DEG_TO_RAD(angle) ((angle) * M_PI / 180.0f)

//typedef
typedef GLfloat GLMatrix[16];
typedef GLfloat GLVector[3];

//struct 
typedef struct
{
    GLVector vLocation;
    GLVector vUp;
    GLVector vForward;

} GLFrame;

//Main.cpp
bool LoadGLTexture(GLuint *texture, TCHAR resourceID[]);

//CameraMovement.cpp

void ApplyCameraTransform(GLFrame *);
void CameraMoveForward(GLFrame *, GLfloat);
void CameraMoveRight(GLFrame *, GLfloat);
void CameraMoveUp(GLFrame *, GLfloat);
void CameraRotateAlongX(GLFrame *, GLfloat);    //angle
void CameraRotateAlongY(GLFrame *, GLfloat);    //angle
void CameraRotateAlongZ(GLFrame *, GLfloat);    //angle


//Temple.cpp
void LoadTempleTexture(void);
void DeleteTempleTexture(void);
void vjdRenderBigTemple(void);
void abTemple(void);

//House.cpp
void LoadHouseTexture(void);
void DeleteHouseTexture(void);
void vmRenderHouse(void);

//Diya.cpp
void dvDiya(void);

// Ground.cpp
void LoadGroundTexture(void);
void abRenderGround(void);
void DeleteGroundTexture(void);

// Kandil.cpp
void LoadKandilTexture(void);
void abRenderKandil(void);
void abKandilRod(void);
void DeleteKandilTexture(void);

// Rangoli.cpp
void ARB_Rangoli_1(void);
void ARB_Rangoli_2(void);
void ARB_Rangoli_3(void);
void ARB_Rangoli_4(void);

// SunAndMoon.cpp
void LoadSunAndMoonTexture(void);
void snRenderSun(void);
void snRenderMoon(void);
void abRenderStars(void);
void abRenderDarkSphere(void);
void DeleteSunAndMoonTexture(void);

// Tree.cpp
void LoadTreeTexture(void);
void Tree(void);
void Tree2(void);
void DeleteTreeTexture(void);

// Cloud.cpp
void vmCloudTypeOne(void);
void vmCloudTypeTwo(void);
void vmCloudTypeThree(void);

//Tulsi.cpp
void LoadTulasTextureAndData(void);
void DeleteTualsTextureAndData(void);
void nlRenderTulas(void);
BITMAPFILEHEADER *DibLoadImage(PTSTR);

//Humanoid.cpp
void LoadHumanTexture(void);
void vmAdultHuman(void);
void vmChildHuman(void);
void DeleteHumanTexture(void);

//Firecracker.cpp
void LoadFireCrackerAndBottleTexture(void);
void DeleteFireCrackerAndBottleTexture(void);
void ankitFireWorks(void);
void ankitDrawFireCracker(void);
bool ankitDrawBurst(void);

//DiwaliWishSceneEffect.cpp
bool InitializeDiwaliWishesScene(void);

    //false: if particles are not at their final position (call function again)
    //true:  All Particles are at their final position
bool vjdRenderDiwaliParticlesScene(void);
void UninitializeDiwaliWishesResources(void);

//CreditScene.cpp
bool ankitCreditScene(void);
void ankitPrintString(char str[]);																																				  

//FinalCreditEffect.cpp
void vjdInitializeFinalScene(void);
void vjdRenderFinalCredit(void);
void vjdUninitializeFinalScene(void);


