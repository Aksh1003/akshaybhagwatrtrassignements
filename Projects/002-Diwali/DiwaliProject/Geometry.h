//
//  2D-3D shape drawing function
//

#pragma once

#define _USE_MATH_DEFINES

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <math.h>

//macro
#define CIRCLE_POINT 512

//function declaration
void RenderTriangle(GLfloat fExtent);
void RenderSquare(GLfloat fLength);
void RenderCircle(GLfloat fRadius);
void RenderPlane(GLfloat fLength);
void RenderCube(GLfloat fLength);
void RenderCone(GLfloat fBaseRadius, GLfloat fHeight);
void RenderPyramid(GLfloat fExtent, GLfloat fHeight);

