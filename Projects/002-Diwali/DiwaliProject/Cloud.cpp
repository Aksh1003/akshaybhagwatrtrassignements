//Headers
#include "Main.h"
#include "Resource.h"

void vmCloudTypeOne(void) 
{
	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.03f * cos(angle) - 0.35f, 0.03f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.045f * cos(angle) - 0.3f, 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.04f * cos(angle) - 0.25f, 0.04f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.2f, 0.07f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.05f * cos(angle) - 0.1f, 0.05f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.1f, 0.07f * sin(angle) - 0.055f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.06f * cos(angle) - 0.0f, 0.06f * sin(angle) - 0.055f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1f + 0.06f * cos(angle), 0.06f * sin(angle) - 0.05f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.09f * cos(angle) - 0.07f, 0.09f * sin(angle) + 0.07f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.09f * cos(angle) + 0.05f, 0.09f * sin(angle) + 0.05f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.9f, 0.9f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) + 0.15f, 0.07f * sin(angle) + 0.03f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.9f, 0.9f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.15f, 0.07f * sin(angle) + 0.03f, 0.0f);
	}
	glEnd();


	// MID OF CLOUD
	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.08f * cos(angle), 0.08f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1 + 0.05f * cos(angle), 0.05f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.2 + 0.07f * cos(angle), 0.07f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.25 + 0.04f * cos(angle), 0.04f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.3 + 0.045f * cos(angle), 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.35 + 0.03f * cos(angle), 0.03f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.38 + 0.02f * cos(angle), 0.02f * sin(angle), 0.0f);
	}
	glEnd();
}

void vmCloudTypeTwo(void) 
{
	// Cloud 2
	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.035f * cos(angle) - 0.33f, 0.035f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.04f * cos(angle) - 0.2f, 0.04f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.04f * cos(angle) - 0.25f, 0.04f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.2f, 0.07f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.045f * cos(angle) - 0.1f, 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.1f, 0.07f * sin(angle) - 0.055f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.06f * cos(angle) - 0.05f, 0.06f * sin(angle) - 0.05f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1f + 0.06f * cos(angle), 0.06f * sin(angle) - 0.05f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.07f, 0.07f * sin(angle) + 0.07f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.09f * cos(angle) + 0.05f, 0.09f * sin(angle) + 0.05f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.9f, 0.9f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) + 0.15f, 0.07f * sin(angle) + 0.03f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.9f, 0.9f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.15f, 0.07f * sin(angle) + 0.03f, 0.0f);
	}
	glEnd();


	// MID OF CLOUD
	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.08f * cos(angle), 0.08f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1 + 0.05f * cos(angle), 0.05f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.2 + 0.07f * cos(angle), 0.07f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.25 + 0.04f * cos(angle), 0.04f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.3 + 0.045f * cos(angle), 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.35 + 0.03f * cos(angle), 0.03f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.38 + 0.02f * cos(angle), 0.02f * sin(angle), 0.0f);
	}
	glEnd();
}

void vmCloudTypeThree(void) 
{
	// Cloud 3
	glBegin(GL_POLYGON);
	glColor4f(0.7f, 0.7f, 0.7f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.03f * cos(angle) - 0.25f, 0.03f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.7f, 0.7f, 0.7f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.045f * cos(angle) - 0.2f, 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.06f * cos(angle) + 0.17f, 0.06f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.1f, 0.07f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.05f * cos(angle) - 0.1f, 0.05f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.07f * cos(angle) - 0.1f, 0.07f * sin(angle) - 0.055f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.06f * cos(angle) - 0.0f, 0.06f * sin(angle) - 0.055f, 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1f + 0.06f * cos(angle), 0.06f * sin(angle) - 0.05f, 0.0f);
	}
	glEnd();



	// MID OF CLOUD
	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.8f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.08f * cos(angle), 0.08f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.8f, 0.8f, 0.8f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.1 + 0.05f * cos(angle), 0.05f * sin(angle), 0.0f);
	}
	glEnd();



	glBegin(GL_POLYGON);
	glColor4f(0.7f, 0.7f, 0.7f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.25f + 0.045f * cos(angle), 0.045f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.3f + 0.03f * cos(angle), 0.03f * sin(angle), 0.0f);
	}
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.6f, 0.6f, 0.9f);
	for (GLfloat angle = 0.0f; angle <= 3.14 * 2; angle = angle + 0.01f) {
		glVertex3f(0.32f + 0.02f * cos(angle), 0.02f * sin(angle), 0.0f);
	}
	glEnd();
}
