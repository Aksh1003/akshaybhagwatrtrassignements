/*
        Code :- Swapnil Nikam & Akshay Bhagwat
 */

 //Headers
#include "Main.h"
#include "Resource.h"

//global variable declarations
static GLuint sunTexture, moonTexture;
GLUquadric* quadric = NULL;

void LoadSunAndMoonTexture(void)
{
    //code
    LoadGLTexture(&sunTexture, MAKEINTRESOURCE(SN_SUN_BITMAP));
    LoadGLTexture(&moonTexture, MAKEINTRESOURCE(SN_MOON_BITMAP));
}

void DeleteSunAndMoonTexture(void)
{
    //code
    if (sunTexture)
    {
        glDeleteTextures(1, &sunTexture);
        sunTexture = 0;
    }

    if (moonTexture)
    {
        glDeleteTextures(1, &moonTexture);
        moonTexture = 0;
    }
}

void snRenderSun(void)
{
    //code
    glBindTexture(GL_TEXTURE_2D, 0);
    // glBindTexture(GL_TEXTURE_2D, sunTexture);
    glColor3f(1.0f, 1.0f, 0.0f);
    //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);              //GLUT WireSphere
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);              //  B

//    glTranslatef(0.0f, s1, 0.0f);					 // sun translate
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.30, 20, 20);
    // glBindTexture(GL_TEXTURE_2D, 0);
}

void snRenderMoon(void)
{
    //code
    glColor3f(1.0f, 1.0f, 1.0f);
    glBindTexture(GL_TEXTURE_2D, moonTexture);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

//    glTranslatef(0.0f, m1, 0.0f);					// moon translate
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.20, 20, 20);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void abRenderDarkSphere(void)
{
	//code
	glColor3f(0.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, GL_TRUE);
	gluSphere(quadric, 0.20, 20, 20);
}

void abRenderStars(void)
{
    //code
	glColor3f(1.0f, 1.0f, 1.0f);

//	glPointSize(3.0f);

	glBegin(GL_POINTS);

	glVertex3f(10.5f, 20.5f, 0.0f);
	glVertex3f(-10.5f, 20.5f, 0.0f);
	glVertex3f(-10.5f, 30.5f, 0.0f);
	glVertex3f(10.5f, 30.5f, 0.0f);

	glVertex3f(19.0f, 11.0f, 0.0f);
	glVertex3f(-19.0f, 11.0f, 0.0f);
	glVertex3f(-19.0f, 11.0f, 0.0f);
	glVertex3f(19.0f, 11.0f, 0.0f);

	glVertex3f(13.3f, 10.0f, 0.0f);
	glVertex3f(13.3f, 10.0f, 0.0f);
	glVertex3f(13.3f, 10.0f, 0.0f);
	glVertex3f(13.3f, 10.0f, 0.0f);

	glVertex3f(40.5f, 25.5f, 0.0f);
	glVertex3f(40.5f, 25.5f, 0.0f);
	glVertex3f(40.5f, 25.5f, 0.0f);
	glVertex3f(40.5f, 25.5f, 0.0f);

	glVertex3f(50.8f, 20.4f, 0.0f);
	glVertex3f(50.8f, 21.4f, 0.0f);
	glVertex3f(50.8f, 30.4f, 0.0f);
	glVertex3f(50.8f, 41.4f, 0.0f);

	glVertex3f(11.8f, 21.2f, 0.0f);
	glVertex3f(21.8f, 12.2f, 0.0f);
	glVertex3f(31.8f, 13.2f, 0.0f);
	glVertex3f(41.8f, 17.2f, 0.0f);

	glVertex3f(10.5f, 20.5f, 0.0f);
	glVertex3f(-10.5f, 20.5f, 0.0f);
	glVertex3f(-10.5f, 30.5f, 0.0f);
	glVertex3f(10.5f, 30.5f, 0.0f);

	glVertex3f(-19.0f, 21.0f, 0.0f);
	glVertex3f(-19.0f, 25.0f, 0.0f);
	glVertex3f(-19.0f, 11.0f, 0.0f);
	glVertex3f(-19.0f, 19.0f, 0.0f);

	glVertex3f(-13.3f, 10.0f, 0.0f);
	glVertex3f(-13.3f, 10.0f, 0.0f);
	glVertex3f(-13.3f, 10.0f, 0.0f);
	glVertex3f(-13.3f, 10.0f, 0.0f);

	glVertex3f(-40.5f, 25.5f, 0.0f);
	glVertex3f(-40.5f, 25.5f, 0.0f);
	glVertex3f(-40.5f, 25.5f, 0.0f);
	glVertex3f(-40.5f, 25.5f, 0.0f);

	glVertex3f(-50.8f, 20.4f, 0.0f);
	glVertex3f(-50.8f, 21.4f, 0.0f);
	glVertex3f(-50.8f, 30.4f, 0.0f);
	glVertex3f(-50.8f, 41.4f, 0.0f);

	glVertex3f(-11.8f, 21.2f, 0.0f);
	glVertex3f(-21.8f, 12.2f, 0.0f);
	glVertex3f(-31.8f, 13.2f, 0.0f);
	glVertex3f(-41.8f, 17.2f, 0.0f);

	glVertex3f(0.2f, 0.10f, 0.0f);
	glVertex3f(-0.5f, 0.10f, 0.0f);
	glVertex3f(-0.6f, 0.20f, 0.0f);
	glVertex3f(0.3f, 0.19f, 0.0f);

	glVertex3f(0.0f, 0.29f, 0.0f);
	glVertex3f(0.3f, 0.30f, 0.0f);
	glVertex3f(-0.3f, 0.11f, 0.0f);
	glVertex3f(2.0f, 0.29f, 0.0f);
	glVertex3f(-1.3f, 1.40f, 0.0f);
	glVertex3f(0.0f, 1.25f, 0.0f);

	glVertex3f(-1.8f, 0.30f, 0.0f);
	glVertex3f(0.3f, 1.30f, 0.0f);
	glVertex3f(-0.3f, 1.30f, 0.0f);

	glVertex3f(10.3f, 3.3f, 0.0f);
	glVertex3f(20.3f, 3.3f, 0.0f);
	glVertex3f(30.3f, 3.3f, 0.0f);
	glVertex3f(40.3f, 3.3f, 0.0f);

	glVertex3f(-10.3f, 5.3f, 0.0f);
	glVertex3f(-20.3f, 5.3f, 0.0f);
	glVertex3f(-30.3f, 5.3f, 0.0f);
	glVertex3f(-40.3f, 5.3f, 0.0f);

	glVertex3f(-2.5f, 22.5f, 0.0f);
	glVertex3f(2.5f, 23.5f, 0.0f);
	glVertex3f(2.5f, 24.5f, 0.0f);
	glVertex3f(-2.5f, 25.5f, 0.0f);

	glVertex3f(-50.0f, 3.0f, 0.0f);
	glVertex3f(50.0f, 3.0f, 0.0f);
	glVertex3f(60.0f, 3.0f, 0.0f);
	glVertex3f(-60.0f, 3.0f, 0.0f);

	glVertex3f(-3.2f, 27.0f, 0.0f);
	glVertex3f(3.2f, 28.0f, 0.0f);
	glVertex3f(3.2f, 29.0f, 0.0f);
	glVertex3f(-3.2f, 29.0f, 0.0f);

	glEnd();
}