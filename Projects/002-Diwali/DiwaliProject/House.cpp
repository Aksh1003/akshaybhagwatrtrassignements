/*
        Code :- Vishwajeet Mane
 */

//Headers
#include "Main.h"
#include "Geometry.h"
#include "Resource.h"

//variable declarations
static GLuint vmDOOR_TEXTURE;
static GLuint vmWALL_TEXTURE;
static GLuint vmROOF_TEXTURE;
static GLuint vmROOF2_TEXTURE;
static GLuint vmPILLAR_TEXTURE;
static GLuint vmFLOOR_TEXTURE;
static GLuint vmFLOOR2_TEXTURE;
static GLuint vmSTEPFLOOR_TEXTURE;
static GLuint vmWINDOWBIG_TEXTURE;
static GLuint vmWINDOWSMALL_TEXTURE;
static GLuint vmMARIGOLDFLOWER_TEXTURE;

static GLuint vjdHouseGroundTexture;								
static GLUquadric* vmQuadric = NULL;

static GLfloat vmBulbColor[4][3] =
                {
                    {1.0f,1.0f,0.0f},
                    {0.0f,1.0f,0.0f},
                    {1.0f,0.0f,0.0f},
                    {0.0f,0.0f,1.0f}
                };

static int     vmBulbColorIndex = 0;
static BOOLEAN vmBulbBoolean = TRUE;
static int     vmBulbTimer = 0;


extern GLfloat xTrans, yTrans, zTrans;

//
//LoadHouseTexture() :- Initialize texture required in this file
//
void LoadHouseTexture(void)
{
    //code
    LoadGLTexture(&vmDOOR_TEXTURE, MAKEINTRESOURCE(VM_DOOR_BITMAP));
	LoadGLTexture(&vmWALL_TEXTURE, MAKEINTRESOURCE(VM_WALL_BITMAP));
	LoadGLTexture(&vmROOF_TEXTURE, MAKEINTRESOURCE(VM_ROOF_BITMAP));
    
	LoadGLTexture(&vmPILLAR_TEXTURE, MAKEINTRESOURCE(VM_PILLAR_BITMAP));
	LoadGLTexture(&vmFLOOR_TEXTURE,  MAKEINTRESOURCE(VM_FLOOR_BITMAP));
	LoadGLTexture(&vmFLOOR2_TEXTURE, MAKEINTRESOURCE(VM_FLOOR2_BITMAP));
    
	LoadGLTexture(&vmSTEPFLOOR_TEXTURE, MAKEINTRESOURCE(VM_STEPFLOOR_BITMAP));
	LoadGLTexture(&vmROOF2_TEXTURE,     MAKEINTRESOURCE(VM_ROOF2_BITMAP));
	LoadGLTexture(&vmWINDOWBIG_TEXTURE, MAKEINTRESOURCE(VM_WINDOWBIG_BITMAP));
    
	LoadGLTexture(&vmWINDOWSMALL_TEXTURE, MAKEINTRESOURCE(VM_WINDOWSMALL_BITMAP));
	LoadGLTexture(&vmMARIGOLDFLOWER_TEXTURE, MAKEINTRESOURCE(VM_MARIGOLDFLOWER_BITMAP));
	LoadGLTexture(&vjdHouseGroundTexture, MAKEINTRESOURCE(VJD_HOUSE_GROUND_TEXTURE));																			  
}


//
//DeleteHouseTexture() :- Delete Texture object created in this file
//
void DeleteHouseTexture(void)
{
    //code
    if(vmQuadric)
    {
        gluDeleteQuadric(vmQuadric);
        vmQuadric = NULL;
    }
    
    glDeleteTextures(1, &vmDOOR_TEXTURE);
	glDeleteTextures(1, &vmWALL_TEXTURE);
	glDeleteTextures(1, &vmROOF_TEXTURE);
	glDeleteTextures(1, &vmPILLAR_TEXTURE);
	glDeleteTextures(1, &vmFLOOR_TEXTURE);
	glDeleteTextures(1, &vmFLOOR2_TEXTURE);
	glDeleteTextures(1, &vmSTEPFLOOR_TEXTURE);
	glDeleteTextures(1, &vmROOF2_TEXTURE);
	glDeleteTextures(1, &vmWINDOWBIG_TEXTURE);
	glDeleteTextures(1, &vmWINDOWSMALL_TEXTURE);
	glDeleteTextures(1, &vmMARIGOLDFLOWER_TEXTURE);
	glDeleteTextures(1, &vjdHouseGroundTexture);										 
}


//
//vmRenderHouse()
//
void vmRenderHouse(void) {

	// Function Declaration
	void vmBottomCubeHouse(void);
	void vmRoof(void);
	void vmRoofEntry(void);
	void vmFoundationCube(void);
	void vmPillarFrontSideRight1(void);
	void vmPillarFrontSideRight2(void);
	void vmPillarFrontSideLeft1(void);
	void vmPillarFrontSideLeft2(void);
	void vmPillarBackSide1(void);
	void vmPillarBackSide2(void);
	void vmHouseSteps(void);
	void vmHouseFrontDoor(void);
	void vmHouseBigWindow(void);
	void vmRoundLightingCenter(void);
	void vmPillarLights(void);

	// Variable Declaration

	// Code
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glColor3f(0.5f, 0.3f, 0.05f);


	glTranslatef(0.0f, 0.0f, -6.0f);

	glRotatef(5.0f, 1.0f, 0.0f, 0.0f);

	//glRotatef(0.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(35.0f, 1.0f, 0.0f, 0.0f);   // Camera Angle
	

	
	// Model Calling
	vmFoundationCube();
	vmRoof();
	vmBottomCubeHouse();
	vmRoofEntry();

	// Pillars
	vmPillarFrontSideRight1();
	vmPillarFrontSideRight2();
	vmPillarFrontSideLeft1();
	vmPillarFrontSideLeft2();

	vmPillarBackSide1();
	vmPillarBackSide2();

	// STEPS
	vmHouseSteps();

	// DOOR
	vmHouseFrontDoor();

	// HOUSE WINDOWS
	vmHouseBigWindow();

	// Lighting
	vmRoundLightingCenter();
	vmPillarLights();
	
		 glPushMatrix();
        glColor3f(1.0f, 1.0f, 1.0f);
        glBindTexture(GL_TEXTURE_2D, vjdHouseGroundTexture);
        glTranslatef(0.0f, -0.73f, 0.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        
        RenderCircle(3.95f);
    glPopMatrix();		   
	glBindTexture(GL_TEXTURE_2D, 0);

	// vmDoorTooranCenter();
    
    
    if (vmBulbBoolean == TRUE && vmBulbTimer <= 50) {
		vmBulbTimer++;
	}
	if (vmBulbTimer == 50) {
		vmBulbBoolean = FALSE;
		vmBulbColorIndex++;
	}

	// Changing Color - Reset
	if (vmBulbColorIndex >= 4) {
		vmBulbColorIndex = 0;
	}

	if (vmBulbBoolean == FALSE && vmBulbTimer >= 0) {
		vmBulbTimer --;
	}
	if (vmBulbTimer == 00) {
		vmBulbBoolean = TRUE;
	}
}


//
//vmHouseBigWindow()
//
void vmHouseBigWindow(void) {
	glColor3f(0.7f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, vmWINDOWBIG_TEXTURE);
	// RIGHT SIDE WINDOW - FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.25f, 0.25f, 1.0001f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.75f, 0.25f, 1.0001f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.75f, -0.1f, 1.0001f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.25f, -0.1f, 1.0001f);
	glEnd();


	// LEFT SIDE WINDOW - FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.25f, 0.25f, 1.0001f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.75f, 0.25f, 1.0001f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.75f, -0.1f, 1.0001f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.25f, -0.1f, 1.0001f);
	glEnd();


	// RIGHT SIDE - BACK
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.25f, 0.25f, -1.0001f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.75f, 0.25f, -1.0001f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.75f, -0.1f, -1.0001f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.25f, -0.1f, -1.0001f);
	glEnd();

	// LEFT SIDE - BACK
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.25f, 0.25f, -1.0001f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.75f, 0.25f, -1.0001f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.75f, -0.1f, -1.0001f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.25f, -0.1f, -1.0001f);
	glEnd();


	glColor3f(0.7f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, vmWINDOWSMALL_TEXTURE);
	// RIGHT SIDE - LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5001f, 0.25f, 0.55f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5001f, 0.25f, 0.75f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5001f, -0.1f, 0.75f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5001f, -0.1f, 0.55f);
	glEnd();

	// RIGHT SIDE - RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5001f, 0.25f, -0.55f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5001f, 0.25f, -0.75f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5001f, -0.1f, -0.75f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5001f, -0.1f, -0.55f);
	glEnd();


	// LEFT SIDE - RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5001f, 0.25f, 0.55f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5001f, 0.25f, 0.75f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5001f, -0.1f, 0.75f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5001f, -0.1f, 0.55f);
	glEnd();

	// LEFT SIDE - LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5001f, 0.25f, -0.55f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5001f, 0.25f, -0.75f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5001f, -0.1f, -0.75f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5001f, -0.1f, -0.55f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}


//
//vmFoundationCube()
//
void vmFoundationCube() {
	// Front
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmFLOOR2_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.0f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -0.75f, 1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, -0.75f, 1.5f);
	glEnd();

	// Right
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(2.0f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(2.0f, -0.75f, 1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, -0.75f, -1.5f);
	glEnd();

	// Back
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -0.75f, -1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, -0.75f, -1.5f);
	glEnd();

	// Left
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -0.75f, 1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-2.0f, -0.75f, -1.5f);
	glEnd();

	// Top 
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmFLOOR_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, -0.5f, -1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -0.5f, 1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, -0.5f, 1.5f);
	glEnd();

}


//
//vmBottomCubeHouse()
//
void vmBottomCubeHouse() {
	glColor3f(1.0f, 1.0f, 1.0f);

	// Front Wall
	glBindTexture(GL_TEXTURE_2D, vmWALL_TEXTURE);
	// Texture and Color
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5f, 0.5f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5f, 0.5f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5f, -0.5f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5f, -0.5f, 1.0f);
	glEnd();

	// Below Roof Patch - Center
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 0.75, 1.0f);
	glTexCoord2f(0.35f, 0.75f);
	glVertex3f(-0.35f, 0.5f, 1.0f);
	glTexCoord2f(0.65f, 0.75f);
	glVertex3f(0.35f, 0.5f, 1.0f);
	glEnd();

	// Right Wall
	glBegin(GL_QUADS);
	glTexCoord2f(0.75f, 1.0f);
	glVertex3f(1.5f, 0.5f, -1.0f);
	glTexCoord2f(0.25f, 1.0f);
	glVertex3f(1.5f, 0.5f, 1.0f);
	glTexCoord2f(0.25f, 0.0f);
	glVertex3f(1.5f, -0.5f, 1.0f);
	glTexCoord2f(0.75f, 0.0f);
	glVertex3f(1.5f, -0.5f, -1.0f);
	glEnd();

	// Back Wall
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5f, 0.5f, -1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5f, 0.5f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5f, -0.5f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5f, -0.5f, -1.0f);
	glEnd();

	// Left Wall
	glBegin(GL_QUADS);
	glTexCoord2f(0.75f, 1.0f);
	glVertex3f(-1.5f, 0.5f, -1.0f);
	glTexCoord2f(0.25f, 1.0f);
	glVertex3f(-1.5f, 0.5f, 1.0f);
	glTexCoord2f(0.25f, 0.0f);
	glVertex3f(-1.5f, -0.5f, 1.0f);
	glTexCoord2f(0.75f, 0.0f);
	glVertex3f(-1.5f, -0.5f, -1.0f);
	glEnd();

}


//
//vmHouseFrontDoor()
//
void vmHouseFrontDoor() {
	glColor3f(1.0f, 1.0f, 1.0f);

	// Texture
	glBindTexture(GL_TEXTURE_2D, vmDOOR_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.3f, 0.3f, 1.0001f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.3f, 0.3f, 1.0001f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.3f, -0.5f, 1.0001f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.3f, -0.5f, 1.0001f);
	glEnd();
}


//
//vmRoof()
//
void vmRoof() {
	// RED TINT FOR ROOF
	glColor3f(0.7f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, vmROOF_TEXTURE);
	// FRONT ROOF
	// - LEFT SIDE
	// USE THIS for ROOF 
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f,0.6f);
	glVertex3f(0.0f, 0.75f, 0.55f);
	glTexCoord2f(0.25f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, 0.35f, 1.5f);
	glTexCoord2f(0.75f, 0.0f);
	glVertex3f(-0.5f, 0.35f, 1.5f);
	glEnd();

	// - MIDDLE PATCH
	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.35f, 0.25f);
	glVertex3f(-0.5f, 0.5f, 1.0f);
	glTexCoord2f(0.65f, 0.25f);
	glVertex3f(0.5f, 0.5f, 1.0f);
	glEnd();

	// - FRONT - RIGHT SIDE 
	//glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.6f);
	glVertex3f(0.0f, 0.75f, 0.55f);
	glTexCoord2f(0.25f, 1.0f);
	glVertex3f(1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(2.0f, 0.35f, 1.5f);
	glTexCoord2f(0.75f, 0.0f);
	glVertex3f(0.5f, 0.35f, 1.5f);
	glEnd();


	
	// RIGHT ROOF
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(2.0f, 0.35f, 1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, 0.35f, -1.5f);
	glEnd();

	

	// LEFT ROOF
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, 0.35f, -1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-2.0f, 0.35f, 1.5f);
	glEnd();


	//glColor3f(0.7f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, vmROOF2_TEXTURE);
	// BACK ROOF - Different Texture
	glBegin(GL_QUADS);
	glTexCoord2f(0.85f, 1.0f);
	glVertex3f(1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.15f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, 0.35f, -1.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.0f, 0.35f, -1.5f);
	glEnd();

}


//
//vmRoofEntry()
//
void vmRoofEntry() {
	// RED TINT FOR ROOF
	glColor3f(0.7f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, vmROOF_TEXTURE);

	// RIGHT ENTRY ROOF
	// - TOP
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.77f, 0.4f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 0.77f, 1.9f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.5f, 0.37f, 1.9f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(0.5f, 0.37f, 1.35f);
	glEnd();
	
	// - FRONT
	glBegin(GL_QUADS);
	glVertex3f(0.5f, 0.37f, 1.9f);
	glVertex3f(0.0f, 0.77f, 1.9f);
	glVertex3f(0.0f, 0.75f, 1.9f);
	glVertex3f(0.5f, 0.35f, 1.9f);
	glEnd();

	// - BOTTOM
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.75f, 0.55f);
	glVertex3f(0.0f, 0.75f, 1.9f);
	glVertex3f(0.5f, 0.35f, 1.9f);
	glVertex3f(0.5f, 0.35f, 1.5f);
	glEnd();


	// LEFT ENTRY ROOF
	// RED TINT FOR ROOF
	glColor3f(0.7f, 0.7f, 0.7f);
	// - TOP
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.77f, 0.4f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 0.77f, 1.9f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, 0.37f, 1.9f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(-0.5f, 0.37f, 1.35f);
	glEnd();

	// - FRONT
	glBegin(GL_QUADS);
	glVertex3f(-0.5f, 0.37f, 1.9f);
	glVertex3f(0.0f, 0.77f, 1.9f);
	glVertex3f(0.0f, 0.75f, 1.9f);
	glVertex3f(-0.5f, 0.35f, 1.9f);
	glEnd();

	// - BOTTOM
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.75f, 0.55f);
	glVertex3f(0.0f, 0.75f, 1.9f);
	glVertex3f(-0.5f, 0.35f, 1.9f);
	glVertex3f(-0.5f, 0.35f, 1.5f);
	glEnd();
}


//
//vmPillarFrontSideRight1()
//
void vmPillarFrontSideRight1(void) {
    //variable declarations    
    GLfloat vmBulbLocationY;
    
    //code
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.95f);
	glVertex3f(1.95f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.95f);
	glVertex3f(1.85f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, 1.45f);
	glEnd();


	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.95f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(1.95f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, 1.35f);
	glEnd();

	// BACK
	// USE THIS for ROOF glColor3f(0.5f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(1.85f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(1.95f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, 1.35f);
	glEnd();


	// LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(1.85f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.85f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, 1.45f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}


//
//vmPillarFrontSideRight2()
//
void vmPillarFrontSideRight2(void) {
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.9f);
	glVertex3f(0.5f, 0.35f, 1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.45f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 1.45f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.9f);
	glVertex3f(0.5f, 0.35f, 1.35f);
	glTexCoord2f(0.0f, 0.9f);
	glVertex3f(0.5f, 0.35f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 1.35f);
	glEnd();

	// BACK
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.45f, 1.35f);
	glTexCoord2f(0.0f, 0.9f);
	glVertex3f(0.5f, 0.35f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 1.35f);
	glEnd();


	// LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.45f, 1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.45f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 1.45f);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, 0);
}


//
//vmPillarFrontSideLeft1()
//
void vmPillarFrontSideLeft1(void) {
	
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.95f);
	glVertex3f(-1.85f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.95f);
	glVertex3f(-1.95f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, 1.45f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(-1.85f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.85f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, 1.45f);
	glEnd();

	// BACK
	// TINT FOR BRICKS- glColor3f(0.5f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(-1.85f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(-1.95f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, 1.35f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.95f, 0.4f, 1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(-1.95f, 0.375f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, 1.35f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// FRONT - LIGHT
	// STRIP - 
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-1.9f, 0.374f, 1.4501f);
	glVertex3f(-1.9f, -0.48f, 1.4501f);
	glEnd();
}


//
//vmPillarFrontSideLeft2()
//
void vmPillarFrontSideLeft2(void) {
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.4f, 0.45f, 1.45f);
	glTexCoord2f(0.0f, 0.9f);
	glVertex3f(-0.5f, 0.35f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 1.45f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.4f, 0.45f, 1.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.45f, 1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 1.35f);
	glEnd();

	// BACK
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.9f);
	glVertex3f(-0.5f, 0.35f, 1.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.45f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 1.35f);
	glEnd();


	// LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.9f);
	glVertex3f(-0.5f, 0.35f, 1.45f);
	glTexCoord2f(0.0f, 0.9f);
	glVertex3f(-0.5f, 0.35f, 1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 1.45f);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, 0);
}


//
//vmPillarBackSide1()
//
void vmPillarBackSide1(void) {
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.95f);
	glVertex3f(1.95f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.95f);
	glVertex3f(1.85f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, -1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, -1.45f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.95f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(1.95f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, -1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, -1.35f);
	glEnd();

	// BACK
	// USE THIS for ROOF glColor3f(0.5f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(1.85f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(1.95f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.95f, -0.5f, -1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, -1.35f);
	glEnd();


	// LEFT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(1.85f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.85f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, -1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.85f, -0.5f, -1.45f);
	glEnd();
}


//
//vmPillarBackSide2()
//
void vmPillarBackSide2(void) {
	glBindTexture(GL_TEXTURE_2D, vmPILLAR_TEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.95f);
	glVertex3f(-1.85f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.95f);
	glVertex3f(-1.95f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, -1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, -1.45f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(-1.85f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.85f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, -1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, -1.45f);
	glEnd();

	// BACK
	// TINT FOR BRICKS- glColor3f(0.5f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.975f);
	glVertex3f(-1.85f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(-1.95f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, -1.35f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.85f, -0.5f, -1.35f);
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.95f, 0.4f, -1.35f);
	glTexCoord2f(0.0f, 0.975f);
	glVertex3f(-1.95f, 0.375f, -1.45f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, -1.45f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.95f, -0.5f, -1.35f);
	glEnd();
}


//
//vmHouseSteps()
//
void vmHouseSteps(void) {
	// TOP STEP
	// - FRONT
	glColor3f(0.6f, 0.6f, 0.6f);
	glBindTexture(GL_TEXTURE_2D, vmWALL_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.5f, -0.5f, 1.65f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, -0.5f, 1.65f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.75f, 1.65f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.75f, 1.65f);
	glEnd();

	glColor3f(0.9f, 0.9f, 0.9f);
	// - RIGHT
	glBegin(GL_QUADS);
	glVertex3f(0.5f, -0.5f, 1.5f);
	glVertex3f(0.5f, -0.5f, 1.65f);
	glVertex3f(0.5f, -0.75f, 1.65f);
	glVertex3f(0.5f, -0.75f, 1.5f);
	glEnd();
	// - LEFT
	glBegin(GL_QUADS);
	glVertex3f(-0.5f, -0.5f, 1.5f);
	glVertex3f(-0.5f, -0.5f, 1.65f);
	glVertex3f(-0.5f, -0.75f, 1.65f);
	glVertex3f(-0.5f, -0.75f, 1.5f);
	glEnd();

	// - TOP
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmSTEPFLOOR_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.5f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, -0.5f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 1.65f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 1.65f);
	glEnd();


	// MIDDLE STEP
	// - FRONT
	glColor3f(0.6f, 0.6f, 0.6f);
	glBindTexture(GL_TEXTURE_2D, vmWALL_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.6f, -0.5833f, 1.8f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.6f, -0.5833f, 1.8f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -0.75f, 1.8f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.6f, -0.75f, 1.8f);
	glEnd();

	glColor3f(0.9f, 0.9f, 0.9f);
	// - RIGHT
	glBegin(GL_QUADS);
	glVertex3f(0.6f, -0.5833f, 1.5f);
	glVertex3f(0.6f, -0.5833f, 1.8f);
	glVertex3f(0.6f, -0.75f, 1.8f);
	glVertex3f(0.6f, -0.75f, 1.5f);
	glEnd();

	// - LEFT
	glBegin(GL_QUADS);
	glVertex3f(-0.6f, -0.5833f, 1.8f);
	glVertex3f(-0.6f, -0.5833f, 1.5f);
	glVertex3f(-0.6f, -0.75f, 1.5f);
	glVertex3f(-0.6f, -0.75f, 1.8f);
	glEnd();

	// - TOP
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmSTEPFLOOR_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.6f, -0.5833f, 1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.6f, -0.5833f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -0.5833f, 1.8f);
	glTexCoord2f(1.0f, 0.0f); 
	glVertex3f(0.6f, -0.5833f, 1.8f);
	glEnd();


	// BOTTOM STEP
	// - FRONT
	glColor3f(0.6f, 0.6f, 0.6f);
	glBindTexture(GL_TEXTURE_2D, vmWALL_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.7f, -0.66f, 1.95f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.7f, -0.66f, 1.95f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.7f, -0.75f, 1.95f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.7f, -0.75f, 1.95f);
	glEnd();

	glColor3f(0.9f, 0.9f, 0.9f);
	// - RIGHT
	glBegin(GL_QUADS);
	glVertex3f(0.7f, -0.66f, 1.5f);
	glVertex3f(0.7f, -0.66f, 1.95f);
	glVertex3f(0.7f, -0.75f, 1.95f);
	glVertex3f(0.7f, -0.75f, 1.5f);
	glEnd();
	// - LEFT
	glBegin(GL_QUADS);
	glVertex3f(-0.7f, -0.66f, 1.95f);
	glVertex3f(-0.7f, -0.66f, 1.5f);
	glVertex3f(-0.7f, -0.75f, 1.5f);
	glVertex3f(-0.7f, -0.75f, 1.95f);
	glEnd();
	// - TOP
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, vmSTEPFLOOR_TEXTURE);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.7f, -0.66f, 1.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.7f, -0.66f, 1.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.7f, -0.66f, 1.95f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.7f, -0.66f, 1.95f);
	glEnd();

}


void vmRoundLightingCenter(void) {

	// Center Lightning
	// - Light Gray Wire

	for (GLfloat angle = 0.0; angle >= -3.14; angle = angle - 0.01) {

		glColor3f(0.2f, 0.2f, 0.2f);
		glPointSize(1.0f);

		// Center
		glBegin(GL_POINTS);
		glVertex3f((0.45f * GLfloat(cos(angle))), (0.05f * GLfloat(sin(angle)) + 0.395f), 1.45f);
		glEnd();


		// Front - Right 1
		glBegin(GL_POINTS);
		glVertex3f((0.35f * cos(angle) + 0.85f), (0.1f * sin(angle) + 0.35f), 1.45f);
		glEnd();

		// Front - Right 2
		glBegin(GL_POINTS);
		glVertex3f((0.35f * cos(angle) + 1.55f), (0.1f * sin(angle) + 0.35f), 1.45f);
		glEnd();

		// Front - Left 1
		glBegin(GL_POINTS);
		glVertex3f((0.35f * cos(angle) - 0.85f), (0.1f * sin(angle) + 0.35f), 1.45f);
		glEnd();

		// Front - Left 2
		glBegin(GL_POINTS);
		glVertex3f((0.35f * cos(angle) - 1.55f), (0.1f * sin(angle) + 0.35f), 1.45f);
		glEnd();

		// Side - Right 1
		glBegin(GL_POINTS);
		glVertex3f(1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) + 0.75f));
		glEnd();


		// Side - Right 2
		glBegin(GL_POINTS);
		glVertex3f(1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) - 0.75f));
		glEnd();


		// Side - Left 1
		glBegin(GL_POINTS);
		glVertex3f(-1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) + 0.75f));
		glEnd();

		// Side - Left 2
		glBegin(GL_POINTS);
		glVertex3f(-1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) - 0.75f));
		glEnd();






	}



	for (GLfloat angle = 0.0f; angle >= -3.14; angle = angle - 0.3f) {

		if (vmBulbBoolean == TRUE) {
			glColor3f(vmBulbColor[vmBulbColorIndex][0], vmBulbColor[vmBulbColorIndex][1], vmBulbColor[vmBulbColorIndex][2]);
		}
		else {
			glColor3f(1.0f, 1.0f, 0.0f);
		}
		// Center
		glPushMatrix();
		glTranslatef((0.45f * cos(angle)), (0.05f * sin(angle) + 0.395f), 1.45f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Front - Right 1
		glPushMatrix();
		glTranslatef((0.35f * cos(angle) + 0.85f), (0.1f * sin(angle) + 0.35f), 1.45f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();

		// Front - Right 2
		glPushMatrix();
		glTranslatef((0.35f * cos(angle) + 1.55f), (0.1f * sin(angle) + 0.35f), 1.45f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Front - Left 1
		glPushMatrix();
		glTranslatef((0.35f * cos(angle) - 0.85f), (0.1f * sin(angle) + 0.35f), 1.45f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Front - Left 2
		glPushMatrix();
		glTranslatef((0.35f * cos(angle) - 1.55f), (0.1f * sin(angle) + 0.35f), 1.45f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Side Right - 1
		glPushMatrix();
		glTranslatef(1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) + 0.75f));
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Side Right - 2
		glPushMatrix();
		glTranslatef(1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) - 0.75f));
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Side Left - 1
		glPushMatrix();
		glTranslatef(-1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) + 0.75f));
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


		// Side Left - 2
		glPushMatrix();
		glTranslatef(-1.95f, (0.1f * sin(angle) + 0.35f), (0.65f * cos(angle) - 0.75f));
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


	}

}


void vmPillarLights(void) {
	// FRONT - LIGHT
	// STRIP - FrontSideRight1
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(1.9f, 0.374f, 1.4501f);
	glVertex3f(1.9f, -0.5f, 1.4501f);
	glEnd();

	// FRONT - LIGHT
	// STRIP - FrontSideRight2
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.45f, 0.374f, 1.4501f);
	glVertex3f(0.45f, -0.45f, 1.4501f); // Height of wire intentionally kept to look more realistic
	glEnd();

	// FRONT - LIGHT
	// STRIP - FrontSideRight1
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-1.9f, 0.374f, 1.4501f);
	glVertex3f(-1.9f, -0.48f, 1.4501f);
	glEnd();

	// FRONT - LIGHT
	// STRIP - FrontSideRight2
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.45f, 0.374f, 1.4501f);
	glVertex3f(-0.45f, -0.45f, 1.4501f);
	glEnd();



	for (GLfloat vmBulbLocationY = 0.36f; vmBulbLocationY > -0.48f; vmBulbLocationY -= 0.09f) {
		if (vmBulbBoolean == TRUE) {
			glColor3f(vmBulbColor[vmBulbColorIndex][0], vmBulbColor[vmBulbColorIndex][1], vmBulbColor[vmBulbColorIndex][2]);
		}
		else {
			glColor3f(1.0f, 1.0f, 0.0f);
		}
		// Right 1
		glPushMatrix();
		glTranslatef(1.9f, vmBulbLocationY, 1.450f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();

		// Right 2
		glPushMatrix();
		glTranslatef(0.45f, vmBulbLocationY, 1.450f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();

		// Left 1
		glPushMatrix();
		glTranslatef(-1.9f, vmBulbLocationY, 1.450f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();

		// Left 2
		glPushMatrix();
		glTranslatef(-0.45f, vmBulbLocationY, 1.450f);
		vmQuadric = gluNewQuadric();
		gluSphere(vmQuadric, 0.01, 20, 20);
		glPopMatrix();


	}
}




