/*
    Code :- Ankit Agrawal
*/

//Headers
#include "Main.h"
#include "Resource.h"

//variable declaration
static int alphabetArray[26][7][7] = {
    //A
    {
        {0,0,0,1,0,0,0},
        {0,0,1,0,1,0,0},
        {0,1,0,0,0,1,0},
        {0,1,0,0,0,1,0},
        {1,1,1,1,1,1,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1}
    },
    //b
    {
        {1,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,1,1,1,1,1,0}
    },
    //c
    {
        {0,1,1,1,1,1,1},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {0,1,1,1,1,1,1}
    },
    //d
    {
        {1,1,1,1,1,1,0},
        {0,1,0,0,0,0,1},
        {0,1,0,0,0,0,1},
        {0,1,0,0,0,0,1},
        {0,1,0,0,0,0,1},
        {0,1,0,0,0,0,1},
        {1,1,1,1,1,1,0}
    },
    //e
    {
        {0,1,1,1,1,1,1},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,1,1,1,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {0,1,1,1,1,1,1}
    },
    //f
    {
        {0,1,1,1,1,1,1},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,1,1,1,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0}
    },
    //g
    {
        {0,1,1,1,1,1,1},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,1,1,0},
        {1,0,0,1,0,0,1},
        {0,1,1,1,0,0,1}
    },
    //h
    {
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,1,1,1,1,1,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1}
    },
    //i
    {
        {1,1,1,1,1,1,1},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {1,1,1,1,1,1,1}
    },
    //j
    {
        {1,1,1,1,1,1,1},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {1,0,0,1,0,0,0},
        {1,0,0,1,0,0,0},
        {1,1,1,1,0,0,0}
    },
    //k
    {
        {1,0,0,0,1,0,0},
        {1,0,0,1,0,0,0},
        {1,0,1,0,0,0,0},
        {1,1,0,0,0,0,0},
        {1,0,1,0,0,0,0},
        {1,0,0,1,0,0,0},
        {1,0,0,0,1,0,0}
    },
    //l
    {
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,1,1,1,1,1,1}
    },
    //m
    {
        {1,1,0,0,0,1,1},
        {1,0,1,0,1,0,1},
        {1,0,0,1,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1}
    },
    //n
    {
        {1,0,0,0,0,0,1},
        {1,1,0,0,0,0,1},
        {1,0,1,0,0,0,1},
        {1,0,0,1,0,0,1},
        {1,0,0,0,1,0,1},
        {1,0,0,0,0,1,1},
        {1,0,0,0,0,0,1}
    },
    //o
    { 
        {0,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {0,1,1,1,1,1,0}
    },
    //p
    {
        {0,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,1,1,1,1,1,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0},
        {1,0,0,0,0,0,0}
    },
    //q
    {
        {0,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {0,1,1,1,1,1,0},
        {0,0,0,0,0,0,1}
    },
    //r
    {
        {1,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,1,1,1,1,1,0},
        {1,1,0,0,0,0,0},
        {1,0,1,0,0,0,0},
        {1,0,0,1,0,0,0},
        {1,0,0,0,1,0,0}
    },
    //s
    {
        {0,1,1,1,1,1,0},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,0},
        {0,1,1,1,1,1,0},
        {0,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {0,1,1,1,1,1,0}
    },
    //t
    { 
        {1,1,1,1,1,1,1},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0},
        {0,0,0,1,0,0,0}
    },
    //u
    { 
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {0,1,1,1,1,1,0}
    },
    //v
    { 
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {0,1,0,0,0,1,0},
        {0,0,1,0,1,0,0},
        {0,0,0,1,0,0,0}
    },
    //w
    { 
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1},
        {1,0,0,1,0,0,1},
        {1,0,0,1,0,0,1},
        {1,0,0,1,0,0,1},
        {0,1,1,1,1,1,0}
    },
    //x
    { 
        {1,0,0,0,0,0,1},
        {0,1,0,0,0,1,0},
        {0,0,1,0,1,0,0},
        {0,0,0,1,0,0,0},
        {0,0,1,0,1,0,0},
        {0,1,0,0,0,1,0},
        {1,0,0,0,0,0,1}
    },
    //y
    {
        {1,0,0,0,0,0,1},
        {0,1,0,0,0,1,0},
        {0,0,1,0,1,0,0},
        {0,0,0,1,0,0,0},
        {0,0,1,0,0,0,0},
        {0,1,0,0,0,0,0},
        {1,0,0,0,0,0,0}
    },
    //z
    { 
        {1,1,1,1,1,1,0},
        {0,0,0,0,0,1,0},
        {0,0,0,0,1,0,0},
        {0,0,0,1,0,0,0},
        {0,0,1,0,0,0,0},
        {0,1,0,0,0,0,0},
        {0,1,1,1,1,1,1}
    }
};

static char groupMemberString[] = "GROUP MEMBERS";
// char groupMemersNameString[] = "AKSHAY BHAGWAT,ANAGHA BABAR,ANKIT AGRAWAL,ANURAG G,DARSHAN VIKAM,MAYUR PHASE,NIKHIL LELE,SWAPNIL NIKAM,VIJAYKUMAR DANGI,VISHWAJEET MANE";
static char groupMemersNameString[] = "AKSHAY BHAGWAT,ANAGHA BABAR,ANKIT AGRAWAL,DARSHAN VIKAM,NIKHIL LELE,SWAPNIL NIKAM,VIJAYKUMAR DANGI,VISHWAJEET MANE";
static char groupLeaderString[] = "GROUP LEADER";
static char groupLeaderNameString[] = "CHAITANYA RAHANE";
static float timerCount = 0;

//ankitCreditScene()
bool ankitCreditScene(void)
{
    //function declaration
    void ankitPrintString(char str[]);
    
    //code
    if (timerCount < 360)
    {
        glTranslatef(-13.0f, 8.0f, -20.0f);
        glScalef(0.2f, 0.2f, 0.2f);

        ankitPrintString(groupMemberString);
        glTranslatef(0.0f, -20.0f, 0.0f);
        glScalef(0.5f, 0.5f, 0.5f);
        ankitPrintString(groupMemersNameString);
        timerCount+=0.3f;
    }
    else
    {
        glTranslatef(-13.0f, 8.0f, -20.0f);
        glScalef(0.2f, 0.2f, 0.2f);

        ankitPrintString(groupLeaderString);
        glTranslatef(0.0f, -20.0f, 0.0f);
        glScalef(0.5f, 0.5f, 0.5f);
        ankitPrintString(groupLeaderNameString);
        timerCount+=0.6f;
    } 
    
    if(timerCount > 720.0f)
        return(true);       //done with credit scene
    else
        return(false);      //otherwose
}


void ankitPrintString(char str[])
{
    char c;
    int charCountTotal = 0, newLineCharacterCount = 0, charactersInNewLineCount = 0;
    static int horizontalSpace = 9;
    static int verticalSpace = 10;
    while ((c = str[charCountTotal]) != '\0')
    {
        if (c == ',')
        {
            newLineCharacterCount++;
            charactersInNewLineCount = 0;
        }
        else
        {
            if (c != ' ') {

                int index = c - 65;

                for (int i = 0; i < 7; i++)
                {
                    int y = newLineCharacterCount * verticalSpace + i;

                    for (int j = 0; j < 7; j++)
                    {
                        int x = charactersInNewLineCount * horizontalSpace + j;
                        if (alphabetArray[index][i][j])
                        {
                            glBegin(GL_QUADS);
                            if (newLineCharacterCount % 3 == 0)
                                glColor3f(1.0f, 0.0f, 0.0f);
                            else if (newLineCharacterCount % 3 == 1)
                                glColor3f(0.0f, 1.0f, 0.0f);
                            else
                                glColor3f(0.0f, .0f, 1.0f);
                            glVertex3f(x + 1, -y, 0.0f);
                            glVertex3f(x, -y, 0.0f);
                            glVertex3f(x, -y - 1, 0.0f);
                            glVertex3f(x + 1, -y - 1, 0.0f);
                            glEnd();
                        }
                    }
                }
            }
            charactersInNewLineCount++;
        }
        charCountTotal++;
    }
}

