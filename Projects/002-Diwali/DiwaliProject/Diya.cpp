// To Show panti with flame for Diwali
// Date : 08 November 2020
// By : Darshan Vikam

// Header files
#include "Main.h"
#include "Geometry.h"
#include "Resource.h"

//
//dvDiya()
//
void dvDiya() {
	// Function declaration
	void dvHemisphere(float, int, int);			// void Hemisphere(float radius, int longitudes, int latitudes) - 3d
	void dvSphere(float, int, int);				// void Sphere(float radius, int longitudes, int latitudes) - 3d
	void dvFlame(int);					// void Flame(int radius) - 2d

	// Code
	glRotatef((GLfloat)90, 1.0f, 0.0f, 0.0f);
	glScalef(1.0f, 1.0f, 0.75f);
	dvHemisphere(1, 15, 5);
/*
	glRotatef((GLfloat)90, -1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, 0.25f, 0.0f);
	glScalef(0.5f, 0.5f, 0.5f);
	dvFlame(1);
*/
	glTranslatef(0.0f, 0.0f, -0.25f);
	glScalef(0.5f, 0.5f, 1.5f);
	dvSphere(0.5, 10, 10);
}

void dvHemisphere(float radius, int longs, int lats) {
	// Variable declaration
	int i;
	float x[4], y[4], z[2];
	float rad = ((float)22/(float)7)/(float)180;
	float latConst, longConst, j;

	latConst = 90.0f / (float)lats;
	longConst = 360.0f / longs;
	glBegin(GL_QUADS);
	for(i = 0; i < 360; i += longConst) {
		for(j = 0; j < 180.0f; j += latConst) {
			x[0] = radius * cos(i*rad) * sin(j*rad/2.0f);
			y[0] = radius * sin(i*rad) * sin(j*rad/2.0f);
			x[1] = radius * cos((i+longConst)*rad) * sin(j*rad/2.0f);
			y[1] = radius * sin((i+longConst)*rad) * sin(j*rad/2.0f);
			z[0] = radius * cos(j*rad/2.0f);

			x[2] = radius * cos(i*rad) * sin((j+latConst)*rad/2.0f);
			y[2] = radius * sin(i*rad) * sin((j+latConst)*rad/2.0f);
			x[3] = radius * cos((i+longConst)*rad) * sin((j+latConst)*rad/2.0f);
			y[3] = radius * sin((i+longConst)*rad) * sin((j+latConst)*rad/2.0f);
			z[1] = radius * cos((j+latConst)*rad/2.0f);

			glColor3f(j/(float)180, 0.f, 0.0f);
			glVertex3f(x[0], y[0], z[0]);
			glVertex3f(x[1], y[1], z[0]);
			glColor3f(((j+latConst)/(float)180), 0.0f, 0.0f);
			glVertex3f(x[3], y[3], z[1]);
			glVertex3f(x[2], y[2], z[1]);
		}
	}
	glEnd();
}

void dvSphere(float radius, int longs, int lats) {
	// Variable declaration
	int i;
	float x[4], y[4], z[2];
	float rad = ((float)22/(float)7)/(float)180;
	float latConst, longConst, j;

	latConst = 90.0f / (float)lats;
	longConst = 360.0f / longs;
	glBegin(GL_QUADS);
	//glBegin(GL_LINES);
	for(i = 0; i < 360; i += longConst) {
		for(j = 0; j < 360.0f; j += latConst) {
			x[0] = radius * cos(i*rad) * sin(j*rad/2.0f);
			y[0] = radius * sin(i*rad) * sin(j*rad/2.0f);
			x[1] = radius * cos((i+longConst)*rad) * sin(j*rad/2.0f);
			y[1] = radius * sin((i+longConst)*rad) * sin(j*rad/2.0f);
			z[0] = radius * cos(j*rad/2.0f);

			x[2] = radius * cos(i*rad) * sin((j+latConst)*rad/2.0f);
			y[2] = radius * sin(i*rad) * sin((j+latConst)*rad/2.0f);
			x[3] = radius * cos((i+longConst)*rad) * sin((j+latConst)*rad/2.0f);
			y[3] = radius * sin((i+longConst)*rad) * sin((j+latConst)*rad/2.0f);
			z[1] = radius * cos((j+latConst)*rad/2.0f);

			//glColor3f(((float)j/(float)360), (j*0.3)/(float)360, 0.0f);
			glColor3f(1.0f, (float)0.95-(j*0.7/360), (360-j)*0.3/(float)360);
			glVertex3f(x[0], y[0], z[0]);
			glVertex3f(x[1], y[1], z[0]);
			//glColor3f(((float)(j+latConst)/(float)360), ((j+latConst)*0.3)/(float)360, 0.0f);
			glColor3f(1.0f, (float)0.95-((j+latConst)*0.7/360), (360-(j+latConst))*0.3/(float)360);
			glVertex3f(x[3], y[3], z[1]);
			glVertex3f(x[2], y[2], z[1]);
		}
	}
	glEnd();
}

void dvFlame(int radius) {
	// Variable declaration
	int i;
	float x[2], y[2];
	float rad = ((float)22/(float)7)/(float)180;

	glScalef(0.6f, 2.0f, 0.0f);
	glRotatef((GLfloat)90, 0.0f, 0.0f, -1.0f);
	glBegin(GL_TRIANGLE_FAN);
	for(i = 0; i < 360; i++) {
		x[0] = radius * cos(i*rad);
		y[0] = radius * sin(i*rad);
		x[1] = radius * cos((i+1)*rad);
		y[1] = radius * sin((i+1)*rad);
		//glColor3f(0.0f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.35f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		if(i < 180)
			glColor3f(((float)i/(float)180), ((float)(i * 0.8)/(float)180), 0.0f);
		else
			glColor3f(((float)(360-i)/(float)180), ((float)((360 - i) * 0.8)/(float)180), 0.0f);
		glVertex3f(x[0], y[0], 0.0f);
		glVertex3f(x[1], y[1], 0.0f);
	}
	glEnd();
}
