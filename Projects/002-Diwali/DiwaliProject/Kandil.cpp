/*
        Code :- Akshay Bhagwat
 */

 //Headers
#include "Main.h"
#include "Resource.h"

//global variable declarations
static GLuint kandilTexture;

void LoadKandilTexture(void)
{
    //code
    LoadGLTexture(&kandilTexture, MAKEINTRESOURCE(AB_KANDIL_BITMAP));
}

void DeleteKandilTexture(void)
{
    //code
    if (kandilTexture)
    {
        glDeleteTextures(1, &kandilTexture);
        kandilTexture = 0;
    }
}

void abKandilRod()
{
	//code
	glBegin(GL_LINES);

	glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.0f, 0.3f, 0.0f);
		glVertex3f(0.2f, 0.3f, 0.0f);

	glEnd();
}

void abRenderKandil(void)
{
    //code
    glBindTexture(GL_TEXTURE_2D, kandilTexture);

	glBegin(GL_QUADS);

		// front
		glColor3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.05f, 0.1f, 0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-0.05f, 0.1f, 0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-0.05f, -0.1f, 0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, 0.05f);

		// back
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.05f, 0.1f, -0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-0.05f, 0.1, -0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-0.05f, -0.1f, -0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, -0.05f);

		// right side
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.05f, 0.1f, -0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(0.05f, 0.1f, 0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, 0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, -0.05f);

		// left side
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-0.05f, 0.1f, -0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-0.05f, 0.1f, 0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-0.05f, -0.1f, 0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-0.05f, -0.1f, -0.05f);

		// top side
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-0.05f, 0.1f, -0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-0.05f, 0.1f, 0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(0.05f, 0.1f, 0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.05f, 0.1f, -0.05f);

		// down side
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-0.05f, -0.1f, -0.05f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-0.05f, -0.1f, 0.05f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, 0.05f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.05f, -0.1f, -0.05f);

	glEnd();

	glBegin(GL_LINES);

		glVertex3f(0.05f, 0.1f, 0.05f);
		glVertex3f(0.0f, 0.3f, 0.0f);

		glVertex3f(-0.05f, 0.1f, 0.05f);
		glVertex3f(0.0f, 0.3f, 0.0f);

		glVertex3f(0.05f, 0.1f, -0.05f);
		glVertex3f(0.0f, 0.3f, 0.0f);

		glVertex3f(-0.05f, 0.1f, -0.05f);
		glVertex3f(0.0f, 0.3f, 0.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}