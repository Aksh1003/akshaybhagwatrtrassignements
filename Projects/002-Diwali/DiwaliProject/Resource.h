#pragma once 

/*
    AB  :- Akshay Bhagwat
    VJD :- Vijaykumar J. Dangi
    VM  :- Vishwajeet Mane
    AA  :- Ankit Agrawal
    ARB :- Anagha Babar
    SN  :- Swapnil Nikam
    DV  :- Darshan Vikam
    MP  :- Mayur Phase
    AG  :- Anurag 
*/

//macro
#define IDI_MYICON 101

// temple
#define  VJD_TEMPLE_WALL_BITMAP  201
#define  VJD_TEMPLE_FLOOR_BITMAP 202
#define  VJD_TEMPLE_STEP_BITMAP  203
#define  VJD_TEMPLE_BITMAP       204
#define  VJD_INNER_TEMPLE_ROOF    205
#define  VJD_INNER_TEMPLE_WALL    206

// house
#define  VM_DOOR_BITMAP             301
#define  VM_WALL_BITMAP             302
#define  VM_ROOF_BITMAP             303
#define  VM_PILLAR_BITMAP           304
#define  VM_FLOOR_BITMAP            305
#define  VM_FLOOR2_BITMAP           306
#define  VM_STEPFLOOR_BITMAP        307
#define  VM_ROOF2_BITMAP            308
#define  VM_WINDOWBIG_BITMAP        309
#define  VM_WINDOWSMALL_BITMAP      310
#define  VM_MARIGOLDFLOWER_BITMAP   311
#define  VJD_HOUSE_GROUND_TEXTURE   312								   

// ground
#define AB_GROUND_BITMAP	401
#define VJD_ROAD_BITMAP     402						   

// kandil
#define AB_KANDIL_BITMAP	501

// sun and moon
#define SN_SUN_BITMAP	601
#define SN_MOON_BITMAP	602

// tree
#define TREE_BARK_BITMAP 701
#define TREE_LEAF_BITMAP 702

//Tulas
#define NL_STONE_BITMAP        801
#define NL_STONE_BOTTOM_BITMAP 802
#define NL_SOIL_BITMAP         803
#define VJD_TREE_BARK_BITMAP   804

// Humanoid
#define VM_FACE_BITMAP            901
#define VM_FACECHILDGIRL_BITMAP   902
#define VM_SHIRTMENFRONT_BITMAP   903
#define VM_SHIRTMEN_BITMAP        904
#define VM_KURTAWOMANFRONT_BITMAP 905
#define VM_KURTAWOMAN_BITMAP      906
#define VM_TROUSER_BITMAP         907

//FireCracker.cpp
#define VM_COLA_LOGO_BITMAP 951

//ShriRam
#define SHRI_RAM_BITMAP 981

//FinalCreditEffect.cpp
#define ASTROMEDICOMP_BITMAP 985


//DiwaliWishSceneEffect.cpp 
#define VJD_DIWALI_WISH   TEXT("Texture\\DiwaliImage\\DiwaliMain.bmp")


