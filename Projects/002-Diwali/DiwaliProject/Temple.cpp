/*
        Code :- Akshay Bhagwat & Vijay Dangi
 */

//Headers
#include "Main.h"
#include "Geometry.h"
#include "Resource.h"

//global variable declarations
static GLuint vjdTempleWallTexture,
              vjdTempleFloorTexture,
              vjdTempleStep,
              vjdTemple;

static GLuint vjdInnerTempleWall,
              vjdInnerTempleRoof;


//
//LoadTempleTexture() :- Initialize texture required in this file
//
void LoadTempleTexture(void)
{
    //code
    LoadGLTexture(&vjdTempleWallTexture,  MAKEINTRESOURCE(VJD_TEMPLE_WALL_BITMAP));
    LoadGLTexture(&vjdTempleFloorTexture, MAKEINTRESOURCE(VJD_TEMPLE_FLOOR_BITMAP));
    LoadGLTexture(&vjdTempleStep, MAKEINTRESOURCE(VJD_TEMPLE_STEP_BITMAP));
    LoadGLTexture(&vjdTemple, MAKEINTRESOURCE(VJD_TEMPLE_BITMAP));
    
    LoadGLTexture(&vjdInnerTempleWall, MAKEINTRESOURCE(VJD_INNER_TEMPLE_WALL));
    LoadGLTexture(&vjdInnerTempleRoof, MAKEINTRESOURCE(VJD_INNER_TEMPLE_ROOF));
}


//
//DeleteTempleTexture() :- Delete Texture object created in this file
//
void DeleteTempleTexture(void)
{
    //code
    if(vjdTempleWallTexture)
    {
        glDeleteTextures(1, &vjdTempleWallTexture);
        vjdTempleWallTexture = 0;
    }
    
    if(vjdTempleFloorTexture)
    {
        glDeleteTextures(1, &vjdTempleFloorTexture);
        vjdTempleFloorTexture = 0;
    }
    
    if(vjdTempleStep)
    {
        glDeleteTextures(1, &vjdTempleStep);
        vjdTempleStep = 0;
    }
    
    if(vjdTemple)
    {
        glDeleteTextures(1, &vjdTemple);
        vjdTemple = 0;
    }
    
    if(vjdInnerTempleWall)
    {
        glDeleteTextures(1, &vjdInnerTempleWall);
        vjdInnerTempleWall = 0;
    }
    
    if(vjdInnerTempleRoof)
    {
        glDeleteTextures(1, &vjdInnerTempleRoof);
        vjdInnerTempleRoof = 0;
    }
}

//
//vjdRenderBigTemple()
//
void vjdRenderBigTemple(void)
{
    //variable declarations
   GLUquadric *quadric = NULL;
   int     i;
   GLfloat fAngle;

   //code
// vjdTemple Front Door
   //top cube
    glPushMatrix();
       glBindTexture(GL_TEXTURE_2D, vjdTempleFloorTexture);
        glTranslatef(0.00f, 3.77f, 8.81f);
        glScalef(5.05f, 1.00f, 1.00f);
        RenderCube(1.00f);
    glPopMatrix();


    glPushMatrix();
        //left side (place for diya)
        // glColor3f(0.0f, 0.5f, 0.0f);
        glBindTexture(GL_TEXTURE_2D, vjdTemple);
        glTranslatef(-3.01f, 0.77f, 9.78f);
        glScalef(1.00f, 1.51f, 1.00f);
        RenderCube(1.00f);
   
        //right side (place for diya)
        glTranslatef(3.01f + 3.01f, 0.0f, 0.0f);
        RenderCube(1.00f);
        
        glBindTexture(GL_TEXTURE_2D, vjdTempleFloorTexture);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();


    glPushMatrix();    
        //left pillar
        glTranslatef(-3.01f, 2.13f, 8.81f);
        glScalef(1.00f, 4.25f, 1.00f);
        RenderCube(1.00f);

        //right pillar
        glTranslatef(3.01f + 3.01f, 0.0f, 0.0f);
        RenderCube(1.00f);
    glPopMatrix();

//PYRAMID
    glPushMatrix();
        //left pillar top pyramid
        glTranslatef(-3.02f, 4.74f, 8.81f);
        RenderPyramid(0.50f, 1.00f);

        //right pillar top pyramid
        glTranslatef(3.02f + 3.02f, 0.0f, 0.0f);
        RenderPyramid(0.50f, 1.00f);
    glPopMatrix();

//left pyramid top Flag stick
    glPushMatrix();        
        glTranslatef(-3.02f, 6.16f, 8.81f);
        glRotatef(90.00f, 0.0f, 0.0f, 1.0f);

        glLineWidth(1.81f);
        glBegin(GL_LINES);
            glVertex3f(-0.50f, 0.0f, 0.0f);
            glVertex3f( 0.50f, 0.0f, 0.0f);
        glEnd();
        glLineWidth(1.0f);
    glPopMatrix();

    glBindTexture(GL_TEXTURE_2D, 0);

//Left pillar flag
    glPushMatrix();
        glTranslatef(-2.72f, 6.37f, 8.81f);
        glRotatef(-90.00f, 0.0f, 0.0f, 1.0f);
        
        glColor3f(1.0f, 0.5f, 0.0f);
        RenderTriangle(0.30f);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();

    glBindTexture(GL_TEXTURE_2D, vjdTempleFloorTexture);

//right pyramid top Flag stick
    glPushMatrix();
        glTranslatef(3.02f, 6.16f, 8.81f);
        glRotatef(90.00f, 0.0f, 0.0f, 1.0f);

        glLineWidth(1.81f);
        glBegin(GL_LINES);
            glVertex3f(-0.50f, 0.0f, 0.0f);
            glVertex3f( 0.50f, 0.0f, 0.0f);
        glEnd();
        glLineWidth(1.0f);
    glPopMatrix();

    glBindTexture(GL_TEXTURE_2D, 0);

//right pillar flag
    glPushMatrix();
        glTranslatef(3.32f, 6.37f, 8.81f);
        glRotatef(-90.00f, 0.0f, 0.0f, 1.0f);
        
        glColor3f(1.0f, 0.5f, 0.0f);
        RenderTriangle(0.30f);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();

    glBindTexture(GL_TEXTURE_2D, vjdTempleFloorTexture);

//SQUARE(XZ plane) (ground)
    glPushMatrix();
        glTranslatef(0.00f, -0.85f, 2.64f);
        RenderPlane(9.85f);
    glPopMatrix();

//vjdTemple Foundation
    glBindTexture(GL_TEXTURE_2D, vjdTempleFloorTexture);
   //CUBE
    glPushMatrix();
        glTranslatef(0.00f, -0.40f, 2.96f);
        glScalef(14.82f, 0.84f, 14.68f);

        glBegin(GL_QUADS);
    
            glColor3f(0.5f, 0.5f, 0.5f);    //add little grey color in texture
            //Front Face
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, 0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, 0.5f);
            
            //Right Face
            // glColor3f(0.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(0.5f, -0.5f, -0.5f);
            
            //Back Face
            // glColor3f(0.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);
            
            //Left Face
            // glColor3f(0.0f, 1.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);
            
            
            //Top Face
            glColor3f(1.0f, 1.0f, 1.0f);        //white color in texture
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, 0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, 0.5f,  0.5f);
            
            
            //Bottom Face
            // glColor3f(1.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f,  0.5f);
        
        glEnd();
    glPopMatrix();
    
//vjdTemple Steps
    glPushMatrix();
        glBindTexture(GL_TEXTURE_2D, vjdTempleStep);
        glTranslatef(0.00f, -0.74f, 11.12f);
        glScalef(4.78f, 0.28f, 1.69f);
        glBegin(GL_QUADS);
    
            glColor3f(0.5f, 0.5f, 0.5f);
            //Front Face
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, 0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, 0.5f);
            
            
            glColor3f(1.0f, 1.0f, 1.0f);
            //Right Face
            // glColor3f(0.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(0.5f, -0.5f, -0.5f);
            
            
            //Back Face
            // glColor3f(0.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);
            
            
            //Left Face
            // glColor3f(0.0f, 1.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);
            
            
            //Top Face
            // glColor3f(1.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, 0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, 0.5f,  0.5f);
            
            
            //Back Face
            // glColor3f(1.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f,  0.5f);
        
        glEnd();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0.00f, -0.45f, 10.91f);
        glScalef(4.78f, 0.28f, 1.30f);
        glBegin(GL_QUADS);
    
            glColor3f(0.5f, 0.5f, 0.5f);
            //Front Face
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, 0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, 0.5f);
            
            
            glColor3f(1.0f, 1.0f, 1.0f);
            //Right Face
            // glColor3f(0.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(0.5f, -0.5f, -0.5f);
            
            
            //Back Face
            // glColor3f(0.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);
            
            
            //Left Face
            // glColor3f(0.0f, 1.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);
            
            
            //Top Face
            // glColor3f(1.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, 0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, 0.5f,  0.5f);
            
            
            //Back Face
            // glColor3f(1.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f,  0.5f);
        
        glEnd();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0.00f, -0.17f, 10.73f);
        glScalef(4.78f, 0.28f, 0.87f);
        glBegin(GL_QUADS);
    
            glColor3f(0.5f, 0.5f, 0.5f);
            //Front Face
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, 0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, 0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, 0.5f);
            
            
            glColor3f(1.0f, 1.0f, 1.0f);
            //Right Face
            // glColor3f(0.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(0.5f, -0.5f, -0.5f);
            
            
            //Back Face
            // glColor3f(0.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f( 0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);
            
            
            //Left Face
            // glColor3f(0.0f, 1.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f,  0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f,  0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);
            
            
            //Top Face
            // glColor3f(1.0f, 0.0f, 1.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, 0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, 0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, 0.5f,  0.5f);
            
            
            //Back Face
            // glColor3f(1.0f, 1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-0.5f, -0.5f, -0.5f);            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-0.5f, -0.5f,  0.5f);            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 0.5f, -0.5f,  0.5f);
        
        glEnd();
    glPopMatrix();

//WALLs
    glBindTexture(GL_TEXTURE_2D, vjdTempleWallTexture);
   //Back side wall
    glPushMatrix();
        glTranslatef(0.00f, 2.13f, -3.87f);
        glScalef(14.75f, 4.24f, 1.00f);
        RenderCube(1.00f);
    glPopMatrix();

    glPushMatrix();
        //left side wall (z-more)
        glTranslatef(-6.89f, 2.13f, 0.78f);
        glScalef(1.00f, 4.24f, 8.30f);
        RenderCube(1.00f);
        
        //right side wall (z-more)
        glTranslatef(6.89f+6.89f, 0.0f, 0.0f);
        RenderCube(1.00f);
    glPopMatrix();

    glPushMatrix();
        //right side (x-more)
        glTranslatef(-4.94f, 2.13f, 4.30f);
        glScalef(2.89f, 4.24f, 1.00f);
        RenderCube(1.00f);
        
        //left side (x-more)
        glTranslatef(3.42f, 0.0f, 0.0f);
        RenderCube(1.00f);
    glPopMatrix();

    glPushMatrix();
        //right side (behind pillar)
        glTranslatef(-3.01f, 2.13f, 6.07f);
        glScalef(1.00f, 4.24f, 4.49f);
        RenderCube(1.00f);
        
        //left side (begind pillar)
        glTranslatef(3.01f + 3.01f, 0.0f, 0.0f);
        RenderCube(1.00f);
    glPopMatrix();
    
    
    //unbind texture
    glBindTexture(GL_TEXTURE_2D, 0);
}


//
//abTemple()
//
void abTemple()
{
    //code
    
    glColor3f(1.0f, 1.0f, 1.0f);
    glBindTexture(GL_TEXTURE_2D, vjdInnerTempleWall);
    
	glBegin(GL_QUADS);

    // right front

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.2f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, -0.2f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.2f, -0.2f, 0.4f);

        // right
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.2f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, -0.2f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.2f, -0.4f);

        // left front
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.2f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.2f, -0.2f, 0.4f);

        // left
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.2f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, 0.2f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.2f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, -0.2f, -0.4f);

        // back
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.2f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, 0.2f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.2f, -0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.2f, -0.4f);

    /* top 1*/
        // front
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.22f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.22f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.20f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.20f, 0.4f);

        // back
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.20f, -0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.20f, -0.4f);

        // left
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.22f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.20f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.20f, -0.4f);

        // right
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.22f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.20f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.20f, -0.4f);

        // up
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.22f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.22f, 0.4f);

        // down
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.20f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.20f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.20f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.20f, 0.4f);

    /* top 2*/
        // front
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.30f, 0.25f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.30f, 0.25f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.30f, 0.22f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.30f, 0.22f, 0.4f);

        // back
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.30f, 0.22f, -0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.30f, 0.22f, -0.4f);

        // left
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.30f, 0.25f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.30f, 0.22f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.30f, 0.22f, -0.4f);

        // right
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.30f, 0.25f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.30f, 0.22f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.30f, 0.22f, -0.4f);

        // up
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.30f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.30f, 0.25f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.30f, 0.25f, 0.4f);

        // down
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.30f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.30f, 0.22f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.30f, 0.22f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.30f, 0.22f, 0.4f);

    /* top 3*/
        // front
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.25f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.25f, 0.4f);

        // back
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.25f, -0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.25f, -0.4f);

        // left
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.25f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.27f, 0.25f, -0.4f);

        // right
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.27f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.27f, 0.25f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.25f, -0.4f);

        // up
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.27f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.27f, 0.4f);

        // down
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.27f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.27f, 0.25f, -0.4f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.27f, 0.25f, 0.4f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.27f, 0.25f, 0.4f);

    /* upper ghumat rod */
        // front
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.005f, 0.5f, 0.01f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, 0.01f);

        // back
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, -0.01f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.005f, 0.7f, -0.01f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.005f, 0.5f, -0.01f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, -0.01f);

        // left
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, -0.01f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, 0.01f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, -0.01f);

        // right
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, -0.01f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, 0.01f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.005f, 0.5f, -0.01f);

        // up
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.005f, 0.7f, 0.01f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.005f, 0.7f, -0.01f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.005f, 0.7f, -0.01f);

    glEnd();


    glColor3f(0.585f, 0.25f, 0.0f);
    glBindTexture(GL_TEXTURE_2D, vjdInnerTempleRoof);
    glBegin(GL_TRIANGLES);

    /* ghumat */
        // front
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, 0.55f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.2f, 0.27f, 0.4f);

        // back
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, 0.55f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, 0.27f, -0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.2f, 0.27f, -0.4f);

        // right
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, 0.55f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.2f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.2f, 0.27f, -0.4f);

        // left
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, 0.55f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, 0.27f, 0.4f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.2f, 0.27f, -0.4f);
      
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    /* flag */

    glBegin(GL_TRIANGLES);

        glColor3f(1.0f, 0.5f, 0.0f);
        glVertex3f(0.0f, 0.7f, 0.0f);
        glVertex3f(0.3f, 0.65f, 0.0f);
        glVertex3f(0.0f, 0.6f, 0.0f);

	glEnd();
}

