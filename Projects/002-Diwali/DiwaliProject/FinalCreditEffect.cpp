/*
    Code :- Vijaykumar Dangi
*/

//Headers
#include "Main.h"
#include "Resource.h"

//macro
#define GL_PI   M_PI

#define GRADIENT_VALUE(colorOne, colorTwo, Percent) (colorOne + percent * (colorTwo - colorOne))

    //Texture Variables
extern FILE *gpFile;
extern HWND  ghwnd;

static GLfloat fExtent = 10.0f;
static GLfloat fStep = 0.05f;
static GLfloat y = -0.5f;

static GLfloat *pVertex = NULL,
               *pTexCoord = NULL;

static int iTotalVertex = 0;
static GLfloat xRotation = 0.0f;

GLuint astromedicompTexture;



//
//vjdInitializeFinalScene()
//
void vjdInitializeFinalScene(void)
{
    //variable declarations
    GLfloat fStrip, fRun;
    int     i;
    
    //code
        //2 * OuterLoop Iteration * InnerLoop Iteration
        //2 for in every iteration there are 2 vertices generate
    iTotalVertex = 2 * ( ((fExtent - (-fExtent))/fStep) * ((fExtent - (-fExtent))/fStep) );
    
        //Allocate Memory
    pVertex = (GLfloat *) malloc(iTotalVertex * sizeof(GLfloat) * 3);
    if(pVertex == NULL)
    {
        fprintf(gpFile, "FinalCreditScene.cpp :- Not enough memory for vertex\n");
        DestroyWindow(ghwnd);
    }
    
    pTexCoord = (GLfloat *) malloc(iTotalVertex * sizeof(GLfloat) * 2);
    if(pTexCoord == NULL)
    {
        free(pVertex);
        fprintf(gpFile, "FinalCreditScene.cpp :- Not enough memory for texture co-ordinates\n");
        DestroyWindow(ghwnd);
    }
    
    LoadGLTexture(&astromedicompTexture, MAKEINTRESOURCE(ASTROMEDICOMP_BITMAP));
    
    //initialize memory
    i = 0;
    for(fStrip = -fExtent; fStrip <= fExtent; fStrip += fStep)
    {
        for(fRun = fExtent; fRun >= -fExtent; fRun -= fStep)
        {
            if(i >= iTotalVertex)
            {
                break;
            }
            
            pTexCoord[i*2 + 0] = (fStrip + fExtent)/(2.0f * fExtent);
            pTexCoord[i*2 + 1] = fabs((fRun - fExtent)/( 2.0f * fExtent));
            
            pVertex[i*3 + 0] = fStrip;
            pVertex[i*3 + 1] = y;
            pVertex[i*3 + 2] = fRun ;
            
            i++;
           
            pTexCoord[i*2 + 0] = (fStrip + fStep + fExtent)/(2.0f * fExtent);
            pTexCoord[i*2 + 1] = fabs((fRun - fExtent)/( 2.0f * fExtent));
            
            pVertex[i*3 + 0] = (fStrip + fStep);
            pVertex[i*3 + 1] = y;
            pVertex[i*3 + 2] = fRun;
            
            i++;
        }
    }
}


//
//vjdUninitializeFinalScene()
//
void vjdUninitializeFinalScene(void)
{
    //code
    if(pVertex)
    {
        free(pVertex);
        pVertex = NULL;
    }
    
    if(pTexCoord)
    {
        free(pTexCoord);
        pTexCoord = NULL;
    }
    
    if(astromedicompTexture)
    {
        glDeleteTextures(1, &astromedicompTexture);
    }
}

//
//vjdRenderFinalCredit()
//
void vjdRenderFinalCredit(void)
{   
    //function declarations
    void RenderScene(void);
   
    //code
    if(pVertex == NULL || pTexCoord == NULL)
    {
        return;
    }
    
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    
    glPushMatrix();
    
        glTranslatef(0.0f, 0.0f, -11.0f);
        glRotatef(xRotation, 1.0f, 0.0f, 0.0f);
        
        //Draw
        glBindTexture(GL_TEXTURE_2D, astromedicompTexture);
        
        RenderScene();
    glPopMatrix();
    
    glDisable(GL_CULL_FACE);
    
    if(xRotation <= 70.0f)
        xRotation = xRotation + 0.3f;
}


//RenderScene()
void RenderScene(void)
{    
    //variable declarations
    static GLfloat fAngleInc = 0.0f;
    
    GLfloat fStrip, fRun;
    GLfloat fAngle = 0.0f;
    
    int i = 0;
    GLfloat x, y, z;
    
    //code
    fAngle = fAngleInc;
    
    glBegin(GL_TRIANGLE_STRIP);
    
        fRun = fExtent;
        for(i = 0; i < iTotalVertex;)
        {            
            x = *(pVertex + i*3);
            z = *(pVertex + i*3 + 2);
            y = *(pVertex + i*3 + 1) + 0.5f * (sin(x) + cos(z)) * cos(GL_PI/180.0f * fAngle);
            
            glTexCoord2f(
                *(pTexCoord + i*2),
                *(pTexCoord + i*2 + 1)
            );
            
            glVertex3f( x, y, z );
            
            i++;
            
                /******************/
            
            x = *(pVertex + i*3);
            z = *(pVertex + i*3 + 2);
            y = *(pVertex + i*3 + 1) + 0.5f * (sin(x) + cos(z)) * cos(GL_PI/180.0f * (fAngle + 8.0f));
            
            glTexCoord2f(
                *(pTexCoord + i*2),
                *(pTexCoord + i*2 + 1)
            );
            
            glVertex3f( x, y, z );
            
            i++;
            
            if(fRun >= -fExtent)
                fRun -= fStep;
            else
            {
                fRun = fExtent;
                fAngle += 8.0f;
            }
        }
    
    glEnd();
    
    fAngleInc += 20.0f;
}

