/*
    Ankit Agrawal And Vishwajeet Mane
*/

//Headers
#include "Main.h"
#include "Resource.h"

//variable declaration
GLuint vmColaTexture;

extern GLfloat xScale, yScale, zScale;
extern GLfloat xTrans, yTrans, zTrans;

//LoadFireCrackerAndBottleTexture()
void LoadFireCrackerAndBottleTexture(void)
{
    //code
    LoadGLTexture(&vmColaTexture, MAKEINTRESOURCE(VM_COLA_LOGO_BITMAP));
}

//DeleteFireCrackerAndBottleTexture()
void DeleteFireCrackerAndBottleTexture(void)
{
    //code
    glDeleteTextures(1, &vmColaTexture);
}


void ankitFireWorks(void)
{
	// Function Declaration
    void vmBottle(void);
    void ankitDrawFireCracker(void);
    
    //variable declaration
	static GLfloat cubeRotation = 0.0;
	
    //code
    // glTranslatef(0.0f, 0.0f, -5.0f); 
	// glScalef(0.15f, 0.15f, 0.15f);
	// glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	// glRotatef(cubeRotation, 0.0f, 0.0f, 1.0f);

	// // glPushMatrix();
        // // glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
        // // glTranslatef(-0.1f, 2.0f, 12.25f);
        // // glScalef(2.04f, 3.45f, 2.45f);
		// // ankitDrawFireCracker();
	// // glPopMatrix();

    vmBottle();
	//cubeRotation = cubeRotation + 0.5f;
}

void vmBottle(void)
{
    // Variable Declaration
    GLUquadric* quadric;
    
	// Code
    
	// Cube
	// glTranslatef(0.0f, 0.0f, -5.0f);
	// glScalef(0.15f, 0.15f, 0.15f);
	// glRotatef(90.0f, 1.0f, 0.0f, 0.0f);


	// Bottle
	// Middle
	glPushMatrix();
		// Upper Middle
		glRotatef(180.0f, 1.0f, 0.0f, 0.0f);

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		quadric = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, vmColaTexture);
		gluQuadricDrawStyle(quadric, GLU_FILL);
		gluQuadricTexture(quadric, TRUE);
		gluQuadricNormals(quadric, GLU_SMOOTH);
		gluCylinder(quadric, 0.5f,0.5f,0.75f, 32, 80);

		glBindTexture(GL_TEXTURE_2D, 0);

		glColor4f(1.0f, 1.0f, 1.0f, 0.8f);
		glTranslatef(0.0f, 0.0f, -1.25f);
		quadric = gluNewQuadric();
		gluCylinder(quadric, 0.5f, 0.5f, 1.25f, 32, 80);
	glPopMatrix();

	// Top Frustum Cone
	glPushMatrix();
		glColor4f(1.0f, 1.0f, 1.0f, 0.8f);
		glTranslatef(0.0f, 0.0f, -1.75f);
		quadric = gluNewQuadric();
		gluCylinder(quadric, 0.25f, 0.5f, 1.0f, 32, 80);
	glPopMatrix();

	// Top Frustum Upper
	glPushMatrix();
		glColor4f(1.0f, 1.0f, 1.0f, 0.8f);
		glTranslatef(0.0f, 0.0f, -2.25f);
		quadric = gluNewQuadric();
		gluCylinder(quadric, 0.2f, 0.25f, 0.5f, 32, 80);
	glPopMatrix();

    gluDeleteQuadric(quadric);
}


void ankitDrawFireCracker(void)
{
	//function declaration
	void ankitDrawHandStick(void);
	void ankitDrawCylinder(void);
	void ankitDrawRope(void);
	void ankitDrawRocketTopCylinder(void);
	void ankitDrawRocketFire(void);
	bool ankitDrawBurst(void);

	//variable 
	static GLfloat rocketScaleValue = 0.5f;
	static GLfloat burstScaleValue = 1.0f;
	static GLfloat xValue = 0.0f;
	static GLfloat xDisplacement = 0.00001f;
	static GLfloat xValueLimit = 0.5f;
	static GLfloat yValue = -0.1f;
	static GLfloat yDisplacement = 0.0001f;
	static GLfloat yValueLimit = 1.0f;
	static GLfloat angleValue = 0.0f;
	static GLfloat angleDisplacement = -0.001f;

	// glMatrixMode(GL_MODELVIEW);
	// glLoadIdentity();

	glTranslatef(xValue, yValue, -5.0f);

	// if (yValue <= yValueLimit)
	// {
		// glRotatef(angleValue, 0.0f, 0.0f, 1.0f);
		glScalef(rocketScaleValue, rocketScaleValue, rocketScaleValue);
		ankitDrawHandStick();

		glTranslatef(-0.125f, 0.5f, 0.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		ankitDrawCylinder();

		glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
		ankitDrawRocketTopCylinder();

		glTranslatef(0.0f, 0.0f, -0.7f);
		ankitDrawRocketFire();

		glTranslatef(0.0f, 0.0f, 0.3625f);
		ankitDrawRope();

		// yValue += yDisplacement;
		// if (xValue <= xValueLimit)
			// xValue += xDisplacement;
	// }
	// else
	// {
		// glScalef(burstScaleValue, burstScaleValue, burstScaleValue);
		// ankitDrawBurst();
	// }

	// angleValue += angleDisplacement;
	// if (angleValue >= 360)
	// {
		// angleValue = 0;
	// }
}

void ankitDrawHandStick()
{
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);

	//frontPlane
	glVertex3f(0.05f, 0.25f, 0.025f);
	glVertex3f(0.0f, 0.25f, 0.025f);
	glVertex3f(0.0f, -0.5f, 0.025f);
	glVertex3f(0.05f, -0.5f, 0.025f);

	//rightPlane
	glVertex3f(0.05f, 0.25f, -0.025f);
	glVertex3f(0.05f, 0.25f, 0.025f);
	glVertex3f(0.05f, -0.5f, 0.025f);
	glVertex3f(0.05f, -0.5f, -0.025f);

	//backPlane
	glVertex3f(0.0f, 0.25f, -0.025f);
	glVertex3f(0.05f, 0.25f, -0.025f);
	glVertex3f(0.05f, -0.5f, -0.025f);
	glVertex3f(0.0f, -0.5f, -0.025f);

	//leftPlane
	glVertex3f(0.0f, 0.25f, 0.025f);
	glVertex3f(0.0f, 0.25f, -0.025f);
	glVertex3f(0.0f, -0.5f, -0.025f);
	glVertex3f(0.0f, -0.5f, 0.025f);

	//topPlane
	glVertex3f(0.05f, 0.25f, -0.025f);
	glVertex3f(0.0f, 0.25f, -0.025f);
	glVertex3f(0.0f, 0.25f, 0.025f);
	glVertex3f(0.05f, 0.25f, 0.025f);

	//bottomPlane
	glVertex3f(0.05f, -0.25f, -0.025f);
	glVertex3f(0.0f, -0.25f, -0.025f);
	glVertex3f(0.0f, -0.25f, 0.025f);
	glVertex3f(0.05f, -0.25f, 0.025f);

	glEnd();


	//following code draw the support like rope, which ties the stike and the cylinder filled material
	//there are 2 parts to this, one on the stick and another to the cylinder filled material.
	//this code is for the stick, another code can be found in drawRope
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	//frontPlane
	glVertex3f(0.05f, 0.2025f, 0.025f);
	glVertex3f(0.0f, 0.2025f, 0.025f);
	glVertex3f(0.0f, 0.1625f, 0.025f);
	glVertex3f(0.05f, 0.1625f, 0.025f);

	//rightPlane
	glVertex3f(0.05f, 0.2025f, -0.025f);
	glVertex3f(0.05f, 0.2025f, 0.025f);
	glVertex3f(0.05f, 0.1625f, 0.025f);
	glVertex3f(0.05f, 0.1625f, -0.025f);

	//backPlane
	glVertex3f(0.0f, 0.2025f, -0.025f);
	glVertex3f(0.05f, 0.2025f, -0.025f);
	glVertex3f(0.05f, 0.1625f, -0.025f);
	glVertex3f(0.0f, 0.1625f, -0.025f);

	//leftPlane
	glVertex3f(0.0f, 0.2025f, 0.025f);
	glVertex3f(0.0f, 0.2025f, -0.025f);
	glVertex3f(0.0f, 0.1625f, -0.025f);
	glVertex3f(0.0f, 0.1625f, 0.025f);

	glEnd();
}

void ankitDrawCylinder()
{
	GLUquadric* quadric = NULL;
	quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);//beautification
	glColor3f(1.0f, 0.0f, 0.0f);
	gluCylinder(quadric, 0.125f, 0.125f, 0.5f, 30, 30);
    gluDeleteQuadric(quadric);
}

void ankitDrawRope()
{
	//following code draw the support like rope, which ties the stike and the cylinder filled material
	//there are 2 parts to this, one on the stick and another to the cylinder filled material.
	//this code is for the cylinder filled material, another code can be found in drawHandStick
	GLUquadric* quadric = NULL;
	quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluCylinder(quadric, 0.125f, 0.125f, 0.04f, 30, 30);
    gluDeleteQuadric(quadric);
}

void ankitDrawRocketTopCylinder()
{
	GLUquadric* quadric = NULL;

	quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(1.0f, 0.0f, 0.0f);
	gluCylinder(quadric, 0.175f, 0.0f, 0.2f, 30, 30);
    gluDeleteQuadric(quadric);
}

void ankitDrawRocketFire()
{
	GLUquadric* quadric = NULL;

	quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(1.0f, 1.0f, 0.0f);
	gluCylinder(quadric, 0.175f, 0.125f, 0.20f, 30, 30);
    gluDeleteQuadric(quadric);
}

bool ankitDrawBurst()
{
	static GLfloat circle1 = 0.0f;
	static GLfloat circle2 = 0.0f;
	static GLfloat circle3 = 0.0f;
	static GLfloat circle4 = 0.0f;
	static GLfloat circle5 = 0.0f;

	static GLfloat circle1MaxRadius = 6.0f;
	static GLfloat circle2MaxRadius = 4.8f;
	static GLfloat circle3MaxRadius = 3.6f;
	static GLfloat circle4MaxRadius = 2.4f;
	static GLfloat circle5MaxRadius = 1.2f;

	static GLfloat circleRadiusDisplacement = 0.005f;

	static int startFadeOutCircle1 = 0;
	static int startFadeOutCircle2 = 0;
	static int startFadeOutCircle3 = 0;
	static int startFadeOutCircle4 = 0;
	static int startFadeOutCircle5 = 0;

	static GLfloat colorFadeDisplacement = 0.01f;

	static GLfloat redCircle1 = 1.0f;
	static GLfloat greenCircle1 = 0.0f;
	static GLfloat blueCircle1 = 0.0f;

	static GLfloat redCircle2 = 0.0f;
	static GLfloat greenCircle2 = 1.0f;
	static GLfloat blueCircle2 = 0.0f;

	static GLfloat redCircle3 = 0.0f;
	static GLfloat greenCircle3 = 0.0f;
	static GLfloat blueCircle3 = 1.0f;

	static GLfloat redCircle4 = 1.0f;
	static GLfloat greenCircle4 = 1.0f;
	static GLfloat blueCircle4 = 0.0f;

	static GLfloat redCircle5 = 0.0f;
	static GLfloat greenCircle5 = 1.0f;
	static GLfloat blueCircle5 = 1.0f;

	glBegin(GL_LINES);
	for (GLfloat i = 0.0f; i <= (2.0f * 3.14f); i += (2.0f * 3.14f) / 30.0f)
	{
		if (redCircle1 > 0.0f)
		{
			glColor3f(redCircle1, greenCircle1, blueCircle1);
			glVertex2f(0.0f, 0.0f);
			glVertex2f(cos(i) * circle1, sin(i) * circle1);
		}

		if (greenCircle2 > 0.0f)
		{
			glColor3f(redCircle2, greenCircle2, blueCircle2);
			glVertex2f(0.5f, 0.5f);
			glVertex2f(0.5f + (cos(i) * circle2), 0.5f + (sin(i) * circle2));
		}

		if (blueCircle3 > 0.0f)
		{
			glColor3f(redCircle3, greenCircle3, blueCircle3);
			glVertex2f(-0.5f, 0.5f);
			glVertex2f(-0.5f + (cos(i) * circle3), 0.5f + (sin(i) * circle3));
		}

		if (redCircle4 > 0.0f)
		{
			glColor3f(redCircle4, greenCircle4, blueCircle4);
			glVertex2f(-0.5f, -0.5f);
			glVertex2f(-0.5f + (cos(i) * circle4), -0.5f + (sin(i) * circle4));
		}

		if (greenCircle5 > 0.0f)
		{
			glColor3f(redCircle5, greenCircle5, blueCircle5);
			glVertex2f(0.5f, -0.5f);
			glVertex2f(0.5f + (cos(i) * circle5), -0.5f + (sin(i) * circle5));
		}
	}
	glEnd();

	if (circle1 < circle1MaxRadius)
		circle1 += circleRadiusDisplacement;
	else
		startFadeOutCircle1 = 1;

	if (circle2 < circle2MaxRadius)
		circle2 += circleRadiusDisplacement;
	else
		startFadeOutCircle2 = 1;

	if (circle3 < circle3MaxRadius)
		circle3 += circleRadiusDisplacement;
	else
		startFadeOutCircle3 = 1;

	if (circle4 < circle4MaxRadius)
		circle4 += circleRadiusDisplacement;
	else
		startFadeOutCircle4 = 1;

	if (circle5 < circle5MaxRadius)
		circle5 += circleRadiusDisplacement;
	else
		startFadeOutCircle5 = 1;

	if (startFadeOutCircle1)
	{
		redCircle1 -= colorFadeDisplacement;
        if(redCircle1 < 0.0f)
            redCircle1 = 0.0f;
	}

	if (startFadeOutCircle2)
	{
		greenCircle2 -= colorFadeDisplacement;
        if(greenCircle2 < 0.0f)
            greenCircle2 = 0.0f;
	}

	if (startFadeOutCircle3)
	{
		blueCircle3 -= colorFadeDisplacement;
        if(blueCircle3 < 0.0f)
            blueCircle3 = 0.0f;
	}

	if (startFadeOutCircle4)
	{
		redCircle4 -= colorFadeDisplacement;
        if(redCircle4 < 0.0f)
            redCircle4 = 0.0f;
        
		greenCircle4 -= colorFadeDisplacement;
        if(greenCircle4 < 0.0f)
            greenCircle4 = 0.0f;
	}

	if (startFadeOutCircle5)
	{
		greenCircle5 -= colorFadeDisplacement;
        if(greenCircle5 < 0.0f)
            greenCircle5 = 0.0f;
        
		blueCircle5 -= colorFadeDisplacement;
        if(blueCircle5 < 0.0f)
            blueCircle5 = 0.0f;
	}
    
    if(
        blueCircle5 == 0.0f  && greenCircle5 == 0.0f &&
        greenCircle4 == 0.0f && redCircle4 == 0.0f && 
        blueCircle3 == 0.0f  && 
        greenCircle2 == 0.0f && 
        redCircle1 == 0.0f
    )
    {
        return(true);
    }
    else
    {
        return(false);
    }
}

