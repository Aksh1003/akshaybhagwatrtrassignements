/**
 *  From OpenGL SuperBible (3rd Edition)
 **/

#include "Main.h"


//LoadIdentityMatrix()
void LoadIdentityMatrix(GLfloat *fMatrix)
{
    //variable declarations
    static GLfloat identity[16] = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };
    
    //code
    memcpy(fMatrix, identity, sizeof(identity));
}

// Calculate the cross product of two vectors
void VectorCrossProduct(const GLVector vU, const GLVector vV, GLVector vResult)
{
    //code
    vResult[0] =  vU[1]*vV[2] - vV[1]*vU[2];
    vResult[1] = -vU[0]*vV[2] + vV[0]*vU[2];
    vResult[2] =  vU[0]*vV[1] - vV[0]*vU[1];
}

// Rotates a vector using a 4x4 matrix. Translation column is ignored
void RotationVector(const GLVector vSrcVector, const GLMatrix mMatrix, GLVector vOut)
{
    //code
    vOut[0] = mMatrix[0] * vSrcVector[0] + mMatrix[4] * vSrcVector[1] + mMatrix[8] *  vSrcVector[2];
    vOut[1] = mMatrix[1] * vSrcVector[0] + mMatrix[5] * vSrcVector[1] + mMatrix[9] *  vSrcVector[2];
    vOut[2] = mMatrix[2] * vSrcVector[0] + mMatrix[6] * vSrcVector[1] + mMatrix[10] * vSrcVector[2];    	
}

//RotateMatrix()
void RotationMatrix(GLfloat fAngleInRad, GLfloat x, GLfloat y, GLfloat z, GLfloat *fMatrix)
{    
    //variable declarations
    GLfloat vecLength, sinSave, cosSave, oneMinusCos;
    GLfloat xx, yy, zz, xy, yz, zx, xs, ys, zs;
    
    //code
        //If NULL vactor pass for rotation calculation then program crashes....
    if(x == 0.0f && y == 0.0f && z == 0.0f)
    {
        LoadIdentityMatrix(fMatrix);
        return;
    }
    
    // fAngleInRad = fAngleInDeg * GL_PI / 180.0f;
    
    //scale vector
    vecLength = (GLfloat) sqrt(x*x + y*y + z*z);
    
    //Rotation matrix is normalized
    x /= vecLength;
    y /= vecLength;
    z /= vecLength;
    
    sinSave = (GLfloat) sin(fAngleInRad);
    cosSave = (GLfloat) cos(fAngleInRad);
    oneMinusCos = 1.0f - cosSave;
    
    xx = x * x;
    yy = y * y;
    zz = z * z;
    xy = x * y;
    yz = y * z;
    zx = z * x;
    xs = x * sinSave;
    ys = y * sinSave;
    zs = z * sinSave;
    
    fMatrix[0]  = (oneMinusCos * xx) + cosSave;
    fMatrix[4]  = (oneMinusCos * xy) - zs;
    fMatrix[8]  = (oneMinusCos * zx) + ys;
    fMatrix[12] = 0.0f;
    
    fMatrix[1]  = (oneMinusCos * xy) + zs;
    fMatrix[5]  = (oneMinusCos * yy) + cosSave;
    fMatrix[9]  = (oneMinusCos * yz) - xs;
    fMatrix[13] = 0.0f;
    
    fMatrix[2]  = (oneMinusCos * zx) - ys;
    fMatrix[6]  = (oneMinusCos * yz) + xs;
    fMatrix[10] = (oneMinusCos * zz) + cosSave;
    fMatrix[14] = 0.0f;
    
    fMatrix[3]  = 0.0f;
    fMatrix[7]  = 0.0f;
    fMatrix[11] = 0.0f;
    fMatrix[15] = 1.0f;
}


//ApplyCameraTransform()
void ApplyCameraTransform(GLFrame *pCamera)
{    
    //variable declarations
    GLfloat mMatrix[16];
    GLfloat vAxisX[3];
    GLfloat zFlipped[3];
    
    //code
    zFlipped[0] = -pCamera->vForward[0];
    zFlipped[1] = -pCamera->vForward[1];
    zFlipped[2] = -pCamera->vForward[2];
    
    //Derive X vector (X = Y * Z) => cross product of Y and Z vector
   VectorCrossProduct(pCamera->vUp, zFlipped, vAxisX);
   
   //Populate matrix, note this is just the rotation and is transposed
   mMatrix[0]  = vAxisX[0];
   mMatrix[4]  = vAxisX[1];
   mMatrix[8]  = vAxisX[2];
   mMatrix[12] = 0.0f;
   
   mMatrix[1]  = pCamera->vUp[0];
   mMatrix[5]  = pCamera->vUp[1];
   mMatrix[9]  = pCamera->vUp[2];
   mMatrix[13] = 0.0f;
   
   mMatrix[2]  = zFlipped[0];
   mMatrix[6]  = zFlipped[1];
   mMatrix[10] = zFlipped[2];
   mMatrix[14] = 0.0f;
   
   mMatrix[3]  = 0.0f;
   mMatrix[7]  = 0.0f;
   mMatrix[11] = 0.0f;
   mMatrix[15] = 1.0f;
   
   //Do the rotation first
   glMultMatrixf(mMatrix);
   
   //Now, translate backwards
   glTranslatef(
    -pCamera->vLocation[0],
    -pCamera->vLocation[1],
    -pCamera->vLocation[2]
   );
   
   // gluLookAt(
    // pCamera->vLocation[0],
    // pCamera->vLocation[1],
    // pCamera->vLocation[2],
    
    // // 0.0f, 0.0f, 0.0f,
    // // vAxisX[0],
    // // vAxisX[1],
    // // vAxisX[2],
    // pCamera->vForward[0],
    // pCamera->vForward[1],
    // pCamera->vForward[2],
    
    // pCamera->vUp[0],
    // pCamera->vUp[1],
    // pCamera->vUp[2]
   // );
}

//Move Camera forward or backward (local z-axis)
void CameraMoveForward(GLFrame *pFrame, GLfloat fStep)
{
    //code
    pFrame->vLocation[0] += pFrame->vForward[0] * fStep;
    pFrame->vLocation[1] += pFrame->vForward[1] * fStep;
    pFrame->vLocation[2] += pFrame->vForward[2] * fStep;
}

//Move Camera right or left (local x-axis)
void CameraMoveRight(GLFrame *pFrame, GLfloat fStep)
{
    //variable declarations
    GLVector vCross;
    
    //code
    VectorCrossProduct(pFrame->vUp, pFrame->vForward, vCross);
    
    pFrame->vLocation[0] += vCross[0] * fStep;
    pFrame->vLocation[1] += vCross[1] * fStep;
    pFrame->vLocation[2] += vCross[2] * fStep;
}

//Move Camera up or down (local y-axis)
void CameraMoveUp(GLFrame *pFrame, GLfloat fStep)
{
    //code
    pFrame->vLocation[0] += pFrame->vUp[0] * fStep;
    pFrame->vLocation[1] += pFrame->vUp[1] * fStep;
    pFrame->vLocation[2] += pFrame->vUp[2] * fStep;
}

//Rotate cemara along y-axis
void CameraRotateAlongY(GLFrame *pFrame, GLfloat fAngle)
{
    //variable declarations
    GLMatrix mRotation;
    GLVector vNewForward;
    
    //code
    RotationMatrix((GLfloat)DEG_TO_RAD(fAngle), 0.0f, 1.0f, 0.0f, mRotation);
    RotationMatrix(fAngle, pFrame->vUp[0], pFrame->vUp[1], pFrame->vUp[2], mRotation);
    
    RotationVector(pFrame->vForward, mRotation, vNewForward);
    memcpy(pFrame->vForward, vNewForward, sizeof(GLVector));
}

//Rotate cemara along x-axis
void CameraRotateAlongX(GLFrame *pFrame, GLfloat fAngle)
{
    //variable declarations
    GLMatrix mRotation;
    GLVector vCross;
    GLVector vNewVect;
    
    //code
    VectorCrossProduct(pFrame->vUp, pFrame->vForward, vCross);
    RotationMatrix(fAngle, vCross[0], vCross[1], vCross[2], mRotation);
    
    //Inline 3x3 matrix multiply for rotation only
    vNewVect[0] = mRotation[0] * pFrame->vForward[0] + mRotation[4] * pFrame->vForward[1] + mRotation[8] *  pFrame->vForward[2];	
    vNewVect[1] = mRotation[1] * pFrame->vForward[0] + mRotation[5] * pFrame->vForward[1] + mRotation[9] *  pFrame->vForward[2];	
    vNewVect[2] = mRotation[2] * pFrame->vForward[0] + mRotation[6] * pFrame->vForward[1] + mRotation[10] * pFrame->vForward[2];
    
    // Update pointing up vector
    vNewVect[0] = mRotation[0] * pFrame->vUp[0] + mRotation[4] * pFrame->vUp[1] + mRotation[8] *  pFrame->vUp[2];	
    vNewVect[1] = mRotation[1] * pFrame->vUp[0] + mRotation[5] * pFrame->vUp[1] + mRotation[9] *  pFrame->vUp[2];	
    vNewVect[2] = mRotation[2] * pFrame->vUp[0] + mRotation[6] * pFrame->vUp[1] + mRotation[10] * pFrame->vUp[2];	
    memcpy(pFrame->vUp, vNewVect, sizeof(GLfloat) * 3);
}

//Rotate cemara along x-axis
void CameraRotateAlongZ(GLFrame *pFrame, GLfloat fAngle)
{
    GLMatrix mRotation;
    GLVector vNewVect;

    // Only the up vector needs to be rotated
    RotationMatrix(fAngle, pFrame->vForward[0], pFrame->vForward[1], pFrame->vForward[2], mRotation);

    vNewVect[0] = mRotation[0] * pFrame->vUp[0] + mRotation[4] * pFrame->vUp[1] + mRotation[8] *  pFrame->vUp[2];	
    vNewVect[1] = mRotation[1] * pFrame->vUp[0] + mRotation[5] * pFrame->vUp[1] + mRotation[9] *  pFrame->vUp[2];	
    vNewVect[2] = mRotation[2] * pFrame->vUp[0] + mRotation[6] * pFrame->vUp[1] + mRotation[10] * pFrame->vUp[2];	
    memcpy(pFrame->vUp, vNewVect, sizeof(GLfloat) * 3);
}

