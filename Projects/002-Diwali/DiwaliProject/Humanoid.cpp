//Headers
#include "Main.h"
#include "Resource.h"

GLuint vmFACE_TEXTURE;
GLuint vmFACECHILDGIRL_TEXTURE;
GLuint vmSHIRTMENFRONT_TEXTURE;
GLuint vmSHIRTMEN_TEXTURE;

GLuint vmKURTAWOMANFRONT_TEXTURE;
GLuint vmKURTAWOMAN_TEXTURE;

GLuint vmTROUSER_TEXTURE;

void LoadHumanTexture(void)
{
    LoadGLTexture(&vmFACE_TEXTURE, MAKEINTRESOURCE(VM_FACE_BITMAP));
	LoadGLTexture(&vmFACECHILDGIRL_TEXTURE, MAKEINTRESOURCE(VM_FACECHILDGIRL_BITMAP));
	LoadGLTexture(&vmSHIRTMENFRONT_TEXTURE, MAKEINTRESOURCE(VM_SHIRTMENFRONT_BITMAP));
	LoadGLTexture(&vmSHIRTMEN_TEXTURE, MAKEINTRESOURCE(VM_SHIRTMEN_BITMAP));
	LoadGLTexture(&vmTROUSER_TEXTURE, MAKEINTRESOURCE(VM_TROUSER_BITMAP));

	LoadGLTexture(&vmKURTAWOMANFRONT_TEXTURE, MAKEINTRESOURCE(VM_KURTAWOMANFRONT_BITMAP));
	LoadGLTexture(&vmKURTAWOMAN_TEXTURE, MAKEINTRESOURCE(VM_KURTAWOMAN_BITMAP));
}

void DeleteHumanTexture(void)
{
    glDeleteTextures(1, &vmFACE_TEXTURE);
	glDeleteTextures(1, &vmTROUSER_TEXTURE);
	glDeleteTextures(1, &vmSHIRTMENFRONT_TEXTURE);
	glDeleteTextures(1, &vmSHIRTMEN_TEXTURE);
	glDeleteTextures(1, &vmFACECHILDGIRL_TEXTURE);

	glDeleteTextures(1, &vmKURTAWOMANFRONT_TEXTURE);
	glDeleteTextures(1, &vmKURTAWOMAN_TEXTURE);
}

void vmAdultHuman(void) {
	// Function Declaration
	void vmLeftForeArm(void);
	void vmRightForeArm(void);

	void vmChestStomach(GLuint, GLuint);

	void vmLeftArm(GLuint);
	void vmRightArm(GLuint);


	void vmLeftThigh(void);
	void vmRightThigh(void);

	void vmLeftCalf(void);
	void vmRightCalf(void);


	// Code
	glPushMatrix();
	glRotatef(5.0f, 0.0f, 1.0f, 0.0f); // -> refer above comment
	vmChestStomach(vmSHIRTMENFRONT_TEXTURE, vmSHIRTMEN_TEXTURE);
	glTranslatef(0.0f, 2.0f, 0.0f);
	glPushMatrix();
	// Neck
	glColor3f(0.8f, 0.6f, 0.5f);

	glPushMatrix();
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		GLUquadricObj* quadratic = NULL;
		quadratic = gluNewQuadric();
		gluCylinder(quadratic, 0.1f, 0.125f, 0.15f, 32, 32);
	glPopMatrix();

	// Head
	glPushMatrix();
		glTranslatef(0.0f, 0.15f, 0.0f);
		glScalef(1.0f, 1.25f, 1.0f);
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glColor3f(0.8f, 0.6f, 0.5f);
		GLUquadricObj* sphere = NULL;
		sphere = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, vmFACE_TEXTURE);
		gluQuadricDrawStyle(sphere, GLU_FILL);
		gluQuadricTexture(sphere, TRUE);
		gluQuadricNormals(sphere, GLU_SMOOTH);
		//gluSphere(sphere, 0.3, 10, 10);
		gluCylinder(sphere, 0.2f, 0.3f, 0.6f, 32, 32);
		gluDeleteQuadric(sphere);
	glPopMatrix();


	for (GLfloat angle = -3.14; angle <= 3.14; angle = angle + 0.01f) {

		glColor3f(1.0f, 0.874f, 0.768f);
		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.2f, 0.0f);
		glVertex3f(0.2 * cos(angle), 0.15f, 0.2 * sin(angle));
		glEnd();

		glColor3f(0.2f, 0.2f, 0.2f);
		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.9f, 0.0f);
		glVertex3f(0.3 * cos(angle), 0.9f, 0.3 * sin(angle));
		glEnd();
	}
	glPopMatrix();




	// LEFT SIDE - UPPER

	glPushMatrix();
		glRotatef(5.0f, 0.0f, 0.0f, 1.0f);
		glTranslatef(-0.8f, 0.0f, 0.0f);
	glPushMatrix();
	
	
	glRotatef(-10.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(80.0f, 0.0f, 0.0f, 1.0f); 


	vmLeftArm(vmSHIRTMEN_TEXTURE);
	// LEFT ELBOW
	glTranslatef(-1.0f, 0.0f, 0.0f);

	glPushMatrix();
		glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
		
		// Left forearm
		vmLeftForeArm();
	glPopMatrix();

	glPopMatrix();

	glPopMatrix();

	
	glPushMatrix();
		glRotatef(-5.0f, 0.0f, 0.0f, 1.0f);
		glTranslatef(0.8f, 0.0f, 0.0f);
	glPushMatrix();
			glRotatef(-10.0f, 1.0f, 0.0f, 0.0f); 
			glRotatef(-75.0f, 0.0f, 0.0f, 1.0f); 

			// right arm
			vmRightArm(vmSHIRTMEN_TEXTURE);

			// right ELBOW
			glTranslatef(1.0f, 0.0f, 0.0f);
			glPushMatrix();
			glRotatef(-10.0f, 0.0f, 0.0f, 1.0f);

				// right forearm
				vmRightForeArm();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glPopMatrix();



	// LEFT SIDE LOWER
	glPushMatrix();

	glTranslatef(-0.25f, 0.0f, 0.0f);
	glPushMatrix();
	vmLeftThigh();


	glTranslatef(0.0f, -1.5f, 0.0f);

	vmLeftCalf();
	
	glPopMatrix();

	glPopMatrix();


	// RIGHT SIDE LOWER
	glPushMatrix();

	
	glTranslatef(0.25f, 0.0f, 0.0f);
	glPushMatrix();

	vmRightThigh();

	glTranslatef(0.0f, -1.5f, 0.0f);
	

	vmRightCalf();

	glPopMatrix();

	glPopMatrix();

	glPopMatrix();
}

void vmChildHuman(void) {
	// Function Declaration
	void vmLeftForeArm(void);
	void vmRightForeArm(void);

	void vmChestStomach(GLuint, GLuint);

	void vmLeftArm(GLuint);
	void vmRightArm(GLuint);


	void vmLeftThigh(void);
	void vmRightThigh(void);

	void vmLeftCalf(void);
	void vmRightCalf(void);
	void vmAgarbatti(void);

	// Variable Declarations

	// Code
	glPushMatrix();
		glRotatef(5.0f, 0.0f, 1.0f, 0.0f); // -> refer above comment
		vmChestStomach(vmKURTAWOMANFRONT_TEXTURE, vmKURTAWOMAN_TEXTURE);
		glTranslatef(0.0f, 2.0f, 0.0f);
	glPushMatrix();

	// Neck
	glColor3f(0.8f, 0.6f, 0.5f);

	glPushMatrix();
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		GLUquadricObj* quadratic = NULL;
		quadratic = gluNewQuadric();
		gluCylinder(quadratic, 0.1f, 0.125f, 0.15f, 32, 32);
	glPopMatrix();

	// Head
	glPushMatrix();
		glTranslatef(0.0f, 0.5f, 0.0f);
		glScalef(1.0f, 1.25f, 1.0f);
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		glColor3f(0.8f, 0.6f, 0.5f);
		GLUquadricObj* sphere = NULL;
		sphere = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, vmFACECHILDGIRL_TEXTURE);
		gluQuadricDrawStyle(sphere, GLU_FILL);
		gluQuadricTexture(sphere, TRUE);
		gluQuadricNormals(sphere, GLU_SMOOTH);
		gluSphere(sphere, 0.3, 10, 10);
		//gluCylinder(sphere, 0.2f, 0.3f, 0.6f, 32, 32);
		glBindTexture(GL_TEXTURE_2D, 0);

		// Girl Round Bun
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, 0.35f, 0.25f);
		sphere = gluNewQuadric();
		gluSphere(sphere, 0.2, 10, 10);


		gluDeleteQuadric(sphere);

	glPopMatrix();


	glPopMatrix();




	// LEFT SIDE - UPPER

	glPushMatrix();
	glRotatef(5.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(-0.8f, 0.0f, 0.0f);
	
	glPushMatrix();
	glRotatef(15.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(80.0f, 0.0f, 0.0f, 1.0f);
	vmLeftArm(vmKURTAWOMAN_TEXTURE);
	// LEFT ELBOW
	glTranslatef(-1.0f, 0.0f, 0.0f);

	glPushMatrix();
	glRotatef(10.0f, 0.0f, 0.0f, 1.0f); // Rotating to make it in one position
	glRotatef(0.0f, 0.0f, 1.0f, 0.0f); // Walk Animation

	// Left forearm
	vmLeftForeArm();
	glPopMatrix();

	glPopMatrix();

	glPopMatrix();


	glPushMatrix();
	glRotatef(-5.0f, 0.0f, 0.0f, 1.0f);

	glTranslatef(0.8f, 0.0f, 0.0f);
	glPushMatrix();
	glRotatef(-15.0f, 1.0f, 0.0f, 0.0f); // Walk Animation
	glRotatef(-80.0f, 0.0f, 0.0f, 1.0f); // Shoulder Bone Sphere right -> Rotate

// right arm
	vmRightArm(vmKURTAWOMAN_TEXTURE);

	// right ELBOW
	glTranslatef(1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glRotatef(-10.0f, 0.0f, 0.0f, 1.0f);    // To Set Position
	glRotatef(-10.0f, 0.0f, 1.0f, 0.0f); // Walk Animation

// right forearm
	vmRightForeArm();
	//vmAgarbatti();

	glPopMatrix();

	glPopMatrix();

	glPopMatrix();

	glPopMatrix();



	// LEFT SIDE LOWER
	glPushMatrix();

	glRotatef(-15.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(-0.25f, 0.0f, 0.0f);
	glPushMatrix();
	vmLeftThigh();

	glTranslatef(0.0f, -1.5f, 0.0f);
	glRotatef(0.0f, 1.0f, 0.0f, 0.0f);
	vmLeftCalf();

	glPopMatrix();

	glPopMatrix();


	// RIGHT SIDE LOWER
	glPushMatrix();

	glRotatef(5.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.25f, 0.0f, 0.0f);
	glPushMatrix();

	vmRightThigh();

	glTranslatef(0.0f, -1.5f, 0.0f);
	glRotatef(40.0f, 1.0f, 0.0f, 0.0f);

	vmRightCalf();

	glPopMatrix();

	glPopMatrix();

	glPopMatrix();
    
}


void vmChestStomach(GLuint vmSHIRT, GLuint vmSHIRTOTHER) {
	glColor3f(1.0f, 1.0f, 1.0f);
	// Chest
	// FRONT FACE
	glBindTexture(GL_TEXTURE_2D, vmSHIRT);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.75f, 2.0f, 0.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.75f, 2.0f, 0.35f);
	glTexCoord2f(0.2f, 0.0f);
	glVertex3f(-0.5f, 0.75f, 0.25f);
	glTexCoord2f(0.8f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.25f);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, vmSHIRTOTHER);
	// RIGHT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.75f, 2.0f, -0.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.75f, 2.0f, 0.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, 0.75f, -0.25f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.75f, 2.0f, -0.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.75f, 2.0f, -0.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, 0.75f, -0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, 0.75f, -0.25f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.75f, 2.0f, -0.35f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.75f, 2.0f, 0.35f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, 0.75f, 0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.5f, 0.75f, -0.25f);
	glEnd();


	// TOP FACE
	glBegin(GL_QUADS);
	glVertex3f(0.75f, 2.0f, -0.35f);
	glVertex3f(-0.75f, 2.0f, -0.35f);
	glVertex3f(-0.75f, 2.0f, 0.35f);
	glVertex3f(0.75f, 2.0f, 0.35f);
	glEnd();



	// Stomach

	// Front - Stomach
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.5f, 0.75f, 0.25f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, 0.75f, 0.25f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.3f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.3f);
	glEnd();


	// Right - Stomach
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.5f, 0.75f, -0.25f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.5f, 0.75f, 0.25f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, -0.25f);
	glEnd();


	// Back - Stomach
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.5f, 0.75f, -0.25f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, 0.75f, -0.25f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, -0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, -0.25f);
	glEnd();


	// Left - Stomach
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.5f, 0.75f, -0.25f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, 0.75f, 0.25f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, 0.0f, 0.25f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.5f, 0.0f, -0.25f);
	glEnd();




	// Bottom - Stomach
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, -0.25f);
	glVertex3f(-0.5f, 0.0f, -0.25f);
	glVertex3f(-0.5f, 0.0f, 0.25f);
	glVertex3f(0.5f, 0.0f, 0.25f);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, 0);
}

void vmLeftForeArm() {
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(0.8f, 0.6f, 0.5f);

	// FRONT FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glVertex3f(-1.0f, 0.15f, 0.1f);
	glVertex3f(-1.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glVertex3f(-1.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glVertex3f(-1.0f, -0.15f, -0.1f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glVertex3f(-1.0f, 0.15f, 0.1f);
	glVertex3f(-1.0f, 0.15f, -0.1f);
	glVertex3f(-1.0f, -0.15f, -0.1f);
	glVertex3f(-1.0f, -0.15f, 0.1f);
	glEnd();


	// TOP FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(-1.0f, 0.15f, -0.1f);
	glVertex3f(-1.0f, 0.15f, 0.1f);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glEnd();

	// BOTTOM FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glVertex3f(-1.0f, -0.15f, -0.1f);
	glVertex3f(-1.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glEnd();

	// Fist
	glTranslatef(-1.0f, 0.0f, 0.0f);
	glScalef(1.5f, 1.0f, 1.0f);
	GLUquadricObj* sphere = NULL;
	sphere = gluNewQuadric();
	gluSphere(sphere, 0.17f, 32, 32);
	gluDeleteQuadric(sphere);

}

void vmLeftArm(GLuint vmARMTEXTURE) {
	glBindTexture(GL_TEXTURE_2D, vmARMTEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -0.15f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.00f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -0.15f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0, -0.15f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0, -0.15f, 0.15f);
	glEnd();


	// TOP FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, 0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0, -0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0, -0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}


void vmRightForeArm() {

	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(0.8f, 0.6f, 0.5f);

	// FRONT FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glVertex3f(1.0f, 0.15f, 0.1f);
	glVertex3f(1.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glVertex3f(1.0f, -0.15f, -0.1f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 0.15f, 0.1f);
	glVertex3f(1.0f, 0.15f, -0.1f);
	glVertex3f(1.0f, -0.15f, -0.1f);
	glVertex3f(1.0f, -0.15f, 0.1f);
	glEnd();


	// TOP FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.15f, -0.1f);
	glVertex3f(1.0f, 0.15f, -0.1f);
	glVertex3f(1.0f, 0.15f, 0.1f);
	glVertex3f(0.0f, 0.15f, 0.1f);
	glEnd();

	// BOTTOM FACE
	glBegin(GL_QUADS);
	glVertex3f(0.0f, -0.15f, -0.1f);
	glVertex3f(1.0f, -0.15f, -0.1f);
	glVertex3f(1.0f, -0.15f, 0.1f);
	glVertex3f(0.0f, -0.15f, 0.1f);
	glEnd();

	glTranslatef(1.0f, 0.0f, 0.0f);
	glScalef(1.5f, 1.0f, 1.0f);
	GLUquadricObj* sphere = NULL;
	sphere = gluNewQuadric();
	gluSphere(sphere, 0.17f, 32, 32);
	gluDeleteQuadric(sphere);

}

void vmAgarbatti(void) {
	glColor3f(0.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	GLUquadricObj* quadratic = NULL;
	quadratic = gluNewQuadric();
	gluCylinder(quadratic, 0.01f, 0.01f, 0.75f, 10, 10);
}

void vmRightArm(GLuint vmARMTEXTURE) {
	glBindTexture(GL_TEXTURE_2D, vmARMTEXTURE);
	glColor3f(1.0f, 1.0f, 1.0f);

	// FRONT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -0.15f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.00f, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -0.15f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0, -0.15f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0, -0.15f, 0.15f);
	glEnd();


	// TOP FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0, 0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0, 0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 0.15f, 0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, -0.15f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0, -0.15f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0, -0.15f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, -0.15f, 0.15f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void vmLeftThigh() {
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindTexture(GL_TEXTURE_2D, vmTROUSER_TEXTURE);

	// FRONT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void vmRightThigh() {
	glBindTexture(GL_TEXTURE_2D, vmTROUSER_TEXTURE);

	// FRONT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, -0.15f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.25f, 0.0f, 0.15f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();
}

void vmLeftCalf() {
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// FRONT FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, -0.15f);
	glVertex3f(0.15f, 0.0f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void vmRightCalf() {
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// FRONT FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();


	// RIGHT FACE
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, -0.15f);
	glVertex3f(0.15f, 0.0f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// BACK FACE
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glEnd();


	// LEFT FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.15f);
	glVertex3f(-0.15f, 0.0f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glEnd();


	// BOTTOM FACE
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, -0.15f);
	glVertex3f(-0.15f, -1.5f, 0.15f);
	glVertex3f(0.15f, -1.5f, 0.15f);
	glEnd();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

