
#include "Font.h"

//hard-coded font height and extra space
int fontHeight = 29;    //for times-new-roman

void RenderBitmapString(float x, float y, char *str)
{
    //variable declarations
    char *c;
    GLubyte *fontFace;
    
    //code
    glRasterPos2f(x, y);
    
    glPushClientAttrib( GL_CLIENT_PIXEL_STORE_BIT);
    glPixelStorei( GL_UNPACK_SWAP_BYTES, GL_FALSE);
    glPixelStorei( GL_UNPACK_LSB_FIRST, GL_FALSE);
    glPixelStorei( GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei( GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
    
    for(c = str; *c != '\0'; c++)
    {
        // fontFace = TimesRoman24_Character_Map[*c];
        glBitmap(
            (TimesRoman24_Character_Map[*c])[0], //width
            fontHeight,
            0,
            0,
            (float)TimesRoman24_Character_Map[*c][0],
            0.0f,
            (TimesRoman24_Character_Map[*c] + 1)
        );
    }
    
    glPopClientAttrib();
}

