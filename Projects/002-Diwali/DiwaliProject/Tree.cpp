//Headers
#include "Main.h"
#include "Resource.h"

//global variable declarations
GLuint treeBarkTexture, treeLeafTexture;

void LoadTreeTexture(void)
{
    //code
    LoadGLTexture(&treeBarkTexture, MAKEINTRESOURCE(TREE_BARK_BITMAP));
    LoadGLTexture(&treeLeafTexture, MAKEINTRESOURCE(TREE_LEAF_BITMAP));
}

void DeleteTreeTexture(void)
{
    //code
    if (treeLeafTexture)
    {
        glDeleteTextures(1, &treeLeafTexture);
        treeLeafTexture = 0;
    }

    if (treeBarkTexture)
    {
        glDeleteTextures(1, &treeBarkTexture);
        treeBarkTexture = 0;
    }
}

//
//Tree2()
//
void Tree2(void)
{
    //variable declarations
    GLUquadric* quadric = NULL;
    int     i;
    GLfloat fAngle;

    //code
    //CYLINDER
    glPushMatrix();
    glColor3f(1.0f, 1.0f, 1.0f);
    //Translate geometry
    glTranslatef(0.00f, -0.00f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(-90.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.32f, 0.32f, 2.33f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 1.93f, 0.20f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(-60.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.14f, 0.14f, 1.27f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 1.93f, -0.20f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(-120.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.14f, 0.14f, 1.27f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 4.33f, 0.00f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.50f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 3.38f, 1.40f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 3.38f, -1.40f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 4.69f, 1.55f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 4.69f, -1.55f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 5.79f, -1.01f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 5.79f, 1.01f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 6.49f, 0.00f);
    //Scale geometry
    glScalef(0.72f, 1.0f, 1.40f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}



//
//Tree()
//
void Tree(void)
{
    //variable declarations
    GLUquadric* quadric = NULL;
    int     i;
    GLfloat fAngle;

    //code
    //CYLINDER
    glPushMatrix();
    glColor3f(1.0f, 1.0f, 1.0f);
    //Translate geometry
    glTranslatef(0.00f, 3.01f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(90.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.31f, 0.31f, 3.01f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(-0.00f, 1.99f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(-33.50f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.31f, 0.31f, 1.50f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(-0.00f, 1.99f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(-146.50f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.31f, 0.31f, 1.50f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 3.86f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1.00f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 3.17f, -1.78f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.84f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(0.00f, 3.17f, 1.78f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.84f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(-0.00f, 1.99f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(90.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(240.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(109.50f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.31f, 0.31f, 1.50f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //CYLINDER
    glPushMatrix();
    //Translate geometry
    glTranslatef(-0.00f, 1.99f, 0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(90.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(120.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(109.50f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluCylinder(quadric, 0.31f, 0.31f, 1.50f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(1.78f, 3.17f, -0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.84f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    //SPHERE
    glPushMatrix();
    //Translate geometry
    glTranslatef(-1.78f, 3.17f, -0.00f);
    //Scale geometry
    glScalef(1.00f, 1.00f, 1.00f);
    //X-Rotation
    glRotatef(0.00f, 1.0f, 0.0f, 0.0f);
    //Y-Rotation
    glRotatef(0.00f, 0.0f, 1.0f, 0.0f);
    //Z-Rotation
    glRotatef(0.00f, 0.0f, 0.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.84f, 30, 30);
    gluDeleteQuadric(quadric);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}
