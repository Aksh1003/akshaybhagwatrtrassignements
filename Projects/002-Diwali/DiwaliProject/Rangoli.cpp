//Headers
#include "Main.h"
#include "Resource.h"

void ARB_Rangoli_1(void)
{
	glColor3f(1.0f, 1.0f, 1.0f);

	//middle vertical line
	glBegin(GL_QUADS);

	glVertex3f(0.1f, 0.7f, 0.0f);

	glVertex3f(0.0f, 0.7f, 0.0f);

	glVertex3f(0.0f, -0.8f, 0.0f);

	glVertex3f(0.1f, -0.8f, 0.0f);

	glEnd();

	//top right horizontal line
	glBegin(GL_QUADS);

	glVertex3f(0.9f, 0.7f, 0.0f);

	glVertex3f(0.1f, 0.7f, 0.0f);

	glVertex3f(0.1f, 0.6f, 0.0f);

	glVertex3f(0.8f, 0.6f, 0.0f);

	glEnd();

	//bottom left horizontal line
	glBegin(GL_QUADS);

	glVertex3f(0.0f, -0.7f, 0.0f);

	glVertex3f(-0.7f, -0.7f, 0.0f);

	glVertex3f(-0.8f, -0.8f, 0.0f);

	glVertex3f(0.0f, -0.8f, 0.0f);

	glEnd();

	//middle horizontal line
	glBegin(GL_QUADS);

	glVertex3f(0.8f, 0.0f, 0.0f);

	glVertex3f(-0.7f, 0.0f, 0.0f);

	glVertex3f(-0.7f, -0.1f, 0.0f);

	glVertex3f(0.8f, -0.1f, 0.0f);

	glEnd();

	//top left vertical line
	glBegin(GL_QUADS);

	glVertex3f(-0.6f, 0.7f, 0.0f);

	glVertex3f(-0.7f, 0.8f, 0.0f);

	glVertex3f(-0.7f, -0.1f, 0.0f);

	glVertex3f(-0.6f, -0.1f, 0.0f);

	glEnd();

	//bottom right vertical line
	glBegin(GL_QUADS);

	glVertex3f(0.8f, 0.0f, 0.0f);

	glVertex3f(0.7f, 0.0f, 0.0f);

	glVertex3f(0.7f, -0.8f, 0.0f);

	glVertex3f(0.8f, -0.9f, 0.0f);

	glEnd();

	//right top middle dot

	glBegin(GL_QUADS);

	glVertex3f(0.45f, 0.35f, 0.0f);
	glVertex3f(0.5f, 0.35f, 0.0f);
	glVertex3f(0.5f, 0.3f, 0.0f);
	glVertex3f(0.45f, 0.3f, 0.0f);

	glEnd();


	//left top middle dot

	glBegin(GL_QUADS);

	glVertex3f(-0.3f, 0.35f, 0.0f);
	glVertex3f(-0.35f, 0.35f, 0.0f);
	glVertex3f(-0.35f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);

	glEnd();

	//left bottom dot

	glBegin(GL_QUADS);

	glVertex3f(-0.3f, -0.45f, 0.0f);
	glVertex3f(-0.35f, -0.45f, 0.0f);
	glVertex3f(-0.35f, -0.4f, 0.0f);
	glVertex3f(-0.3f, -0.4f, 0.0f);

	glEnd();

	//right bottom middle dot

	glBegin(GL_QUADS);

	glVertex3f(0.45f, -0.45f, 0.0f);
	glVertex3f(0.5f, -0.45f, 0.0f);
	glVertex3f(0.5f, -0.4f, 0.0f);
	glVertex3f(0.45f, -0.4f, 0.0f);

	glEnd();
}

void ARB_Rangoli_2(void)
{
	//2nd inner circle
	// glBegin(GL_POLYGON);

	// GLfloat rColor = 1.0f, gColor = 0.0f, bColor = 0.0f;

	// glColor3f(1.5f, 0.0f, 0.0f);
	// for (GLfloat angle = 0.0f; angle <= 180.0f; angle = angle + 0.01f)
	// {
		// glVertex3f(sin(angle) * 1.2f, cos(angle) * 1.2f, 0.0f);

	// }

	// glEnd();

	// //innermost circle
	// glBegin(GL_POLYGON);

	// glColor3f(1.0f, 1.5f, 0.0f);
	// GLint denom = 5;
	// for (GLfloat angle = 0.0f; angle <= 180.0f; angle = angle + 0.01f)
	// {
		// glVertex3f(sin(angle), cos(angle), 0.0f);
	// }

	// glEnd();


	//1st Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	//second diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glEnd();

	//3rd Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glEnd();

	//4th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glEnd();

	//5th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glEnd();

	//6th diamond
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glEnd();

	//7th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	//8th Diamond
	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();

	//9th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	//10th Diamond

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

}

void ARB_Rangoli_3(void)
{
	//1st Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	//second diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0, 1.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glEnd();

	//3rd Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);

	glEnd();

	//4th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glEnd();

	//5th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.5f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glEnd();

	//6th diamond
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.5f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glEnd();

	//7th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	//8th Diamond
	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 0.9f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();

	//9th Diamond
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	//10th Diamond

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, -0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 0.5f, 1.5f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.5f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

}

void ARB_Rangoli_4(void)
{
	//top right - white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glEnd();

	//top left - white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	glEnd();

	//bottom right - white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.9f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

	//bottom left - white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glEnd();

	//2nd top right - green blue
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.9f, 1.0f, 0.0f);

	glEnd();

	//2nd top left - white
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.9f, 1.0f, 0.0f);

	glEnd();

	//2nd bottom left - white
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);

	glEnd();

	//2nd bottom right - blue green
	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, -1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.9f, -1.0f, 0.0f);

	glEnd();

	//3rd top right - yellow green
	glBegin(GL_QUADS);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.7f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 1.0f, 0.0f);

	glEnd();

	//3rd top left - yellow green
	glBegin(GL_QUADS);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(-0.7f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);

	glEnd();

	//3rd bottom left - yellow green
	glBegin(GL_QUADS);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(-0.7f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);

	glEnd();


	//3rd bottom right - yellow green
	glBegin(GL_QUADS);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.9f, 1.5f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, -1.0f, 0.0f);

	glEnd();

	//4th top right - light blue
	glBegin(GL_QUADS);

	glColor3f(01.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.9f, 1.0f, 1.0f);
	glVertex3f(0.6f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 1.0f, 0.0f);

	glEnd();

	//4th top left - light blue
	glBegin(GL_QUADS);

	glColor3f(01.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(0.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.9f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 1.0f, 0.0f);

	glEnd();

	//4th bottom left - light blue
	glBegin(GL_QUADS);

	glColor3f(01.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.9f, 1.0f, 1.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, -1.0f, 0.0f);

	glEnd();

	//4th bottom right - light blue
	glBegin(GL_QUADS);

	glColor3f(01.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glColor3f(0.0f, 0.8f, 1.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.9f, 1.0f, 1.0f);
	glVertex3f(0.6f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);

	glEnd();

	//5th top right - light blue
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.8f, 0.2f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.2f);
	glVertex3f(0.6f, 1.0f, 0.0f);

	glEnd();

	//5th top left - light blue
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.8f, 0.2f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(-0.5f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.2f);
	glVertex3f(-0.6f, 1.0f, 0.0f);

	glEnd();

	//5th bottom left - light blue
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.8f, 0.2f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(-0.5f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.2f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	glEnd();

	//5th bottom right - light blue
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.8f, 0.2f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 0.2f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.2f);
	glVertex3f(0.6f, -1.0f, 0.0f);

	glEnd();

	//6th top right - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.2f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 1.2f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.8f, 1.2f);
	glVertex3f(0.4f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.2f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glEnd();

	//6th top left - pinkish blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.2f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 1.2f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.8f, 1.2f);
	glVertex3f(-0.4f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.2f);
	glVertex3f(-0.5f, 1.0f, 0.0f);

	glEnd();

	//6th bottom left - pinkish blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.2f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 1.2f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.8f, 1.2f);
	glVertex3f(-0.4f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.2f);
	glVertex3f(-0.5f, -1.0f, 0.0f);

	glEnd();

	//6th bottom right - pinkish blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.2f);
	glVertex3f(0.0f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.8f, 1.2f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 1.8f, 1.2f);
	glVertex3f(0.4f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.2f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glEnd();

	//7th top right - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);

	glEnd();

	//7th top left - red white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);

	glEnd();

	//7th bottom left - red white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);

	glEnd();

	//7th bottom left - red white
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);

	glEnd();

	//8th top right - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 1.0f, 0.0f);

	glEnd();

	//8th top left - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glEnd();

	//8th bottom left - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);

	glEnd();

	//8th bottom right - light blue
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(0.8f, 0.0f, 0.6f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);

	glEnd();


	//9th top right - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glEnd();

	//9th top left - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(-0.1f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glEnd();

	//9th bottom left - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(-0.1f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glEnd();

	//9th bottom right - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.8f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 0.5f, 0.0f);
	glVertex3f(0.1f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glEnd();


	//10th top right - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	//10th top left - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 1.0f, 0.0f);

	glEnd();

	//10th bottom right - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, -1.0f, 0.0f);

	glEnd();

	//10th top right - yellow blue
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, -1.0f, 0.0f);

	glEnd();

	//1st right top
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(1.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	//1st right bottom
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(1.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glEnd();

	//1st left top
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

	//1st right bottom
	glBegin(GL_QUADS);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -0.9f, 0.0f);

	glColor3f(1.8f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

	//2nd right top
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(1.0f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(1.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();


	//2nd right bottom
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(1.0f, -0.8f, 0.0f);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(1.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();


	//2nd left top
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(-1.0f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(-1.0f, 0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	//2nd left bottom
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(-1.0f, -0.8f, 0.0f);

	glColor3f(1.5f, 0.0f, 0.5f);
	glVertex3f(-1.0f, -0.9f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glEnd();

	//3rd right top
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(1.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();


	//3rd right bottom
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(1.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();


	//3rd left top
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(-1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(-1.0f, 0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();


	//3rd right bottom
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(-1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 0.0f, 1.5f);
	glVertex3f(-1.0f, -0.8f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glEnd();


	//4th right top
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, 0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	//4th right bottom
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, -0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	//4th left top
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	//4th right bottom
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glEnd();

	//5th outer triangle

	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, 0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.7f, 0.0f, 0.0f);

	glEnd();

	//5th outer triangle right bottom

	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.0f, -0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.7f, 0.0f, 0.0f);

	glEnd();

	//5th left top outer 
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.7f, 0.0f, 0.0f);

	glEnd();


	//5th eft bottom outer
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -0.7f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.7f, 0.0f, 0.0f);

	glEnd();
}