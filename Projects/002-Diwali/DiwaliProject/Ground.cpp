/*
        Code :- Akshay Bhagwat
 */

 //Headers
#include "Main.h"
#include "Resource.h"

//global variable declarations
static GLuint groundTexture;
static GLuint roadTexture;

extern GLfloat xTrans, yTrans, zTrans;
extern GLfloat xScale, yScale, zScale;
extern GLfloat xRot, yRot, zRot;

void LoadGroundTexture(void)
{
    //code
    LoadGLTexture(&groundTexture, MAKEINTRESOURCE(AB_GROUND_BITMAP));
    LoadGLTexture(&roadTexture, MAKEINTRESOURCE(VJD_ROAD_BITMAP));
 }

void DeleteGroundTexture(void)
{
    //code
    if (groundTexture)
    {
        glDeleteTextures(1, &groundTexture);
        groundTexture = 0;
    }
}

void abRenderGround(void)
{
    //code
    glBindTexture(GL_TEXTURE_2D, groundTexture);

    glPushMatrix();
        glTranslatef(0.0f, -0.86f, 17.45f);
        glScalef(1.0f, 1.0f, 1.55f);
        glColor3f(1.0f, 1.0f, 1.0f);

        glBegin(GL_QUADS);
             
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(40.0f, 0.0f, -40.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-40.0f, 0.0f, -40.0f);
            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-40.0f, 0.0f, 40.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(40.0f, 0.0f, 40.0f);

        glEnd();
    glPopMatrix();
    
    glBindTexture(GL_TEXTURE_2D, roadTexture);
    glColor3f(1.0f, 1.0f, 1.0f);
    
    glPushMatrix();
    
        glTranslatef(0.0f, -0.84f, 34.38f);
        glBegin(GL_QUADS);
             
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(2.8f, 0.0f, -45.2f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-2.8f, 0.0f, -45.2f);
            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-2.8f, 0.0f, 45.2f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(2.8f, 0.0f, 45.2f);

        glEnd();
    glPopMatrix();
}