
#include "Geometry.h"

//
//RenderTriangle()
//
void RenderTriangle(GLfloat fExtent)
{
    //code
    glBegin(GL_TRIANGLES);
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f( 0.0f, fExtent, 0.0f);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fExtent,-fExtent, 0.0f);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fExtent,-fExtent, 0.0f);
    glEnd();
}


//
//RenderSquare()
//
void RenderSquare(GLfloat fLength)
{
    //code
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( fLength,  fLength, 0.0f);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength,  fLength, 0.0f);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength, -fLength, 0.0f);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fLength, -fLength, 0.0f);
    glEnd();
}


//
//RenderCircle()
//
void RenderCircle(GLfloat fRadius)
{
    //variable declaration
    int     i;
    GLfloat fAngle;

    //code
    glBegin(GL_POLYGON);
        for(i = 0; i < CIRCLE_POINT; i++)
        {
            fAngle = (2 * M_PI * i)/(float)CIRCLE_POINT;
            
            glTexCoord2f(
                fRadius * cos(fAngle),
                fRadius * sin(fAngle)
            );
            
            glVertex3f(
                fRadius * cos(fAngle),
                fRadius * sin(fAngle),
                0.0f
            );
        }
    glEnd();
}


//
//RenderPlane()
//
void RenderPlane(GLfloat fLength)
{
    //code
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( fLength, 0.0f, -fLength);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength, 0.0f, -fLength);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength, 0.0f,  fLength);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fLength, 0.0f,  fLength);
    glEnd();
}


//
//RenderCube()
//
void RenderCube(GLfloat fLength)
{
    //code
    glBegin(GL_QUADS);
    
        //Front Face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( fLength/2,  fLength/2, fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength/2,  fLength/2, fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength/2, -fLength/2, fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fLength/2, -fLength/2, fLength/2);
        
        //Right Face
        // glColor3f(0.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(fLength/2,  fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(fLength/2,  fLength/2,  fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(fLength/2, -fLength/2,  fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(fLength/2, -fLength/2, -fLength/2);
        
        //Back Face
        // glColor3f(0.0f, 0.0f, 1.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-fLength/2,  fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f( fLength/2,  fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( fLength/2, -fLength/2, -fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-fLength/2, -fLength/2, -fLength/2);
        
        //Left Face
        // glColor3f(0.0f, 1.0f, 1.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-fLength/2,  fLength/2,  fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength/2,  fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength/2, -fLength/2, -fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-fLength/2, -fLength/2,  fLength/2);
        
        //Top Face
        // glColor3f(1.0f, 0.0f, 1.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( fLength/2, fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength/2, fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength/2, fLength/2,  fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fLength/2, fLength/2,  fLength/2);
        
        //Back Face
        // glColor3f(1.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( fLength/2, -fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-fLength/2, -fLength/2, -fLength/2);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fLength/2, -fLength/2,  fLength/2);
        
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( fLength/2, -fLength/2,  fLength/2);
    
    glEnd();
}

//
//RenderCone()
void RenderCone(GLfloat fBaseRadius, GLfloat fHeight)
{
    //code
}


//
//RenderPyramid()
//
void RenderPyramid(GLfloat fExtent, GLfloat fHeight)
{
    //code
    glBegin(GL_TRIANGLES);
    
        //Front Triangle
        // glColor3f(fExtent, 0.0f, 0.0f);
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, fHeight, 0.0f);
        
        // glColor3f(0.0f, fExtent, 0.0f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fExtent, -fExtent, fExtent);
        
        // glColor3f(0.0f, 0.0f, fExtent);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(fExtent, -fExtent, fExtent);
        
        
        //Right Triangle
        // glColor3f(fExtent, 0.0f, 0.0f);
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, fHeight, 0.0f);
        
        // glColor3f(0.0f, 0.0f, fExtent);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(fExtent, -fExtent, fExtent);
        
        // glColor3f(0.0f, fExtent, 0.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(fExtent, -fExtent, -fExtent);
        
        
        //Back Triangle
        // glColor3f(fExtent, 0.0f, 0.0f);
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, fHeight, 0.0f);
        
        // glColor3f(0.0f, fExtent, 0.0f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(fExtent, -fExtent, -fExtent);
        
        // glColor3f(0.0f, 0.0f, fExtent);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-fExtent, -fExtent, -fExtent);
        
        
        //Left Triangle
        // glColor3f(fExtent, 0.0f, 0.0f);
        glTexCoord2f(0.5f, 1.0f);
        glVertex3f(0.0f, fHeight, 0.0f);
        
        // glColor3f(0.0f, 0.0f, fExtent);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-fExtent, -fExtent, -fExtent);
        
        // glColor3f(0.0f, fExtent, 0.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-fExtent, -fExtent, fExtent);
    
    glEnd();
}

