//Headers
#include "Main.h"
#include "Resource.h"


//variable declaration
typedef struct VertexPosition{
    GLfloat fxInitial;
    GLfloat fyInitial;
    GLfloat fzInitial;
    
    GLfloat rColor;
    GLfloat gColor;
    GLfloat bColor;
    
    GLfloat fxFinal;
    GLfloat fyFinal;
    GLfloat fzFinal;
    
} VERTEX_POSITION;

static VERTEX_POSITION *gpvPosition = NULL;
static int iVertexCount = 0; 

extern FILE *gpFile;
extern HWND  ghwnd;


//
//InitializeDiwaliWishesScene()
//
bool InitializeDiwaliWishesScene(void)
{
    BITMAPFILEHEADER *pbmfh;
    BITMAPINFO       *pbmi;
    BYTE             *pBits;
    int               cxDib, cyDib;
    
    bool bResult = false;

    int r, g, b;
    int rowLength;
    int i, j, index;
    GLfloat angle = 0.0f;
    
    //code    
    pbmfh = DibLoadImage(VJD_DIWALI_WISH);  //Defined Tulsi.cpp
    if(pbmfh == NULL)
    {
        fprintf(gpFile, "DiwaliWishSceneEffect.cpp:- Image Not Load\n");
        DestroyWindow(ghwnd);
        return(false);
    }
    
    //Get pointers to the info structure & the bits
    pbmi  = (BITMAPINFO*) (pbmfh + 1);
    pBits = (BYTE *) pbmfh + pbmfh->bfOffBits;
    
    //Get the DIB width and height
    if(pbmi->bmiHeader.biSize == sizeof(BITMAPCOREHEADER))
    {
        cxDib = ((BITMAPCOREHEADER *)pbmi)->bcWidth;
        cyDib = ((BITMAPCOREHEADER *)pbmi)->bcHeight;
    }
    else
    {
        cxDib =     pbmi->bmiHeader.biWidth;
        cyDib = abs(pbmi->bmiHeader.biHeight);
    }
    
        //Image Must be 24-bit bmp
    if(pbmi->bmiHeader.biBitCount == 24)
    {
        bResult = true;
        //Calculate length of each row (row is multiple of 4)
        rowLength = 4 * ((cxDib * pbmi->bmiHeader.biBitCount + 31) / 32);
        
        
        //Looping to know how much amount of memory required
        for(i = 0; i < cyDib; i++)
        {
            for(j = 0; j < cxDib; j++)//((j+1)%2) ? (j+=2) : (j++))
            {
                r = pBits[i*rowLength + 3*j + 2];
                g = pBits[i*rowLength + 3*j + 1];
                b = pBits[i*rowLength + 3*j ];

                if(r == 0xFF && g == 0xFF && b == 0xFF)
                    continue;
                iVertexCount++;
            }
        }
        // glEnd();
        
        gpvPosition = (VERTEX_POSITION *) calloc(sizeof(VERTEX_POSITION), iVertexCount);
        if(gpvPosition == NULL)
        {
            fprintf(gpFile, "DiwaliWishSceneEffect.cpp:- Not Enough Memory: %d\n", iVertexCount);
            DestroyWindow(ghwnd);
            
            free(pbmfh);
            return(false);
        }
        
        fprintf(gpFile, "DiwaliWishSceneEffect.cpp:- Vertex Count = %d\n", iVertexCount);
        
        
        //Actually get data from image and set values as final position of particle
        index = 0;
        for(i = 0; i < cyDib; i++)
        {
            for(j = 0; j < cxDib; j++)
            {
                r = pBits[i*rowLength + 3*j + 2];
                g = pBits[i*rowLength + 3*j + 1];
                b = pBits[i*rowLength + 3*j ];

                if(r == 0xFF && g == 0xFF && b == 0xFF)
                    continue;

                (gpvPosition + index)->rColor = (GLfloat)r/255.0f;
                (gpvPosition + index)->gColor = (GLfloat)g/255.0f;
                (gpvPosition + index)->bColor = (GLfloat)b/255.0f;
                
                (gpvPosition + index)->fxFinal = ((GLfloat)j/(GLfloat)cxDib);
                (gpvPosition + index)->fyFinal = ((GLfloat)i/(GLfloat)cyDib);
                
                index++;
            }
        }
        
        
        //Calculate initial position of particles
        angle = 0.0f;
        for(index = 0; index < iVertexCount; index++)
        {
            // // /***///AMAZING ONE
            (gpvPosition + index)->fxInitial = 4 * tan(angle) + ((gpvPosition + index)->fxFinal * tan(angle)) * ((gpvPosition + index)->fzFinal * tan(angle));
            (gpvPosition + index)->fyInitial = 4 * tan(angle) + ((gpvPosition + index)->fyFinal * tan(angle)) * ((gpvPosition + index)->fxFinal * tan(angle));
            (gpvPosition + index)->fzInitial = 4 * tan(angle) + ((gpvPosition + index)->fzFinal * tan(angle)) * ((gpvPosition + index)->fyFinal * tan(angle));

            angle = angle + 0.01f;
            if(angle > 3.1415f)
                angle = 0.0f;
        }
    }
    else
    {
        bResult = false;
    }
    
    free(pbmfh);
    
    return(bResult);
}

//
//vjdRenderDiwaliParticlesScene(void)()
//
//      false: if particles are not at their final position
//      true:  All Particles are at their final position
//
bool vjdRenderDiwaliParticlesScene(void)
{    
    //variable declarations
    static GLfloat fPercent = 0.0f;
    bool   bDone = false;
    int  i;
    
    //code
    
    if(gpvPosition == NULL)
        return(true);
    
    glTranslatef(-1.00f, -0.63f, 0.0f);
    // glScalef(2.17f, 1.93f, 0.0f);
    glScalef(12.0f, 12.0f, 0.0f);
    glRotatef(320.0f, 1.0f, 0.0f, 0.0f);
    
    glBegin(GL_POINTS);
        for(i = 0; i < iVertexCount; i++)
        {
            glColor3f(
                (gpvPosition+i)->rColor,
                (gpvPosition+i)->gColor,
                (gpvPosition+i)->bColor
            );
            glVertex3f(
                (gpvPosition+i)->fxInitial,
                (gpvPosition+i)->fyInitial,
                (gpvPosition+i)->fzInitial
            );
        }
    glEnd();

    if(fPercent <= 0.027f)
    {
        for(i = 0; i < iVertexCount; i++)
        {
            (gpvPosition+i)->fxInitial = (gpvPosition+i)->fxInitial + fPercent * ((gpvPosition+i)->fxFinal - (gpvPosition+i)->fxInitial);
            (gpvPosition+i)->fyInitial = (gpvPosition+i)->fyInitial + fPercent * ((gpvPosition+i)->fyFinal - (gpvPosition+i)->fyInitial);
            (gpvPosition+i)->fzInitial = (gpvPosition+i)->fzInitial + fPercent * ((gpvPosition+i)->fzFinal - (gpvPosition+i)->fzInitial);
        }
        fPercent += 0.00001f;
        
        fprintf(gpFile, "Perc = %f\n", fPercent);
    }
    else
    {
        bDone = true;
    }

    return(bDone);
}

//
//UninitializeDiwaliWishesResources()
//
void UninitializeDiwaliWishesResources(void)
{
    //code
    if(gpvPosition)
    {
        free(gpvPosition);
        gpvPosition = NULL;
    }
}

