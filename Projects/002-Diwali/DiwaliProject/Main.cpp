//Headers
#include "Main.h"
#include "Geometry.h"
#include "Font.h"			 
#include "Resource.h"

//library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")
#pragma comment(lib, "Winmm")

//macro
#define WIN_WIDTH  800
#define WIN_HEIGHT 600

#define TRANSITION_FADE_OUT 0
#define TRANSITION_FADE_IN  1
#define TRANSITION_NONE     2

    //Current Scenary
#define INITIAl_SCENE 100																																		  
#define DIWALI_SCENE  101
#define DIWALI_WISHES 102
#define CREDIT_SCENE  103
#define FINAL_CREDIT  104

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD           dwStyle;
bool            gbFullScreen = false;

bool            gbActiveWindow = false;

HWND  ghwnd;
HDC   ghdc;
HGLRC ghrc;

FILE *gpFile = NULL;


    //camera position value
GLfloat g_xRot = 330, g_yRot, g_zRot;
GLfloat g_xTrans = -0.05f, g_yTrans = 6.80f, g_zTrans = -52.10f;
// GLfloat g_xTrans = 0.0f, g_yTrans = 0.0f, g_zTrans  = 0.0f;

GLfloat xRot, yRot, zRot;
GLfloat xScale = 1.0f, yScale = 1.0f, zScale = 1.0f;
GLfloat xTrans, yTrans, zTrans;


        //Calculate FPS
int frame, time, timebase = 0;
LONGLONG currTime, lastTime, timeEpoch;
LONGLONG initTime;

int cxClient, cyClient; //current window size

char szFPSString[20], strPosition[20];  //to display FPS and Camera position on screen
// GLfloat lightPos[]  = { 0.0f, 10.0f,  50.0f, 1.0f };
// GLfloat specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
// GLfloat specref[] = {0.1f, 0.1f, 0.1f, 1.0f };
// GLfloat ambientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };

    //variable for data list (initialize())
GLuint houseDataList,
       treeRowDataList,
       templeDataList,
       cloudDataList,
       humanoidDataList;
       
static GLfloat fAlpha = 0.0f;   //for fading
GLfloat fTransitionStep = 0.01f;
int iTransition = TRANSITION_FADE_OUT;

//Camera Variable
GLFrame pCamera = {
    0.0f, 0.0f,  50.8f,  //vLocation :- Location Vector
    0.0f, 1.0f,   0.0f,  //vUp       :- Up Vector
    0.0f, 0.0f,  -1.0f   //vForward  :- Forward Vector
};

GLfloat fMovementStep = 0.1f;
GLfloat fAngleStep = 0.1f;

GLuint shriRamTexture;

GLfloat fTranslateStep = 0.05f; //movement value increment

bool bShowDiyaFrontOfShriRam = false;   //show Diya in front of ShriRam

int moveBack = 0;   //counter for 1st scene move back-word animation from temple

bool bShowFireWorksBurst = false;   //hide rocket and show fire-work burst
bool bShowFireWorks = false;    //show rocket

GLfloat fRocketYMovement = 0.0f;    //for rocket move upward

int currentAnimation = -1;   //calculation for current running animation

int iScene = INITIAl_SCENE;  //for current scenary displaying

//
//WinMain()
//
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
    //function declarations
    void ToggleFullScreen(void);
    void Initialize(void);
    void Display(void);
    
    //variable declarations
    TCHAR      szAppName[] = TEXT("OpenGL");
    WNDCLASSEX wndclass;
    HWND       hwnd;
    MSG        msg;
    bool       bDone = false;
    
    //code
    if(fopen_s(&gpFile, "pixelLog.log", "w") != 0)
    {
        MessageBox(NULL, TEXT("Cannot create \"vjd.log\".\nExiting Now..."), szAppName, MB_ICONERROR);
        exit(0);
    }
    else
    {
        fprintf(gpFile, "Log file created and program started.\n");
    }
    
    wndclass.cbSize         = sizeof(WNDCLASSEX);
    wndclass.style          = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.lpfnWndProc    = WndProc;
    wndclass.lpszClassName  = szAppName;
    wndclass.lpszMenuName   = NULL;
    wndclass.cbClsExtra     = 0;
    wndclass.cbWndExtra     = 0;
    wndclass.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYICON));
    wndclass.hIconSm        = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYICON));
    wndclass.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wndclass.hInstance      = hInstance;
    wndclass.hbrBackground  = (HBRUSH) GetStockObject(BLACK_BRUSH);
    
    if(!RegisterClassEx(&wndclass))
    {
        MessageBox(NULL, TEXT("Error while registering class."), szAppName, MB_ICONERROR);
        exit(0);
    }
    
    
    hwnd = CreateWindowEx(
                WS_EX_APPWINDOW,
                szAppName,
                TEXT("PIXEL GROUP"),
                WS_OVERLAPPEDWINDOW,
                100, 100, WIN_WIDTH, WIN_HEIGHT,
                NULL,
                NULL,
                hInstance,
                NULL
            );
            
    ghwnd = hwnd;
    
    
    ShowWindow(ghwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    
    SetFocus(hwnd);
    Initialize();

    ToggleFullScreen();
    
    //Game Loop
    while(bDone == false)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if(msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if(gbActiveWindow == true)
            {
                Display();
            }
        }
    }
    
    return((int)msg.wParam);
}//End WinMain()


//
//WndProc()
//
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    //function declarations
    void Resize(int, int);
    void ToggleFullScreen(void);
    void Uninitialize(void);
    
    //variable declarations
    char str[255];
    static int operation = 0; 
    
    //code
    switch(message)
    {
        case WM_SETFOCUS:
            gbActiveWindow = true;
            break;
            
        case WM_KILLFOCUS:
            gbActiveWindow = false;
            break;
            
        case WM_ERASEBKGND:
            return(0);
           
        case WM_SIZE:
            cxClient = LOWORD(lParam);
            cyClient = HIWORD(lParam);
            Resize(cxClient, cyClient);	   
            break;
        
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(ghwnd);
                    break;
                
                case 0x46:  //f
                    ToggleFullScreen();
                    break;
                    
                //case VK_UP:
                //    g_xRot = g_xRot + 3.0f;
                //    if(g_xRot >= 360.0f)
                //        g_xRot = 0.0f;
                //    CameraRotateAlongX(&pCamera, -fAngleStep);
                //    break;
                //
                //case VK_DOWN:
                //    g_xRot = g_xRot - 3.0f;
                //    if(g_xRot <= 0.0f)
                //        g_xRot = 360.0f;
                //    CameraRotateAlongX(&pCamera, fAngleStep);
                //    break;
                //
                //case VK_RIGHT:
                //    g_yRot = g_yRot + 3.0f;
                //    if(g_yRot >= 360.0f)
                //        g_yRot = 0.0f;
                //    CameraRotateAlongY(&pCamera, -fAngleStep);
                //    break;
                //    
                //case VK_LEFT:
                //    g_yRot = g_yRot - 3.0f;
                //    if(g_yRot <= 0.0f)
                //        g_yRot = 360.0f;
                //    CameraRotateAlongY(&pCamera,  fAngleStep);
                //    break;

                //case VK_HOME:
                //    g_zRot = g_zRot + 3.0f;
                //    if(g_zRot >= 360.0f)
                //        g_zRot = 0.0f;
                //    CameraRotateAlongZ(&pCamera, -fAngleStep);
                //    break;

                //case VK_END:
                //    g_zRot = g_zRot - 3.0f;
                //    if(g_zRot <= 0.0f)
                //        g_zRot = 360.0f;
                //    CameraRotateAlongZ(&pCamera,  fAngleStep);
                //    break;

                //case 0x57:  //W/w key
                //case VK_NUMPAD8:
                //    CameraMoveForward(&pCamera, fMovementStep);
                //    g_zTrans = g_zTrans + 0.1f;
                //    break;

                //// case 0x53:  //S/s key
                //case VK_NUMPAD2:
                //    CameraMoveForward(&pCamera, -fMovementStep);
                //    g_zTrans = g_zTrans - 0.1f;
                //    break;

                //case 0x41:  //A/a key
                //case VK_NUMPAD4:
                //    CameraMoveRight(&pCamera, fMovementStep);
                //    g_xTrans = g_xTrans + 0.1f;
                //    break;
                //
                //case 0x44:  //D/d key
                //case VK_NUMPAD6:
                //    CameraMoveRight(&pCamera, -fMovementStep);
                //    g_xTrans = g_xTrans - 0.1f;
                //    break;
                //
                //case VK_PRIOR:  //page-up
                //    CameraMoveUp(&pCamera, fMovementStep);
                //    g_yTrans = g_yTrans - 0.1f;
                //    break;
                //
                //case VK_NEXT:   //page-down
                //    CameraMoveUp(&pCamera, -fMovementStep);
                //    g_yTrans = g_yTrans + 0.1f;
                //    break;
                //    
                default:
                    break;
            }
            //sprintf(
            //    str,
            //    "Tx=%f, Ty=%f, Tz=%f, Sx = %f, Sy = %f, Sz = %f, Rx=%f, Ry=%f, Rz=%f",
            //    xTrans, yTrans, zTrans,
            //    xScale, yScale, zScale,
            //    xRot, yRot, zRot
         //   );
//            SetWindowTextA(hwnd, str);
            break;
         
    //    case WM_CHAR:
    //        switch(wParam)
    //        {
    //            case '+':
    //                fAlpha += 0.005f;
    //            break;
    //            
    //            case '-':
    //                fAlpha -= 0.005f;
    //            break;
    //            
    //            case 'm':
    //                CameraMoveForward(&pCamera, 30.0f);
    //            
    //            case 't':
    //            case 'T':
    //                operation = 0;
    //            break;
    //            
    //            case 's':
    //            case 'S':
    //                operation = 1;
    //            break;
    //            
    //            case 'r':
    //            case 'R':
    //                operation = 2;
    //            break;

    //            case 'y':
    //                if(operation == 0)
    //                    yTrans = yTrans - 0.01f;
    //                if(operation == 1)
    //                    yScale = yScale - 0.01f;
    //                    
    //                if(operation == 2)
    //                {
    //                    yRot = yRot + 1.0f;
    //                    if(yRot >= 360.0f)
    //                        yRot = 0.0f;
    //                }
    //            break;
    //            
    //            case 'Y':
    //                if(operation == 0)
    //                    yTrans = yTrans + 0.01f;
    //                if(operation == 1)
    //                    yScale = yScale + 0.01f;
    //                if(operation == 2)
    //                {
    //                    yRot = yRot - 1.0f;
    //                    if(yRot <= 0.0f)
    //                        yRot = 360.0f;
    //                }
    //            break;
    //            
    //            case 'x':
    //                if(operation == 0)
    //                    xTrans = xTrans - 0.01f;
    //                if(operation == 1)
    //                    xScale = xScale - 0.01f;
    //                if(operation == 2)
    //                {
    //                    xRot = xRot + 1.0f;
    //                    if(xRot >= 360.0f)
    //                        xRot = 0.0f;
    //                }
    //            break;
    //            
    //            case 'X':
    //                if(operation == 0)
    //                    xTrans = xTrans + 0.01f;
    //                if(operation == 1)
    //                    xScale = xScale + 0.01f;
    //                if(operation == 2)
    //                {
    //                    xRot = xRot - 1.0f;
    //                    if(xRot <= 0.0f)
    //                        xRot = 360.0f;
    //                }
    //            break;
    //            
    //            case 'z':
    //                if(operation == 0)
    //                    zTrans = zTrans - 0.01f;
    //                if(operation == 1)
    //                    zScale = zScale - 0.01f;
    //                if(operation == 2)
    //                {
    //                    zRot = zRot + 1.0f;
    //                    if(zRot >= 360.0f)
    //                        zRot = 0.0f;
    //                }
    //            break;
    //            
    //            case 'Z':
    //                if(operation == 0)
    //                    zTrans = zTrans + 0.01f;
    //                if(operation == 1)
    //                    zScale = zScale + 0.01f;
    //                if(operation == 2)
    //                {
    //                    zRot = zRot - 1.0f;
    //                    if(zRot <= 0.0f)
    //                        zRot = 360.0f;
    //                }
    //            break;
    //            
    //            // case 'i':
    //                // bShowFireWorksBurst = true;
    //            // break;
    //        }
    //        sprintf(
    //            str,
    //            "Tx=%f, Ty=%f, Tz=%f, Sx = %f, Sy = %f, Sz = %f, Rx=%f, Ry=%f, Rz=%f",
				//xTrans, yTrans, zTrans,
    //            xScale, yScale, zScale,
    //            xRot, yRot, zRot
    //        );
    //        SetWindowTextA(hwnd, str);
    //    break;
    //        
        case WM_CLOSE:
            DestroyWindow(ghwnd);
            break;
            
        case WM_DESTROY:
            Uninitialize();
            PostQuitMessage(0);
            break;
    }
    
    return(DefWindowProc(hwnd, message, wParam, lParam)); 
}//End WndProc()


//
//Initialize()
//
void Initialize(void)
{
    //function declarations
    void Resize(int, int);
    bool LoadGLTexture(GLuint*, TCHAR[]);
    
    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;
    int i;
    
    //code
    ghdc = GetDC(ghwnd);
    
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    
    pfd.nSize       = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion    = 1;
    pfd.dwFlags     = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType  = PFD_TYPE_RGBA;
    pfd.cColorBits  = 32;
    pfd.cRedBits    = 8;
    pfd.cGreenBits  = 8;
    pfd.cBlueBits   = 8;
    pfd.cAlphaBits  = 8;
    pfd.cDepthBits  = 32;
    
    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if(iPixelFormatIndex == 0)
    {
        fprintf(gpFile, "ChoosePixelFormat() Failed!!!\n");
        DestroyWindow(ghwnd);
    }
    
    if(SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        fprintf(gpFile, "SetPixelFormat() Failed!!!\n");
        DestroyWindow(ghwnd);
    }
    
    ghrc = wglCreateContext(ghdc);
    if(ghrc == NULL)
    {
        fprintf(gpFile, "wglCreateContext() Failed!!!\n");
        DestroyWindow(ghwnd);
    }
    
    if(wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        fprintf(gpFile, "wglMakeCurrent() Failed!!!\n");
        DestroyWindow(ghwnd);
    }
    
    
    //set background color
    glClearColor(0.196078f, 0.6f, 0.8f, 1.0f);
    
    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    //Enable lighting
    // glEnable(GL_LIGHTING);
    // glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
    
    // glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    // glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientLight);
    // glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    // glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    
    // glEnable(GL_LIGHT0);
    
    //Load Texture
    LoadTempleTexture();    //Temple.cpp
    LoadHouseTexture();     //House.cpp
    LoadGroundTexture();    //Ground.cpp
    LoadKandilTexture();    //Kandil.cpp
    LoadSunAndMoonTexture();    //SunAndMoon.cpp
    LoadTreeTexture();  //Tree.cpp
    LoadTulasTextureAndData();  //Tulsi.cpp
    LoadHumanTexture();    //Humanoid.cpp
    LoadFireCrackerAndBottleTexture(); //FireCracker.cpp
    
    InitializeDiwaliWishesScene();  //DiwaliWishSceneEffect.cpp
    
    vjdInitializeFinalScene();  //FinalCreditEffect.cpp
    
    LoadGLTexture(&shriRamTexture, MAKEINTRESOURCE(SHRI_RAM_BITMAP));

    glEnable(GL_TEXTURE_2D);
        
    // glEnable(GL_COLOR_MATERIAL);
    // glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    // glMaterialfv(GL_FRONT, GL_SPECULAR, specref);
    // glMateriali(GL_FRONT, GL_SHININESS, 128);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    
    //Initliaize Data List
        /* House Data List */
    {
        houseDataList = glGenLists(1);
        glNewList(houseDataList, GL_COMPILE);
            // glRotatef(-50.0f, 0.0f, 1.0f, 0.0f);
            glRotatef(355.0f, 1.0f, 0.0f, 0.0f);
            vmRenderHouse();

            // glTranslatef(2.95f, -0.31f, 0.0f);
            glTranslatef(-2.95, -0.31f, 1.24f);         
            glScalef(1.0f, 0.85f, 1.0f);
            nlRenderTulas();
            
            /*DIYA*/     
            glTranslatef(2.55f, 0.02f, -0.17f);
            glScalef(0.07f, 0.11f, 0.07f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(11.40f, 0.0f, 0.0f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(2.45f, 0.0f, 4.59f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(17.30f, 0.0f, 0.0f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(-33.72f, 0.0f, 0.0f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(-17.13f, 0.0f, 0.0f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(16.55f, -2.49f, 8.99f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();

            glTranslatef(18.03f, 0.0f, 0.0f);
            glPushMatrix();
                dvDiya();
            glPopMatrix();
        glEndList();
    }
    
        /* Tree Row Data List */
    {
        treeRowDataList = glGenLists(1);
        glNewList(treeRowDataList, GL_COMPILE);
            glPushMatrix();
                glTranslatef(-37.5f, -0.9f, 77.5f);
                
                glPushMatrix();
                    for(i = 0; i < 25; i++)
                    {
                        if(i%3 == 2)
                            Tree2();
                        else
                            Tree();
                        
                        glTranslatef(0.0f, 0.0f, -4.95f);
                    }
                glPopMatrix();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(37.5f, -0.9f, 77.5f);
                glPushMatrix();
                    for(i = 0; i < 25; i++)
                    {
                        if(i%3 == 2)
                            Tree2();
                        else
                            Tree();
                        
                        glTranslatef(0.0f, 0.0f, -4.95f);
                    }
                glPopMatrix();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-31.5f, -0.9f, -41.0f);
                glPushMatrix();
                    for(i = 0; i < 14; i++)
                    {
                        if(i%3 == 2)
                        {
                            glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
                            Tree2();
                            glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                        }
                        else
                            Tree();
                        
                        glTranslatef(4.95f, 0.0f, 0.0f);
                    }
                glPopMatrix();
            glPopMatrix();
        glEndList();
    }
    
        /* Temple Data List */
    {
        templeDataList = glGenLists(1);
        glNewList(templeDataList, GL_COMPILE);
            glPushMatrix();
                glTranslatef(0.0f, 0.0f, -30.0f);
            
                glColor3f(1.0f, 1.0f, 1.0f);
                vjdRenderBigTemple();
                
                glTranslatef(0.0f, 2.44f, 0.15f);
                glScalef(21.44f, 12.60f, 7.05f);
                abTemple();
            glPopMatrix();
            
            // temple diya
            glPushMatrix();
                glTranslatef(3.0f, 1.92f, -20.0f);
                glScalef(0.5f, 0.5f, 0.5f);       
                dvDiya();
            glPopMatrix();
            
            glPushMatrix();
                glTranslatef(-3.0f, 1.92f, -20.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                dvDiya();
            glPopMatrix();
            
        glEndList();
    }

        /* Humanoid Data List */
    {
        humanoidDataList = glGenLists(1);
        
        glNewList(humanoidDataList, GL_COMPILE);
        
            // first layer human and child
            glPushMatrix();
                glTranslatef(-1.35f, -0.4f, 28.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-1.0f, -0.56f, 28.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.35f, -0.4f, 30.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.0f, -0.56f, 30.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            // second layer human and child
            glPushMatrix();
                glTranslatef(-1.35f, -0.4f, 18.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-1.0f, -0.56f, 18.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.35f, -0.4f, 20.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.0f, -0.56f, 20.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            // third layer human and child
            glPushMatrix();
                glTranslatef(-1.35f, -0.4f, 8.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-1.0f, -0.56f, 8.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.35f, -0.4f, 10.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.0f, -0.56f, 10.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            // fourth layer human and child
            glPushMatrix();
                glTranslatef(-1.35f, -0.4f, -2.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-1.0f, -0.56f, -2.0f);
                glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.35f, -0.4f, 0.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.15f, 0.15f, 0.15f);
                vmAdultHuman();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(1.0f, -0.56f, 0.0f);
                glRotatef(-40.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.1f, 0.1f, 0.1f);
                vmChildHuman();
            glPopMatrix();
            
        glEndList();
    }
    
    //Resize() warm-up call
    Resize(WIN_WIDTH, WIN_HEIGHT);
}//End Initialize();


//
//LoadGLTexture() :- Image Loading
//
bool LoadGLTexture(GLuint *texture, TCHAR resourceID[])
{
    //variable declarations
    bool    bResult = false;
    HBITMAP hBitmap = NULL;     //OS image loading
    BITMAP  bmp;                //OS image loading
    
    //code
    hBitmap = (HBITMAP) LoadImage(  //OS image loading
                            GetModuleHandle(NULL),
                            resourceID,
                            IMAGE_BITMAP,
                            0,  //Width for Icon image
                            0,  //Height for Icon image
                            LR_CREATEDIBSECTION      //in which format load image (we say load in the os-native format)
                                //LR => Load as Resource
                                //CREATEDIBSECTION => create DIB section
                        );

    if(hBitmap)
    {
        bResult = true;
        
        GetObject(hBitmap, sizeof(BITMAP), &bmp);   //OS image loading
        
        //From here start OpenGL's Texture code
        glPixelStorei(
                GL_UNPACK_ALIGNMENT, //How you want image in memory
                4   //  RGBA  (every row alignment)  (other options 1, 2, 4, 8)
        );
        
            //tell graphics driver to give me texture object/buffer
        glGenTextures(  //can generate multiple texture object.
            1,          //how many texture object want
            texture    //representative of texture memory location in GPU memory
        );
        
        glBindTexture(  //can bind one texture at a time
            GL_TEXTURE_2D,  //bind object to memory //target
            *texture       //representative of texture memory location in GPU memory
        );
        
            //setting texture parameters
        glTexParameteri(
            GL_TEXTURE_2D,  //where texture data goes there's target name
            GL_TEXTURE_MAG_FILTER,  //Magnification filter
            GL_LINEAR       //I want quality so weighted average (object is closest to camera)
        );
        
        glTexParameteri(
            GL_TEXTURE_2D,  //Target
            GL_TEXTURE_MIN_FILTER,  //Minification filter
            GL_LINEAR_MIPMAP_LINEAR      //Object is far from camera so use MipMaping (mimaping should not be cheap that should be linear)
        );
        
            //Push Data to the graphics memory with help of graphics driver
        gluBuild2DMipmaps(          //Internally call's => glTexImage2D() + glGenerateMipmap()
            GL_TEXTURE_2D,  //target
            3,              //(Internal Format) I want RGB (42 different option can give)
            bmp.bmWidth,    //Image Width
            bmp.bmHeight,   //Image Height
            GL_BGR_EXT,     //(Actual Image Format) OpenGL Identified this is Windows Bitmap format
            GL_UNSIGNED_BYTE,   //Type of data which has been pushed in texture memory
            bmp.bmBits        //Actual Image Data
        );
        
            //Delete Bitmap object. //Data goes to GPU memory.
        DeleteObject(hBitmap);  //OS function

    }

    return(bResult);
    
}//End LoadGLTexture()


//
//ToggleFullScreen()
//
void ToggleFullScreen(void)
{
    //variable declarations
    MONITORINFO mi = { sizeof(MONITORINFO) };
    
    //code
    if(gbFullScreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if(GetWindowPlacement(ghwnd, &wpPrev) &&
                GetMonitorInfo(
                    MonitorFromWindow( ghwnd, MONITORINFOF_PRIMARY),
                    &mi
                )
            )
            {
                SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
                
                SetWindowPos(
                    ghwnd,
                    HWND_TOP,
                    mi.rcMonitor.left,
                    mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED
                );
                
            }
        }
        ShowCursor(FALSE);
        gbFullScreen = true;
    }
    else
    {
        SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
        
        SetWindowPlacement(ghwnd, &wpPrev);
        
        SetWindowPos(
            ghwnd,
            HWND_TOP,
            0, 0, 0, 0,
            SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED
        );
        
        ShowCursor(TRUE);
        gbFullScreen = false;
    }
    
}


//
//Resize()
//
void Resize(int iWidth, int iHeight)
{
    //code
    if(iHeight == 0)
        iHeight = 1;
    
    glViewport(0, 0, (GLfloat)iWidth, (GLfloat)iHeight);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    gluPerspective(
        45, //fov - y
        (GLfloat)iWidth/(GLfloat)iHeight, //aspectRatio
        1.0f,   //near
        250.0f  //far
    );
}


//
//Display()
//
void Display(void)
{
    //function declarations
    void UpdateAnimation(void);
    
	//function declarations
    void ShowFPS(void);			

    //variable declarations
    static GLfloat angle = 0.0f;
    static GLfloat sun_x = -70.0f, sun_y = 0.0f;
    static GLfloat moon_x = -70.0f, moon_y = 0.0f;
    //static GLfloat dark_x = -70.0f, dark_y = 0.0f;
    static GLfloat sky_x = 0.196078f, sky_y = 0.6f, sky_z = 0.8f, sky_a = 1.0f;

    int i, j;
    
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear color and depth buffer
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // glTranslatef(0.15f, 0.0f, -30.8);
    glTranslatef(g_xTrans, g_yTrans, g_zTrans);
    glRotatef(g_zRot, 0.0f, 0.0f, 1.0f);
    glRotatef(g_yRot, 0.0f, 1.0f, 0.0f);
    glRotatef(g_xRot, 1.0f, 0.0f, 0.0f);
    // ApplyCameraTransform(&pCamera);

    
    // glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    // glDisable(GL_LIGHTING);


    if(iScene == INITIAl_SCENE)
    {
        // glMatrixMode(GL_PROJECTION);
        glPushMatrix();
            glTranslatef(-20.0f, 8.00f, 0.0f);
            glScalef(0.45f, 1.0f, 1.0f);
            ankitPrintString("PIXEL");
            
            glColor3f(0.0f, 1.0f, 0.0f);
            glTranslatef(51.0f, 0.0f, 0.0f);
            ankitPrintString("GROUP");
            
            glTranslatef(-38.84f, -7.9f, 0.0f);
            glTranslatef(17.45f, -0.44f, 0.0f);
            glScalef(0.5f, 0.37f, 1.0f);
            ankitPrintString("PRESENTS");
            
            glColorMask(GL_FALSE, GL_TRUE, GL_TRUE, GL_TRUE);
            glTranslatef(-58.00f, -19.38, 0.0f);
            glScalef(3.39f, 4.25f, 1.0f);
            ankitPrintString("DIWALI");
            
            glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
            
            if(fAlpha < 1.0f)
            {
                fAlpha = fAlpha + 0.001f;
            }
            else
            {
                //Camera Setting for next scene
                g_xTrans = -0.05f;
                g_yTrans =  0.0f;
                g_zTrans = -50.8f;
                
                g_xRot = 0.0f;
                g_yRot = 0.0f;
                g_zRot = 0.0f;
                
                iScene = DIWALI_SCENE;
                currentAnimation++;

                // play sound
                PlaySound(TEXT("Song\\Innisai Paadivarum - Sruthi Balamurali - Happy Diwali 2020 - Thalapathy Vijay.wav"), NULL, SND_ASYNC);

            }
            

        glPopMatrix();
    }
    else if(iScene == DIWALI_SCENE)
    {
        // ground
        glPushMatrix();
            abRenderGround();
        glPopMatrix();

        // temple
        glCallList(templeDataList);
        
        glPushMatrix();
            glTranslatef(0.0f, 2.35f, -32.65f);
            glScalef(3.05f, 2.30f, 1.00f);
            
            glBindTexture(GL_TEXTURE_2D, shriRamTexture);
            glBegin(GL_QUADS);
                glColor3f(1.0f, 1.0f, 1.0f);
                glTexCoord2f(1.0f, 1.0f);
                glVertex3f( 1.0f,  1.0f, 0.0f);
                
                glTexCoord2f(0.0f, 1.0f);
                glVertex3f(-1.0f,  1.0f, 0.0f);
                
                glTexCoord2f(0.0f, 0.0f);
                glVertex3f(-1.0f, -1.0f, 0.0f);
                
                glTexCoord2f(1.0f, 0.0f);
                glVertex3f( 1.0f, -1.0f, 0.0f);
            glEnd();
            glBindTexture(GL_TEXTURE_2D, 0);
            
            if(bShowDiyaFrontOfShriRam)
            {
                glPushMatrix();
                    glScalef(0.06f, 0.14f, 0.46f);
                    // glTranslatef(xTrans, yTrans, zTrans);
                    // glRotatef(xRot, 1.0f, 0.0f, 0.0f);
                    // glRotatef(yRot, 0.0f, 1.0f, 0.0f);
                    // glRotatef(zRot, 0.0f, 0.0f, 0.0f);
                    glPushMatrix();
                        glTranslatef(-14.00f, -6.57f, 1.14f);
                        dvDiya();
                    glPopMatrix();
                    
                    glPushMatrix();
                        glTranslatef(0.00f, -6.57f, 1.14f);
                        dvDiya();
                    glPopMatrix();
                    
                    glPushMatrix();
                        glTranslatef(14.00f, -6.57f, 1.14f);
                        dvDiya();
                    glPopMatrix();
                    
                glPopMatrix();
                
                moveBack++;
                if(moveBack == 80)
                {
                    currentAnimation++;
                }
            }
        glPopMatrix();

        // right side homes line
        for(i = 0; i < 4; i++)  /*NEW*/
        {
            glPushMatrix();
                glTranslatef(4.0f, 0.4f, 10.0f * i);
                glRotatef(-50.0f, 0.0f, 1.0f, 0.0f);
                
                glCallList(houseDataList);
            glPopMatrix();
        }

        // left side homes line
        for(i = 0; i < 4; i++)      /*NEW*/
        {
            glPushMatrix();
                glTranslatef(-4.0f, 0.4f, 10.0f * i);
                glRotatef(50.0f, 0.0f, 1.0f, 0.0f);
                       
                glCallList(houseDataList);
            glPopMatrix();
        }

        // right side kandils
        {
            glPushMatrix();
                glTranslatef(0.12f, 0.03f, 0.0f);
                glTranslatef(7.0f, 0.3f, 27.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(0.12f, 0.03f, 0.0f);
                glTranslatef(7.0f, 0.3f, 17.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(0.12f, 0.03f, 0.0f);
                glTranslatef(7.0f, 0.3f, 7.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(0.12f, 0.03f, 0.0f);
                glTranslatef(7.0f, 0.3f, -2.6f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();
        }

        // left side kandils
        {
            glPushMatrix();
                glTranslatef(-0.12f, 0.03f, 0.0f);
                glTranslatef(-7.0f, 0.3f, 27.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-0.12f, 0.03f, 0.0f);
                glTranslatef(-7.0f, 0.3f, 17.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-0.12f, 0.03f, 0.0f);
                glTranslatef(-7.0f, 0.3f, 7.4f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-0.12f, 0.03f, 0.0f);
                glTranslatef(-7.0f, 0.3f, -2.6f);
                glRotatef(angle, 0.0f, 1.0f, 0.0f);
                abRenderKandil();
            glPopMatrix();
        }

        {
            // temple rangoli
            glPushMatrix();
                glTranslatef(0.0f, -0.85f, -13.0f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                ARB_Rangoli_1();
            glPopMatrix();

            // first layer homes rangoli
            glPushMatrix();
                glTranslatef(6.2f, -0.85f, 28.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_3();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-6.2f, -0.85f, 28.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_3();
            glPopMatrix();

            // second layer homes rangoli
            glPushMatrix();
                glTranslatef(6.2f, -0.85f, 18.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-6.2f, -0.85f, 18.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_2();
            glPopMatrix();

            // third layer homes rangoli
            glPushMatrix();
                glTranslatef(6.2f, -0.85f, 8.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_4();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-6.2f, -0.85f, 8.2f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_4();
            glPopMatrix();

            // fourth layer homes rangoli
            glPushMatrix();
                glTranslatef(6.2f, -0.85f, -1.8f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-6.2f, -0.85f, -1.8f);
                glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
                glScalef(0.5f, 0.5f, 0.5f);
                ARB_Rangoli_2();
            glPopMatrix();
        }

        // sun
        glPushMatrix();
            if (sun_x < 70.0f)
            {
                if (sun_x >= 0.0f)
                {
                    sun_y -= 0.008f;
                    glClearColor(sky_x -= 0.0005f, sky_y -= 0.0005f, sky_z -= 0.0005f, sky_a -= 0.0005f);
                }
                else if((sun_y < 25.0f) && (sun_x <= 0.0f))
                {
                    sun_y += 0.008f;
                }

                glTranslatef(sun_x, sun_y, -45.0f);
                sun_x += 0.025f;
            }
            else
            {
                glTranslatef(150.0f, 150.0f, 0.0f);
            }
            glScalef(5.5f, 5.5f, 5.5f);
            snRenderSun();
        glPopMatrix();

        // moon
        glPushMatrix();
            if ((moon_x < 70.0f) && (sun_x > 68.0f))
            {
                if (moon_x >= 0.0f)
                {
                    moon_y -= 0.009f;
                }
                else if ((moon_y < 25.0f) && (moon_x <= 0.0f))
                {
                    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                    moon_y += 0.008f;
                }
                glTranslatef(moon_x, moon_y, -45.0f);
                moon_x += 0.025f;
            }
            else
            {
                glTranslatef(150.0f, 150.0f, 0.0f);
            }
            glScalef(5.5f, 5.5f, 5.5f);
            snRenderMoon();
        glPopMatrix();
        
        // stars
        glPushMatrix();
            if (sun_x > 0.0f)
            {
                glTranslatef(0.0f, 0.0f, -50.0f);
                abRenderStars();
            }
        glPopMatrix();

        // first layer tree
        {
            glPushMatrix();
                glTranslatef(-2.6f, -0.85f, 28.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(2.6f, -0.85f, 28.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();
        }
        
        // second layer tree
        {
            glPushMatrix();
                glTranslatef(2.6f, -0.85f, 18.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-2.6f, -0.85f, 18.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();
        }
        
        // third layer tree
        {
            glPushMatrix();
                glTranslatef(-2.6f, -0.85f, 8.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(2.6f, -0.85f, 8.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();
        }
        
        // fourth layer tree
        {
            glPushMatrix();
                glTranslatef(-2.6f, -0.85f, -2.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(2.6f, -0.85f, -2.0f);
                glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
                glScalef(0.3f, 0.3f, 0.3f);
                Tree2();
            glPopMatrix();
        }
        
        // cloud
        {
            glPushMatrix();
                glTranslatef(0.0f, 20.0f, -40.0f);
                glScalef(15.15f, 15.15f, 15.15f);
                vmCloudTypeOne();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(30.0f, 20.0f, -40.0f);
                glScalef(15.15f, 15.15f, 15.15f);
                vmCloudTypeTwo();
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-30.0f, 20.0f, -40.0f);
                glScalef(15.15f, 15.15f, 15.15f);
                vmCloudTypeThree();
            glPopMatrix();
        }

        // firecracker
        if(bShowFireWorks)
        {
                //Left side Rockets
            glPushMatrix();        
                //Left side Rockets
                {
                    glTranslatef(-0.70f, -0.51f, 3.01f);
                    glTranslatef(0.0f, fRocketYMovement, 0.0f);
                    
                    if(!bShowFireWorksBurst)
                    {
                        for(i = 0; i < 4; i++)
                        {
                            glPushMatrix();
                                ankitDrawFireCracker();
                            glPopMatrix();
                            glTranslatef(0.0f, 0.0f, 10.01f);
                        }
                    }
                    else
                    {
                        for(i = 0; i < 4; i++)
                        {
                            glPushMatrix();
                                if(ankitDrawBurst())
                                    bShowFireWorks = false;
                            glPopMatrix();
                            glTranslatef(0.0f, 0.0f, 10.01f);
                        }
                    }
                }
            glPopMatrix();
            
            glPushMatrix();
                glScalef(-1.0f, 1.0f, 1.0f);
                
                {
                    glTranslatef(-0.75f, -0.51f, 5.17f);
                    glTranslatef(0.0f, fRocketYMovement, 0.0f);
                    
                    if(!bShowFireWorksBurst)
                    {
                        for(i = 0; i < 4; i++)
                        {
                            glPushMatrix();
                                ankitDrawFireCracker();
                            glPopMatrix();
                            glTranslatef(0.0f, 0.0f, 10.01f);
                        }
                    }
                    else
                    {
                        for(i = 0; i < 4; i++)
                        {
                            glPushMatrix();
                                if(ankitDrawBurst())
                                    bShowFireWorks = false;
                            glPopMatrix();
                            glTranslatef(0.0f, 0.0f, 10.01f);
                        }
                    }
                }
            glPopMatrix();
        }
        
        //Tree's Row
        glCallList(treeRowDataList);
        
        //Humanoid Render
        glCallList(humanoidDataList);

        glBindTexture(GL_TEXTURE_2D, 0);    //unbind all texture
        
        angle += 2.0f;
        if (angle >= 360.0f)
            angle = 0.0f;
        
    }
    
    else if(iScene == DIWALI_WISHES)
    {
        glPushMatrix();
            if(vjdRenderDiwaliParticlesScene() == true)
            {
                iScene = CREDIT_SCENE;
               
                //CAMERA POSITION REQUIRED FOR "CREDIT_SCENE"
                g_xTrans =  0.20f;
                g_yTrans =  0.0f;
                g_zTrans = -4.60f;
                
                g_xRot = g_yRot = g_zRot = 0.0f;
                
                fAlpha = 0.0f;
            }
        glPopMatrix();
    }
    
    else if(iScene == CREDIT_SCENE)
    {
        if(ankitCreditScene())
        {
            iScene = FINAL_CREDIT;
            
            //CAMERA POSITION REQUIRED FOR "FINAL_CREDIT"
            g_xTrans = g_yTrans = 0.0f;
            g_xRot = g_yRot = g_zRot = 0.0f;
            
            g_zTrans = -14.00f;
            
            fAlpha = 0.0f;
        }
    }
    
    else if(iScene = FINAL_CREDIT)
    {
        glColor3f(1.0f, 1.0f, 1.0f);
        vjdRenderFinalCredit();
    }

    //Transparent Black Lens on front of camera
    {
        glPushMatrix();
            // glPushAttrib(GL_LIGHTING);
                // glDisable(GL_LIGHTING);
                
                glMatrixMode(GL_PROJECTION);
                glPushMatrix();
                    
                    //Set Ortho
                    glLoadIdentity();            
                    gluOrtho2D(0, cxClient, 0, cyClient);
                    glMatrixMode(GL_MODELVIEW);
                    
                    //Render bitmap
                    glPushMatrix();
                        glLoadIdentity();
                        
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                        glEnable(GL_BLEND);
                        
                            // glTranslatef(xTrans, yTrans, zTrans);
                            // glScalef(xScale, yScale, zScale);
                            // glRotatef(xRot, 1.0f, 0.0f, 0.0f);
                            // glRotatef(yRot, 0.0f, 1.0f, 0.0f);
                            // glRotatef(zRot, 0.0f, 0.0f, 1.0f);
                            glTranslatef(0.0f, 0.0f, 0.979999f);
                            glBegin(GL_QUADS);
                                glColor4f(0.0f, 0.0f, 0.0f, fAlpha);
                                glVertex3f( (GLfloat)cxClient, (GLfloat)cyClient, 0.0f);
                                glVertex3f(              0.0f, (GLfloat)cyClient, 0.0f);
                                glVertex3f(              0.0f,  0.0f, 0.0f);
                                glVertex3f( (GLfloat)cxClient,  0.0f, 0.0f);
                            glEnd();
                        
                        glDisable(GL_BLEND);
                    glPopMatrix();
                    
                    //Reset Perspective
                    glMatrixMode(GL_PROJECTION);
                glPopMatrix();
                glMatrixMode(GL_MODELVIEW);
                
                // glEnable(GL_LIGHTING);
            // glPopAttrib();
            // glLoadIdentity();        
            // glRotatef(g_xRot, 1.0f, 0.0f, 0.0f);
            // glRotatef(-g_yRot, 0.0f, 1.0f, 0.0f);
            // glRotatef(-g_zRot, 0.0f, 0.0f, 1.0f);
            // glTranslatef(-g_xTrans, -g_yTrans, -g_zTrans-2.0f);
            // glScalef(xScale, yScale, zScale);

            
        glPopMatrix();
    }
    
   //Display Frame-Per-Second
   // ShowFPS();
    
    UpdateAnimation();

    SwapBuffers(ghdc);
}

//
//ShowFPS()
//
void ShowFPS(void)
{
    //code
    frame++;
    currTime = GetTickCount();
    if(currTime < lastTime)
        timeEpoch++;
    
    lastTime = currTime | timeEpoch << 32;
    time = lastTime - initTime;
    
    if(time - timebase > 1000)
    {
        sprintf(szFPSString, "FPS: %4.2f", frame*1000.0f/(time-timebase));
        timebase = time;
        frame = 0;
        // SetWindowTextA(ghwnd, str);
    }
    
    
    glPushAttrib(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        
        glColor3f(1.0f, 1.0f, 1.0f);
        
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
            
            //Set Ortho
            glLoadIdentity();            
            gluOrtho2D(0, cxClient, 0, cyClient);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslatef(0, -cyClient, 0);
            
            glMatrixMode(GL_MODELVIEW);
            
            //Render bitmap
            glPushMatrix();
                glLoadIdentity();
                
                // glTranslatef(xTrans, yTrans, zTrans);
                // glScalef(xScale, yScale, zScale);
                // glRotatef(xRot, 1.0f, 0.0f, 0.0f);
                // glRotatef(yRot, 0.0f, 1.0f, 0.0f);
                // glRotatef(zRot, 0.0f, 0.0f, 1.0f);
                
                glTranslatef(0.0f, 0.0f, 0.999999f);
                RenderBitmapString(10, 60, (char*)szFPSString);
                
                sprintf(strPosition, "gTx = %4.2f", g_xTrans);
                RenderBitmapString(10, 90, (char*)strPosition);
                
                sprintf(strPosition, "gTy = %4.2f", g_yTrans);
                RenderBitmapString(10,120, (char*)strPosition);
                
                sprintf(strPosition, "gTz = %4.2f", g_zTrans);
                RenderBitmapString(10,150, (char*)strPosition);
                
                sprintf(strPosition, "gRx = %4.2f", g_xRot);
                RenderBitmapString(10,180, (char*)strPosition);
                
                sprintf(strPosition, "gRy = %4.2f", g_yRot);
                RenderBitmapString(10,210, (char*)strPosition);
                
                sprintf(strPosition, "gRz = %4.2f", g_zRot);
                RenderBitmapString(10,240, (char*)strPosition);
                
                sprintf(strPosition, "Alpha = %4.2f", fAlpha);
                RenderBitmapString(10,270, (char*)strPosition);
                
            glPopMatrix();
            
            //Reset Perspective
            glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        
        glEnable(GL_LIGHTING);
    glPopAttrib();
}


//
//UpdateAnimation()
//
void UpdateAnimation(void)
{
    //variable declarations
    
    //code
    switch(currentAnimation)
    {
        // fed in
        case 0:     //Transition Animation
            if(fAlpha >= -0.1f)
                fAlpha = fAlpha - fTransitionStep;
            else
            {
                fAlpha = 0.0f;
                currentAnimation++;
            }
        break;
        
        // towards temple
        case 1:
            if(g_zTrans <= 16.80f)
            {
                g_zTrans = g_zTrans + 0.05f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // temple first step
        case 2:
            if(g_zTrans <= 17.40f && g_yTrans >= -0.30f)
            {
                g_zTrans = g_zTrans + 0.1f;
                g_yTrans = g_yTrans - 0.05f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // temple second step
        case 3:
            if(g_zTrans <= 17.80f && g_yTrans >= -0.60f)
            {
                g_zTrans = g_zTrans + 0.1f;
                g_yTrans = g_yTrans - 0.08f;
            }
            else
            {
                currentAnimation++;
            }
        break;

        // temple third step
        case 4:
            if(g_yTrans >= -1.30f)
            {
                g_yTrans = g_yTrans - 0.1f;
                g_zTrans = g_zTrans + 0.005f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // towards in the tample
        case 5:
            if(g_zTrans <= 24.42f)
            {
                g_zTrans = g_zTrans + 0.05f;
            }
            else
            {
                bShowDiyaFrontOfShriRam = true;
                // currentAnimation++;
            }
        break;
        
        // back to the temple
        case 6:
            if(g_zTrans >= 17.80f)
            {
                g_zTrans = g_zTrans - 0.02f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // step of temple (back)
        case 7:
            if(g_yTrans <= -0.60)
            {
                g_yTrans = g_yTrans + 0.08f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // step of temple (back)
        case 8:
            if(g_zTrans >= 17.40f && g_yTrans >= -0.30f)
            {
                g_zTrans = g_zTrans - 0.1f;
                g_yTrans = g_yTrans + 0.05f;
            }
            else
            {
                currentAnimation++;
            }
        break;

        // back to ground
        case 9:
            if(g_zTrans >= 2.24f)
            {
                g_zTrans = g_zTrans - 0.01f;
                if(g_yTrans < -0.30f)
                    g_yTrans = -0.30f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // black out
        case 10:
            if(fAlpha <= 1.0f)
                fAlpha = fAlpha + 0.01f;
            else
            {
                fAlpha = 1.0f;
                currentAnimation++;
                
                g_xTrans = 29.15f;
                g_yTrans = -3.00f;
                g_zTrans = -76.70f;
                
                g_xRot = 30.0f;
                g_yRot = 0.0f;
                g_zRot = 0.0f;
            }
        break;
        
        // left to right
        case 11:
            if(g_zTrans <= 18.00f)
            {
                if(fAlpha > 0.0f)
                    fAlpha = fAlpha - fTransitionStep;
                else
                    fAlpha = 0.0f;
                
                if(g_xRot > 0.0f)
                {
                    g_xRot = g_xRot - 0.1f;
                }
                g_zTrans = g_zTrans + 0.2f;
                g_xTrans = g_xTrans - 0.1f;
            }
            else
            {
                currentAnimation++;
            }
        break;
        
        // left to right black out
        case 12:
            if(fAlpha <= 1.0f)
            {
                fAlpha = fAlpha + fTransitionStep;
                g_zTrans = g_zTrans + 0.2f;
                g_xTrans = g_xTrans - 0.1f;
            }
            else
            {
                fAlpha = 1.0f;
                
                g_xTrans = 25.05f;
                g_yTrans = 0.20f;
                g_zTrans = -17.60f;
                
                g_xRot = 0.0f;
                g_yRot = 315.0f;
                g_zRot = 0.0f;

                currentAnimation++;
            }
        break;
        
        // left to right for house
        case 13:
            if(fAlpha > 0.5f)
                fAlpha = fAlpha - fTransitionStep;
            else
            {
                fAlpha = (fAlpha > 0.0f) ? fAlpha - fTransitionStep : 0.0f;
                
                if(g_zTrans <= 2.35f || g_xTrans >= 3.70f)
                {
                    g_zTrans = g_zTrans + 0.01f;
                    g_xTrans = g_xTrans - 0.01f;
                }
                else
                {
                    currentAnimation++;
                }
            }
        break;
        
        // black out
        case 14:
            if(fAlpha < 1.0f)
            {
                g_zTrans = g_zTrans + 0.01f;
                g_xTrans = g_xTrans - 0.01f;
                fAlpha = fAlpha + 0.05f;
            }
            else
            {
                fAlpha = 1.0f;
                
                g_xTrans = -1.10f;
                g_yTrans = -11.20f;
                g_zTrans = 31.75f;
                
                g_xRot = g_yRot = g_zRot = 0.0f;
                
                fAlpha = 0.40f;
                currentAnimation++;
            }
        break;
        
        // show all scene
        case 15:
            if(g_zTrans >= -64.25)
            {
                g_zTrans = g_zTrans - 0.1f;
            }
            else
            {
                bShowFireWorks = true;
                currentAnimation++;
            }
        break;
        
        // rocket goes upword
        case 16:
            if(fRocketYMovement <= 22.0f)
            {
                fRocketYMovement += 0.1f;
            }
            else
            {
                bShowFireWorksBurst = true;
                currentAnimation++;
            }
        break;
        
        // firecracker and change scene
        case 17:
            if(bShowFireWorks == false)
            {
                 if(fAlpha < 1.0f)
                {
                    fAlpha = fAlpha + 0.01f;
                }
                 //CAMERA POSITION REQUIRED FOR "DIWALI_WISHES"
                else
                {
                    fAlpha = 0.0f;
                    
                    //CAMERA POSITION REQUIRED FOR "DIWALI_WISHES"
                    g_xTrans = -5.40f;
                    g_yTrans = -3.40f;
                    g_zTrans = -26.00f;
                    
                    g_xRot = 0.0f;
                    g_yRot = 0.0f;
                    g_zRot = 0.0f;
                    
                    currentAnimation++;
                    iScene = DIWALI_WISHES;
                }
            }
        break;
    }
}


//
//Uninitialize()
//
void Uninitialize(void)
{
    //code
    if(gbFullScreen == true)
    {
        SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
        
        SetWindowPlacement(ghwnd, &wpPrev);
        
        SetWindowPos(
            ghwnd,
            HWND_TOP,
            0, 0, 0, 0,
            SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED
        );
        
        ShowCursor(TRUE);
    }
    
    //Delete Textures
    DeleteTempleTexture();  //Temple.cpp
    DeleteHouseTexture();   //House.cpp
    DeleteGroundTexture();  //Ground.cpp
    DeleteKandilTexture();  //Kandil.cpp
    DeleteSunAndMoonTexture();  //SunAndMoon.cpp
    DeleteTreeTexture();    //Tree.cpp
    DeleteHumanTexture();   //Humanoid.cpp
    DeleteFireCrackerAndBottleTexture();    //FireCracker.cpp
    
    UninitializeDiwaliWishesResources();    //DiwaliWishSceneEffect.cpp
    
    vjdUninitializeFinalScene();    //FinalCreditEffect.cpp
    
    if(wglGetCurrentContext() == ghrc)
    {
        wglMakeCurrent(NULL, NULL);
    }
    
    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
    }
    
    if(ghdc)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }
    
    if(gpFile)
    {
        fprintf(gpFile, "Log file closed and Program Terminated.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}//End Uninitialize()

