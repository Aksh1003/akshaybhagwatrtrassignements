/*
    Code :- Nikhil Lele And Vijay Dangi
*/

//Headers
#include "Main.h"
#include "Geometry.h"
#include "Resource.h"

//variable declarations
static GLuint nlTulsiStoneTexture;
static GLuint nlStoneBottomTexture;
static GLuint nlSoilTexture;

static GLuint vjdTreeBarkTexture;

static GLuint leafList;    //data list

extern FILE *gpFile;

//
//LoadTulasTextureAndData()
//
void LoadTulasTextureAndData(void)
{
    //function declarations
    void InitializeLeafDataListFromImage(void);
    
    //code
    if(LoadGLTexture(&nlTulsiStoneTexture, MAKEINTRESOURCE(NL_STONE_BITMAP)))
       fprintf(gpFile, "NL_STONE_BITMAP loded\n");
    else
       fprintf(gpFile, "NL_STONE_BITMAP not loded\n");
        
	if(LoadGLTexture(&nlStoneBottomTexture, MAKEINTRESOURCE(NL_STONE_BOTTOM_BITMAP)))
       fprintf(gpFile, "NL_STONE_BOTTOM_BITMAP loded\n");
    else
       fprintf(gpFile, "NL_STONE_BOTTOM_BITMAP not loded\n");
   
	if(LoadGLTexture(&nlSoilTexture, MAKEINTRESOURCE(NL_SOIL_BITMAP)))
       fprintf(gpFile, "NL_SOIL_BITMAP loded\n");
    else
       fprintf(gpFile, "NL_SOIL_BITMAP not loded\n");
    
    if(LoadGLTexture(&vjdTreeBarkTexture, MAKEINTRESOURCE(VJD_TREE_BARK_BITMAP)))
       fprintf(gpFile, "VJD_TREE_BARK_BITMAP loded\n");
    else
       fprintf(gpFile, "VJD_TREE_BARK_BITMAP not loded\n");
    
    InitializeLeafDataListFromImage();
}


//
//DibLoadImage()
//
BITMAPFILEHEADER *DibLoadImage(PTSTR pstrFileName)
{
    //variable declarations
    BOOL              bSuccess;
    DWORD             dwFileSize, dwHighSize, dwBytesRead;
    HANDLE            hFile;
    BITMAPFILEHEADER *pbmfh;
    
    //code
    hFile = CreateFile(pstrFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
    
    if(hFile == INVALID_HANDLE_VALUE)
        return(NULL);
    
    dwFileSize = GetFileSize(hFile, &dwHighSize);
    
    if(dwHighSize)
    {
        CloseHandle(hFile);
        return(NULL);
    }
    
    pbmfh =(BITMAPFILEHEADER*) malloc(dwFileSize);
    
    if(!pbmfh)
    {
        CloseHandle(hFile);
        return(NULL);
    }
    
    bSuccess = ReadFile(hFile, pbmfh, dwFileSize, &dwBytesRead, NULL);
    CloseHandle(hFile);
    
    if(!bSuccess || (dwBytesRead != dwFileSize)
                 || (pbmfh->bfType != *(WORD*)"BM")
                 || (pbmfh->bfSize != dwFileSize)
    )
    {
        free(pbmfh);
        return(NULL);
    }
    return(pbmfh);
}

//
//InitializeLeafDataListFromImage()
//
void InitializeLeafDataListFromImage(void)
{
    BITMAPFILEHEADER *pbmfh;
    BITMAPINFO       *pbmi;
    BYTE             *pBits;
    int               cxDib, cyDib;

    int r, g, b;
    int rowLength;
    int i, j;
    
    //code    
    pbmfh = DibLoadImage(TEXT("Texture\\tulas\\tulasLeaf.bmp"));
    if(pbmfh == NULL)
    {
        fprintf(gpFile, "InitializeLeafDataListFromImage():- Image Not Load");
        return;
    }
    //Get pointers to the info structure & the bits
    pbmi  = (BITMAPINFO*) (pbmfh + 1);
    pBits = (BYTE *) pbmfh + pbmfh->bfOffBits;
    
    //Get the DIB width and height
    if(pbmi->bmiHeader.biSize == sizeof(BITMAPCOREHEADER))
    {
        cxDib = ((BITMAPCOREHEADER *)pbmi)->bcWidth;
        cyDib = ((BITMAPCOREHEADER *)pbmi)->bcHeight;
    }
    else
    {
        cxDib =     pbmi->bmiHeader.biWidth;
        cyDib = abs(pbmi->bmiHeader.biHeight);
    }
    
        //Image Must be 24-bit bmp
    if(pbmi->bmiHeader.biBitCount == 24)
    {            
        //Calculate length of each row (row is multiple of 4)
        rowLength = 4 * ((cxDib * pbmi->bmiHeader.biBitCount + 31) / 32);
        
        leafList = glGenLists(1);
        glNewList(leafList, GL_COMPILE);
            glBegin(GL_POINTS);
                for(i = 0; i < cyDib; i++)//((i+1)%2) ? (i += 2) : (i++))
                {
                    for(j = 0; j < cxDib; j++)//((j+1)%2) ? (j+=2) : (j++))
                    {
                        r = pBits[i*rowLength + 3*j + 2];
                        g = pBits[i*rowLength + 3*j + 1];
                        b = pBits[i*rowLength + 3*j ];

                        if(r == 0xFF && g == 0xFF && b == 0xFF)
                            continue;
                        
                        glColor3ub((GLubyte)r, (GLubyte)g, (GLubyte)b);
                        glVertex3f(
                            ((GLfloat)j/(GLfloat)cxDib)/4.0f,
                            ((GLfloat)i/(GLfloat)cyDib)/4.0f,
                            0.0f
                        );
                    }
                }
            glEnd();
        glEndList();
    }
    
    free(pbmfh);
}


//
//DeleteTualsTextureAndData()
//
void DeleteTualsTextureAndData(void)
{
    //code
    if(nlTulsiStoneTexture)
        glDeleteTextures(1, &nlTulsiStoneTexture);
    if(nlStoneBottomTexture)
        glDeleteTextures(1, &nlStoneBottomTexture);
    if(nlSoilTexture)
        glDeleteTextures(1, &nlSoilTexture);
    
    glDeleteLists(leafList, 1);
}


//
//nlRenderTulas()
//
void nlRenderTulas(void)
{   
    //function declarations
    void nlTulsiVrindavan(void);
    void vjdTulsiPlant(void);
    
    //code
    glColor3f(1.0f, 1.0f, 1.0f);
    nlTulsiVrindavan();
    
    glTranslatef(0.0f, -0.17f, 0.0f);
    vjdTulsiPlant();
}


//
//nlTulsiVrindavan()
//
void nlTulsiVrindavan(void)
{
    //code
    glBindTexture(GL_TEXTURE_2D, nlStoneBottomTexture);

	glBegin(GL_TRIANGLES);
    //top left front corner
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.10f, 0.4f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.10f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.4f, 0.10f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, 0.10f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.4f, 0.25f);

	//2nd right front corner
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.10f, 0.4f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.4f, 0.10f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, 0.10f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.10f, 0.4f, 0.25f);

	//right back corner
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.4f, -0.10f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.10f, 0.4f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.5f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.10f, 0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.4f, -0.10f);

	//left back corner
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.10f, 0.4f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.4f, -0.10f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.5f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, -0.10f);
        glTexCoord2f(1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.10f, 0.4f, -0.25f);

	glEnd();

	glBegin(GL_QUADS);

	//top square
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.4f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.40f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.40f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.3f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.40f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, 0.40f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, 0.3f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, 0.3f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.40f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.40f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.3f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, 0.40f, 0.25f);

    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, nlSoilTexture);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.40f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.4f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.40f, -0.25f);
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, nlStoneBottomTexture);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, 0.30f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, 0.3f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, 0.30f, -0.25f);
	
	glEnd();

	//lower square
	glBegin(GL_QUADS);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, -0.3f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, -0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, -0.4f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, -0.3f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.3f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, -0.4f, 0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.25f, -0.3f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.3f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.3f, -0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.25f, -0.3f, -0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, -0.4f, 0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.3f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.3f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, -0.3f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, -0.3f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.3f, 0.25f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.25f, -0.4f, 0.25f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.25f, -0.4f, -0.25f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.25f, -0.4f, 0.25f);

	//bottom
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.35f, -0.4f, 0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.35f, -0.5f, 0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.35f, -0.5f, 0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.35f, -0.4f, 0.3f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.35f, -0.4f, 0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.35f, -0.5f, 0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.35f, -0.4f, -0.3f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.35f, -0.4f, -0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.35f, -0.4f, -0.3f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.35f, -0.4f, -0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.35f, -0.5f, 0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.35f, -0.4f, 0.3f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.35f, -0.4f, 0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.35f, -0.4f, -0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.35f, -0.4f, -0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.35f, -0.4f, 0.3f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.35f, -0.5f, 0.3f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.35f, -0.5f, -0.3f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.35f, -0.5f, 0.3f);

	glEnd();
	
	glBindTexture(GL_TEXTURE_2D, nlTulsiStoneTexture);
	//middle part
	glBegin(GL_QUADS);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.2f, 0.3f, 0.2f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, -0.3f, 0.2f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.2f, -0.3f, 0.2f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.2f, 0.30f, 0.2f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.2f, 0.30f, 0.2f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.2f, -0.3f, 0.2f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.2f, -0.3f, -0.2f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.2f, 0.30f, -0.2f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.2f, 0.30f, -0.2f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.2f, -0.3f, -0.2f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.2f, -0.3f, -0.2f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.2f, 0.30f, -0.2f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-0.2f, 0.30f, -0.2f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.2f, -0.3f, -0.2f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-0.2f, -0.3f, 0.2f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-0.2f, 0.30f, 0.2f);

	glEnd();
    
    glBindTexture(GL_TEXTURE_2D, 0);    //unbind texture
}


//
//vjdTulsiPlant()
//
void vjdTulsiPlant(void)
{
    //variable declarations
    GLUquadric *quadric;
    
    //code
    glPushMatrix();
    //bark
        quadric = gluNewQuadric();
        gluQuadricTexture(quadric, GL_TRUE);
        glBindTexture(GL_TEXTURE_2D, vjdTreeBarkTexture);
        glPushMatrix();
            glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
            gluCylinder(quadric, 0.01f, 0.01f, 1.00f, 5, 5);
        glPopMatrix();
        gluDeleteQuadric(quadric);
        glBindTexture(GL_TEXTURE_2D, 0);

    //leaf
        glPushMatrix();
            glTranslatef(-0.24f, 0.85f, 0.0f);
            glCallList(leafList);
        glPopMatrix();
        
    //leaf
        glPushMatrix();
             glTranslatef(0.16f, 1.03f, 0.0f);
            glRotatef(208.0f, 0.0f, 0.0f, 1.0f);
            glCallList(leafList);
        glPopMatrix();
        
    //leaf
        glPushMatrix();
            glTranslatef(-0.24f, 0.61f, 0.0f);
            glCallList(leafList);
        glPopMatrix();
    glPopMatrix();
}

