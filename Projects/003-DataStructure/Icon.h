#pragma once

// Icon
#define MYICON 100

// Tree
#define TREE_BARK_BITMAP 200
#define TREE_LEAF_BITMAP 201

// Bird
#define BIRD_BITMAP 300
#define BIRD_LEG_BITMAP 301

// Ground
#define GROUND_BITMAP 400

// Humanoid
#define SHIRT_BITMAP 500
#define PANT_BITMAP 501
#define SHIRT1_BITMAP 502

// Home
#define WALL_BITMAP 601
#define TERRACE_BITMAP 602

// Cloud
#define CLOUD_BITMAP 801

// Kite
#define KITE_BITMAP 901
