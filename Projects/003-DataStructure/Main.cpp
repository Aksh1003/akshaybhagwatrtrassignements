// headers
#include <Windows.h>
#include <stdio.h>
#include <math.h>
#include"Icon.h"
#include <gl\gl.h>
#include <gl\glu.h>

// macros
#pragma comment(lib, "OPENGL32.LIB")
#pragma comment(lib, "GLU32.LIB")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define pi 3.14

// global variables
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLfloat g_xRot, g_yRot, g_zRot;
GLfloat g_xTrans, g_yTrans, g_zTrans = -20;

GLUquadric* quadric = NULL;

// generic
void getMultipleTree(void);
void getMultipleCloud(void);

// tree
GLuint treeBarkTexture, treeLeafTexture;
void LoadTreeTexture(void);
void Tree(void);
void DeleteTreeTexture(void);

// bird
GLuint birdTexture;
GLuint birdLegTexture;
void LoadBirdTexture(void);
void Bird(void);
void DeleteBirdTexture(void);

// ground
GLuint groundTexture;
void LoadGroundTexture(void);
void Ground(void);
void DeleteGroundTexture(void);

// humanoid
GLuint shirtTexture, pantTexture, shirt1Texture;
static GLfloat left_leg;
static GLfloat right_leg;
static GLfloat main_body;
static GLfloat left_hand;
static GLfloat right_hand;
void LoadHumanoidTexture(void);
void Humanoid1(void);
void Humanoid2(void);
void DeleteHumanoidTexture(void);
void GetWalk(void);

// home
GLuint wallTexture, terraceTexture;
void LoadHomeTexture(void);
void Home(void);
void HomeBase(void);
void DeleteHomeTexture(void);

// cloud
GLuint cloudTexture;
void LoadCloudTexture(void);
void Cloud(void);
void DeleteCloudTexture(void);

// kite
GLuint kiteTexture;
void LoadKiteTexture(void);
void Kite(void);
void DeleteKiteTexture(void);

// Aasari
void Aasari(void);

// Animation
GLfloat human_x_axis = 10.0f;
GLfloat aasari_x_axis = 10.0f;
GLfloat kite_x_axis = -2.5f;
GLfloat main_x_axis = -5.0f;

// variable for data list
GLuint cloudDataList;
GLuint treeDataList;
GLuint homeDataList;

// structure declaration
struct manja_points
{
	float x_axis;
	float y_axis;
	float z_axis;
	struct manja_points* next;
};

// typedef
typedef struct manja_points MANJA_POINTS;
typedef struct manja_points* P_MANJA_POINTS;
typedef struct manja_points** PP_MANJA_POINTS;

static int manja_vertex_count = -1;
float manja_array[10][3] = { {5.0f, 0.74f, 29.3f}, 
							{4.0f, 0.77f + 0.27f, 29.3f}, 
							{3.0f, 0.8f + 0.54f, 29.3f},
							{2.0f, 0.83f + 0.81f, 29.3f},
							{1.0f, 0.86f + 1.08f, 29.3f},
							{0.0f, 0.89f + 1.35f, 29.3f},
							{-1.0f, 0.92f + 1.62f, 29.3f},
							{-2.0f, 0.95f + 1.89f, 29.3f},
							{-3.0f, 0.98f + 2.16f, 29.3f},
							{-4.0f, 1.01f + 2.43f, 29.3f}};

P_MANJA_POINTS head = NULL;

bool InsertFirst(PP_MANJA_POINTS);

// function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
bool LoadGLTexture(GLuint*, TCHAR[]);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	void Initialize();
	void Display();
	void ToggleFullScreen();

	// local variable
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, "AKSHAY.TXT", "w") != 0)
	{
		MessageBox(NULL, TEXT("Can Not Create Desired File!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
							szAppName,
							TEXT("AKSHAY BHAGWAT"),
							WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							((GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2)),
							((GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2)),
							WIN_WIDTH,
							WIN_HEIGHT,
							NULL,
							NULL,
							hInstance,
							NULL);
	
	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	ToggleFullScreen();

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen();
	void Resize(int, int);
	void UnInitialize();

	// local variable

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		case VK_UP:
			g_xRot = g_xRot + 3.0f;
			if (g_xRot >= 360.0f)
				g_xRot = 0.0f;
			break;

		case VK_DOWN:
			g_xRot = g_xRot - 3.0f;
			if (g_xRot <= 0.0f)
				g_xRot = 360.0f;
			break;

		case VK_RIGHT:
			g_yRot = g_yRot + 3.0f;
			if (g_yRot >= 360.0f)
				g_yRot = 0.0f;
			break;

		case VK_LEFT:
			g_yRot = g_yRot - 3.0f;
			if (g_yRot <= 0.0f)
				g_yRot = 360.0f;
			break;

		case VK_HOME:
			g_zRot = g_zRot + 3.0f;
			if (g_zRot >= 360.0f)
				g_zRot = 0.0f;
			break;

		case VK_END:
			g_zRot = g_zRot - 3.0f;
			if (g_zRot <= 0.0f)
				g_zRot = 360.0f;
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'w':
		case 'W':
			g_zTrans = g_zTrans + 0.3f;
			break;

		case 's':
		case 'S':
			g_zTrans = g_zTrans - 0.3f;
			break;

		case 'a':
		case 'A':
			g_xTrans = g_xTrans + 0.3f;
			break;

		case 'd':
		case 'D':
			g_xTrans = g_xTrans - 0.3f;
			break;

		case 'r':
		case 'R':
			GetWalk();
			break;

		case 'm':
		case 'M':
			manja_vertex_count++;
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	// function declaration

	// local variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

}

void Initialize()
{
	// function declaration
	void Resize(int, int);
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// local variable
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.196078f, 0.6f, 0.8f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// Load Texture
	LoadTreeTexture();
	LoadBirdTexture();
	LoadGroundTexture();
	LoadHumanoidTexture();
	LoadHomeTexture();
	LoadCloudTexture();
	LoadKiteTexture();

	glEnable(GL_TEXTURE_2D);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	{
		treeDataList = glGenLists(1);
		glNewList(treeDataList, GL_COMPILE);
		glPushMatrix();
		//glTranslatef(-15.0f, -0.15f, 0.0f);
		//glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		//glScalef(1.3f, 0.9f, 0.9f);
			Tree();
		glPopMatrix();
		glEndList();
	}

	{
		cloudDataList = glGenLists(1);
		glNewList(cloudDataList, GL_COMPILE);
		glPushMatrix();
			Cloud();
		glPopMatrix();
		glEndList();
	}

	{
		homeDataList = glGenLists(1);
		glNewList(homeDataList, GL_COMPILE);
		glPushMatrix();
			Home();
		glPopMatrix();
		glEndList();
	}

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	// function declaration

	// local variable
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	// code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bResult = true;

		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// push data into graphic memory with help of graphics driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits); //(gluBuild2DMipmaps() = glText2D() + glGenerateBitMap())
		DeleteObject(hBitmap);
	}

	return bResult;
}

void Resize(int width, int height)
{
	// function declaration

	// local variable

	// code
	if (height == 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void Display()
{
	// function declaration
	
	// local variable
	static GLfloat angle = 0.0f;
	P_MANJA_POINTS temp = head;

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if(human_x_axis >= 9.5f)
		glTranslatef(main_x_axis, 0.0f, -20.0f);
	else
		glTranslatef(0.0f, 0.0f, -20.0f);

	glRotatef(5.0f, 1.0f, 0.0f, 0.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glTranslatef(g_xTrans, g_yTrans, g_zTrans);
	glRotatef(g_xRot, 0.0f, 1.0f, 0.0f);
	glRotatef(g_yRot, 0.0f, 0.0f, 1.0f);
	glRotatef(g_zRot, 1.0f, 0.0f, 0.0f);

///	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glPushMatrix();
		Ground();
	glPopMatrix();

	glPushMatrix();
		getMultipleTree();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.0f, -0.65f, 25.5f);
		glScalef(0.02f, 0.02f, 0.030f);
		Bird();
	glPopMatrix();
	
	// blue shirt
	glPushMatrix();
		glTranslatef(10.0f - 0.2f, 0.4f, 30.0f);
		if(human_x_axis >= 9.63f)
			GetWalk();
		
//		glTranslatef(8.0f, 1.5f, 12.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(0.12f, 0.12f, 0.12f);
		Humanoid1();
	glPopMatrix();

	glPushMatrix();
		if ((human_x_axis >= 9.63f) && (manja_vertex_count < 0))
		{
			glTranslatef(kite_x_axis, -2.3f, 9.3f);
			kite_x_axis -= 0.024f;
		}
		else if ((human_x_axis <= 9.63f) && (manja_vertex_count < 0))
		{
			glTranslatef(-7.3f, -1.7f, 8.2f);
//			glTranslatef(kite_x_axis, -2.3f, 9.3f);
		}
		else if ((manja_vertex_count <= 9) && (manja_vertex_count >= 0))
		{
			glTranslatef(-7.3f + manja_array[manja_vertex_count][0] - 5.9f, -1.7f + manja_array[manja_vertex_count][1] - 0.4f, 8.2f);
		}
		else
		{
			glTranslatef(-7.3f + -4.0f - 5.9f, -1.7f + 1.01f + 2.43f - 0.4f, 8.2f);
		}

		glRotatef(30.0f, 0.0f, 1.0f, 0.0f);
		glScalef(0.8f, 0.8f, 0.8f);
		Kite();
	glPopMatrix();


	// white shirt
	glPushMatrix(); 
		glTranslatef(10.0f, 0.4f, 31.0f);
		if (human_x_axis >= 9.63f)
			GetWalk();

//		glTranslatef(6.5f, 0.4f, 30.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(0.1f, 0.1f, 0.1f);
		Humanoid2();

	glPopMatrix();

	glPushMatrix();
		glTranslatef(11.0f, 2.0f, 11.0f);
		glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);
		glCallList(homeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(11.0f, -0.3f, 12.0f);
		glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.0f, 0.5f, 1.0f);
		HomeBase();
	glPopMatrix();

	glPushMatrix();
		getMultipleCloud();
	glPopMatrix();

	glPushMatrix();
		glLineWidth(2.0f);
		while (temp != NULL)
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glBegin(GL_LINES);
				glVertex3f(temp->x_axis, temp->y_axis, temp->z_axis);
				glVertex3f(temp->x_axis - 1.0f, temp->y_axis + 0.3f, temp->z_axis);

		//		Kite();
			glEnd();

			temp = temp->next;
		}
	glPopMatrix();

	//glPushMatrix();
	//	glTranslatef(head->x_axis, head->y_axis, head->z_axis);
	//	Kite();
	//glPopMatrix();

	// x and y axis
	glPushMatrix();
		glBegin(GL_LINES);
			glVertex3f(-83.0f, 0.0f, 0.0f);
			glVertex3f(83.0f, 0.0f, 0.0f);

			glVertex3f(0.0f, -83.0f, 0.0f);
			glVertex3f(0.0f, 83.0f, 0.0f);
		glEnd();
	glPopMatrix();

	angle = angle + 0.2f;
	if (angle >= 360.0f)
		angle = 0.0f;

	human_x_axis -= 0.002f;
	aasari_x_axis -= 0.002f;
	main_x_axis += 0.02f;

	//sl_x_axis += 0.1f;

	//if ((sl_y_axis == 13.0f) && (sl_x_axis <= 0.0f))
	//{
	//	sl_y_axis += 0.001f;
	//}
	//else
	//{
	//	sl_y_axis -= 0.001f;
	//}

	SwapBuffers(ghdc);
}

bool InsertFirst(PP_MANJA_POINTS first)
{
	if (manja_vertex_count <= 9)
	{
		P_MANJA_POINTS newn = NULL;

		newn = (P_MANJA_POINTS)malloc(sizeof(MANJA_POINTS));
		if (newn == NULL)
			return FALSE;

		newn->next = NULL;

		if (*first == NULL)
		{
			*first = newn;
		}
		else
		{
			newn->next = *first;
			*first = newn;
		}

		newn->x_axis = manja_array[manja_vertex_count][0];
		newn->y_axis = manja_array[manja_vertex_count][1];
		newn->z_axis = manja_array[manja_vertex_count][2];
	}

	return TRUE;
}

void Aasari(void)
{
	glPushMatrix();
		// middle black cylinder
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, 3.0f, 30.0f);

		quadric = gluNewQuadric();
		gluCylinder(quadric, 0.1f, 0.1f, 0.3f, 30, 30);
		gluDeleteQuadric(quadric);

		glPushMatrix();
			// side red sphere
			glColor3f(1.0f, 0.0f, 0.0f);
			glScalef(1.0f, 1.0f, 0.5f);

			quadric = gluNewQuadric();
			gluSphere(quadric, 0.12f, 30, 30);
			gluDeleteQuadric(quadric);

			glPushMatrix();
				// side red sphere
				glColor3f(1.0f, 0.0f, 0.0f);
				glTranslatef(0.0f, 0.0f, 0.6f);
				glScalef(1.0f, 1.0f, 0.5f);

				quadric = gluNewQuadric();
				gluSphere(quadric, 0.12f, 30, 30);
				gluDeleteQuadric(quadric);

				glPushMatrix();
					// side handle cylinder
					glTranslatef(0.0f, 0.0f, -2.1f);
					glColor3f(1.0f, 0.0f, 0.0f);
					glScalef(1.0f, 1.0f, 1.0f);

					quadric = gluNewQuadric();
					gluCylinder(quadric, 0.03f, 0.03f, 3.0f, 30, 30);
					gluDeleteQuadric(quadric);

					if (human_x_axis >= 9.63f)
					{
						glPushMatrix();
							// manja (asari to kite)
							glColor3f(0.0f, 0.0f, 0.0f);
							glBegin(GL_LINES);
								glVertex3f(0.0f, 0.0f, 1.3f);
								glVertex3f(0.47f, -0.78f, -2.0f);
							glEnd();
						glPopMatrix();
					}
					else
					{
						glPushMatrix();
							// manja (asari to kite)
							glColor3f(0.0f, 0.0f, 0.0f);
							glBegin(GL_LINES);
								glVertex3f(0.0f, 0.0f, 1.3f);
								glVertex3f(0.2f, -1.1f, 0.0f);
							glEnd();
						glPopMatrix();

						// manja left hand to right hand
						glPushMatrix();
							glColor3f(0.0f, 0.0f, 0.0f);
							glBegin(GL_LINES);
								glVertex3f(0.2f, -1.1f, 0.0f);
								glVertex3f(-0.16f, -1.5f, -4.0f);
							glEnd();

							// manja 
							glPushMatrix();
								InsertFirst(&head);
							glPopMatrix();
						glPopMatrix();
					}
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
}

void LoadKiteTexture(void)
{
	LoadGLTexture(&kiteTexture, MAKEINTRESOURCE(KITE_BITMAP));
}

void DeleteKiteTexture(void)
{
	if (kiteTexture)
	{
		glDeleteTextures(1, &kiteTexture);
		kiteTexture = 0;
	}
}

void Kite()
{
	glPushMatrix();
		glLineWidth(5.0f);
		glTranslatef(0.0f, 3.0f, 30.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		// white squre
		glBindTexture(GL_TEXTURE_2D, kiteTexture);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
	
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-0.5f, 0.0f, 0.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.0f, -0.5f, 0.0f);
			
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);

		glPushMatrix();
			// kaman
			glColor3f(0.5f, 0.5f, 0.5f);
			glBegin(GL_POINTS);
				for (GLfloat i = 0.0f; i <= pi; i += 0.001f)
				{
					glVertex3f(-0.48f * cos(i), 0.1f * sin(i), 0.0f);
				}
				glEnd();

			glPushMatrix();
				// ddattya
				glColor3f(0.5f, 0.5f, 0.5f);
				glBegin(GL_LINES);
					glVertex3f(0.0f, 0.5f, 0.0f);
					glVertex3f(0.0f, -0.5f, 0.0f);
				glEnd();

				glPushMatrix();
					// andda
					glTranslatef(0.0f, 0.0f, 0.00001f);
					glColor3f(1.0f, 0.0f, 0.0f);
					glBegin(GL_TRIANGLES);
						glVertex3f(0.0f, -0.2f, 0.0f);
						glVertex3f(-0.25f, -0.6f, 0.0f);
						glVertex3f(0.25f, -0.6f, 0.0f);
					glEnd();

					glPushMatrix();
						// kaanna
						glLineWidth(2.0f);
						glColor3f(0.0f, 0.0f, 0.0f);
						glBegin(GL_LINES);
							glVertex3f(0.0f, 0.3f, 0.0f);
							glVertex3f(0.0f, 0.0f, 0.5f);
							glVertex3f(0.0f, -0.3f, 0.0f);
							glVertex3f(0.0f, 0.0f, 0.5f);
						glEnd();
					glPopMatrix();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
}

void getMultipleCloud(void)
{
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(1.0f, 7.0f, -25.0f);
		glScalef(3.0f, 3.0f, 3.0f);
		glCallList(cloudDataList);
	glPopMatrix();

	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(-20.0f, 7.0f, -25.0f);
		glScalef(3.0f, 3.0f, 3.0f);
		glCallList(cloudDataList);
	glPopMatrix();

	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(40.0f, 7.0f, -25.0f);
		glScalef(3.0f, 3.0f, 3.0f);
		glCallList(cloudDataList);
	glPopMatrix();
}

void getMultipleTree(void)
{
	glPushMatrix();
		glTranslatef(-15.0f, -0.15f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-11.0f, -0.15f, -7.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-5.0f, -0.15f, 3.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(10.0f, -0.15f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.0f, -0.15f, -7.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(2.0f, -0.15f, 3.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(25.0f, -0.15f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-20.0f, -0.15f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-20.0f, -0.15f, -5.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-30.0f, -0.15f, -10.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-27.0f, -0.15f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-15.0f, -0.15f, -5.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-11.0f, -0.15f, -12.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-5.0f, -0.15f, 8.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(10.0f, -0.15f, 5.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.0f, -0.15f, -12.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(2.0f, -0.15f, 8.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(25.0f, -0.15f, 5.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-20.0f, -0.15f, -5.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-20.0f, -0.15f, -15.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-30.0f, -0.15f, -10.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-7.0f, -0.15f, 3.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.0f, -0.15f, 12.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-1.0f, -0.15f, 13.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-9.0f, -0.15f, 19.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-14.0f, -0.15f, 15.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-19.0f, -0.15f, 22.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(10.0f, -0.15f, -18.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glScalef(1.3f, 0.9f, 0.9f);
		glCallList(treeDataList);
	glPopMatrix();
}

void LoadCloudTexture(void)
{
	LoadGLTexture(&cloudTexture, MAKEINTRESOURCE(CLOUD_BITMAP));
}

void DeleteCloudTexture(void)
{
	if (cloudTexture)
	{
		glDeleteTextures(1, &cloudTexture);
		cloudTexture = 0;
	}
}

void Cloud()
{
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glPushMatrix();
		glTranslatef(-1.0f, 3.0f, 0.0f);
		glScalef(0.7f, 0.5f, 0.75f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(-0.5f, 3.0f, 0.0f);
		glScalef(0.9f, 0.9f, 1.35f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.0f, 3.0f, 0.0f);
		glScalef(1.5f, 1.5f, 2.0f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.25f, 3.35f, 0.0f);
		glScalef(1.0f, 1.0f, 1.45f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.5f, 3.0f, 0.0f);
		glScalef(1.5f, 1.5f, 2.0f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.5f, 3.5f, 0.0f);
		glScalef(1.0f, 1.0f, 1.45f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.75f, 3.35f, 0.0f);
		glScalef(1.0f, 1.0f, 1.45f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(1.0f, 3.0f, 0.0f);
		glScalef(1.5f, 1.5f, 2.0f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(1.5f, 3.0f, 0.0f);
		glScalef(0.9f, 0.9f, 1.35f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(1.75f, 3.0f, 0.0f);
		glScalef(0.5f, 0.5f, 0.75f);

		glBindTexture(GL_TEXTURE_2D, cloudTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 20, 20);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void LoadHomeTexture(void)
{
	LoadGLTexture(&wallTexture, MAKEINTRESOURCE(WALL_BITMAP));
	LoadGLTexture(&terraceTexture, MAKEINTRESOURCE(TERRACE_BITMAP));
}

void DeleteHomeTexture(void)
{
	if (wallTexture)
	{
		glDeleteTextures(1, &wallTexture);
		wallTexture = 0;
	}

	if (terraceTexture)
	{
		glDeleteTextures(1, &terraceTexture);
		terraceTexture = 0;
	}
}

void HomeBase(void)
{
	// base sphere
	glPushMatrix();
		glColor3f(0.8f, 0.4f, 0.3f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glBegin(GL_QUADS);
			// front
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(5.0f, 1.0f, 5.0f);

			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-5.0f, 1.0f, 5.0f);
		
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-5.0f, -1.0f, 5.0f);

			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(5.0f, -1.0f, 5.0f);

			// back
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(5.0f, 1.0f, -5.0f);
		
			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-5.0f, 1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-5.0f, -1.0f, -5.0f);
			
			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(5.0f, -1.0f, -5.0f);

			// right
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(5.0f, 1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(5.0f, 1.0f, 5.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(5.0f, -1.0f, 5.0f);
			
			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(5.0f, -1.0f, -5.0f);

			// left
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(-5.0f, 1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-5.0f, 1.0f, 5.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-5.0f, -1.0f, 5.0f);
			
			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(-5.0f, -1.0f, -5.0f);

			// up
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(5.0f, 1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-5.0f, 1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-5.0f, 1.0f, 5.0f);
			
			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(5.0f, 1.0f, 5.0f);

			// down
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(5.0f, -1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-5.0f, -1.0f, -5.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-5.0f, -1.0f, 5.0f);
			
			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(5.0f, -1.0f, 5.0f);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Home(void)
{
	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glBegin(GL_QUADS);

			// left front
			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(-2.0f, 2.0f, 3.0f);

			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(-3.5f, 2.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-3.5f, -2.0f, 3.0f);

			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(-2.0f, -2.0f, 3.0f);

				// right front
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(4.5f, 2.0f, 3.0f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.0f, 2.0f, 3.0f);
		
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.0f, -2.0f, 3.0f);
		
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(4.5f, -2.0f, 3.0f);

			// up front
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.0f, 2.0f, 3.0f);
		
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-2.5f, 2.0f, 3.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-2.5f, 1.0f, 3.0f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(0.0f, 1.0f, 3.0f);

			// right
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(4.5f, 2.0f, -3.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(4.5f, 2.0f, 3.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(4.5f, -2.0f, 3.0f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(4.5f, -2.0f, -3.0f);

			// left
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-3.5f, 2.0f, 3.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-3.5f, 2.0f, -3.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-3.5f, -2.0f, -3.0f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-3.5f, -2.0f, 3.0f);

			// back
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(4.5f, 2.0f, -3.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-3.5f, 2.0f, -3.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-3.5f, -2.0f, -3.0f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(4.5f, -2.0f, -3.0f);

		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// front wall window black box
	glPushMatrix();
		glLineWidth(3.0f);
		glColor3f(0.3f, 0.1f, 0.3f);
		glTranslatef(2.5f, -0.2f, 3.1f);
		glBegin(GL_LINES);
			glVertex3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			glVertex3f(-1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			glVertex3f(-1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glVertex3f(1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glEnd();
	glPopMatrix();

	// front wall window blue box
	glPushMatrix();
		glColor3f(0.6f, 0.8f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glTranslatef(2.5f, -0.2f, 3.1f);
		glBegin(GL_QUADS);
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(0.99f, 0.99f, 0.0f);
		
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-0.99f, 0.99f, 0.0f);
		
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-0.99f, -0.99f, 0.0f);
		
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(0.99f, -0.99f, 0.0f);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// front window middle line and window handle
	glPushMatrix();
		glLineWidth(3.0f);
		glColor3f(0.3f, 0.1f, 0.3f);
		glTranslatef(2.5f, -0.2f, 3.13f);
		glBegin(GL_LINES);
			glVertex3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.0f, -1.0f, 0.0f);

			glVertex3f(0.1f, 0.1f, 0.0f);
			glVertex3f(0.1f, -0.1f, 0.0f);

			glVertex3f(-0.1f, 0.1f, 0.0f);
			glVertex3f(-0.1f, -0.1f, 0.0f);
		glEnd();
	glPopMatrix();

	// front door black border
	glPushMatrix();
		glLineWidth(3.0f);
		glColor3f(0.3f, 0.1f, 0.3f);
		glTranslatef(0.0f, -1.0f, 0.1f);
		glBegin(GL_LINES);
			glVertex3f(-2.0f, 2.0f, 3.0f);
			glVertex3f(0.0f, 2.0f, 3.0f);

			glVertex3f(0.0f, 2.0f, 3.0f);
			glVertex3f(0.0f, -1.0f, 3.0f);

			glVertex3f(-2.0f, 2.0f, 3.0f);
			glVertex3f(-2.0f, -1.0f, 3.0f);
		glEnd();
	glPopMatrix();

	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, terraceTexture);
		glBegin(GL_QUADS);

			// terest up front
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(5.0f, 5.0f, 0.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-4.0f, 5.0f, 0.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-4.0f, 1.8f, 3.2f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(5.0f, 1.8f, 3.2f);

				// terest up back
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(5.0f, 5.0f, 0.0f);
			
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-4.0f, 5.0f, 0.0f);
			
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-4.0f, 1.8f, -3.2f);
			
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(5.0f, 1.8f, -3.2f);

		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glBegin(GL_TRIANGLES);
			// left uppar big triangle
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-3.5f, 2.0f, 3.0f);
		
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-3.5f, 5.0f, 0.0f);
		
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-3.5f, 2.0f, -3.0f);

			// right uppar big triangle
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(4.5f, 2.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(4.5f, 5.0f, 0.0f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(4.5f, 2.0f, -3.0f);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// uppar window
	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glTranslatef(0.5f, 3.0f, 0.0f);
		glBegin(GL_QUADS);
			// front
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 3.0f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 3.0f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 3.0f);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// circle black border
	glPushMatrix();
		glPointSize(3.0f);
		glColor3f(0.3f, 0.1f, 0.3f);
		glTranslatef(0.5f, 3.0f, 3.0f);
		glBegin(GL_POINTS);
			for (float axis = 0.0f; axis <= 2 * pi; axis += 0.01)
			{
				glVertex3f(0.9f * cos(axis), -0.9f * sin(axis), 0.0f);
				glVertex3f(-0.9f * cos(axis), 0.9f * sin(axis), 0.0f);
			}
		glEnd();
	glPopMatrix();

	// circle sky blue
	glPushMatrix();
		glColor3f(0.6f, 0.8f, 1.0f);
		glTranslatef(0.5f, 3.0f, 3.0f);
		glBegin(GL_LINES);
			for (float axis = 0.0f; axis <= 2 * pi; axis += 0.01)
			{
				glVertex3f(0.85f * cos(axis), -0.85f * sin(axis), 0.0f);
				glVertex3f(-0.85f * cos(axis), 0.85f * sin(axis), 0.0f);
			}
		glEnd();
	glPopMatrix();

	// lines
	glPushMatrix();
		glLineWidth(3.0f);
		glColor3f(0.3f, 0.1f, 0.3f);
		glTranslatef(0.5f, 3.0f, 3.1f);
		glBegin(GL_LINES);
			glVertex3f(0.9, 0.0f, 0.0f);
			glVertex3f(-0.9, 0.0f, 0.0f);
			glVertex3f(0.0, 0.9f, 0.0f);
			glVertex3f(0.0, -0.9f, 0.0f);
		glEnd();
	glPopMatrix();

	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glTranslatef(0.5f, 3.0f, 0.0f);
		glBegin(GL_QUADS);
			// right
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 3.0f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			// left
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 3.0f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, terraceTexture);
		glTranslatef(0.5f, 3.0f, 0.0f);
		glBegin(GL_QUADS);
			// uppar window left
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(-1.3f, 0.7f, 3.3f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.0f, 2.0f, 3.3f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.0f, 2.0f, 0.3f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(-1.3f, 0.7f, 0.3f);

			// uppar window right
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.3f, 0.7f, 3.3f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(0.0f, 2.0f, 3.3f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.0f, 2.0f, 0.3f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.3f, 0.7f, 0.3f);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glColor3f(1.0f, 0.7f, 0.6f);
		glBindTexture(GL_TEXTURE_2D, wallTexture);
		glTranslatef(0.5f, 3.0f, 0.0f);
		glBegin(GL_TRIANGLES);

			// front triangle
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(0.0f, 2.0f, 3.0f);
		
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 3.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, 3.0f);

		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void LoadGroundTexture(void)
{
	LoadGLTexture(&groundTexture, MAKEINTRESOURCE(GROUND_BITMAP));
}

void DeleteGroundTexture(void)
{
	if (groundTexture)
	{
		glDeleteTextures(1, &groundTexture);
		groundTexture = 0;
	}
}

void Ground(void)
{
	glPushMatrix();
		glColor3f(0.0f, 1.0f, 0.0f);
		glBindTexture(GL_TEXTURE_2D, groundTexture);
		glTranslatef(0.0f, -0.86f, 17.45f);
		glScalef(1.0f, 1.0f, 1.55f);

		glBegin(GL_QUADS);

			glTexCoord2f(53.0f, 53.0f);
			glVertex3f(40.0f, 0.0f, -40.0f);

			glTexCoord2f(0.0f, 53.0f);
			glVertex3f(-40.0f, 0.0f, -40.0f);

			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-40.0f, 0.0f, 40.0f);

			glTexCoord2f(53.0f, 0.0f);
			glVertex3f(40.0f, 0.0f, 40.0f);

		glEnd();

	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void LoadHumanoidTexture(void)
{
	LoadGLTexture(&shirtTexture, MAKEINTRESOURCE(SHIRT_BITMAP));
	LoadGLTexture(&pantTexture, MAKEINTRESOURCE(PANT_BITMAP));
	LoadGLTexture(&shirt1Texture, MAKEINTRESOURCE(SHIRT1_BITMAP));
}

void DeleteHumanoidTexture(void)
{
	if (shirtTexture)
	{
		glDeleteTextures(1, &shirtTexture);
		shirtTexture = 0;
	}

	if (shirt1Texture)
	{
		glDeleteTextures(1, &shirt1Texture);
		shirt1Texture = 0;
	}

	if (pantTexture)
	{
		glDeleteTextures(1, &pantTexture);
		pantTexture = 0;
	}
}

void Humanoid1(void)
{
	// function declaration
	void getHead(void);
	void getMiddlePortion(void);
	void getLeftHand(void);
	void getRightHand(void);
	void getLeftLeg(void);
	void getRightLeg(void);

	// local variable

	// code
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, main_body);
		getMiddlePortion();

		glPushMatrix();
			getHead();
		glPopMatrix();

		glPushMatrix();
			getLeftHand();
		glPopMatrix();

		glPushMatrix();
			getRightHand();
		glPopMatrix();

		glPushMatrix();
			getLeftLeg();
		glPopMatrix();

		glPushMatrix();
			getRightLeg();
		glPopMatrix();

	glPopMatrix();
}

void Humanoid2(void)
{
	// function declaration
	void getHead(void);
	void getMiddlePortion2(void);
	void getLeftHand2(void);
	void getRightHand2(void);
	void getLeftLeg(void);
	void getRightLeg(void);

	// local variable

	// code
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, main_body);
		getMiddlePortion2();

		glPushMatrix();
			getHead();
		glPopMatrix();

		glPushMatrix();
			getLeftHand2();
		glPopMatrix();

		glPushMatrix();
			getRightHand2();
		glPopMatrix();

		glPushMatrix();
			getLeftLeg();
		glPopMatrix();

		glPushMatrix();
			getRightLeg();
		glPopMatrix();

	glPopMatrix();
}

void getRightHand2()
{
	glPushMatrix();
		// first sphere
		glColor3f(0.6f, 0.7f, 0.7f);
		glBindTexture(GL_TEXTURE_2D, shirt1Texture);
		glTranslatef(3.0f, -0.3f, 0.0f);
		glRotatef(70.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.05f, 5.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.6, 30, 30);

//		glRotatef(right_hand, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			glPushMatrix();
				// second sphere
				glColor3f(0.6f, 0.7f, 0.7f);
				glTranslatef(0.0f, 0.0f, 3.2f);
				quadric = gluNewQuadric();
				gluQuadricTexture(quadric, GL_TRUE);
				gluSphere(quadric, 0.5, 30, 30);

				glPushMatrix();
					// middle portion of 2nd and 3rd sphere
					glColor3f(1.0f, 1.0f, 1.0f);
					glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

					glBindTexture(GL_TEXTURE_2D, 0);

					glPushMatrix();
						// third sphere
						glColor3f(1.0f, 0.8f, 0.6f);
						glTranslatef(0.0f, 0.0f, 3.4f);
						quadric = gluNewQuadric();
						gluSphere(quadric, 0.5, 30, 30);

						glPushMatrix();
							glTranslatef(0.0f, -304.0f, 30.4f);
							glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
							glScalef(10.0f, 10.0f, 10.0f);
							Aasari();
						glPopMatrix();

					glPopMatrix();

				glPopMatrix();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getLeftHand2()
{
	glPushMatrix();
		// first sphere
		glColor3f(0.6f, 0.7f, 0.7f);
		glBindTexture(GL_TEXTURE_2D, shirt1Texture);
		glTranslatef(-3.0f, -0.3f, 0.0f);
		glRotatef(-80.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.05f, 5.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.6, 30, 30);

//		glRotatef(left_hand, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			glPushMatrix();
				// second sphere
				glColor3f(0.6f, 0.7f, 0.7f);
				glTranslatef(0.0f, 0.0f, 3.2f);
				glRotatef(50.0f, 0.0f, 0.0f, 1.0f);
				quadric = gluNewQuadric();
				gluQuadricTexture(quadric, GL_TRUE);
				gluSphere(quadric, 0.5, 30, 30);

				glPushMatrix();
					// middle portion of 2nd and 3rd sphere
					glColor3f(1.0f, 1.0f, 1.0f);
					glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluCylinder(quadric, 0.47f, 0.47f, 1.5f, 32, 32);

					glBindTexture(GL_TEXTURE_2D, 0);

					glPushMatrix();
						// third sphere
						glColor3f(1.0f, 0.8f, 0.6f);
						glTranslatef(0.0f, 0.0f, 2.0f);
						quadric = gluNewQuadric();
						gluSphere(quadric, 0.5, 30, 30);

					glPopMatrix();

				glPopMatrix();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getMiddlePortion2()
{
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, shirt1Texture);
	glTranslatef(0.0f, 5.3f, 0.0f);
	glScalef(8.0f, 8.0f, 4.5f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.31f, 0.21f, 0.9f, 32, 32);

	glTranslatef(-0.38f, 0.0f, 0.06f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.06f, 0.06f, 0.09f, 32, 32);

	glTranslatef(0.0f, 0.0f, 0.68f);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.06f, 0.06f, 0.09f, 32, 32);

	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void GetWalk(void)
{
	main_body += 0.1f;
	static bool bflag = true, bflag1 = false;

	if(bflag)
	{
		bflag = false;
		left_leg = -10.0f;
		right_leg = 10.0f;
		left_hand = 10.0f;
		right_hand = -10.0f;
	}

	if ((bflag1 == false))
	{
		left_leg += 1.0f;
		right_leg -= 1.0f;
		left_hand += 1.0f;
		right_hand += 1.0f;

		if (left_leg == 10.0f)
		{
			bflag1 = true;
		}
	}
	else if((bflag1 == true))
	{
		left_leg -= 1.0f;
		right_leg += 1.0f;
		left_hand -= 1.0f;
		right_hand -= 1.0f;

		if (left_leg == -10.0f)
		{
			bflag1 = false;
		}
	}
}

void getLeftLeg()
{
	glPushMatrix();
		// first sphere
		glColor3f(0.6f, 0.7f, 0.7f);
		glTranslatef(-0.9f, -7.3f, 0.0f);
		glScalef(1.5f, 1.0f, 1.7f);
		glBindTexture(GL_TEXTURE_2D, pantTexture);
		glTranslatef(0.05f, 5.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.6, 30, 30);

		glRotatef(left_leg, 1.0f, 0.0f, 0.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			glPushMatrix();
				// second sphere
				glColor3f(0.6f, 0.7f, 0.7f);
				glTranslatef(0.0f, 0.0f, 3.2f);
				quadric = gluNewQuadric();
				gluQuadricTexture(quadric, GL_TRUE);
				gluSphere(quadric, 0.5, 30, 30);

				glPushMatrix();
					// middle portion of 2nd and 3rd sphere
					glColor3f(1.0f, 1.0f, 1.0f);
					glRotatef(10.0f, 1.0f, 0.0f, 0.0f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

					glBindTexture(GL_TEXTURE_2D, 0);

					glPushMatrix();
						// leg
						glColor3f(1.0f, 0.8f, 0.6f);
						glTranslatef(0.0f, 0.0f, 3.5f);
						quadric = gluNewQuadric();
						gluCylinder(quadric, 0.35f, 0.35f, 0.5f, 32, 32);

						glPushMatrix();
							// shoes
							glBindTexture(GL_TEXTURE_2D, pantTexture);
							glColor3f(1.0f, 1.0f, 1.0f);
							glTranslatef(0.0f, 0.3f, 0.8f);
							glScalef(1.0f, 1.5f, 1.0f);
							quadric = gluNewQuadric();
							gluQuadricTexture(quadric, GL_TRUE);
							gluSphere(quadric, 0.5, 30, 30);

						glPopMatrix();
					
					glPopMatrix();

				glPopMatrix();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getRightLeg()
{
	glPushMatrix();
		// first sphere
		glColor3f(0.6f, 0.7f, 0.7f);
		glBindTexture(GL_TEXTURE_2D, pantTexture);
		glTranslatef(0.9f, -7.3f, 0.0f);
		glScalef(1.5f, 1.0f, 1.7f);
		glTranslatef(0.05f, 5.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.6, 30, 30);

		glRotatef(right_leg, 1.0f, 0.0f, 0.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			glPushMatrix();
				// second sphere
				glColor3f(0.6f, 0.7f, 0.7f);
				glTranslatef(0.0f, 0.0f, 3.2f);
				quadric = gluNewQuadric();
				gluQuadricTexture(quadric, GL_TRUE);
				gluSphere(quadric, 0.5, 30, 30);

				glPushMatrix();
					// middle portion of 2nd and 3rd sphere
					glColor3f(1.0f, 1.0f, 1.0f);
					glRotatef(10.0f, 1.0f, 0.0f, 0.0f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

					glBindTexture(GL_TEXTURE_2D, 0);

					glPushMatrix();
						// leg
						glColor3f(1.0f, 0.8f, 0.6f);
						glTranslatef(0.1f, 0.0f, 3.5f);
						quadric = gluNewQuadric();
						gluCylinder(quadric, 0.35f, 0.35f, 0.5f, 32, 32);

						glPushMatrix();
							// shoes
							glBindTexture(GL_TEXTURE_2D, pantTexture);
							glColor3f(1.0f, 1.0f, 1.0f);
							glTranslatef(0.0f, 0.3f, 0.8f);
							glScalef(1.0f, 1.5f, 1.0f);
							quadric = gluNewQuadric();
							gluQuadricTexture(quadric, GL_TRUE);
							gluSphere(quadric, 0.5, 30, 30);

						glPopMatrix();

					glPopMatrix();

				glPopMatrix();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getRightHand()
{
	glPushMatrix();
		// first sphere
		glColor3f(0.6f, 0.7f, 0.7f);
		glBindTexture(GL_TEXTURE_2D, shirtTexture);
		glTranslatef(3.0f, -0.3f, 0.0f);
		glRotatef(70.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.05f, 5.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.6, 30, 30);

//		glRotatef(right_hand, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			glPushMatrix();
				// second sphere
				glColor3f(0.6f, 0.7f, 0.7f);
				glTranslatef(0.0f, 0.0f, 3.2f);
				quadric = gluNewQuadric();
				gluQuadricTexture(quadric, GL_TRUE);
				gluSphere(quadric, 0.5, 30, 30);

				glPushMatrix();
					if (human_x_axis >= 9.63f)
					{	
						// middle portion of 2nd and 3rd sphere
						glColor3f(1.0f, 1.0f, 1.0f);
						glRotatef(-20.0f, 0.0f, 1.0f, 0.0f);
						quadric = gluNewQuadric();
						gluQuadricTexture(quadric, GL_TRUE);
						gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

						glBindTexture(GL_TEXTURE_2D, 0);
					}
					else
					{
						// middle portion of 2nd and 3rd sphere
						glColor3f(1.0f, 1.0f, 1.0f);
						glRotatef(-90.0f, 0.0f, 1.0f, -1.0f);
						quadric = gluNewQuadric();
						gluQuadricTexture(quadric, GL_TRUE);
						gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

						glBindTexture(GL_TEXTURE_2D, 0);
					}

					glPushMatrix();
						// third sphere
						glColor3f(1.0f, 0.8f, 0.6f);
						glTranslatef(0.0f, 0.0f, 3.4f);
						quadric = gluNewQuadric();
						gluSphere(quadric, 0.5, 30, 30);
					
					glPopMatrix();

				glPopMatrix();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getLeftHand()
{
	glPushMatrix();
		// first sphere
		if (manja_vertex_count <= 10)
		{
			if (manja_vertex_count % 2 == 0)
			{
				glRotatef(-3.0f, 0.0f, 0.0f, 1.0f);
			}
			else
			{
				glRotatef(3.0f, 0.0f, 0.0f, 1.0f);
			}
		}
		
		if (human_x_axis >= 9.63f)
		{
			glColor3f(0.6f, 0.7f, 0.7f);
			glBindTexture(GL_TEXTURE_2D, shirtTexture);
			glTranslatef(-3.0f, -0.3f, 0.0f);
			glRotatef(-70.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(0.05f, 5.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluSphere(quadric, 0.6, 30, 30);
		}
		else
		{
			glColor3f(0.6f, 0.7f, 0.7f);
			glBindTexture(GL_TEXTURE_2D, shirtTexture);
			glTranslatef(-3.0f, -0.3f, 0.0f);
			glRotatef(-130.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(0.05f, 5.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluSphere(quadric, 0.6, 30, 30);
		}
		glRotatef(left_hand, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
			// middle portion of 1st and 2nd sphere
			glColor3f(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, -0.3f, 0.1f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			quadric = gluNewQuadric();
			gluQuadricTexture(quadric, GL_TRUE);
			gluCylinder(quadric, 0.5f, 0.5f, 3.0f, 32, 32);

			if (human_x_axis >= 9.63f)
			{
				glPushMatrix();
					// second sphere
					glColor3f(0.6f, 0.7f, 0.7f);
					glTranslatef(0.0f, 0.0f, 3.2f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluSphere(quadric, 0.5, 30, 30);

					glPushMatrix();
						// middle portion of 2nd and 3rd sphere
						glColor3f(1.0f, 1.0f, 1.0f);
						glRotatef(20.0f, 0.0f, 1.0f, 0.0f);
						quadric = gluNewQuadric();
						gluQuadricTexture(quadric, GL_TRUE);
						gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

						glBindTexture(GL_TEXTURE_2D, 0);

						glPushMatrix();
							// third sphere
							glColor3f(1.0f, 0.8f, 0.6f);
							glTranslatef(0.0f, 0.0f, 3.4f);
							quadric = gluNewQuadric();
							gluSphere(quadric, 0.5, 30, 30);

						glPopMatrix();
					glPopMatrix();
				glPopMatrix();
			}
			else
			{
				glPushMatrix();
					// second sphere
					glColor3f(0.6f, 0.7f, 0.7f);
					glTranslatef(0.0f, 0.0f, 3.2f);
					quadric = gluNewQuadric();
					gluQuadricTexture(quadric, GL_TRUE);
					gluSphere(quadric, 0.5, 30, 30);

					glPushMatrix();
						// middle portion of 2nd and 3rd sphere
						glColor3f(1.0f, 1.0f, 1.0f);
						glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
						quadric = gluNewQuadric();
						gluQuadricTexture(quadric, GL_TRUE);
						gluCylinder(quadric, 0.47f, 0.47f, 3.5f, 32, 32);

						glBindTexture(GL_TEXTURE_2D, 0);

						glPushMatrix();
							// third sphere
							glColor3f(1.0f, 0.8f, 0.6f);
							glTranslatef(0.0f, 0.0f, 3.4f);
							quadric = gluNewQuadric();
							gluSphere(quadric, 0.5, 30, 30);

						glPopMatrix();
					glPopMatrix();
				glPopMatrix();
			}

		glPopMatrix();

	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void getMiddlePortion()
{
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, shirtTexture);
		glTranslatef(0.0f, 5.3f, 0.0f);
		glScalef(8.0f, 8.0f, 4.5f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.31f, 0.21f, 0.9f, 32, 32);

		glTranslatef(-0.38f, 0.0f, 0.06f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.06f, 0.06f, 0.09f, 32, 32);

		glTranslatef(0.0f, 0.0f, 0.68f);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.06f, 0.06f, 0.09f, 32, 32);

	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void getHead()
{
	// local variables
	static GLfloat axis = 0.0f;
	
	// code
	glPushMatrix();

		glTranslatef(0.0f, 8.0f, 0.0f);
		glScalef(15.0f, 15.0f, 15.0f);

		// head
		glColor3f(1.0f, 0.8f, 0.6f);
		quadric = gluNewQuadric();
		gluSphere(quadric, 0.1, 30, 30);

		// neck
		glTranslatef(0.0f, -0.09f, 0.0f);
		glScalef(1.0f, 1.0f, 1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		quadric = gluNewQuadric();
		gluCylinder(quadric, 0.04f, 0.04f, 0.08f, 32, 32);

	glPopMatrix();

	glPushMatrix();

		// ear right
		glTranslatef(1.6f, 8.0f, 0.0f);
		glScalef(10.0f, 20.0f, 10.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		quadric = gluNewQuadric();
		gluSphere(quadric, 0.01, 30, 30);

	glPopMatrix();

	glPushMatrix();

		// ear left
		glTranslatef(-1.6f, 8.0f, 0.0f);
		glScalef(10.0f, 20.0f, 10.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		quadric = gluNewQuadric();
		gluSphere(quadric, 0.01, 30, 30);

	glPopMatrix();

	glPushMatrix();

		// goggle right
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.6f, 8.0f, 1.5f);
		glRotatef(90.0f, 0.0f, 0.0f, 1.0f);

		glBegin(GL_LINES);

			for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
			{
				glVertex3f(0.45f * cos(axis), -0.45f * sin(axis), 0.0f);
				glVertex3f(-0.45f * cos(axis), 0.45f * sin(axis), 0.0f);
			}

		glEnd();

	glPopMatrix();

	glPushMatrix();

		// goggle right
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(-0.6f, 8.0f, 1.5f);
		glRotatef(90.0f, 0.0f, 0.0f, 1.0f);

		glBegin(GL_LINES);

			for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
			{
				glVertex3f(0.45f * cos(axis), -0.45f * sin(axis), 0.0f);
				glVertex3f(-0.45f * cos(axis), 0.45f * sin(axis), 0.0f);
			}

		glEnd();

	glPopMatrix();

	glPushMatrix();

		// mask
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, 7.0f, 1.5f);
		
		glBegin(GL_QUADS);

			glVertex3f(0.6f, 0.4f, 0.0f);
			glVertex3f(0.6f, -0.4f, 0.0f);
			glVertex3f(-0.6f, -0.4f, 0.0f);
			glVertex3f(-0.6f, 0.4f, 0.0f);

		glEnd();

	glPopMatrix();

	glPushMatrix();

		// mask lines
		glLineWidth(1.5f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, 7.6f, 0.25f);
		glRotatef(-80.0f, 1.0f, 0.0f, 0.0f);

		glBegin(GL_POINTS);

			for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
			{
				glVertex3f(1.3f * cos(axis), -1.3f * sin(axis), 0.0f);
				glVertex3f(-1.3f * cos(axis), 1.3f * sin(axis), 0.0f);
			}

		glEnd();

	glPopMatrix();

	glPushMatrix();

		// mask lines
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, 7.0f, 0.25f);
		glRotatef(-72.0f, 1.0f, 0.0f, 0.0f);

		glBegin(GL_POINTS);

			for (axis = 0.0f; axis <= 2 * pi; axis += 0.0001f)
			{
				glVertex3f(1.3f * cos(axis), -1.3f * sin(axis), 0.0f);
				glVertex3f(-1.3f * cos(axis), 1.3f * sin(axis), 0.0f);
			}

		glEnd();

	glPopMatrix();

	glPushMatrix();

		// goggle lines
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, 7.3f, 1.5f);

		glBegin(GL_LINES);

			glVertex3f(1.3f, 0.7f, 0.0f);
			glVertex3f(-1.3f, 0.7f, 0.0f);

			glVertex3f(1.3f, 0.7f, 0.0f);
			glVertex3f(1.5f, 0.7f, -1.5f);

			glVertex3f(-1.3f, 0.7f, 0.0f);
			glVertex3f(-1.5f, 0.7f, -1.5f);

		glEnd();

	glPopMatrix();
}

void LoadTreeTexture(void)
{
	//code
	LoadGLTexture(&treeBarkTexture, MAKEINTRESOURCE(TREE_BARK_BITMAP));
	LoadGLTexture(&treeLeafTexture, MAKEINTRESOURCE(TREE_LEAF_BITMAP));
}

void DeleteTreeTexture(void)
{
	//code
	if (treeLeafTexture)
	{
		glDeleteTextures(1, &treeLeafTexture);
		treeLeafTexture = 0;
	}

	if (treeBarkTexture)
	{
		glDeleteTextures(1, &treeBarkTexture);
		treeBarkTexture = 0;
	}
}

void Tree(void)
{
	// code
	// main long bark
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, -0.8f, 0.0f);
		glScalef(1.0f, 1.5f, 1.0f);
		glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
		
		glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.45f, 0.45f, 2.46f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// right small bark
	glPushMatrix();
		glTranslatef(0.0f, 1.3f, 0.27f);
		glRotatef(300.0f, 1.0f, 0.0f, 0.0f);
		
		glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.17f, 0.17f, 1.30f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// left small bark
	glPushMatrix();
		glTranslatef(0.0f, 1.3f, -0.27f);
		glRotatef(240.0f, 1.0f, 0.0f, 0.0f);

		glBindTexture(GL_TEXTURE_2D, treeBarkTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluCylinder(quadric, 0.17f, 0.17f, 1.30f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// middle sphere
	glPushMatrix();
		glTranslatef(0.0f, 4.43f, 0.0f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.53f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// right sphere
	glPushMatrix();
		glTranslatef(0.0f, 3.38f, 1.40f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// left sphere
	glPushMatrix();
		glTranslatef(0.0f, 3.38f, -1.40f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// uppar front middle sphere
	glPushMatrix();
		glTranslatef(0.0f, 4.69f, 1.55f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// uppar back middle sphere
	glPushMatrix();
		glTranslatef(0.0f, 4.69f, -1.55f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// top right back sphere
	glPushMatrix();
		glTranslatef(0.0f, 5.79f, -1.01f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// top left back sphere
	glPushMatrix();
		glTranslatef(0.0f, 5.79f, 1.01f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// top center sphere
	glPushMatrix();
		glTranslatef(0.0f, 6.49f, 0.0f);
		glScalef(0.75f, 1.03f, 1.43f);

		glBindTexture(GL_TEXTURE_2D, treeLeafTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 1.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void LoadBirdTexture(void)
{
	//code
	LoadGLTexture(&birdTexture, MAKEINTRESOURCE(BIRD_BITMAP));
	LoadGLTexture(&birdLegTexture, MAKEINTRESOURCE(BIRD_LEG_BITMAP));
}

void DeleteBirdTexture(void)
{
	//code
	if (birdTexture)
	{
		glDeleteTextures(1, &birdTexture);
		birdTexture = 0;
	}

	if (birdLegTexture)
	{
		glDeleteTextures(1, &birdLegTexture);
		birdLegTexture = 0;
	}
}

void Bird(void)
{
	// local variable
	static GLfloat axis;

	// function declaration
	void GetSmallSphere(void);

	// code
	// uppar body sphere
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, 2.0f, -3.0f);
		glScalef(1.9f, 1.2f, 1.0f);

		glBindTexture(GL_TEXTURE_2D, birdTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 3.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// left white eye
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(-3.0f, 1.8f, -0.4f);
		glRotatef(-19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.2f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 2.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right white eye
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(3.0f, 1.8f, -0.4f);
		glRotatef(19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.2f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 2.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// left light blue eye
	glPushMatrix();
		glColor3f(0.95f, 0.95f, 1.0f);
		glTranslatef(-2.9f, 1.7f, 0.0f);
		glRotatef(-19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 2.0f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right light blue eye
	glPushMatrix();
		glColor3f(0.95f, 0.95f, 1.0f);
		glTranslatef(2.9f, 1.7f, 0.0f);
		glRotatef(19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 2.0f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// left black eye
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(-2.0f, 1.2f, 0.5f);
		glRotatef(-19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 1.0f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right black eye
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(2.0f, 1.2f, 0.5f);
		glRotatef(19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 1.0f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// left grey eye
	glPushMatrix();
		glColor3f(0.1f, 0.1f, 0.1f);
		glTranslatef(-1.95f, 1.2f, 0.58f);
		glRotatef(-19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.76f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right grey eye
	glPushMatrix();
		glColor3f(0.1f, 0.1f, 0.1f);
		glTranslatef(1.95f, 1.2f, 0.58f);
		glRotatef(19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.76f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// left uppar white small circle
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(-2.5f, 1.8f, 0.43f);
		glRotatef(-19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.25f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right uppar white small circle
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(2.5f, 1.8f, 0.43f);
		glRotatef(19.9f, 0.0f, 1.0f, 0.0f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.25f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// left below white small circle
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(-1.17f, 0.8f, 0.86f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.25f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// right below white small circle
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(1.17f, 0.8f, 0.86f);
		glScalef(1.1f, 1.0f, 0.1f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.25f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	// below body sphere
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glTranslatef(0.0f, -4.0f, -10.5f);
		glScalef(1.5f, 1.0f, 2.5f);

		glBindTexture(GL_TEXTURE_2D, birdTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 4.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// nose
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, -0.35f, -0.25f);
		glScalef(1.3f, 1.0f, 0.5f);

		quadric = gluNewQuadric();
		gluSphere(quadric, 0.5f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();

	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, -0.18f, -0.85f);
		glRotatef(-110.0f, 1.0f, 0.0f, 0.0f);
		glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);
		glScalef(4.5f, 20.5f, 0.0f);

		glBegin(GL_TRIANGLES);

			glVertex3f(-0.15f, -0.03f, 0.0f);
			glVertex3f(0.0f, -0.03f, 0.0f);
			glVertex3f(0.0f, -0.13f, 0.0f);

		glEnd();
	glPopMatrix();

	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, -0.18f, -0.85f);
		glRotatef(-110.0f, 1.0f, 0.0f, 0.0f);
		glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
		glScalef(4.5f, 20.5f, 0.0f);

		glBegin(GL_TRIANGLES);

			glVertex3f(0.0f, -0.03f, 0.0f);
			glVertex3f(0.15f, -0.03f, 0.0f);
			glVertex3f(0.0f, -0.13f, 0.0f);

		glEnd();
	glPopMatrix();

	glPushMatrix();
		glColor3f(0.1f, 0.1f, 0.1f);
		glTranslatef(0.0f, 0.0f, -0.85f);
		glRotatef(-50.0f, 1.0f, 0.0f, 0.0f);
		glScalef(4.5f, 20.5f, 0.0f);

		glBegin(GL_TRIANGLES);

			glVertex3f(-0.13f, -0.03f, 0.0f);
			glVertex3f(0.13f, -0.03f, 0.0f);
			glVertex3f(0.0f, -0.13f, 0.0f);

		glEnd();
	glPopMatrix();

	// foot
	// left side
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(-1.5f, -9.0f, -8.0f);
		glScalef(2.0f, 5.0f, 2.0f);

		glBindTexture(GL_TEXTURE_2D, birdLegTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(0.4f, -3.5f, -7.5f);
		GetSmallSphere();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.5f, -3.3f, -8.6f);
		glRotatef(30.0f, 0.0f, 1.0f, 0.0f);
		GetSmallSphere();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.2f, -3.4f, -6.7f);
		glRotatef(-30.0f, 0.0f, 1.0f, 0.0f);
		GetSmallSphere();
	glPopMatrix();

	// right side
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(1.5f, -9.0f, -8.0f);
		glScalef(2.0f, 5.0f, 2.0f);

		glBindTexture(GL_TEXTURE_2D, birdLegTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.3f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
		glTranslatef(3.5f, -3.5f, -7.5f);
		GetSmallSphere();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(3.5f, -3.3f, -8.6f);
		glRotatef(30.0f, 0.0f, 1.0f, 0.0f);
		GetSmallSphere();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(3.0f, -3.4f, -6.7f);
		glRotatef(-30.0f, 0.0f, 1.0f, 0.0f);
		GetSmallSphere();
	glPopMatrix();

}

void GetSmallSphere()
{
	glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		glTranslatef(-2.0f, -7.0f, -0.25f);
		glRotatef(-60.0f, 1.0f, 0.0f, 0.0f);
		glScalef(1.6f, 7.0f, 1.0f);

		glBindTexture(GL_TEXTURE_2D, birdLegTexture);
		quadric = gluNewQuadric();
		gluQuadricTexture(quadric, GL_TRUE);
		gluSphere(quadric, 0.1f, 30, 30);
		gluDeleteQuadric(quadric);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void UnInitialize()
{
	// function declaration

	// local variable

	// code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER | SWP_NOZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	DeleteTreeTexture();
	DeleteBirdTexture();
	DeleteGroundTexture();
	DeleteHumanoidTexture();
	DeleteHomeTexture();
	DeleteCloudTexture();
	DeleteKiteTexture();
}
