#include<stdio.h>
#include<cuda.h>

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

static void HandleError(cudaError_t err, const char* file, int line)
{
	if (err != cudaSuccess)
	{
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		exit(EXIT_FAILURE);
	}
}

int main(void)
{
	cudaDeviceProp prop;
	int count;

	HANDLE_ERROR(cudaGetDeviceCount(&count));

	for (int i = 0; i < count; i++)
	{
		HANDLE_ERROR(cudaGetDeviceProperties(&prop, i));

		printf("\n***General Information For Device %d***\n", i);
		printf("Name : %s\n", prop.name);
		printf("Compute Capability : %d.%d\n", prop.major, prop.minor);
		printf("Clock Rate : %d\n", prop.clockRate);
		printf("Device Copy Overlap : ");
		if (prop.deviceOverlap)
			printf("Enabled\n");
		else
			printf("Diabled\n");

		printf("\n***Memory Information For Device %d***\n", i);
		printf("Total Global Memory : %zd\n", prop.totalGlobalMem);
		printf("Total Constant Memory : %zd\n", prop.totalConstMem);
		printf("Max Mem Pitch: %zd\n", prop.memPitch);
		printf("Texture Alignment : %zd\n", prop.textureAlignment);

		printf("\n***MP Information For Device %d***\n", i);
		printf("Multiprocessor Count : %d\n", prop.multiProcessorCount);
		printf("Shared Mem Per MP : %zd\n", prop.sharedMemPerBlock);
		printf("Register Per MP: %d\n", prop.regsPerBlock);
		printf("Threads In Warp : %d\n", prop.warpSize);
		printf("Max Thread Per Block : %d\n", prop.maxThreadsPerBlock);
		printf("Threads In Warp : %d\n", prop.warpSize);
//		printf("Max Thread Per Block : %d\n", prop.maxThreadPerBlock);
		printf("Max Thread Dimensions : (%d, %d, %d)\n", prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2]);
		printf("Max Grid Dimensions : (%d, %d, %d)\n", prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);

		printf("\n");
	}
}


