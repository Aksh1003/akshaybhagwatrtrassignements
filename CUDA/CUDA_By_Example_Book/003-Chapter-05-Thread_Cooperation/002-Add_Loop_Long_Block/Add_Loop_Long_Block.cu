#include<stdio.h>
#include<cuda.h>

#define N (33 * 1024)
#define HANDLE_ERROR(err) (HandleError(err, __FILE__, __LINE__))

static void HandleError(cudaError_t err, const char* file, int line)
{
	if (err != cudaSuccess)
	{
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		exit(EXIT_FAILURE);
	}
}

__global__ void add(int* a, int* b, int* c)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	while (tid < N)
	{
		c[tid] = a[tid] + b[tid];
		tid += blockDim.x * gridDim.x;
	}
}

int main()
{
	int a[N], b[N], c[N];
	int* dev_a, * dev_b, * dev_c;

	// allocate memory on GPU
	HANDLE_ERROR(cudaMalloc((void**)&dev_a, N * sizeof(int)));
	HANDLE_ERROR(cudaMalloc((void**)&dev_b, N * sizeof(int)));
	HANDLE_ERROR(cudaMalloc((void**)&dev_c, N * sizeof(int)));

	// fill arrays a and b on CPU
	for (int i = 0; i < N; i++)
	{
		a[i] = i;
		b[i] = i * i;
	}

	// copy arrays a and b to GPU
	HANDLE_ERROR(cudaMemcpy(dev_a,
							a,
							N * sizeof(int),
							cudaMemcpyHostToDevice));

	HANDLE_ERROR(cudaMemcpy(dev_b,
							b,
							N * sizeof(int),
							cudaMemcpyHostToDevice));

	add << <128, 128 >> > (dev_a, dev_b, dev_c);

	// copy array c back from GPU to CPU
	HANDLE_ERROR(cudaMemcpy(c,
		dev_c,
		N * sizeof(int),
		cudaMemcpyDeviceToHost));

	// display the results
	for (int i = 0; i < N; i++)
	{
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
	}

	// free memory allocated on GPU
	cudaFree(dev_a);
	cudaFree(dev_b);
	cudaFree(dev_c);

	return 0;
}





