#include<stdio.h>
#include<cuda.h>

__global__ void helloCuda()
{
	printf("Hello CUDA!\n");
}

int main()
{
	helloCuda << <1, 1 >> > ();
	
	cudaDeviceSynchronize();

	cudaDeviceReset();

	return 0;
}
