#include<stdio.h>
#include<cuda.h>

__global__ void helloCuda()
{
	printf("Hello CUDA!\n");
}

int main()
{
	int nx, ny;
	nx = 16;
	ny = 4;

	dim3 block(8, 2);
	dim3 grid(nx / block.x, ny / block.y);

	helloCuda << <grid, block >> > ();
	
	cudaDeviceSynchronize();

	cudaDeviceReset();

	return 0;
}
