#include<stdio.h>
#include<cuda.h>

__global__ void helloCuda()
{
	printf("Hello CUDA!\n");
}

int main()
{
	dim3 block(4, 1, 1);
	dim3 grid(8, 1, 1);

	helloCuda << <grid, block >> > ();
	
	cudaDeviceSynchronize();

	cudaDeviceReset();

	return 0;
}
