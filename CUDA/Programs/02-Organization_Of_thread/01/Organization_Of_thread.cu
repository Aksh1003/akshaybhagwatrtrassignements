#include<stdio.h>
#include<cuda.h>

__global__ void print_ThreadIds()
{
	printf("threadIdx.x : %d, threadIdx.y : %d, threadIdx.z : %d\n", threadIdx.x, threadIdx.y, threadIdx.z);
}

int main()
{
	int nx, ny;
	nx = 16;
	ny = 16;

	dim3 block(8, 8);
	dim3 grid(nx / block.x, ny / block.y);

	print_ThreadIds << <grid, block >> > ();
	
	cudaDeviceSynchronize();

	cudaDeviceReset();

	return 0;
}
